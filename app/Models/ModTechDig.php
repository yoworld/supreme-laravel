<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ModTechDig
 * @package App\Models
 * @property string mod_hash
 * @property string tech_dig
 */
class ModTechDig extends Model
{

    protected $fillable = ['mod_hash', 'tech_dig'];
    protected $primaryKey = false;
    protected $table = 'mod_tech_dig';

    public static function getModHash($mod_hash){
        return self::query()->where('mod_hash', $mod_hash)->first();
    }

    public static function findTechDig($mod_hash){
        return self::query()->where('mod_hash', $mod_hash)->first()->tech_dig;
    }

    public static function modHashExist($mod_hash){
        return self::query()->where('mod_hash', $mod_hash)->exists();
    }

    public static function applyTechDig($mod_hash, $tech_dig){
        if(!self::modHashExist($mod_hash)) {
            return self::query()->create(['mod_hash' => $mod_hash, 'tech_dig' => $tech_dig]);
        }
        return self::getModHash($mod_hash);
    }
}
