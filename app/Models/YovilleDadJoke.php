<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string joke
 * @method static YovilleDadJoke find(int $id)
 * Class YovilleDadJoke
 * @package App\Models
 */
class YovilleDadJoke extends Model
{


    protected $fillable = [
        'id', 'joke'
    ];

    protected $table = "yoville_dadjokes";
}

/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 8/19/2018
 * Time: 1:50 AM
 */