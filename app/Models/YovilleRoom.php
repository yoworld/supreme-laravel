<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int id
 * @property int room_id
 * @property int instance_id
 * @property int owner_player_id
 * @property int home_id
 * @property string server_zone_name
 * @property string zone_name
 * @property int request_id
 * @property int party_id
 * @method static YovilleRoom find(int $id)
 * Class YovilleRoom
 * @package App\Models
 */
class YovilleRoom extends Model
{


    use SoftDeletes;

    public $incrementing = false;

    protected $primaryKey = "id";
    protected $fillable = [
        'room_id',
        'instance_id',
        'owner_player_id',
        'home_id',
        'server_zone_name',
        'zone_name',
        'request_id',
        'party_id',
        'room_name',
        'home_name',
        'owner_name',
        'store_uid',
        'store_key'
    ];

    protected $appends = ['owner_name'];

    public function getOwnerNameAttribute(){
        $user = YovilleCharacter::find($this->owner_player_id);
        return $user != null ?  $user->user_name : null;
    }

    protected $table = 'yoville_rooms';
}
