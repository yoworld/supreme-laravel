<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property  int id
 * @property  boolean rainbow
 * @property  boolean copycat
 * @property  boolean chatrepeater
 * @property  boolean chaosmode
 * @property  boolean spoofer
 * @property  boolean canfreeze
 * @property  boolean canttt
 * @property  int frequency
 * @property  int lfrequency
 * @property  boolean cancorruptgb
 * @property  boolean petchat
 * Class UserSetting
 * @package App\Models
 */
class UserSetting extends Model
{
    protected $table = "user_settings";

    protected $primaryKey = "id";

    public $incrementing = false;


    protected $fillable = [
        "rainbow", "copycat", "chatrepeater",
        "chaosmode", "spoofer", "canfreeze",
        "canttt", "frequency", "lfrequency",
        "cancorruptgb", "petchat", "id",
        "defaultroom", "sortorder","laglessclothes",
        "laglessroom", "supremeentrance", "stalker", "id"
    ];

    protected $casts = [
        "rainbow" => "boolean", "copycat" => "boolean", "chatrepeater" => "boolean",
        "chaosmode" => "boolean", "spoofer" => "boolean", "canfreeze" => "boolean",
        "canttt" => "boolean",
        "cancorruptgb" => "boolean", "petchat"=>"boolean",
        "defaultroom" => "array", "sortorder" => "array",
        "laglessclothes" => "boolean", "laglessroom" => "boolean",
        "supremeentrance" => "boolean", "stalker" => "boolean"
    ];

    protected $hidden = ['created_at', 'updated_at'];


}
