<?php
/**
 * Created by PhpStorm.
 * User: Charlton
 * Date: 1/15/18
 * Time: 11:08 AM
 */

namespace App\Models;


use Illuminate\Support\Collection;

/**
 * Class GeorgeOrwell
 * @package App\Models
 */
class GeorgeOrwell
{


    const QUOTES = [
        [
            "size" => 16,
            "quote" => 'The Ministry of Peace concerns itself with war, the Ministry of Truth with lies, the Ministry of Love with torture and the Ministry of Plenty with starvation. These contradictions are not accidental, nor do they result from from ordinary hypocrisy: they are deliberate exercises in doublethink.',
        ],
        ["size" => 16,
            "quote" => 'Don\'t you see that the whole aim of Newspeak is to narrow the range of thought? In the end we shall make thoughtcrime literally impossible, because there will be no words in which to express it. Every concept that can ever be needed will be expressed by exactly one word, with its meaning rigidly defined and all its subsidiary meanings rubbed out and forgotten.',
        ],
        ["size" => 16,
            "quote" => 'Every record has been destroyed or falsified, every book rewritten, every picture has been repainted, every statue and street building has been renamed, every date has been altered. And the process is continuing day by day and minute by minute. History has stopped. Nothing exists except an endless present in which the Party is always right.',
        ],
        ["size" => 16,
            "quote" => 'In a way, the world−view of the Party imposed itself most successfully on people incapable of understanding it. They could be made to accept the most flagrant violations of reality, because they never fully grasped the enormity of what was demanded of them, and were not sufficiently interested in public events to notice what was happening. By lack of understanding they remained sane. They simply swallowed everything, and what they swallowed did them no harm, because it left no residue behind, just as a grain of corn will pass undigested through the body of a bird.',
        ],
        ["size" => 16,
            "quote" => 'Reality exists in the human mind, and nowhere else. Not in the individual mind, which can make mistakes, and in any case soon perishes: only in the mind of the Party, which is collective and immortal.',
        ],
        ["size" => 16,
            "quote" => 'To know and not to know, to be conscious of complete truthfulness while telling carefully constructed lies, to hold simultaneously two opinions which cancelled out, knowing them to be contradictory and believing in both of them, to use logic against logic, to repudiate morality while laying claim to it, to believe that democracy was impossible and that the Party was the guardian of democracy, to forget whatever it was necessary to forget, then to draw it back into memory again at the moment when it was needed, and then promptly to forget it again: and above all, to apply the same process to the process itself -- that was the ultimate subtlety: consciously to induce unconsciousness, and then, once again, to become unconscious of the act of hypnosis you had just performed. Even to understand the word \'doublethink\' involved the use of doublethink.',
        ],
        ["size" => 13,
            "quote" => 'How does one man assert his power over another? By making him suffer. Exactly. By making him suffer. Obedience is not enough. Unless he is suffering, how can you be sure that he is obeying your will and not his own? Power is in inflicting pain and humiliation. Power is in tearing human minds to pieces and putting them together again in new shapes of your own choosing. Do you begin to see, then, what kind of world we are creating? It is the exact opposite of the stupid hedonistic Utopias that the old reformers imagined. A world of fear and treachery is torment, a world of trampling and being trampled upon, a world which will grow not less but MORE merciless as it refines itself. Progress in our world will be progress towards more pain. The old civilizations claimed that they were founded on love or justice. Ours is founded upon hatred. In our world there will be no emotions except fear, rage, triumph, and self-abasement. Everything else we shall destroy – everything. Already we are breaking down the habits of thought which have survived from before the Revolution. We have cut the links between child and parent, and between man and man, and between man and woman. No one dares trust a wife or a child or a friend any longer. But in the future there will be no wives and no friends. Children will be taken from their mothers at birth, as one takes eggs from a hen. The sex instinct will be eradicated. Procreation will be an annual formality like the renewal of a ration card. We shall abolish the orgasm. Our neurologists are at work upon it now. There will be no loyalty, except loyalty towards the Party. There will be no love, except the love of Big Brother. There will be no laughter, except the laugh of triumph over a defeated enemy. There will be no art, no literature, no science. When we are omnipotent we shall have no more need of science. There will be no distinction between beauty and ugliness. There will be no curiosity, no enjoyment of the process of life. All competing pleasures will be destroyed.',
        ],
        ["size" => 16,
            "quote" => "Does Big Brother exist?\nOf course he exists. The Party exists. Big Brother is the embodiment of the Party.\nDoes he exist in the same way as I exist?\nYou do not exist.",
        ],
        ["size" => 16,
            "quote" => 'How could you tell how much of it was lies? It might be true that the average human being was better off now than before the Revolution. The only evidence to the contrary was the mute protest in your own bones, the instinctive feeling that the conditions you lived in were intolerable and that at some other time they must have been different. It struck him that the truly characteristic thing about modern life was not its cruelty and insecurity, but simply its bareness, its dinginess, its listlessness. Life, if you looked about you, bore no resemblance not only to the lies that streamed out of the telescreens, but even to the ideals that the Party was trying to achieve. Great areas of it, even for a party member, were neutral and nonpolitical, a matter of slogging through dreary jobs, fighting for a place on the Tube, darning a worn-out sock, cadging a saccharine tablet, saving a cigarette end. The ideal set up by the Party was something huge, terrible, and glittering--a world of steel and concrete, of monstrous machines and terrifying weapons--a nation of warriors and fanatics, marching forward in perfect unity, all thinking the same thoughts and shouting the same slogans, perpetually working, fighting, triumphing, persecuting--three hundred million people all with the same face.',
        ],
        ["size" => 16,
            "quote" => 'If the Party could thrust its hand into the past and say of this or that event, it never happened—that, surely, was more terrifying than mere torture and death.',
        ],
        ["size" => 16,
            "quote" => 'Big Brother is Watching You.'
        ]
    ];

    const SIZE = [
        16,
        16,
        16,
        16,
        16,
        16,
        13,
        16,
        16,
        16,
        16
    ];

    /**
     * Get an inspiring quote.
     *
     * Taylor & Dayle made this commit from Jungfraujoch. (11,333 ft.)
     *
     * May McGinnis always control the board. #LaraconUS2015
     *
     * @return array
     */
    public static function quote()
    {

        $orwell = Collection::make(GeorgeOrwell::QUOTES)->random();

        return $orwell;
    }

}