<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int user_id
 * @property int user_name
 * @property int user_gender
 * @property int user_badge_id
 * @property int user_last_login
 * @property int user_created_on
 * @property int user_first_time
 * @property int logged_by_id
 * @property string logged_by_name
 * @property int user_mod_level
 * @property string user_avatar
 * @property int user_network_id
 * @property int user_facebook_id
 * @property string user_type
 * @method static YovilleCharacter find(int $id)
 * Class YovilleCharacter
 * @package App\Models
 */
class YovilleCharacter extends Model
{


    protected $primaryKey = 'user_id';

    public $incrementing = false;

    protected $appends = ['user_avatar'];

    protected $fillable = ['user_id', 'user_name',
        'user_gender', 'user_badge_id',
        'user_last_login',
        'user_created_on',
        'user_first_time',
        'logged_by_id',
        'logged_by_name',
        'user_mod_level',
        'user_avatar',
        'user_network_id',
        'user_facebook_id',
        'user_type',
        'user_clothing'
    ];

    /**
     * @return string
     */
    public function getUserAvatarAttribute()
    {
        return get_avatar_url($this->user_id);
    }

    /**
     * @return string
     */
    public function getUserFacebookPageAttribute()
    {
        return $this->user_facebook_id != null ? sprintf("https://www.facebook.com/%s", $this->user_facebook_id) : null;
    }
}
