<?php

namespace App\Models;

use App\Helpers\SoundCloudHelper;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int track_id
 * @property string username
 * @property string title
 * @property string description
 * @property string genre
 * @property string mp3_loc
 * @property string jpg_loc
 * @property string created_at
 * @property string updated_at
 * @property string jpg_link
 * @property string permalink
 * Class SoundCloud
 * @package App\Models
 */
class SoundCloud extends Model
{
    protected $primaryKey = "track_id";

    protected $fillable = ['track_id', 'title', 'username', 'artist', 'description', 'genre', 'link', 'mp3_loc', 'jpg_loc', 'jpg_link'];

    protected $appends = ['mp3_link', 'id', 'permalink'];

    protected $hidden = [ 'mp3_loc', 'jpg_loc'];

    protected $table = "soundcloud";
    public $incrementing = false;

    public static function findDownload($url)
    {
        $query = self::query();
        if(is_numeric($url)){
            return $query->where('track_id', $url)->first();
        }else if(SoundCloudHelper::is_url($url)){
            return $query->where('link', $url)->first();
        }else{
            return $query->where('link', 'LIKE', "%$url%")->first();
        }
    }

    public function getMp3LinkAttribute(){
        return sprintf("%s/api/v1/soundcloud/%s",url('/'), $this->permalink);
    }

    public function getIdAttribute(){
        return $this->track_id;
    }

    public function getPermalinkAttribute(){
        return str_replace("https://soundcloud.com/", "", $this->link);
    }

    public static function doesExists($url_or_id){
        return self::query()->where('link', 'LIKE', "%$url_or_id%")->orWhere('track_id', $url_or_id)->exists();
    }

    public static function getSoundCloud($url_or_id){
        return self::query()->where('link', 'LIKE', "%$url_or_id%")->orWhere('track_id', $url_or_id)->first();
    }



}
