<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;


/**
 * Class YovilleSupremeBuddy
 * @package App\Models
 * @property int user_id
 * @property string user_name
 * @property bool user_is_online
 * @property string user_room_name
 * @property string user_last_login
 * @property int category_id
 * @property int instance_id
 * @property string user_facebook_name
 * @property int user_online_status
 * @property string saved_category_id
 * @property string visible_room_name
 * @property int user_facebook_id
 * @property int supreme_user_id
 * @method static YovilleSupremeBuddy find(int $id)
 * @method static Builder supreme($supreme, $buddy)
 * @method static Builder id($id)
 */
class YovilleSupremeBuddy extends Model
{

    protected $primaryKey = ['user_id', 'supreme_user_id'];

    public $incrementing = false;

    protected $fillable = [
        'user_name',
        'user_is_online',
        'user_id',
        'user_room_name',
        'user_last_login',
        'category_id',
        'instance_id',
        'user_facebook_name',
        'user_facebook_id',
        'user_online_status',
        'saved_category_id',
        'visible_room_name',
        'supreme_user_id'
    ];

    protected $appends = ['user_facebook_page', 'user_avatar', 'composite_key'];

    protected $casts = ['user_is_online', 'boolean'];
    protected $table = "yoville_supreme";


    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if(!is_array($keys)){
            return parent::setKeysForSaveQuery($query);
        }

        foreach($keys as $keyName){
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if(is_null($keyName)){
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeId($query, $id)
    {
        return $query->where('supreme_user_id', $id);
    }


    /**
     * @return string
     */
    public function getUserAvatarAttribute()
    {
        return get_avatar_url($this->user_id);
    }


    public function getCompositeKeyAttribute(){
        return sprintf("%s-%s",$this->supreme_user_id, $this->user_id);
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSupreme($query, $supreme, $buddy)
    {
        return $query->where('supreme_user_id', $supreme)->where('user_id', $buddy);
    }

    public function  getUserFacebookPageAttribute(){
        return $this->user_facebook_id != null ? sprintf("https://facebook.com/%s", $this->user_facebook_id) : null;
    }
}
