<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int user_id
 * @property string track_id
 * @property int position
 * @property string title
 * @method static SoundCloud|Builder fetch(int $user_id, string $track_id)
 * Class SoundcloudPlaylist
 * @package App\Models
 */
class SoundcloudPlaylist extends Model
{

    protected $primaryKey = ['user_id', 'track_id'];
    protected $table = 'soundcloud_playlist';

    protected $fillable = ['user_id', 'track_id', 'position'];
    protected $appends = ['title', 'jpg_link', 'mp3_link', 'composite_key', 'youtube_id', 'permalink'];

    public $incrementing = false;

    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if (!is_array($keys)) {
            return parent::setKeysForSaveQuery($query);
        }

        foreach ($keys as $keyName) {
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if (is_null($keyName)) {
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }


    /**
     * Scope a query to only include popular users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $user_id
     * @param string $track_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFetch($query, int $user_id, string $track_id)
    {
        return $query->where('user_id', $user_id)->where('track_id', $track_id);
    }


    public function getTitleAttribute()
    {
        return SoundCloud::find($this->track_id)->title;
    }

    public function getJpgLinkAttribute()
    {
        return sprintf("%s/api/v1/s-jpg/%s",url('/'),$this->track_id);
    }

    public function getMp3LinkAttribute()
    {
        return sprintf("%s/api/v1/soundcloud/%s",url('/'),$this->track_id);
    }

    public function getPermalinkAttribute(){
        return SoundCloud::query()->find($this->track_id)->permalink;
    }

    public function getCompositeKeyAttribute(){
        return sprintf("%s-%s", $this->user_id, $this->track_id);
    }

    public function getYoutubeIdAttribute(){
        return $this->track_id;
    }
}
