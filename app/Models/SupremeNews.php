<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SupremeNews
 * @package App\Models
 * @property int id
 * @property int user_id
 * @property string title
 * @property string description
 * @property string artwork_url
 * @property string avatar
 */
class SupremeNews extends Model
{

    protected $primaryKey = 'id';
    protected $table = 'supreme_news';

    protected $fillable = ['user_id', 'title', 'description', 'artwork_url'];

}
