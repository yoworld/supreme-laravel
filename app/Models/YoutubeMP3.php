<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string id
 * @property string link
 * @property string title
 * @property string mp4_md5_hash
 * @property string mp3_md5_hash
 * @property string mp4_loc
 * @property string mp3_loc
 * @property string jpg_loc
 * @method static YoutubeMP3 find($id)
 * Class YoutubeMP3
 * @package App\Models
 */
class YoutubeMP3 extends Model
{
    public $incrementing = false;

    protected $fillable = ['id','link', 'title', 'mp4_md5_hash', 'mp3_md5_hash','jpg_loc','jpg_md5_hash', 'mp4_loc', 'mp3_loc'];

    protected $appends = ['mp3_link', 'jpg_link'];

    protected $table = "youtube_download";

    public function getMp3LinkAttribute(){
        return sprintf("%s/api/v1/mp3/%s",url('/'), $this->id);
    }

    public function getJpgLinkAttribute(){
        return sprintf("%s/api/v1/jpg/%s",url('/'), $this->id);
    }

    public static function doesExists($id){
        return self::query()->where('id', $id)->exists();
    }
}
