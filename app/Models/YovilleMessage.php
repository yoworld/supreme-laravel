<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property int room_id
 * @property int instance_id
 * @property int user_id
 * @property string user_name
 * @property string message
 * @property string log_by
 * @property int log_by_uid
 * @property int suid
 * @method static YovilleMessage find(int $id)
 * Class YovilleMessage
 * @package App\Models
 */
class YovilleMessage extends Model
{


    protected $fillable = [
        'room_id', 'instance_id',
        'user_id', 'user_name',
        'message', 'message',
        'logged_by', 'logged_by_uid',
        'suid'
    ];

    protected $table = "yoville_messages";

    protected $hidden = [
        'id', 'logged_by',
        'logged_by_uid', 'suid'];
}
