<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int user_id
 * @property string mod_hash
 * @property boolean permission
 * @property string codename
 * @method static Permission|Builder check($id, $hash)
 * Class Permission
 * @package App\Models
 */
class Permission extends Model
{

    public $incrementing = false;

    protected $casts = ['permission' => 'boolean'];


    protected $fillable = ['user_id', 'mod_hash', 'permission', 'status', 'message'];


    protected $appends = ['user_name', 'codename'];


    public function getUserNameAttribute()
    {
        return User::find($this->user_id)->name;
    }

    public function getCodenameAttribute()
    {
        return Versioning::find($this->mod_hash)->codename;
    }


    /**
     * Scope a query to only include popular users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCheck($query, $user, $hash)
    {
        return $query->where('user_id', $user)->where('mod_hash', $hash);
    }


    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if (!is_array($keys)) {
            return parent::setKeysForSaveQuery($query);
        }

        foreach ($keys as $keyName) {
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if (is_null($keyName)) {
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }

    protected $primaryKey = ["user_id", "mod_hash"];

    protected $table = "permission";
}
