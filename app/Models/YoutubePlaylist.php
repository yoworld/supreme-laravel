<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int user_id
 * @property string youtube_id
 * @property int position
 * @property string title
 * @method static YoutubePlaylist|Builder fetch(int $user_id, string $youtube_id)
 * Class YoutubePlaylist
 * @package App\Models
 */
class YoutubePlaylist extends Model
{

    protected $primaryKey = ['user_id', 'youtube_id'];
    protected $table = 'youtube_playlist';

    protected $fillable = ['user_id', 'youtube_id', 'position'];
    protected $appends = ['title', 'jpg_link', 'mp3_link', 'composite_key'];

    public $incrementing = false;

    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if (!is_array($keys)) {
            return parent::setKeysForSaveQuery($query);
        }

        foreach ($keys as $keyName) {
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if (is_null($keyName)) {
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }


    /**
     * Scope a query to only include popular users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $user_id
     * @param string $youtube_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFetch($query, int $user_id, string $youtube_id)
    {
        return $query->where('user_id', $user_id)->where('youtube_id', $youtube_id);
    }


    public function getTitleAttribute()
    {
        return YoutubeMP3::find($this->youtube_id)->title;
    }

    public function getJpgLinkAttribute()
    {
        return sprintf("%s/api/v1/jpg/%s",url('/'),$this->youtube_id);
    }

    public function getMp3LinkAttribute()
    {
        return sprintf("%s/api/v1/mp3/%s",url('/'),$this->youtube_id);
    }

    public function getCompositeKeyAttribute(){
        return sprintf("%s-%s", $this->user_id, $this->youtube_id);
    }
}
