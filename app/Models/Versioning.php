<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string md5
 * @property string codename
 * @property string version
 * @property boolean publish
 * @property string location
 * @property string type
 * Class Versioning
 * @package App\Models
 */
class Versioning extends Model
{
    protected $primaryKey = "md5";
    public $incrementing = false;

    protected $fillable = ["md5", "codename", "version", "size", "type", "location", "created_at", "publish"];

    protected $hidden = ["location"];

    protected $table = "versioning";

    protected $appends = ["swf_link", "tech_dig"];


    protected $casts = ["publish"=> "boolean"];



    public function getSwfLinkAttribute()
    {
        return sprintf("%s/api/v1/version/%s",url('/'),$this->md5);
    }

    public function getTechDigAttribute()
    {
        if(ModTechDig::modHashExist($this->md5)) {
            return ModTechDig::findTechDig($this->md5);
        }else{
            return "TechDig Not Set";
        }
    }

}
