<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * Class YovilleAuth
 * @property int user_id
 * @property string user_name
 * @property bool user_banned
 * @property string user_ip
 * @property string user_ipv6
 * @property int user_facebook_id
 * @property string user_snapi_auth
 * @property string user_lk
 * @property string user_yo_auth_key
 * @property string user_network_credentials
 * @property string user_facebook_name
 * @property string user_avatar
 * @property string user_key
 * @property array user_settings
 * @property int user_level
 * @property int assigned_to
 * @property bool user_active
 * @property string api_token
 * @property User user
 * @method static Builder ip($ip)
 * @method static Builder facebookIp($ip, $fb)
 * @method static YovilleAuth find($id);
 * @package App\Models
 */
class YovilleAuth extends Model
{

    use SoftDeletes;

    /**
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|YovilleAuth
     */
    public static function auth(){
        return Auth::user();
    }

    public function isAdmin()
    {
        return $this->user_level == 80085;
    }

    public function isModerator()
    {
        return $this->user_level == 69;
    }

    public function isUser()
    {
        return $this->user_level == 0;
    }


    /**
     * @var array ,
     * "fbId":"107898333333489",
     * "snapiAuth":"185003932::f5ac4cbf5518d3663d1118cdf837dd07",
     * "network_credentials":"fb_sig_user=107898333333489&snapi_auth=185003932::f5ac4cbf5518d3663d1118cdf837dd07&yo_u=187576520&yo_t=foo"
     */
    protected $fillable = [
        'user_id', 'user_name', 'user_banned', 'user_ip', 'user_ipv6', 'user_facebook_id',
        'user_snapi_auth', 'user_network_credentials', 'user_yo_auth_key',
        'user_lk', 'user_facebook_name', 'user_key', 'user_level',
        'user_active', 'user_last_auth', 'assigned_to'
    ];

    protected $appends = ['user_secret', 'user_game_window', 'user_avatar', 'user_settings'];

    protected $hidden = ['user_network_credentials', 'user_yo_auth_key', 'user_lk', 'api_token', 'password', 'remember_token', 'user_secret', 'user_snapi_auth', 'user'];

    /**
     * @return string
     */
    public function getUserAvatarAttribute()
    {
        return get_avatar_url($this->user_id);
    }




    public function getUserSettingsAttribute(){
        return $this->user->user_settings;
    }


    public function getUserGameWindowAttribute()
    {
        return sprintf(url('/api/v1/secret/%s'), $this->user_id);
    }

    public function getUserSecretAttribute()
    {
        return sprintf("%s&lk=%s&yoAuthKey=%s", $this->user_network_credentials, $this->user_lk, $this->user_yo_auth_key);
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIp($query, $ip)
    {
        return $query->where('user_ip', $ip);
    }


    /**
     * Scope a query to only include popular users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFacebookIP($query, $ip, $fb)
    {
        return $query->where('user_ip', $ip)->where('user_facebook_id', $fb);
    }



    public function user(){
        return $this->hasOne('App\Models\User', 'id', 'assigned_to');
    }


    public $incrementing = false;

    protected $primaryKey = "user_id";

    protected $casts = ['user_banned' => 'boolean', 'user_active' => 'boolean'];

    protected $table = "yoville_auth";

}
