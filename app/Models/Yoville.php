<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Yoville
 * @property int id
 * @property string type
 * @property array data
 * @package App\Models
 */
class Yoville extends Model
{


    protected $fillable = [
        'data', 'type'
    ];

    protected $casts = [
        'data' => 'array',
    ];

    protected $table = "yoville";
}
