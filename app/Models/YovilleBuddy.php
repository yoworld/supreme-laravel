<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class YovilleBuddy
 * @package App\Models
 * @property int user_id
 * @property string user_name
 * @property bool user_is_online
 * @property string user_room_name
 * @property string user_last_login
 * @property int category_id
 * @property int instance_id
 * @property string user_facebook_name
 * @property int user_online_status
 * @property string saved_category_id
 * @property string visible_room_name
 * @property int user_facebook_id
 * @method static YovilleBuddy find(int $id)
 */
class YovilleBuddy extends Model
{

    protected $primaryKey = 'user_id';

    public $incrementing = false;



    protected $fillable = [
        'user_name',
        'user_is_online',
        'user_id',
        'user_room_name',
        'user_last_login',
        'category_id',
        'instance_id',
        'user_facebook_name',
        'user_facebook_id',
        'user_online_status',
        'saved_category_id',
        'visible_room_name',
        'logged_by_id',

        ];


    /**
     * @return string
     */
    public function getUserAvatarAttribute()
    {
        return get_avatar_url($this->user_id);
    }

    protected $appends = ['user_facebook_page', 'user_avatar'];

    protected $casts = ['user_is_online', 'boolean'];
    protected $table = "yoville_buddy";

    public function  getUserFacebookPageAttribute(){
        return $this->user_facebook_id != null ? sprintf("https://facebook.com/%s", $this->user_facebook_id) : null;
    }

}
