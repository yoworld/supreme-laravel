<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

/**
 * @property int id
 * @property string name
 * @property string email
 * @property string password
 * @property string user_ip
 * @property string user_ipv6
 * @property string api_token
 * @property string user_banned
 * @property string mod_hash
 * @property string mod_title
 * @property string latest_tech_dig
 * @property YovilleAuth[]|Collection accounts;
 * @property UserSetting settings
 * @property UserSetting user_settings
 * @method static User find(int $id)
 * Class User
 * @package App\Models
 */
class User extends \TCG\Voyager\Models\User
{
    use Notifiable, HasApiTokens;


    public static $DEFAULT_SETTINGS = [
        "rainbow" => false,
        "spoofer" => false,
        "frequency" => 0,
        "lfrequency" => -1,
        "stalker" => false,
        "copycat" => false,
        "chaosmode" => false,
        "canfreeze" => false,
        "summoned" => null,
        "chatrepeater" => false,
        "canttt" => false,
        "ignore" => [],
        "cancorruptgb" => false,
        "petchat" => false,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'user_ip',
        'user_ipv6', 'level', 'user_banned', 'api_token',
        'mod_hash', 'active', 'user_last_auth'
    ];

    protected $appends = ['mod_title', 'user_settings', 'latest_tech_dig'];

    protected $casts = ['active' => 'boolean'];

    public function getModTitleAttribute()
    {
        if ($this->mod_hash != null) {
            $ver = Versioning::find($this->mod_hash);
            if ($ver == null) return "Version no-longer exists";
            return $ver->codename;
        } else {
            return "Not On Mod ???";
        }
    }


    public function getUserSettingsAttribute()
    {
        return $this->settings()->first();
    }

    public function isAdmin()
    {
        return $this->level == 80085;
    }

    public function isModerator()
    {
        return $this->level == 69;
    }

    public function isUser()
    {
        return $this->level == 0;
    }


    public function accounts()
    {
        return $this->hasMany('App\Models\YovilleAuth', 'assigned_to', 'id');
    }


    /**
     * @return Collection|YovilleAuth[]
     */
    public function getActiveAccounts(){
        return $this->accounts()->where('yoville_auth.user_active', true)->orderByDesc('updated_at')->get();
    }


    /**
     * @return YovilleAuth|null
     */
    public function getLatestAccount(){
        return $this->accounts()->orderByDesc('updated_at')->first();
    }

    public function getLatestTechDigAttribute()
    {
        if (ModTechDig::modHashExist($this->mod_hash)) {
            return ModTechDig::findTechDig($this->mod_hash);
        } else {
            return null;
        }
    }


    public function settings()
    {
        return $this->hasOne('App\Models\UserSetting', 'id', 'id');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'user_ip', 'user_ipv6', 'api_token'
    ];
}
