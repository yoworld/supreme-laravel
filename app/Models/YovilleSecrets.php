<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class YovilleSecrets
 * @package App\Models
 */
class YovilleSecrets extends Model
{
    protected $casts = ['data'=>'array'];
    protected $fillable = ['data', 'type'];

}
