<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @property string video_id
 * @property string channel_id
 * @property string title;
 * @property string description
 * @property string channel
 * @property string etag
 * @property string thumbnail
 * @property string publish_at
 * Class YoutubeVideo
 * @package App\Models
 */
class YoutubeVideo extends Model
{
    protected $primaryKey = 'video_id';

    public $incrementing = false;

    protected $fillable = ['video_id', 'channel_id', 'title', 'description', 'channel', 'etag', 'thumbnail', 'publish_at'];

    public $timestamps = null;

    protected $appends = ['video_link', 'mp3_link'];

    protected $table = "youtube_video";

    public static function doesExists($id)
    {
        return self::query()->where('video_id', $id)->exists();
    }

    public function getVideoLinkAttribute(){
        return sprintf("https://youtube.com/watch?v=%s", $this->video_id);
    }

    public function getMp3LinkAttribute(){
        $request = Request::capture();
        $data =$request->getSchemeAndHttpHost();
        $port = $request->getPort();
        return sprintf("%s:%s/api/v1/mp3/%s", $data, $port, $this->video_id);
    }
}
