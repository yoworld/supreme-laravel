<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string joke
 * @method static YovilleYomamaJoke find(int $id)
 * Class YovilleYomamaJoke
 * @package App
 */
class YovilleYomamaJoke extends Model
{


    protected $fillable = [
        'id', 'joke'
    ];

    protected $table = "yoville_yomama";
}
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 8/19/2018
 * Time: 3:35 AM
 */