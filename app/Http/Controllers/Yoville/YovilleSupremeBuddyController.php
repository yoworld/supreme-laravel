<?php

namespace App\Http\Controllers\Yoville;

use App\Http\Controllers\Controller;
use App\Models\YovilleAuth;
use App\Models\YovilleSupremeBuddy;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class YovilleSupremeBuddyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param $supreme
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $supreme)
    {
        $auth_query = YovilleAuth::query()->where('user_id', $supreme);
        if ($auth_query->count() == null) {
            return $this->json_error("Bitch you is not supreme.");
        }

        /**
         * @var YovilleAuth $auth
         */
        $auth = $auth_query->first();

        $yoville_query = YovilleSupremeBuddy::id($supreme)->where(function (Builder $builder) use ($request) {
            $allowed = [
                'user_name',
                'user_is_online',
                'user_id',
                'user_room_name',
                'user_last_login',
                'category_id',
                'instance_id',
                'user_facebook_name',
                'user_online_status',
                'saved_category_id',
                'visible_room_name'];

            $like = [
                'user_name',
                'user_last_login',
                'user_facebook_name',
                'visible_room_name',
            ];


            $equal = [
                'user_is_online',
                'user_id',
                'user_room_name',
                'category_id',
                'instance_id',
                'user_online_status',
                'saved_category_id',
            ];


            $time = ['created_at', 'updated_at'];

            foreach ($request->input() as $key => $value) {
                if (in_array($key, $allowed)) {
                    if (in_array($key, $like)) {
                        $builder->where($key, 'LIKE', "%$value%");
                    }
                    if (in_array($key, $equal)) {

                        $builder->where($key, $value);
                    }
                    if (in_array($key, $time)) {
                        $builder->where($key, 'LIKE', "%$value%");
                    }
                }
            }
        });

        if (array_has($request->all(), 'fb') && is_true(array_get($request->all(), 'fb', false))) {
            $yoville_query->whereNotNull('user_facebook_id');
        }
        if (array_has($request->all(), 'sync') && is_true(array_get($request->all(), 'sync', false))) {
            $yoville_query->whereBetween('updated_at', [Carbon::now()->subMinutes(intval($request->input('ago', 10))), Carbon::now()]);
        }

        $yoville = $yoville_query->orderByDesc('updated_at')->paginate($request->input('per', 20));
        $yoville_info = array_except(json_decode(json_encode($yoville), true), ['data']);
        $yoville_info['supreme_id'] = $auth->user_id;
        $yoville_info['supreme_name'] = $auth->user_name;
        return $this->json_response($yoville->items(), "RESPONSE", true, 200, $yoville_info)
            ->header('x-supreme-name', $auth->user_name)
            ->header('x-supreme-id', $auth->user_id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $supreme)
    {
        if (YovilleAuth::query()->where('user_id', $supreme)->count() == null) {
            return $this->json_error("Bitch you is not supreme.");
        }
        $data = json_decode($request->has('data') ? $request->input('data') : $request->getContent(), true);
        $buddy = [
            'user_name' => array_get($data, 'name'),
            'user_is_online' => array_get($data, 'isOnline'),
            'user_id' => array_get($data, 'playerId'),
            'user_room_name' => array_get($data, 'roomName'),
            'user_last_login' => array_get($data, 'lastLogin'),
            'category_id' => $supreme/*array_get($data, 'categoryId')*/,
            'instance_id' => array_get($data, 'instanceId'),
            'user_facebook_name' => array_get($data, 'facebookName'),
            'user_facebook_id' => array_get($data, 'facebookId'),
            'user_online_status' => array_get($data, 'onlineStatus'),
            'saved_category_id' => $supreme/*array_get($data, 'savedCategoryId')*/,
            'visible_room_name' => array_get($data, 'visibleRoomName'),
            'supreme_user_id' => $supreme
        ];
        $stuff = Collection::wrap($buddy)->filter(function ($value, $key) {
            return $value !== null;
        })->all();
        $user_id = ['supreme_user_id' => intval($supreme), 'user_id' => intval($stuff['user_id'])];
        $replace = array_except($stuff, ['user_id', 'supreme_user_id']);
        $model = YovilleSupremeBuddy::query()->where($user_id)->updateOrCreate($user_id, $replace);
        return $this->json_response("Posted");
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $supreme
     * @param $buddy
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $supreme, $buddy)
    {
        $user = YovilleSupremeBuddy::supreme($supreme, $buddy)->firstOrFail();
        return $this->json_response($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $supreme, $buddy)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
