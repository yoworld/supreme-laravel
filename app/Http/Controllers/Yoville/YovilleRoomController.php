<?php

namespace App\Http\Controllers\Yoville;

use App\Http\Controllers\Controller;
use App\Models\YovilleAuth;
use App\Models\YovilleRoom;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class YovilleRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /**
         * SELECT r.*, c.user_name, c.user_facebook_id FROM  yoville_rooms r LEFT JOIN yoville_characters c ON r.owner_player_id = c.user_id
         */
        /**
         * @var Builder $rooms_query
         */
        $rooms_query = YovilleRoom::leftJoin('yoville_characters', 'yoville_rooms.owner_player_id', '=', 'yoville_characters.user_id')
            ->select('yoville_rooms.*', 'yoville_characters.user_name', 'yoville_characters.user_facebook_id')
            ->where(function (Builder $builder) use ($request) {
                $allowed = [
                    'id',
                    'room_id',
                    'instance_id',
                    'owner_player_id',
                    'home_id',
                    'server_zone_name',
                    'zone_name',
                    'request_id',
                    'party_id',
                    'room_name',
                    'home_name',
                    'user_name',
                    'user_facebook_id'
                ];

                $like = [
                    'server_zone_name' => 'yoville_rooms.server_zone_name',
                    'zone_name' => 'yoville_rooms.zone_name',
                    'room_name' => 'yoville_rooms.room_name',
                    'home_name' => 'yoville_rooms.home_name',
                    'user_name' => 'yoville_characters.user_name'
                ];

                $equal = [
                    'id' => 'yoville_rooms.id',
                    'room_id' => 'yoville_rooms.room_id',
                    'instance_id' => 'yoville_rooms.instance_id',
                    'owner_player_id' => 'yoville_rooms.owner_player_id',
                    'home_id' => 'yoville_rooms.home_id',
                    'request_id' => 'yoville_rooms.request_id',
                    'party_id' => 'yoville_rooms.party_id',
                    'user_facebook_id' => 'yoville_characters.user_facebook_id'
                ];

                $time = [
                    'created_at' => 'yoville_rooms.created_at',
                    'updated_at' => 'yoville_rooms.updated_at',
                ];


                foreach ($request->input() as $key => $value) {
                    if (in_array($key, $allowed)) {
                        if (array_has($like, $key)) {
                            $builder->where($like[$key], 'LIKE', "%$value%");
                        }
                        if (array_has($equal, $key)) {
                            $builder->where($equal[$key], $value);
                        }
                        if (array_has($time, $key)) {
                            $builder->where($time[$key], 'LIKE', "%$value%");
                        }
                    }
                }

            });
        $rooms = $rooms_query->paginate($request->input('per', 20));
        return $this->json_response($rooms->items());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        /**
         * @var YovilleAuth $auth
         */

        $auth = Auth::user();

        $data = $request->has('data') ? $request->input('data') : $request->getContent();
        $room_json = json_decode($data, true);
        $room = [
            'room_id' => intval(array_get($room_json, 'roomId', 0)),
            'instance_id' => intval(array_get($room_json, 'instanceId', 0)),
            'owner_player_id' => intval(array_get($room_json, 'ownerPlayerId', 0)),
            'home_id' => intval(array_get($room_json, 'homeId', 0)),
            'server_zone_name' => array_get($room_json, 'serverZoneName', null),
            'zone_name' => array_get($room_json, 'zoneName', null),
            'request_id' => array_get($room_json, 'requestId', 0),
            'party_id' => array_get($room_json, 'partyId', 0),
            'room_name' => array_get($room_json, 'roomName', null),
            'home_name' => array_get($room_json, 'homeName', null),
            'store_uid' => $auth->assigned_to,
            'store_key' => array_get($room_json, 'store_key', null)

        ];
        $room_model = YovilleRoom::query()->updateOrCreate(
            [
                'room_id' => intval($room['room_id']),
                'store_uid' => intval(Auth::id()),
                'store_key' => array_get($room_json, 'store_key')
            ]
            , array_except($room, ['room_id', 'store_uid', 'store_key']));

        return $this->json_response($room_model, "Stored");

        //return $this->json_response(null, "This API is temporarily down for maintenance.", true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return $this->json_response(YovilleRoom::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /**
         * @var YovilleRoom $room
         */
        $room = YovilleRoom::find($id);
        try {
            return $this->json_response($room->delete());
        } catch (\Exception $e) {
            return $this->json_response(true);
        }
    }
}
