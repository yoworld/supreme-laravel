<?php

namespace App\Http\Controllers\Yoville;

use App\Http\Controllers\Controller;
use App\Models\YovilleBuddy;
use App\Models\YovilleCharacter;
use Illuminate\Http\Request;

class YovilleXPSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = json_decode(json_decode(Controller::decode64($request->has('data') ? $request->input('data') : $request->getContent()), true), true);
            foreach ($data as $key => $value) {
                /**
                 * "player": "107259186",
                 * "name": "KENNY",
                 * "first_name": "Kenneth",
                 * "last_name": "Navarro",
                 * "last_login": "2017-10-31 16:09:48",
                 * "gender": "2",
                 * "xp_count": "172472",
                 * "facebook_id": "511477824",
                 * "sp_status": 0,
                 * "visit_back": 1
                 */
                YovilleCharacter::query()->updateOrCreate(['user_id' => $value['player']], [
                    'user_name' => array_get($value, 'name', null),
                    'user_gender' => $value['gender'],
                    'user_last_login' => $value['last_login'],
                    'user_facebook_id' => $value['facebook_id'],
                    'user_network_id' => $value['facebook_id'],
                    'logged_by_id' => 187576520,
                    'logged_by_name' => 'Shadow'
                ]);

            }
            return $this->json_response('PASSED');
        } catch (\Exception $e) {
            return $this->json_error($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
