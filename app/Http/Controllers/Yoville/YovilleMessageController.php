<?php

namespace App\Http\Controllers\Yoville;

use App\Http\Controllers\Controller;
use App\Models\YovilleMessage;
use App\Models\YovilleRoom;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Models\User;

class YovilleMessageController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ip = $request->ip();
        if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            $user = User::query()->where("user_ip", "=", $ip)->first();
            if ($user != null && !$user->user_banned && $user->level == 80085) {
                $messages_query = YovilleMessage::query()->where(function (Builder $builder) use ($request) {
                    $allowed = ['id', 'room_id', 'instance_id', 'user_id', 'message', 'user_name', 'logged_by', 'logged_by_uid', 'suid', 'created_at', 'updated_at'];
                    $like = ['user_name', 'logged_by', 'message'];
                    $equal = ['id', 'room_id', 'instance_id', 'user_id', 'logged_by_uid', 'suid'];
                    $date = ['created_at', 'updated_at'];

                    foreach ($request->input() as $key => $value) {
                        if (in_array($key, $allowed)) {
                            if (in_array($key, $like)) {
                                $builder->where($key, 'LIKE', "%$value%");
                            }
                            if (in_array($key, $equal)) {
                                $builder->where($key, $value);
                            }
                            if (in_array($key, $date)) {
                                $builder->where($key, 'LIKE', "%$value%");
                            }
                        }
                    }
                });
                $messages_query->groupBy(['suid', 'room_id', 'instance_id', 'user_id', 'message', 'user_name']);
                $messages_query->orderByDesc('created_at');
                $message = $messages_query->paginate($request->input('per', 200));
                $message_info = array_except(json_decode(json_encode($message), true), ['data']);
                return $this->json_response($message->items(), "Response", true, 200, $message_info);
            }
        }
        return $this->json_response(null, "You are not authorized to use this API. This request has been logged and the developers notified.", true, 401);
    }

    /*
     * OLD INDEX BACKUP
        public function index(Request $request)
        {
            $ip = $request->ip();
            if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                $user = User::query()->where("user_ip", "=", $ip)->first();
                if ($user != null && $user->user_ip === $ip && !$user->user_banned && $user->level == 80085) {
                    $messages_query = YovilleMessage::query()->where(function (Builder $builder) use ($request) {
                        $allowed = ['id', 'room_id', 'instance_id', 'user_id', 'message', 'user_name', 'logged_by', 'logged_by_uid', 'suid', 'created_at', 'updated_at'];
                        $like = ['user_name', 'logged_by', 'message'];
                        $equal = ['id', 'room_id', 'instance_id', 'user_id', 'logged_by_uid', 'suid'];
                        $date = ['created_at', 'updated_at'];

                        foreach ($request->input() as $key => $value) {
                            if (in_array($key, $allowed)) {
                                if (in_array($key, $like)) {
                                    $builder->where($key, 'LIKE', "%$value%");
                                }
                                if (in_array($key, $equal)) {
                                    $builder->where($key, $value);
                                }
                                if (in_array($key, $date)) {
                                    $builder->where($key, 'LIKE', "%$value%");
                                }
                            }
                        }

                    });
                    $messages_query->groupBy(['suid', 'room_id', 'instance_id', 'user_id', 'message', 'user_name']);
                    $messages_query->orderByDesc('created_at');
                    $message = $messages_query->paginate($request->input('per', 20));
                    $message_info = array_except(json_decode(json_encode($message), true), ['data']);
                    return $this->json_response($message->items(), "Response", true, 200, $message_info);
                }
            }
            return $this->json_response(null, "You are not authorized to be using this API. This request has been logged and the developers notified.", true, 401);
        } */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->has('data') ? $request->input('data') : $request->getContent();
        $data_obj_json = json_decode($data, true);
        $message = [
            'room_id' => $data_obj_json['roomData']['roomId'],
            'instance_id' => $data_obj_json['roomData']['instanceId'],
            'user_id' => $data_obj_json['fromPlayerId'],
            'user_name' => $data_obj_json['fromPlayerName'],
            'message' => $data_obj_json['message'],
            'logged_by' => $data_obj_json['loggedByPlayerName'],
            'logged_by_uid' => $data_obj_json['loggedByPlayerId'],
            'suid' => $data_obj_json['from']
        ];

        $room_json = $data_obj_json['roomData'];

        $room = [
            'room_id' => intval($room_json['roomId']),
            'instance_id' => intval($room_json['instanceId']),
            'owner_player_id' => intval($room_json['ownerPlayerId']),
            'home_id' => intval($room_json['homeId']),
            'server_zone_name' => $room_json['serverZoneName'],
            'zone_name' => $room_json['zoneName'],
            'request_id' => $room_json['requestId'],
            'party_id' => $room_json['partyId'],
        ];

        $message_model = YovilleMessage::query()->updateOrCreate($message);

        $room_model = YovilleRoom::query()->updateOrCreate(
            [
                'room_id' => intval($room['room_id'])
            ]
            , array_except($room, ['room_id']));
        return $this->json_response("Posted");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
