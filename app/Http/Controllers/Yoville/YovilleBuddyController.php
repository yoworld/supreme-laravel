<?php

namespace App\Http\Controllers\Yoville;

use App\Http\Controllers\Controller;
use App\Models\YovilleBuddy;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class YovilleBuddyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $yoville_query = YovilleBuddy::query()->where(function (Builder $builder) use ($request) {
            $allowed = [
                'user_name',
                'user_is_online',
                'user_id',
                'user_room_name',
                'user_last_login',
                'category_id',
                'instance_id',
                'user_facebook_name',
                'user_online_status',
                'saved_category_id',
                'logged_by_id',
                'visible_room_name'];

            $like = [
                'user_name',
                'user_last_login',
                'user_facebook_name',
                'visible_room_name',
            ];


            $equal = [
                'user_is_online',
                'user_id',
                'user_room_name',
                'category_id',
                'instance_id',
                'user_online_status',
                'logged_by_id',
                'saved_category_id',
            ];


            $time = ['created_at', 'updated_at'];

            foreach ($request->input() as $key => $value) {
                if (in_array($key, $allowed)) {
                    if (in_array($key, $like)) {
                        $builder->where($key, 'LIKE', "%$value%");
                    }
                    if (in_array($key, $equal)) {

                        $builder->where($key, $value);
                    }
                    if (in_array($key, $time)) {
                        $builder->where($key, 'LIKE', "%$value%");
                    }
                }
            }
        });

        if (array_has($request->all(), 'fb') && is_true(array_get($request->all(), 'fb', false))) {
            $yoville_query->whereNotNull('user_facebook_id');
        }
        if (array_has($request->all(), 'sync') && is_true(array_get($request->all(), 'sync', false))) {
            $yoville_query->whereBetween('updated_at', [Carbon::now()->subMinutes(intval($request->input('ago', 10))), Carbon::now()]);
        }

        $yoville = $yoville_query->orderByDesc('updated_at')->paginate($request->input('per', 20));
        $yoville_info = array_except(json_decode(json_encode($yoville), true), ['data']);
        return $this->json_response($yoville->items(), "RESPONSE", true, 200, $yoville_info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = json_decode($request->has('data') ? $request->input('data') : $request->getContent(), true);
        $buddy = [
            'user_name' => array_get($data, 'name'),
            'user_is_online' => array_get($data, 'isOnline'),
            'user_id' => array_get($data, 'playerId'),
            'user_room_name' => array_get($data, 'roomName'),
            'user_last_login' => array_get($data, 'lastLogin'),
            'category_id' => array_get($data, 'categoryId'),
            'instance_id' => array_get($data, 'instanceId'),
            'user_facebook_name' => array_get($data, 'facebookName'),
            'user_facebook_id' => array_get($data, 'facebookId'),
            'user_online_status' => array_get($data, 'onlineStatus'),
            'saved_category_id' => array_get($data, 'savedCategoryId'),
            'visible_room_name' => array_get($data, 'visibleRoomName'),
            'logged_by_id' => $request->header('id', Auth::id())
        ];
        $stuff = Collection::wrap($buddy)->filter(function ($value, $key) {
            return $value !== null;
        })->all();

        $user_id = ['user_id' => intval($stuff['user_id'])];
        $replace = array_except($stuff, 'user_id');
        $model = YovilleBuddy::query()->updateOrCreate($user_id, $replace);
        return $this->json_response("Posted");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->json_response(YovilleBuddy::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
