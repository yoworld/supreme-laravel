<?php

namespace App\Http\Controllers\Yoville;

use App\Http\Controllers\Controller;
use App\Models\YovilleCharacter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class YovilleCharacterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $yoville_query = YovilleCharacter::query()->where(function (Builder $builder) use ($request) {
            $allowed = ['user_id', 'user_name',
                'user_gender', 'user_badge_id',
                'user_last_login',
                'user_created_on',
                'user_first_time',
                'logged_by_id',
                'logged_by_name',
                'user_mod_level',
                'user_clothing'
            ];

            $like = ['user_name',
                'logged_by_name'
            ];

            $equal = ['user_id',
                'user_gender', 'user_badge_id',
                'user_first_time',
                'logged_by_id',
                'user_mod_level'
            ];

            $time = [
                'user_last_login',
                'user_created_on',
            ];

            foreach ($request->input() as $key => $value) {
                if (in_array($key, $allowed)) {
                    if (in_array($key, $like)) {
                        $builder->where($key, 'LIKE', "%$value%");
                    }
                    if (in_array($key, $equal)) {

                        $builder->where($key, $value);
                    }
                    if (in_array($key, $time)) {
                        $builder->where($key, 'LIKE', "%$value%");
                    }
                }
            }

        });

        if (array_has($request->all(), 'fb') && is_true(array_get($request->all(), 'fb', false))) {
            $yoville_query->whereNotNull('user_facebook_id');
        }
        $yoville = $yoville_query->orderByDesc('updated_at')->paginate($request->input('per', 20));
        $yoville_info = array_except(json_decode(json_encode($yoville), true), ['data']);
        return $this->json_response($yoville->items(), "RESPONSE", true, 200, $yoville_info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = json_decode($request->has('data') ? $request->input('data') : $request->getContent(), true);
        $character = [
            'user_id' => array_get($data, 'playerId', null),
            'user_name' => array_get($data, 'name', null),
            'user_gender' => array_get($data, 'gender', null),
            'user_badge_id' => array_get($data, 'badgeId', 0),
            'user_last_login' => array_get($data, 'lastLogin', null),
            'user_created_on' => array_get($data, 'createdOn', null),
            'user_first_time' => array_get($data, 'firstTime', 0),
            'logged_by_id' => array_get($data, 'loggedByPlayerId', Auth::user()->user_id),
            'logged_by_name' => array_get($data, 'loggedByPlayerName', null),
            'user_mod_level' => array_get($data, 'mod_level', 0),
            'user_avatar' => array_get($data, 'pic_url', null),
            'user_network_id' => array_get($data, 'network_id', null),
            'user_facebook_id' => array_get($data, 'facebook_id', null),
            'user_type' => array_get($data, 'type', null),
            'user_clothing' => json_encode(array_get($data, 'clothing', null))
        ];
        $stuff = Collection::wrap($character)->filter(function ($value, $key) {
            return $value !== null;
        })->all();
        if (array_has($stuff, 'user_id')) {
            $user_id = ['user_id' => intval($data['playerId'])];
            $replace = array_except($stuff, 'user_id');
            $model = YovilleCharacter::query()->updateOrCreate($user_id, $replace);
            return $this->json_response($model);
        } else {
            return $this->json_error("does not user_id to update character");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->json_response(YovilleCharacter::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allowed = ['user_name',
            'user_gender', 'user_badge_id',
            'user_last_login',
            'user_created_on',
            'user_first_time',
            'logged_by_id',
            'logged_by_name',
            'user_mod_level',
            'user_avatar',
            'user_network_id',
            'user_facebook_id',
            'user_type',
            'user_clothing'
        ];
        $filter = \Illuminate\Database\Eloquent\Collection::wrap($request->input())->filter(function ($value, $key) use ($allowed) {
            if (in_array($key, $allowed)) {
                return $value !== null;
            } else return false;
        });
        $character = YovilleCharacter::find($id);
        $character->update($filter->toArray());
        return $this->json_response("Posted");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $character = YovilleCharacter::find($id);
        try {
            return $character != null ? $this->json_response($character->delete()) : $this->json_error("User no longer exists");
        } catch (\Exception $e) {
            return $this->json_error("User no longer exists");
        }
    }
}
