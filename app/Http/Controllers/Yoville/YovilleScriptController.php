<?php
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 9/13/2018
 * Time: 11:41 PM
 */

namespace App\Http\Controllers\Yoville;

use App\Http\Controllers\Controller;
use App\Models\YovilleCharacter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

/**
 * THIS CLASS IS UNFINISHED AND UNTESTED.
 * Class YovilleScriptController
 * @package App\Http\Controllers\Yoville
 */
class YovilleScriptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /**
         * Fetch Avatars from JSON script 1 by 1
         * Add them each to an array of characters
         * For each avatar listed in the script, attach their script and name it Script
         * return script json_encoded
         */
        try {
            $script = json_decode(File::get(storage_path('app/public/scripts.json')), false);
        } catch (\Illuminate\Contracts\Filesystem\FileNotFoundException $e) {
            return $this->json_response(null, "Failed to generate script.", false, 500);
        }
        $clients = $script->clients;
        $characters = [];
        foreach ($clients as $char) {
            $character = YovilleCharacter::query()->find($char->playerId);
            $player = json_encode(array('gender' => $character->user_gender, 'clothing' => $character->user_clothing, 'isViking' => false,
                'name' => $character->user_name, 'badgeId' => $character->user_badge_id, 'mod_level' => $character->user_mod_level), JSON_FORCE_OBJECT);
            $character->user_clothing = null;
            $character->player = $player;
            $character->serverUserId = rand(100000, 9999999);
            $character->x = $char->script->x;
            $character->y = $char->script->y;
            $character->d = $char->script->d;
            $character->speechBalloonColor = 16711680;
            $character->script = $char->script;
            array_push($characters, $character);
        }
        return $this->json_response($characters, "Script successfully generated.", true, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}