<?php

namespace App\Http\Controllers\Yoville;

use App\Http\Controllers\Controller;
use App\Models\Yoville;
use Illuminate\Http\Request;

class YovilleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $yoville = Yoville::query()->orderByDesc('created_at')->paginate($request->input('per', 20));

        $yoville_info = array_except(json_decode(json_encode($yoville), true), ['data']);
        return $this->json_response($yoville->items(),"RESPONSE", true, 200, $yoville_info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->has('data') ? $request->input('data') : $request->getContent();
        try {
            $data_obj = json_decode($data, true);
            $result = array_has($data_obj, 'data') ? $data_obj['data'] : array_has($data_obj, 'charData') ? $data_obj['charData'] : $data_obj;
            if (is_array($request)) {
                $data_obj_json = $result;
            } else {
                $data_obj_json = $result;
            }
            return Controller::json_response_force(Yoville::query()->create(['data' => $data_obj_json]));
        }catch (\Exception $e){
            return Controller::json_response_force(Yoville::query()->create(['data' => $data]));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->json_response(Yoville::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
