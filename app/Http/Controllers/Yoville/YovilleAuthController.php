<?php

namespace App\Http\Controllers\Yoville;

use App\Http\Controllers\Controller;
use App\Http\Resources\AccountCollection;
use App\Http\Resources\AccountResource;
use App\Models\User;
use App\Models\UserSetting;
use App\Models\YovilleAuth;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class YovilleAuthController extends Controller
{


    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return AccountCollection
     */
    public function index(Request $request)
    {

        $yoville_query = YovilleAuth::query()->where(function (Builder $builder) use ($request) {
            $allowed = [
                'user_id',
                'user_name',
                'user_banned',
                'user_ip',
                'created_at',
                'updated_at',
                'user_facebook_id',
                'user_snapi_auth',
                'user_network_credentials',
                'user_yo_auth_key',
                'user_lk',
                'user_facebook_name',
                'user_key',
                'user_settings',
                'user_level',
                'user_active',
                'assigned_to'
            ];

            $like = [
                'user_name',
                'user_ip',
                'user_snapi_auth',
                'user_network_credentials',
                'user_yo_auth_key',
                'user_lk',
                'user_facebook_name'
            ];


            $equal = [
                'user_id',
                'user_banned',
                'user_facebook_id',
                'user_key',
                'user_level',
                'user_active',
                'assigned_to'
            ];

            $json = [
                'user_settings'
            ];

            $time = ['created_at', 'updated_at'];

            foreach ($request->input() as $key => $value) {
                if (in_array($key, $allowed)) {
                    if (in_array($key, $like)) {
                        $builder->where($key, 'LIKE', "%$value%");
                    }
                    if (in_array($key, $equal)) {

                        $builder->where($key, $value);
                    }
                    if (in_array($key, $time)) {
                        $builder->where($key, 'LIKE', "%$value%");
                    }
                    if (in_array($key, $json)) {

                    }
                }
            }
        });


        /**
         * @var Builder $yoville_query
         */

        $yoville_query = is_true($request->header('bypass', false)) || is_true($request->input('deleted', false)) ? $yoville_query : $yoville_query;
        $yoville = $yoville_query->orderByDesc('updated_at')->paginate($request->input('per', 20));
        return new AccountCollection($yoville, "Accounts");

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->has('data') ? $request->input('data') : $request->getContent();
            $dec_data = Controller::decode64($data);
            $data_obj = json_decode($dec_data, true);

            $ip = $request->ip();

            $data = [
                'user_name' => array_get($data_obj, 'user_name'),
                'user_snapi_auth' => array_get($data_obj, 'snapi_auth'),
                'user_facebook_id' => array_get($data_obj, 'fb_id', array_get($data_obj, 'fbId')),
                'user_network_credentials' => array_get($data_obj, 'network_credentials'),
                'user_yo_auth_key' => array_get($data_obj, 'yo_auth_key'),
                'user_lk' => array_get($data_obj, 'lk'),
                'user_facebook_name' => array_get($data_obj, 'fb_name'),
                'user_active' => true,
                'user_last_auth' => Carbon::now()->toDateTimeString(),
                'assigned_to' => Auth::id()
            ];

            if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                $data['user_ip'] = $ip;
            } else if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
                $data['user_ipv6'] = $ip;
            }

            /**
             * @var User $user
             * @var \App\YovilleAuth $model
             */
            $model = YovilleAuth::query()->updateOrCreate(['user_id' => $data_obj['user_id']], $data)->refresh();
            $user = Auth::user();
            $user->update(['active' => true, 'user_last_auth' => Carbon::now()->toDateTimeString()]);
            if($model->api_token == null){
                $model->api_token = self::create_token_force(sprintf("%s-%s-%s", $model->user_id, $model->user_facebook_id, $model->user_facebook_name));
                $model->save();
            }
            if($user->settings == null) {
                UserSetting::query()->create(['id' => Auth::id()]);
            }
            $time = strval(time());
            if ($model->user_banned) return response()->json(['s' => Controller::encode64('0')], 401);
            else return response()->json(['s' => Controller::encode64(strval($data_obj['user_id'])), 'x' => Controller::encode64($time), 'b' => Controller::encode64($data['user_facebook_id'])], 202);
        } catch (Exception $e) {
            return response()->json(['s' => Controller::encode64('0'), 'l' => $e->getLine(), 'e' => Controller::encode64($e->getMessage())], 406);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return AccountResource
     */
    public function show($id)
    {
        return new AccountResource(YovilleAuth::find($id));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return AccountResource
     */
    public function update(Request $request, $id)
    {

        $data_obj = json_decode($request->has('data') ? $request->input('data') : $request->getContent(), true);

        if (array_key_exists('user_ip', $data_obj)) {
            if ($data_obj['user_ip'] == '<currentip>') {
                $data_obj['user_ip'] = $request->ip();
            }
            if (!filter_var($data_obj['user_ip'], FILTER_VALIDATE_IP)) {
                return $this->json_error("IP could not be validated properly");
            }
        }

        $filter = Collection::wrap($data_obj)->filter(function ($value, $key) {
            $allowed = [
                'user_id',
                'user_name',
                'user_banned',
                'user_ip',
                'created_at',
                'updated_at',
                'user_facebook_id',
                'user_snapi_auth',
                'user_network_credentials',
                'user_yo_auth_key',
                'user_lk',
                'user_facebook_name',
                'user_key',
                'user_settings',
                'user_level',
                'assigned_to'
            ];
            if (in_array($key, $allowed)) {
                return $value !== null;
            } else return false;
        });

        /**
         * @var YovilleAuth $auth
         */
        $query = YovilleAuth::query();
        if (is_true($request->header('bypass', false))) {
            $auth = $query->withTrashed()->findOrFail($id);
            if($auth->deleted_at !== null) {
                $auth->restore();
            }
        }else{
            $auth = $query->findOrFail($id);
        }
        $updated = $auth->update($filter->toArray());


        if ($updated) {
            return new AccountResource($auth->refresh(), "Values updated");
        } else {
            return new AccountResource($auth, "Values not updated");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return AccountResource
     */
    public function destroy($id)
    {
        $auth = YovilleAuth::query()->findOrFail($id);
        try {
            $delete = $auth->delete();
            return new AccountResource($auth, $delete ? "This user has been removed." : "This user did not exist.");
        } catch (\Exception $e) {
            return new AccountResource($auth, 'This user has been removed - Error:' . $e->getMessage());
        }
    }
}
