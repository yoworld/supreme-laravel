<?php

namespace App\Http\Controllers;

use App\Http\Resources\NewsCollection;
use App\Http\Resources\NewsResource;
use App\Models\SupremeNews;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class SupremeNewsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return NewsCollection|Response
     */
    public function index(Request $request)
    {
        return new NewsCollection(SupremeNews::query()->where(function (Builder $builder) use ($request) {
            $allowed = ['title', 'description', 'user_id'];
            $only = $request->only($allowed);
            foreach ($only as $key => $value) {
                if (is_numeric($value)) {
                    $builder->where($key, $value);
                } else {
                    $builder->where($key, 'LIKE', "%$value%");
                }
            }
        })->paginate($request->input('limit', 15)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return NewsResource
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $allowed = [
            'user_id' => 'int|required|exists:users',
            'title' => 'string|required',
            'description' => 'string|required',
            'artwork_url' => 'string|required'
        ];
        $inputs = $request->only(array_keys($allowed));
        $this->validate($request, $allowed);
        return new NewsResource(SupremeNews::query()->create($inputs));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return NewsResource
     */
    public function show($id)
    {

        return new NewsResource(SupremeNews::query()->find($id));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return NewsResource
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        $supreme = SupremeNews::query()->find($id);

        $allowed = [
            'user_id' => 'int|exists:users',
            'title' => 'string',
            'description' => 'string',
            'artwork_url' => 'string'
        ];
        $inputs = $request->only(array_keys($allowed));
        $this->validate($request, $allowed);
        $supreme->update($inputs);
        return new NewsResource($supreme);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {

        $supreme = SupremeNews::query()->find($id);
        $deleted = $supreme->delete();
        return $this->json_response($deleted > 0);
    }
}
