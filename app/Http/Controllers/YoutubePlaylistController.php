<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\YoutubePlaylist;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDOException;

class YoutubePlaylistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function index(Request $request, User $user)
    {
        $youtube = YoutubePlaylist::query()
            ->where('user_id', $user->id)
            ->orderBy($request->input('by', 'position'))
            ->paginate($request->input('per', 20));

        $youtube_info = array_except(json_decode(json_encode($youtube), true), ['data']);
        return $this->json_response($youtube->items(), sprintf("%s's Playlist", $user->name), true, 200, $youtube_info);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function store(Request $request, User $user)
    {
        if (Auth::user()->isAdmin() || Auth::user()->id == $user->id) {
            try {
                $position = $request->input('position', 0);
                $youtube_id = $request->input('youtube_id');
                $user_id = $user->id;
                if ($youtube_id == null) return $this->json_error("No youtube id found");
                $fetch = YoutubePlaylist::fetch($user->id, $youtube_id);
                if ($fetch->count() > 0) return $this->json_error("Already Exist In Playlist");
                else if ($position == 0) {
                    DB::select("UPDATE youtube_playlist SET youtube_playlist.position = youtube_playlist.position + 1 WHERE user_id = $user_id;");
                }
                $data = [
                    'user_id' => $user_id,
                    'youtube_id' => $youtube_id,
                    'position' => $position
                ];
                YoutubePlaylist::query()->create($data);
                DB::unprepared(DB::raw("SET @count = -1; UPDATE youtube_playlist SET youtube_playlist.position = @count:= @count + 1 WHERE user_id = $user_id ORDER BY position ASC ;"));
                $youtube = YoutubePlaylist::fetch($user_id, $youtube_id)->first();
                return $this->json_response($youtube, sprintf("%s's Playlist", $user->name));
            } catch (PDOException $e) {
                return $this->json_error("Unable to add song to playlist, either doesnt exist or already added. ", $e->getMessage());
            } catch (Exception $e){
                return $this->json_error($e->getMessage(), $e->getCode());
            }
        } else {
            return $this->json_error("You do not have permission to change the user's playlist");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param User $user
     * @param YoutubePlaylist $playlist
     * @return Response
     */
    public function show(Request $request, User $user, $playlist)
    {
        try {
            $youtube = YoutubePlaylist::fetch($user->id, $playlist)->first();
            return $this->json_response($youtube, $youtube->title);
        } catch (Exception $e) {
            return $this->json_error("Song not within playlist doesn't exist");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @param $playlist
     * @return Response
     */
    public function update(Request $request, User $user, $playlist)
    {
        if (Auth::user()->isAdmin() || Auth::user()->id == $user->id) {

            $youtube = YoutubePlaylist::fetch($user->id, $playlist)->firstOrFail();
            $youtube->update(['position' => $request->input('position', 0)]);
            $user_id = $user->id;
            DB::unprepared(DB::raw("SET @count = -1; UPDATE youtube_playlist SET youtube_playlist.position = @count:= @count + 1 WHERE user_id = $user_id ORDER BY position ASC ;"));

            return $this->json_response($youtube, $youtube->title);
        } else {
            return $this->json_error("You do not have permission to update this users playlist");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param User $user
     * @param $playlist
     * @return Response
     * @throws Exception
     */
    public function destroy(Request $request, User $user, $playlist)
    {
        if (Auth::user()->isAdmin() || Auth::user()->id == $user->id) {
            $youtube = YoutubePlaylist::fetch($user->id, $playlist)->firstOrFail();
            $youtube->delete();
            $user_id = $user->id;
            DB::unprepared(DB::raw("SET @count = -1; UPDATE youtube_playlist SET youtube_playlist.position = @count:= @count + 1 WHERE user_id = $user_id ORDER BY position ASC ;"));
            return $this->json_response(sprintf("%s deleted from playlist", $youtube->title));
        } else {
            return $this->json_error("You do not have permission to delete this song off the users playlist");
        }
    }
}
