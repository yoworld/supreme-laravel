<?php

namespace App\Http\Controllers;

use App\Helpers\SoundCloudHelper;
use App\Models\SoundCloud;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class SoundcloudController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit =$request->input('per', 20);
        $original_page = $request->input('page', 1);
        if($original_page < 0){
            $original_page = 1;
        }
        $skip = ($original_page - 1) * $limit;
        $to = ($original_page) * $limit;
        $query = $request->input('query',"");
        $response = SoundCloudHelper::search($query, new Client());
        $query = !empty($query) ? '>' . implode(' >', explode(' ', $query)) : '';
        $soundcloud = SoundCloud::query()
            ->select('*',\DB::raw("match(title, artist, username, description) against ('$query' in boolean mode) as relevance"))
            ->orderByRaw("relevance DESC")
            //->havingRaw("relevance > 0")
            ->take($limit)->skip($skip)->get();

        $count = SoundCloud::query()->count();
        $last_page = $count > 0 ? intval($count / $limit) + (($count % $limit ) > 0 ? 1 : 0)  : 0;
        $from = ($skip > $count ? null : $skip + 1);
        $current_page = ceil($to / $limit);
        $user_info = [
            'current_page' => $current_page,
            'from' => $from,
            'last_page' => $last_page,
            'per_page' => $limit,
            'to' => ($to > $count ? $current_page != $last_page ? null :  ($count) : $to + 1),
            'total' => $count,
        ];
        return $this->json_response($soundcloud, "RESPONSE", true, 200, $user_info);
    }


    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $soundcloud_url
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws Exception
     */
    public function show(Request $request, $soundcloud_url)
    {
        /**
         * @var SoundCloud $soundcloud
         */
        $doesSoundCloudExist = SoundCloud::doesExists($soundcloud_url);


        $client = new Client();
        if ($doesSoundCloudExist) {
            $soundcloud = SoundCloud::getSoundCloud($soundcloud_url);
            if (empty($soundcloud->mp3_loc) || !File::exists(storage_path($soundcloud->mp3_loc))) {
                $soundcloud = SoundCloudHelper::download($soundcloud_url, $client);
            }
        } else {
            $soundcloud = SoundCloudHelper::download($soundcloud_url, $client);
        }
        $path = storage_path($soundcloud->mp3_loc);
        if (File::exists($path)) {
            $timestamp = File::lastModified($path);
            $modified = Carbon::createFromTimestampUTC($timestamp)->toDayDateTimeString();
            $length = File::size($path);
            $hash = File::hash($path);
            $file = File::get($path);
            $type = File::mimeType($path);

            /**
             * @var ResponseFactory $file_response
             */
            $file_response = Response::make($file, 200);
            $file_response->header("Content-Type", $type);
            $file_response->header("Content-Length", $length);
            $file_response->header("Content-MD5", $hash);
            $file_response->header("Last-Modified", $modified);
            $file_response->header("X-Song-Name", $soundcloud->title);
            $referer = $request->header('referer');
            if (starts_with($referer, 'https://yw-web.yoworld.com/')) {
                $file_response->header("Content-Disposition", sprintf('attachment; filename="%s.mp3"', $soundcloud->title));
            } else {
                $should_download = $request->input('dl', false);
                if (is_true($should_download)) {
                    $file_response->header("Content-Disposition", sprintf('attachment; filename="%s.mp3"', $soundcloud->title));
                } else {
                    $file_response->header("Content-Disposition", sprintf('filename="%s.mp3"', $soundcloud->title));
                }
            }
            return $file_response;
        } else {
            return $this->json_error("Unable to download song");
        }
    }


}
