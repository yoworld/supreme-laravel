<?php

namespace App\Http\Controllers;

use App\Http\Resources\VersionCollection;
use App\Http\Resources\VersionResource;
use App\Models\Versioning;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Agent\Agent;

class VersionController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return VersionCollection
     */
    public function index(Request $request)
    {
        $version_query = Versioning::query()->where(function (Builder $builder) use ($request) {


            $allowed = [
                'codename',
                'version',
                'publish',
                'md5',
                'version'
            ];


            $like = [
                'codename',
            ];


            $equal = [
                'md5',
                'version',
                'publish'
            ];


            $time = ['created_at', 'updated_at'];

            foreach ($request->input() as $key => $value) {
                if (in_array($key, $allowed)) {
                    if (in_array($key, $like)) {
                        $builder->where($key, 'LIKE', "%$value%");
                    }
                    if (in_array($key, $equal)) {

                        $builder->where($key, $value);
                    }
                    if (in_array($key, $time)) {
                        $builder->where($key, 'LIKE', "%$value%");
                    }
                }
            }
        });

        $version = $version_query->orderByDesc('updated_at')->paginate($request->input('per', 20));
        return new VersionCollection($version, 'Swf Versions');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return VersionResource
     */
    public function store(Request $request)
    {
        if (Auth::user()->isAdmin()) {

            $swf = $request->file('file');
            $file = array_get($request->input(), 'md5') . '.swf';
            if (File::exists(storage_path('app/swf/' . $file))) {
                return $this->json_response("File Already Exists", "The file was already uploaded...", false);
            }
            $data = [
                'md5' => array_get($request->input(), 'md5'),
                'codename' => array_get($request->input(), 'codename'),
                'version' => array_get($request->input(), 'version'),
                'size' => $swf->getClientSize(),
                'type' => "application/x-shockwave-flash",
                'location' => 'swf/' . $file,
                'created_at' => $swf->getMTime()
            ];
            /**
             * @var Versioning $version
             */
            $version = Versioning::query()->create($data)->refresh();

            $swf->storeAs("swf", $file);
            return new VersionResource($version, "Version Created");
        } else {
            return $this->json_error("??", "??");
        }
    }

    /**
     * @param Versioning $version
     * @return \Illuminate\Http\Response
     * @throws FileNotFoundException
     */
    function getVersionResponse(Versioning $version)
    {
        $path = storage_path("app/" . $version->location);
        if(File::exists($path)) {
            $timestamp = File::lastModified($path);
            $modified = Carbon::createFromTimestampUTC($timestamp)->toDayDateTimeString();
            $length = File::size($path);
            $hash = File::hash($path);
            $file = File::get($path);
            $type = File::mimeType($path);
            $response = Response::make($file, 200);
            /**
             * @var string version
             */
            $version = Versioning::find($hash);

            if ($version == null) {
                $version = Versioning::query()
                    ->create([
                        'md5' => $hash,
                        'codename' => "???",
                        'version' => '???',
                        'size' => $length,
                        'type' => $type,
                        'created_at' => Carbon::createFromTimestampUTC($timestamp)->toDateTimeString()])->refresh();
            }

            Auth::user()->update(['mod_hash' => $version->md5]);


            $response->header("Content-Type", $type);
            $response->header("Content-Length", $length);
            $response->header("Content-MD5", $hash);
            $response->header("Last-Modified", $modified);
            if ($version != null) {
                $response->header("X-Version-Name", $version->codename);
                $response->header("X-Version", $version->version);
            }
            $response->header("Content-Disposition", sprintf('filename="%s.swf"', $version->codename));


            return $response;
        }else{
            return $this->json_error("This currently hasn't been released yet", "Check back later")->header('X-Supreme-Error', 'Content not released');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        try {
            /**
             * @var Versioning $version
             */
            $version = Versioning::query()->findOrFail($id);

            if (Gate::allows('show', $version)) {
                if($request->hasHeader('Informative')){
                    return $this->json_response($version, "Information");
                }
                if ($version->publish) {
                    if ($request->hasHeader('user-agent')) {
                        if ($request->hasHeader('referer')) {
                            $referer = $request->header('referer');
                            if ($referer == 'https://apps.facebook.com/') {
                                return $this->getVersionResponse($version);
                            } else if (starts_with($referer, 'https://yw-web.yoworld.com/')) {
                                $requested_with = $request->header('x-requested-with');
                                $agent = new Agent();
                                if (
                                    ($agent->is('OS X') && $agent->is('Safari')) ||
                                    ($agent->is('OS X') && $agent->is('Mozilla')) ||
                                    (starts_with($requested_with, 'ShockwaveFlash'))
                                ) {
                                    return $this->getVersionResponse($version);
                                } else return Controller::json_error_force("You\'re not Supreme.");
                            } else return Controller::json_error_force("Not sure what you trying to get at...");
                        } else return Controller::json_response_force("Are you trying to download?");
                    } else return Controller::json_error_force("Hmmmm....Something seems a bit off");
                } else {
                    return $this->json_error("This mod has not been approved/published by the developers of Supreme1", "Check back later")->header('X-Supreme-Denied', 'Content not published');
                }
            } else {
                return $this->json_error("You were not approved for this mod, please check back later or speak to the developers", "Permission Denied")->header('X-Supreme-Denied', 'User not permissible');
            }

        } catch (Exception $e) {
            return $this->json_error($e->getMessage(), $e->getLine());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return VersionResource
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->isAdmin()) {
            $version = Versioning::query()->findOrFail($id);
            $allowed = [
                'codename',
                'version',
                'publish'
            ];

            $stuff = Collection::wrap($request->input())->filter(function ($value, $key) use ($allowed) {
                if (in_array($key, $allowed)) {
                    return $value !== null;
                } else {
                    return false;
                }
            })->all();
            $version->update($stuff);
            return new VersionResource($version, "Version Updated");
        } else {
            return $this->json_error("??", "??");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->isAdmin()) {
            $version = Versioning::query()->findOrFail($id);
            $path = storage_path("app/" . $version->location);

            if (File::exists($path)) {
                File::delete($path);
            }
            try {
                return $this->json_response($version->delete(), "Version Updated");
            } catch (Exception $e) {
                return $this->json_error("Not found", "Version Deleted", false, 404);
            }
        } else {
            return $this->json_error("??", "??");
        }
    }
}
