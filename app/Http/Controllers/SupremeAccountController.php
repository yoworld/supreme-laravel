<?php

namespace App\Http\Controllers;

use App\Http\Resources\AccountCollection;
use App\Models\User;
use App\Models\YovilleAuth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class SupremeAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param User $user
     * @return AccountCollection|Response
     */
    public function index(Request $request, User $user)
    {
        if(Auth::user()->isAdmin() || Auth::user()->id == $user->id) {
            return new AccountCollection($user->accounts()->paginate());
        }else{
            return $this->json_error("You do not have permission to view these accounts");
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @return void
     */
    public function store(Request $request, User $user)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param User $user
     * @param YovilleAuth $account
     * @return void
     */
    public function show(Request $request, User $user, YovilleAuth $account)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @param YovilleAuth $account
     * @return void
     */
    public function update(Request $request, User $user, YovilleAuth $account)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param User $user
     * @param YovilleAuth $account
     * @return void
     */
    public function destroy(Request $request, User $user, YovilleAuth $account)
    {
        //
    }
}
