<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return UserCollection
     */
    public function index(Request $request)
    {
        if(!Auth::user()->isAdmin() && !Auth::user()->isModerator()) return $this->json_error("You're just a regular user");
        $user_query = User::query()->where(function (Builder $builder) use ($request) {


            $allowed = [
                'id',
                'name',
                'email',
                'user_ip',
                'user_ipv6',
                'user_banned',
                'level'
            ];

            $like = [
                'name',
                'email',
            ];


            $equal = [
                'id',
                'user_banned',
                'level',
                'user_ip',
                'user_ipv6',
            ];


            $time = ['created_at', 'updated_at'];

            foreach ($request->input() as $key => $value) {
                if (in_array($key, $allowed)) {
                    if (in_array($key, $like)) {
                        $builder->where($key, 'LIKE', "%$value%");
                    }
                    if (in_array($key, $equal)) {

                        $builder->where($key, $value);
                    }
                    if (in_array($key, $time)) {
                        $builder->where($key, 'LIKE', "%$value%");
                    }
                }
            }
        });

        $user = $user_query->orderByDesc('updated_at')
            ->paginate($request->input('per', 20));
        return new UserCollection($user, 'User Accounts');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return UserResource
     */
    public function show($id)
    {
        return new UserResource(User::query()->findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return UserResource
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->isAdmin() || Auth::user()->id == $id) {
            $user = User::query()->findOrFail($id);
            $allowed = Auth::user()->isAdmin() ? [
                'name',
                'email',
                'user_banned',
                'api_token',
                'level',
                'user_ip'
            ] : ['name', 'email'];

            $data_obj = $request->input();


            if (array_key_exists('user_ip', $data_obj)) {
                if ($data_obj['user_ip'] == '<currentip>') {
                    $data_obj['user_ip'] = $request->ip();
                }
                if(!filter_var($data_obj['user_ip'], FILTER_VALIDATE_IP)){
                    return $this->json_error("IP could not be validated properly");
                }
            }

            $stuff = Collection::wrap($data_obj)->filter(function ($value, $key) use ($allowed) {
                if (in_array($key, $allowed)) {
                    return $value !== null;
                } else {
                    return false;
                }
            })->all();
            $user->update($stuff);
            return new UserResource($user);
        } else {
            return $this->json_error("You do no have permission", "You suck.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->isAdmin() || Auth::user()->id == $id) {
            try {
                $delete = User::query()->findOrFail($id)->delete();
                return $this->json_response($delete, "User Removed");
            } catch (Exception $e) {
                return $this->json_response(false, "Resource not found but....User Removed...");
            }
        } else {
            return $this->json_error("You do not have permission", "Fail...");
        }
    }
}
