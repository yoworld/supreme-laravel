<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * @param Request $request
     * @param User $user
     */
    protected function authenticated(Request $request, $user)
    {

        $ip = $request->ip();
        if($user->api_token == null) {
            $user->api_token = Controller::create_token_force(sprintf("%s-%s-%s", $user->id, $user->email, $ip));
        }
        if(filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            $backup_ip = $user->user_ip;
            $user->user_ip = $ip;

            try {
                $user->saveOrFail();
            } catch (\Throwable $e) {
                $user->user_ip = $backup_ip;
                $user->save();
                Auth::logout();
            }
        }else if(filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
            $backup_ip = $user->user_ipv6;
            $user->user_ipv6 = $ip;
            try {
                $user->saveOrFail();
            } catch (\Throwable $e) {
                $user->user_ipv6 = $backup_ip;
                $user->save();
                Auth::logout();
            }
        }
    }
}
