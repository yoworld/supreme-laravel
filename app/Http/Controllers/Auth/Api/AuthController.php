<?php

namespace App\Http\Controllers\Auth\Api;

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\ViewErrorBag;

class AuthController extends LoginController
{
    use SendsPasswordResetEmails, ResetsPasswords;

    public function broker()
    {
        return Password::broker();
    }

    /**
     * email
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgot(Request $request)
    {
        $this->sendResetLinkEmail($request);
        return $this->json_response("Check your email to reset your password.", "Forgot Password");
    }

    /**
     * token, password, email, password_confirmation
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset_pass(Request $request)
    {
        $res = $this->reset($request);
        if ($res->getSession()->has('status')) {
            $message = $res->getSession()->get('status');
            return $this->json_response($message, 'Password Reset');
        } else {
            /**
             * @var ViewErrorBag $error
             */
            $error = $res->getSession()->get("errors");
            return $this->json_error($error->first(), "Reset Errors");
        }
    }

    public function login(Request $request)
    {
        if ($request->isMethod('GET')) {
            return $this->json_error('Invalid Method Request');
        }
        $email = $request->input("email");
        if ($this->attemptLogin($request)) {
            $user = User::query()->where('email', '=', $email);
            if ($user->exists()) {
                return $this->json_response($user->first()->makeVisible(['api_token']), "Successfully Logged In");
            } else {
                return $this->json_error("Fuck off", "Invalid Credentials");
            }
        }
        return $this->json_error("Username Invalid Email/Username Or Password", "Invalid Credentials");
    }

    protected function credentials(Request $request)
    {
        if ($request->path() == 'api/v1/auth/reset') {
            return $request->only('email', 'token', 'password', 'password_confirmation');
        }
        return $request->only('email', 'password');
    }

    public function register(Request $request)
    {
        if ($request->isMethod('GET')) {
            return $this->json_error('Invalid Method Request');
        }
        $name = $request->input("name");
        $email = $request->input("email");
        $password = $request->input('password');


        $user = User::query()->where("email", "=", $email);
        if (!$user->exists()) {
            $token = $this->create_token($email . strval(time()));
            /**
             * @var User $user
             */
            $user = User::query()->create(["name" => $name, "email" => $email, "password" => bcrypt($password), "api_token" => $token]);
            $id = $user->id;
            $user = $user->newQuery()->where('id', '=', $id)->first();
            //MailHelper::registered($user, base64_encode(bcrypt($user->email . $user->created_at)));

            return $this->json_response($user->first()->makeVisible(['api_token']), "Successfully registered");
        }
        return $this->json_error("This user/email already exist.", "Invalid Registration");
    }

}
