<?php

namespace App\Http\Controllers;

use App\Helpers\YoutubeHelper;
use App\Models\YoutubeMP3;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class YoutubeMP3Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $youtube_query = YoutubeMP3::query()->where(function (Builder $builder) use ($request) {


            $allowed = [
                'id',
                'link',
                'title',
                'mp4_md5_hash',
                'mp3_md5_hash',
            ];

            $like = [
                'title',
            ];


            $equal = [
                'id',
                'link',
                'mp4_md5_hash',
                'mp3_md5_hash',
            ];


            $time = ['created_at', 'updated_at'];

            foreach ($request->input() as $key => $value) {
                if (in_array($key, $allowed)) {
                    if (in_array($key, $like)) {
                        $builder->where($key, 'LIKE', "%$value%");
                    }
                    if (in_array($key, $equal)) {

                        $builder->where($key, $value);
                    }
                    if (in_array($key, $time)) {
                        $builder->where($key, 'LIKE', "%$value%");
                    }
                }
            }
        });
        $youtube = $youtube_query->orderByDesc('updated_at')->paginate($request->input('per', 20));
        $user_info = array_except(json_decode(json_encode($youtube), true), ['data']);
        return $this->json_response($youtube->items(), "RESPONSE", true, 200, $user_info);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return ResponseFactory
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function show(Request $request, $id)
    {

        if(starts_with($id, "{youtube_id}")){
            $id = str_replace("{youtube_id}","", $id);
        }

        if(YoutubeMP3::doesExists($id)){
            /**
             * @var YoutubeMP3 $youtube
             */
            $youtube = YoutubeMP3::find($id);
        }else{
            $youtube = YoutubeHelper::download($id);
        }

        $path = storage_path($youtube->mp3_loc);
        if (File::exists($path)) {
            $timestamp = File::lastModified($path);
            $modified = Carbon::createFromTimestampUTC($timestamp)->toDayDateTimeString();
            $length = File::size($path);
            $hash = File::hash($path);
            $file = File::get($path);
            $type = File::mimeType($path);

            /**
             * @var ResponseFactory $file_response
             */
            $file_response = Response::make($file, 200);
            $file_response->header("Content-Type", $type);
            $file_response->header("Content-Length", $length);
            $file_response->header("Content-MD5", $hash);
            $file_response->header("Last-Modified", $modified);
            $file_response->header("X-Song-Name", $youtube->title);
            $referer = $request->header('referer');
            if (starts_with($referer, 'https://yw-web.yoworld.com/')) {
                $file_response->header("Content-Disposition", sprintf('attachment; filename="%s.mp3"', $youtube->title));
            } else {
                $should_download = $request->input('dl', false);
                if (is_true($should_download)) {
                    $file_response->header("Content-Disposition", sprintf('attachment; filename="%s.mp3"', $youtube->title));
                } else {
                    $file_response->header("Content-Disposition", sprintf('filename="%s.mp3"', $youtube->title));
                }
            }
            return $file_response;
        } else {
            return $this->json_error('Song does not exist on this server');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
