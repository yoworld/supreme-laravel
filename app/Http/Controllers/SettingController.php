<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\SettingResource;
use App\Models\User;
use App\Models\UserSetting;
use App\Models\YovilleAuth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return SettingResource|Response
     */
    public function store(Request $request)
    {

        /**
         * @var User $auth
         */
        $auth = Auth::user();
        $settings = $auth->settings;
        if(is_null($settings)){
            $settings = new UserSetting(['id' => $auth->id]);
            $settings->save();
        }
        $data = json_decode($request->has('data') ? $request->input('data') : $request->getContent(), true);
        if (is_array($data)) {
            $auth_settings = Collection::wrap($settings->toArray())->except(['id', 'created_at', 'updated_at'])->toArray();
            foreach ($data as $key => $value) {
                if (array_has($auth_settings, $key)) {
                    if ($auth->isUser() || $auth->isModerator()) {
                        if ($key != "canfreeze" &&
                            $key != "canttt" &&
                            $key != "cancorruptgb"
                        ) {
                            $auth_settings[$key] = $value;
                        }
                    } else {
                        $auth_settings[$key] = $value;
                    }
                }
            }
            $updated = $settings->update($auth_settings);
            return is_true($request->input('decode', false)) ? new SettingResource($auth_settings, $updated ? "Values set!" : "Values not set") : response(Controller::bitEncode($this->json_response($auth_settings, $updated ? "Values set!" : "Values not set")->getData(true), 1), 200);
        } else {
            return $this->json_error("This is not a valid json bitch");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return SettingResource|Response
     */
    public function show(Request $request, $id)
    {
        //s1.supreme-one.net:2052
        $settings = UserSetting::find($id);
        return is_true($request->input('decode', false)) ? new SettingResource($settings, "Retrieving Settings") : response(Controller::bitEncode($this->json_response($settings, "Retrieving Settings")->getData(true), 1), 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return SettingResource|Response
     */
    public function update(Request $request, $id)
    {

        /**
         * @var UserSetting $setting
         */
        $setting = UserSetting::find($id);
        if(is_null($setting)){
            $setting = new UserSetting(['id' => $id]);
            $setting->save();
        }
        /**
         * @var User $auth
         */
        $auth = Auth::user();
        $data = json_decode($request->has('data') ? $request->input('data') : $request->getContent(), true);
        if (is_array($data)) {
            $auth_settings = Collection::wrap($setting->toArray())->except(['id', 'created_at', 'updated_at'])->toArray();
            foreach ($data as $key => $value) {
                if (array_has($auth_settings, $key)) {
                    if ($auth->isUser() || $auth->isModerator()) {
                        if ($key != "canfreeze" &&
                            $key != "canttt" &&
                            $key != "cancorruptgb"
                        ) {
                            $auth_settings[$key] = $value;
                        }
                    } else {
                        $auth_settings[$key] = $value;
                    }
                }
            }
            $updated = $setting->update($auth_settings);
            return is_true($request->input('decode', false)) ? new SettingResource($auth_settings, $updated ? "Values set!" : "Values not set") : response(Controller::bitEncode($this->json_response($auth_settings, $updated ? "Values set!" : "Values not set")->getData(true), 1), 200);
        } else {
            return $this->json_error("This is not a valid json bitch");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
