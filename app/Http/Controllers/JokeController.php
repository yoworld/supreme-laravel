<?php

namespace App\Http\Controllers;

use App\Models\YovilleDadJoke;
use App\Models\YovilleYomamaJoke;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

define("MAX_MSG_SIZE", 146);

class JokeController extends Controller
{

    public $client = null;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function get_random_dad_joke(Request $request)
    {
        /* $response = $this->client->get('https://icanhazdadjoke.com/', ['headers' => [
                 "Accept" => "application/json"
             ]]);
             $body = $response->getBody();
             $json = json_decode($body->getContents()); */
        /**
         * If joke is too large for Chat limit, find another.
         */
        while (strlen($joke = YovilleDadJoke::query()->inRandomOrder()->first()->Joke) > MAX_MSG_SIZE) ;
        return $this->json_response(sprintf('%s - %s', $joke, 'Supreme1Bot'), 'Dad Joke');
      //  return $this->json_response(sprintf('%s - %s', $joke, 'Supreme1Bot'), 'Dad Joke');
    }

    public function get_random_cat_fact(Request $request)
    {
        $response = $this->client->get('https://catfact.ninja/fact', ['headers' => [
            "Accept" => "application/json"
        ]]);
        $body = $response->getBody();
        $json = json_decode($body->getContents());
        return $this->json_response(sprintf('%s - %s', $json->fact, 'Supreme1Bot'), 'Cat Fact');
    }

    public function get_number_fact(Request $request, $number = 'random')
    {
        $url = "http://numbersapi.com/" . (is_numeric($number) ? $number : 'random') . "/trivia?json";
        $response = $this->client->get($url, ['headers' => [
            "Accept" => "application/json"
        ]]);
        $body = $response->getBody();
        $json = json_decode($body->getContents());
        return $this->json_response(sprintf('%s - %s', $json->text, 'Supreme1Bot'), 'Number Fact');
    }

    public function get_yomama_joke(Request $request)
    {
        while (strlen($joke = YovilleYomamaJoke::query()->inRandomOrder()->first()->joke) > MAX_MSG_SIZE) ;
        return $this->json_response(sprintf('%s - %s', $joke, 'Supreme1Bot'), 'Yomama Joke');
    }

    public function get_bible_joke(Request $request)
    {
        return $this->json_response('Not Implemented', 'Bible Joke');
    }
}
