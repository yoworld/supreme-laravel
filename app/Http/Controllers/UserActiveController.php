<?php

namespace App\Http\Controllers;

use App\Http\Resources\AccountCollection;
use App\Models\User;
use Illuminate\Http\Request;

class UserActiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param User $user
     * @return AccountCollection
     */
    public function index(Request $request, User $user)
    {
        $active_accounts = $user->getActiveAccounts()->makeVisible(['api_token']);
        if ($active_accounts->count() > 0) {
            return new AccountCollection($active_accounts);
        }else{
            return $this->json_error("No Active Users", "There were no active users");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
