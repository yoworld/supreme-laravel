<?php

namespace App\Http\Controllers;

use Alaouy\Youtube\Facades\Youtube;
use App\Models\YoutubeVideo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class YoutubeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $query = $request->input('query', null);
        if ($query != null) {
            /**
             * @var Youtube
             */
            $part = ['id', 'snippet'];
            $videoList = Youtube::searchVideos($query, 50, null, $part);
            $youtube_obj = [];
            foreach ($videoList as $key => $value) {
                $obj = (object)$value;
                $yt['video_id'] = $obj->id->videoId;
                $yt['channel_id'] = $obj->snippet->channelId;
                $yt['title'] = $obj->snippet->title;
                $yt['description'] = $obj->snippet->description;
                $yt['channel'] = $obj->snippet->channelTitle;
                $yt['etag'] = $obj->etag;
                $yt['thumbnail'] = $obj->snippet->thumbnails->high->url;
                $yt['publish_at'] = $obj->snippet->publishedAt;

                $youtube_obj[] = YoutubeVideo::query()->updateOrCreate(array_only($yt, 'video_id'), array_except($yt, 'video_id'));
            }
        }

            /**
             * SELECT
             *,
             * MATCH(title, description, channel) AGAINST('+lil >gnar >juice' IN BOOLEAN MODE) AS relevance
             * FROM youtube_video
             * WHERE
             * MATCH(title, description, channel) AGAINST('+lil >gnar >juice' IN BOOLEAN MODE)
             * ORDER BY relevance DESC;
             */
            $title = scrub($request->input('title', $query));
            if($title == null) return $this->json_response(YoutubeVideo::query()->paginate(100));
            $titles = explode(' ', $title);
            foreach ($titles as $key => $value) {
                if ($key == 0) {
                    if(count($titles) == 1){
                        $titles[$key] = sprintf("+%s* >%s*", $value, $value);
                    }else {
                        $titles[$key] = sprintf("+%s*", $value);
                    }
                }
                else {
                    $titles[$key] = sprintf(">%s*", $value);
                }
            }
            $title = implode(' ', $titles);
            $youtube_query = YoutubeVideo::query()
                ->selectRaw(DB::raw(sprintf("*, MATCH(title, description, channel) AGAINST('%s' IN BOOLEAN MODE) AS relevance", $title)))
                ->where(DB::raw(sprintf("MATCH(title, description, channel) AGAINST('%s' IN BOOLEAN MODE)", $title)),'>', 0.20)
                ->orderByDesc('relevance')
                ->orderByDesc('publish_at');

            $youtube = $youtube_query->paginate($request->input('per', 100));
            $youtube_info = array_except(json_decode(json_encode($youtube), true), ['data']);
            return $this->json_response($youtube->items(), "RESPONSE", true, 200, $youtube_info);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return $this->json_response(YoutubeVideo::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
