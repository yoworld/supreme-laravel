<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;




    public static $PASS_SALT = '~~m.u,n.c.h.i.y.e^%$#';
    public static $SERVER_SALT = '77ca5141f4a1a415a3ddd7297b8f05f8'; //MD5 hash of !)(#MuNcHiYe18#)(!

    const INTERNAL_API_KEY = "c216641120ea1d85adfcea17bb17cfd2";
    const INTERNAL_API_KEY_LOWER = "c216641120ea1d85adfcea17bb17cfd2";
    const API_KEY = "ILoveSam4711CJ/062495#Sam/011896";


    const ENCRYPT_KEYPHRASE = "m.u.n.c.h.i.y.e~samanatha+cj";
    const IV_KEY = "^#Munchiye.com#^";
    const ENCRYPT_KEYPHRASE_SECOND_LAYER = "Munchiye(Eat*Sleep^Conquer#Sam@Cj!!!4711*Charlton.Smith)!";
    const API_KEY_ECB = "AES-256-ECB";
    const API_KEY_OFB = "AES-256-OFB";


    public static function json_response_force($data, $message = "RESPONSE", $status = true, $code = 200)
    {
        return response()->json(["status" => $status, "message" => $message, "data" => $data], $code);
    }

    public static function json_error_force($error = "ERROR", $message = "RESPONSE", $status = false, $code = 500)
    {
        return response()->json(["status" => $status, "message" => $message, "error" => $error], $code);
    }

    protected function json_response($data, $message = "RESPONSE", $status = true, $code = 200, $extra = null)
    {
        return response()->json(["status" => $status, "message" => $message, "data" => $data, "extra"=> $extra], $code);
    }

    protected function json_error($error = "ERROR", $message = "RESPONSE", $status = false, $code = 500)
    {
        return response()->json(["status" => $status, "message" => $message, "error" => $error], $code);
    }


    public function create_token($message)
    {
        $value = openssl_encrypt($message, Controller::API_KEY_ECB, Controller::ENCRYPT_KEYPHRASE);
        $value2 = openssl_encrypt($value, Controller::API_KEY_OFB, Controller::ENCRYPT_KEYPHRASE_SECOND_LAYER, 0, Controller::IV_KEY);
        return $value2;
    }

    public static function create_token_force($message)
    {
        $value = openssl_encrypt($message, Controller::API_KEY_ECB, Controller::ENCRYPT_KEYPHRASE);
        $value2 = openssl_encrypt($value, Controller::API_KEY_OFB, Controller::ENCRYPT_KEYPHRASE_SECOND_LAYER, 0, Controller::IV_KEY);
        return $value2;
    }

    public static function create_token_custom($message, $keyphrase, $ivkey)
    {
        $value2 = openssl_encrypt($message, Controller::API_KEY_OFB, $keyphrase, 0, $ivkey);
        return $value2;
    }

    public static function decode_token_custom($message, $keyphrase, $ivkey)
    {
        $array = openssl_decrypt($message, Controller::API_KEY_OFB, $keyphrase, OPENSSL_NO_PADDING, $ivkey);
        return $array;
    }

    public static function decode64($message)
    {
        return strrev(base64_decode(strrev($message)));
    }

    public static function encode64($message)
    {
        return strrev(base64_encode(strrev($message)));
    }


    public static function bitEncode($message, int $shift = -1)
    {
        if(is_array($message) || is_object($message)){
            $message = json_encode($message);
        }else {
            $message = strval($message);
        }
        $encoded_data = "";
        for ($i = 0; $i < strlen($message); $i++) {
            $encoded_data .= chr(ord($message[$i]) ^ $shift);
        }
        return $encoded_data;
    }


}
