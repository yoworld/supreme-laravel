<?php

namespace App\Http\Controllers;

use App\Http\Resources\PermissionCollection;
use App\Http\Resources\PermissionResource;
use App\Models\Permission;
use App\Models\Versioning;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param $id
     * @return PermissionCollection
     */
    public function index(Request $request, $id)
    {
        return new PermissionCollection(Permission::query()->where('user_id', $id)->paginate(), 'User Permissions');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return PermissionResource
     */
    public function store(Request $request, $id)
    {
        /**
         * @var Versioning $version
         */
        $version = Versioning::query()->findOrFail($request->input('mod_hash'));
        $data = [
            'user_id' => $id,
            'mod_hash' => $request->input('mod_hash'),
            'permission' => 'false',
            'status' => 'PENDING',
            'message' => 'Please wait for admins to review'
        ];
        try {
            $permission = Permission::query()->create($data);
            return new PermissionResource($permission, sprintf('You have applied for %s', $version->codename));
        } catch (\PDOException $e) {
            return $this->json_error("You already applied");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return PermissionResource|Response
     */
    public function show($id, $hash)
    {
        try {
            $permission = Permission::check($id, $hash)->firstOrFail();
            return new PermissionResource($permission);
        } catch (\Exception $e) {
            return $this->json_error("Does not exist");
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $hash)
    {
        if(!Auth::user()->isAdmin())
            return $this->json_error("Permission Denied");
        try {
            $permission = Permission::check($id, $hash)->firstOrFail();
            $permission->update(
                [
                    'permission' => $request->input('permission'),
                    'message' => $request->input('message'),
                    'status' => is_true($request->input('permission')) ? 'APPROVED' : 'DENIED'
                ]
            );
            return $this->json_response($permission);
        } catch (\Exception $e) {
            return $this->json_error("Does not exist");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $hash)
    {
        if(!Auth::user()->isAdmin())
            return $this->json_error("Permission Denied");
        try {
            $permission = Permission::check($id, $hash)->firstOrFail();
            $delete = $permission->delete();
            return $this->json_response($delete, "Resource Deleted");
        } catch (\Exception $e) {
            return $this->json_error("Does not exist");
        }
    }
}
