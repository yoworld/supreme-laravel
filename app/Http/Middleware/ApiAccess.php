<?php

namespace App\Http\Middleware;

use App\Models\GeorgeOrwell;
use App\Models\ModTechDig;
use App\Models\User;
use App\Models\Yoville;
use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class ApiAccess
{


    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle($request, Closure $next)
    {

        $query = User::query()
            ->where('user_ip', $request->ip())
            ->where('user_banned', 0)
            ->orWhere('user_ipv6', $request->ip())
            ->where('user_banned', 0);

        /**
         * @var User $user
         */
        $user = $query->first();

        if (($user != null && !$user->user_banned) ||
            ($request->hasHeader('bypass') && $request->header('bypass') == 1) ||
            is_swf_paths($request)) {
            if (is_swf_paths($request)) {
                $swf_version = get_swf_version($request);
                if ($user != null && !$user->user_banned) {
                    Auth::login($user, true);
                    $user->mod_hash = $swf_version;
                    $user->save();
                    return $next($request)
                        ->header('X-Supreme-User-Name', $user->name)
                        ->header('X-Supreme-User-Id', $user->id)
                        ->header('X-Supreme-User-Email', $user->email)
                        ->header('X-Supreme-Status', 'Supreme API\'s Functional.')
                        ->header("X-Supreme-Auth", "Authorized")
                        ->header("X-Supreme-Message", "Welcome to Supreme1")
                        ->header("X-Supreme-Version", $swf_version)
                        ->header('X-Supreme-Tech-Dig-Applied', ModTechDig::modHashExist($swf_version));
                } else {
                    $path = storage_path('app/YoVilleApp.swf');
                    $file = File::get($path);
                    $type = File::mimeType($path);
                    $response = \Illuminate\Support\Facades\Response::make($file, 200);
                    $response->header("Content-Type", $type);
                    $response->header("X-Supreme-Auth", "Not Authorized");
                    $response->header("X-Supreme-Status", "Not Functional");
                    $response->header("X-Supreme-Message", "You're a little bitch. I hope you know that.");
                    Yoville::query()->create(['data' => ['request_ip' => $request->ip(), 'headers' => $request->headers->all(), 'cookies' => $request->cookies->all(), 'query' => $request->query->all(), 'inputs' => $request->input()]]);
                    return $response;
                }
            } else if (!$request->hasHeader('bypass')) {
                Auth::login($user, true);
                return $next($request)
                    ->header('X-Supreme-User-Name', $user->name)
                    ->header('X-Supreme-User-Id', $user->id)
                    ->header('X-Supreme-User-Email', $user->email)
                    ->header('X-Supreme-Status', 'Supreme API\'s Functional.')
                    ->header("X-Supreme-Auth", "Authorized")
                    ->header("X-Supreme-Message", "Welcome to the dark side.");
            } else {
                Auth::login($user, true);
                return $next($request)->header('X-Supreme-Bypass-Enabled', 'true')->header('X-Supreme-Authorize-Message', 'Be careful you are accessing the api in bypass mode.');
            }
        } else {
            return new Response(view('error', GeorgeOrwell::quote())); //Controller::json_error_force("Logged.",GeorgeOrwell::quote(),false, 401);
        }
    }
}
