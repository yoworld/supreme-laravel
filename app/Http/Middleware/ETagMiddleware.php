<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class ETagMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if ($response->getStatusCode() !== 200 || ! $request->expectsJson()) {
            return $response;
        }
        if ($request->method() === 'GET' || $request->method() === 'HEAD') {
            /**
             * @var Response $response
             */
            $response->setEtag(md5($response->getContent()),false);
            $etag = $response->getEtag();
            $ifMatch = $request->header('If-Match');
            $ifNotMatch = $request->header('If-Not-Match');
            if ( ! is_null($ifMatch) ) {
                $etagList = explode(',', $ifMatch);
                if ( ! in_array($etag, $etagList) && ! in_array('*', $etagList) ) {
                    return response()->json([
                        'error' => [
                            'http_code' => 412,
                            'code' => 'PRECONDITION_FAILED',
                            'message' => 'Precondition failed.'
                        ]
                    ], 412);
                }
            } else if ( ! is_null($ifNotMatch) ) {
                $etagList = explode(',', $ifNotMatch);
                if ( in_array($etag, $etagList) || in_array('*', $etagList) ) {
                    return response()->json(null, 304);
                }
            }

            $response->header("pragma", "private");
            $response->header("Cache-Control", " private, max-age=86400");
        }
        return $response;
    }
}
