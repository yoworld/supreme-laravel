<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    /**
     * @var string
     */
    private $message;
    /**
     * @var int
     */
    private $code;

    /**
     * UserCollection constructor.
     * @param $resource
     * @param string $message
     * @param int $code
     */
    public function __construct($resource, string $message = "User Accounts", int $code = 200)
    {
        parent::__construct($resource);
        $this->message = $message;
        $this->code = $code;
    }

    public function with($request)
    {
        return ['status' => true, 'message' => $this->message, 'code' => $this->code];
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function($item){
            return new UserResource($item, $this->message, $this->code);
        })->toArray();
    }

}
