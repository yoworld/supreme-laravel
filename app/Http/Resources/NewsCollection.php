<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class NewsCollection extends ResourceCollection
{



    /**
     * @var string
     */
    private $message;
    /**
     * @var int
     */
    private $code;

    public function __construct($resource, string $message = "S1 News", int $code = 200)
    {
        parent::__construct($resource);
        $this->message = $message;
        $this->code = $code;
    }

    public function with($request)
    {
        return [
            'status' => true,
            'code' => $this->code,
            'message' => $this->message
        ];
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($item){
            return new NewsResource($item);
        })->toArray();
    }

}
