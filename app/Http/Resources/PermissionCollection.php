<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PermissionCollection extends ResourceCollection
{

    /**
     * @var string
     */
    private $message;
    /**
     * @var int
     */
    private $code;

    public function __construct($resource, $message = "Permissions", $code = 200)
    {
        parent::__construct($resource);
        $this->message = $message;
        $this->code = $code;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($item) {
            return new PermissionResource($item, $this->message, $this->code);
        })->toArray();
    }


    public function with($request)
    {
        return ['status' => true, 'code' => $this->code, 'message' => $this->message];
    }
}
