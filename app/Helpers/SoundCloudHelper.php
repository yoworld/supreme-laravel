<?php
/**
 * Created by PhpStorm.
 * User: Charlton
 * Date: 2/20/18
 * Time: 8:59 PM
 */

namespace App\Helpers;

use App\Models\SoundCloud;
use Carbon\Carbon;
use Exception;
use FFMpeg\FFMpeg;
use FFMpeg\Format\Audio\Mp3;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class SoundCloudHelper
{
    //https://soundcloud.com/xxx-bad-vibes-forever/s4dd
    const RESOLVE_URL = "https://api.soundcloud.com/resolve.json?url=%s&client_id=%s";
    const DOWNLOAD_URL = "https://api.soundcloud.com/i1/tracks/%s/streams?client_id=%s";
    const TRACK_SEARCH_URL = "https://api.soundcloud.com/tracks?q=%s&client_id=%s";
    const PLAYLIST_URL = "https://api.soundcloud.com/playlists/%s?client_id=%s";

    const SOUNDCLOUD_MP3_PATH = "soundcloud/%s/%s.mp3";
    const SOUNDCLOUD_MP3_TEMP_PATH = "soundcloud/%s/%s-temp.mp3";
    const SOUNDCLOUD_M3U8_TEMP_PATH = "soundcloud/%s/%s-temp.m3u8";
    const SOUNDCLOUD_JPG_PATH = "soundcloud/%s/%s.jpg";

    const V2_TRACK_INFO = "https://api-v2.soundcloud.com/tracks/%s?client_id=%s";


    /**
     * @param $search
     * @param Client $client
     * @return mixed
     */
    public static function search($search, Client $client)
    {
        $url = sprintf(self::TRACK_SEARCH_URL, $search, env('SC_CLIENT_ID'));
        $body = self::request($url, $client);
        $collection = new Collection();
        foreach ($body as $b) {
            $collection->add(SoundCloud::query()->updateOrCreate(['track_id' => $b->id],
                [
                    'username' => $b->user->username,
                    'artist' => $b->user->username,
                    'title' => $b->title,
                    'description' => $b->description,
                    'genre' => $b->genre,
                    'link' => $b->permalink_url,
                    'jpg_link' => $artwork = str_replace('-large.jpg', "-t500x500.jpg", $b->artwork_url ?? $b->user->avatar_url)
                ]
            ));
        }
        return $collection;
    }


    /**
     * @param $url
     * @param Client $client
     * @return mixed
     */
    public static function resolve($url, Client $client)
    {
        if (!self::is_url($url)) {
            $url = "https://soundcloud.com/" . $url;
        }
        $resolve_url = sprintf(SoundCloudHelper::RESOLVE_URL, $url, env('SC_CLIENT_ID'));
        return self::request($resolve_url, $client);
    }


    /**
     * @param $id
     * @param Client $client
     * @return mixed
     */
    public static function getTrackInfo($id, Client $client)
    {
        $resolve_url = sprintf(SoundCloudHelper::V2_TRACK_INFO, $id, env('SC_CLIENT_ID'));
        return self::request($resolve_url, $client);
    }

    /**
     * @param $url
     * @param Client $client
     * @return mixed
     */
    public static function request($url, Client $client)
    {
        $mp3_json_response = $client->get($url);
        return \GuzzleHttp\json_decode($mp3_json_response->getBody(), false);
    }

    /**
     * @param $url
     * @param Client $client
     * @return Builder|Model
     * @throws Exception
     */
    public static function download($url, Client $client)
    {
        try {
            if (is_numeric($url)) {
                $body = self::getTrackInfo($url, $client);
            } else {
                $resolved = self::resolve($url, $client);
                $body = self::getTrackInfo($resolved->id, $client);
            }
        } catch (ClientException $e) {
            $soundcloud = SoundCloud::findDownload($url);
            if (!is_null($soundcloud)) {
                $deleted = $soundcloud->delete();
            }
            throw new Exception("Soundcloud song does not exist");
        }
        //region GET DATA
        $url = $body->permalink_url;
        $track_id = $body->id;
        $username = $body->user->username;
        $artist = clean($body->publisher_metadata->artist ?? $username);
        $genre = strlen($body->genre) == 0 ? "music" : clean($body->genre);
        $title = ucwords(clean($body->title));
        $description = $body->description;
        $created_at = $body->created_at;
        $release_date = $body->release_date ?? $created_at;
        $carbon = Carbon::createFromTimestamp(strtotime($release_date));
        $year = $carbon->year;
        $artwork = str_replace('-large.jpg', "-t500x500.jpg", $body->artwork_url ?? $body->user->avatar_url);
        //endregion

        if (!property_exists($body->media, 'transcodings')) {
            throw new Exception("This track has no transcodings");
        }

        $real_transcoding = self::getValidTranscodings($body->media->transcodings);

        $mp3_download_json_link = sprintf('%s?client_id=%s', $real_transcoding->url, env('SC_CLIENT_ID'));
        $mp3_download_json = self::request($mp3_download_json_link, $client);

        $mp3_download_link = $mp3_download_json->url;
        list($isPlaylist, $temp_template_path) = self::get_temp_url($mp3_download_link);

        $temp_mp3_path = sprintf($temp_template_path, $artist, $title);
        $jpg_path = sprintf(SoundCloudHelper::SOUNDCLOUD_JPG_PATH, $artist, $title);

        $mp3_response = $client->get($mp3_download_link);
        self::store($temp_mp3_path, $mp3_response->getBody());

        $jpg_response = $client->get($artwork);
        self::store($jpg_path, $jpg_response->getBody());

        $ffmpeg = FFMpeg::create(array(
            'ffmpeg.binaries' => '/usr/local/bin/ffmpeg',
            'ffprobe.binaries' => '/usr/local/bin/ffprobe',
            'timeout' => 3600, // The timeout for the underlying process
            'ffmpeg.threads' => 12,   // The number of threads that FFMpeg should use
        ));
        $temp_audio = storage_path('app/' . $temp_mp3_path);
        if ($isPlaylist) {
            $temp_audio = self::handle_hls($temp_audio, storage_path('app/' . sprintf(self::SOUNDCLOUD_MP3_TEMP_PATH, $artist, $title)));
        }
        $mp3 = $ffmpeg->open($temp_audio);
        $mp3->filters()->addMetadata(
            [
                'title' => $title,
                'artwork' => storage_path('app/' . sprintf(SoundCloudHelper::SOUNDCLOUD_JPG_PATH, $artist, $title)),
                'genre' => $genre,
                'description' => $description,
                'artist' => $artist
            ]);
        $audio_format = new Mp3();
        $mp3->save($audio_format, storage_path('app/' . sprintf(SoundCloudHelper::SOUNDCLOUD_MP3_PATH, $artist, $title)));
        self::delete($temp_audio);
        $soundcloud = SoundCloud::query()->updateOrCreate(['track_id' => $track_id]
            , [
                'title' => $title,
                'artist' => $artist,
                'description' => $description,
                'username' => $username,
                'genre' => $genre,
                'link' => $url,
                'jpg_link' => $artwork,
                'mp3_loc' => sprintf('app/' . SoundCloudHelper::SOUNDCLOUD_MP3_PATH, $artist, $title),
                'jpg_loc' => sprintf('app/' . SoundCloudHelper::SOUNDCLOUD_JPG_PATH, $artist, $title)
            ]);
        return $soundcloud;
    }

    /**
     * @param $url
     * @return bool
     */
    public static function is_url($url)
    {
        return filter_var($url, FILTER_VALIDATE_URL);
    }

    /**
     * @param $path
     * @param $body
     * @param string $disk
     * @return bool
     */
    private static function store($path, $body, $disk = 'local')
    {
        return Storage::disk('local')->put($path, $body);
    }

    /**
     * @param $filename
     * @return bool
     */
    private static function delete($filename)
    {
        if (File::exists($filename)) {
            return File::delete($filename);
        }
        return false;
    }

    /**
     * @param $hls_path
     * @param $mp3_path
     * @return mixed
     */
    private static function handle_hls($hls_path, $mp3_path)
    {
        $cli = "/usr/local/bin/ffmpeg -protocol_whitelist file,http,https,tcp,tls -i \"{$hls_path}\" -c copy \"{$mp3_path}\"";
        exec($cli);
        self::delete($hls_path);
        return $mp3_path;
    }

    /**
     * @param $url
     * @return array
     */
    private static function get_temp_url($url)
    {
        if (Str::contains($url, 'playlist.m3u8')) {
            $isPlaylist = true;
            $temp_template_path = self::SOUNDCLOUD_M3U8_TEMP_PATH;
        } else {
            $isPlaylist = false;
            $temp_template_path = self::SOUNDCLOUD_MP3_TEMP_PATH;
        }
        return [$isPlaylist, $temp_template_path];
    }

    /**
     * @param array $transcodings
     * @return mixed
     * @throws Exception
     */
    private static function getValidTranscodings($transcodings)
    {
        if (count($transcodings) > 0) {
            $progressive = null;
            $hls = null;
            foreach ($transcodings as $transcoding) {
                if ($transcoding->format->mime_type === 'audio/mpeg') {
                    if ($transcoding->format->protocol === 'progressive') {
                        $progressive = $transcoding;
                    }
                    if ($transcoding->format->protocol === 'hls') {
                        $hls = $transcoding;
                    }
                }
            }
            if (is_null($progressive) && is_null($hls)) {
                throw new Exception("Unable to get valid transcoding");
            } else {
                return $progressive ?? $hls;
            }
        } else {
            throw new Exception("There are no transcodings");
        }
    }

}