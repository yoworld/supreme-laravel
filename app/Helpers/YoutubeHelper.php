<?php
/**
 * Created by PhpStorm.
 * User: Charlton
 * Date: 3/5/18
 * Time: 4:06 AM
 */

namespace App\Helpers;


use App\Http\Controllers\Controller;
use App\Models\YoutubeMP3;
use App\Models\YoutubeVideo;
use DOMDocument;
use Exception;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use FFMpeg\Format\Audio\Mp3;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use YoutubeDl\YoutubeDl;
use YouTubeDownloader;

class YoutubeHelper
{

    private $dl;

    public function __construct()
    {
        $this->dl = new YoutubeDl([
            'extract-audio' => true,
            'audio-format' => 'mp3',
            'audio-quality' => 0,
            'output' => '%(title)s.%(ext)s',
        ]);
        $this->dl->setBinPath('/usr/local/bin/youtube-dl');
        $this->dl->setDownloadPath(storage_path('app/mp3'));
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function download_mp3($id){
        $video = $this->dl->download("https://www.youtube.com/watch?v={$id}");
        $title = $video->getTitle();
        $id = $video->getId();
        $name = $video->getFilename();
        return compact('title','name', 'id');
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public static function yt_download($id)
    {
        $youtube = new YoutubeHelper();
        return $youtube->download_mp3($id);
    }

    public static function download($id)
    {


        return self::yt_download($id);

        $url = sprintf("https://www.youtube.com/watch?v=%s", $id);

        if (YoutubeVideo::doesExists($id)) {
            /**
             * @var YoutubeVideo $video
             */
            $video = YoutubeVideo::find($id);

            $title = clean(str_replace(' - YouTube', '', $video->title));
            $local_mp3_path = storage_path(sprintf('app/youtube/%s/%s.mp3', $id, $title));
            $local_mp4_path = storage_path(sprintf('app/youtube/%s/%s.mp4', $id, $title));
            $local_jpg_path = storage_path(sprintf('app/youtube/%s/%s.jpg', $id, $title));

            $mp4_path = sprintf('youtube/%s/%s.mp4', $id, $title);
            $mp3_path = sprintf('youtube/%s/%s.mp3', $id, $title);
            $jpg_path = sprintf('youtube/%s/%s.jpg', $id, $title);
        } else {
            $doc = new DOMDocument(1.0, 'utf-8');
            $html = @$doc->loadHTMLFile($url);
            $mp3_path = sprintf('youtube/%s/%s.mp3', $id, $id);
            $mp4_path = sprintf('youtube/%s/%s.mp4', $id, $id);
            $jpg_path = sprintf('youtube/%s/%s.jpg', $id, $id);
            $local_mp3_path = storage_path(sprintf('app/youtube/%s/%s.mp3', $id, $id));
            $local_mp4_path = storage_path(sprintf('app/youtube/%s/%s.mp4', $id, $id));
            $local_jpg_path = storage_path(sprintf('app/youtube/%s/%s.jpg', $id, $id));
            $title = $id;
            if ($html) {
                $list = $doc->getElementsByTagName("title");
                if ($list->length > 0) {
                    //T8AY7d2Z5Eo
                    $title = clean(str_replace(' - YouTube', '', $list->item(0)->textContent));
                    $local_mp3_path = storage_path(sprintf('app/youtube/%s/%s.mp3', $id, $title));
                    $local_mp4_path = storage_path(sprintf('app/youtube/%s/%s.mp4', $id, $title));
                    $local_jpg_path = storage_path(sprintf('app/youtube/%s/%s.jpg', $id, $title));

                    $mp4_path = sprintf('youtube/%s/%s.mp4', $id, $title);
                    $mp3_path = sprintf('youtube/%s/%s.mp3', $id, $title);
                    $jpg_path = sprintf('youtube/%s/%s.jpg', $id, $title);
                }
            } else {
                return Controller::json_error_force("FAILED", ['stupid' => $html, 'title' => $title]);
            }
        }
        $yt = new YouTubeDownloader();
        $links = $yt->getDownloadLinks($url);
        if ($links == false) {
            return Controller::json_error_force("Unable to find song");
        }
        $links = array_values($links);
        $mp4_720 = null;
        $mp4_360 = null;
        foreach ($links as $key => $value) {
            if ($value['format'] == "MP4 720p (HD)") {
                $mp4_720 = $value['url'];
            } else if ($value['format'] == "MP4 360p") {
                $mp4_360 = $value['url'];
            }
        }

        if ($mp4_720 == null && $mp4_360 == null) {
            return Controller::json_error_force("No Video Found");
        }
        $yt_url = $mp4_720 != null ? $mp4_720 : $mp4_360;

        $client = new Client();
        $response = $client->get($yt_url);
        Storage::disk('local')->put($mp4_path, $response->getBody());

        $ffmpeg = FFMpeg::create(array(
            'ffmpeg.binaries' => '/usr/local/bin/ffmpeg',
            'ffprobe.binaries' => '/usr/local/bin/ffprobe',
            'timeout' => 3600, // The timeout for the underlying process
            'ffmpeg.threads' => 12,   // The number of threads that FFMpeg should use
        ));
        $temp_audio = storage_path(sprintf('app/youtube/%s/%s-temp.mp3', $id, $title));
        $video = $ffmpeg->open($local_mp4_path);
        $audio_format = new Mp3();
        $video->save($audio_format, $temp_audio);
        $frame = $video->frame(TimeCode::fromSeconds(3));
        $frame->save($local_jpg_path);

        $mp3 = $ffmpeg->open($temp_audio);
        $mp3->filters()->addMetadata(['title' => $title, 'artwork' => $local_jpg_path]);
        $mp3->save($audio_format, $local_mp3_path);


        if (!File::delete($temp_audio)) {
            return Controller::json_error_force("Unable to delete file");
        }


        /**
         * @var YoutubeMP3 $youtube
         */
        $youtube = YoutubeMP3::query()->updateOrCreate(['id' => $id], [
            'link' => $url,
            'mp4_loc' => sprintf('app/%s', $mp4_path),
            'mp3_loc' => sprintf('app/%s', $mp3_path),
            'jpg_loc' => sprintf('app/%s', $jpg_path),
            'mp4_md5_hash' => File::hash($local_mp4_path),
            'mp3_md5_hash' => File::hash($local_mp3_path),
            'jpg_md5_hash' => File::hash($local_jpg_path),
            'title' => $title
        ]);
        return $youtube;
    }
}