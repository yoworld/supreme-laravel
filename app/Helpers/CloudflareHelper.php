<?php
/**
 * Created by PhpStorm.
 * User: Charlton
 * Date: 1/15/18
 * Time: 12:33 AM
 */

if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
    $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
}

function is_swf_paths(\Illuminate\Http\Request $request)
{
    $query = explode('/', $request->path());
    $last = $query[count($query) - 1];
    if(str_contains($request->path(),'api/v1/version')){
        return $last != "version" && $request->method() == 'GET';
    }
    return false;
}

function get_swf_version(\Illuminate\Http\Request $request)
{
    $query = explode('/', $request->path());
    $last = $query[count($query) - 1];
    if(str_contains($request->path(),'api/v1/version')){
        if($last != "version" && $request->method() == 'GET'){
            return $last;
        }
    }
    return null;
}

//strip cleans
function clean($string)
{
    $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-_&$().]/', '', $string); // Removes special chars.
    $string = str_replace('_', ' ', $string);
    return $string;
}


function scrub($string)
{
    $string = preg_replace('/[^A-Za-z0-9\s]/', '', $string); // Removes special chars.
    $string = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $string)));
    return $string;
}


function get_avatar_url($id)
{
    $user_id = strval($id);
    $from = 0;
    $to = strlen($user_id) - 3;
    if ($to > -1) {
        $truncated = strrev(substr($user_id, $from, $to));
        /**
         * 000/000/000/id
         */
        $arr = [];
        for ($i = 0; $i < intval(strlen($truncated) / 3) + ((strlen($truncated) % 3) > 0 ? 1 : 0); $i++) {
            $string = "";
            for ($j = 0; $j < 3; $j++) {
                $position = ($i * 3) + $j;
                try {
                    $string .= $truncated[$position];
                } catch (Exception $e) {

                }
            }
            $arr[] = strrev($string);
        }
        switch (count($arr)) {
            case 3:
                if (strlen($arr[2]) == 2) {
                    $arr[2] = "0" . $arr[2];
                } else if (strlen($arr[2]) == 1) {
                    $arr[2] = "00" . $arr[2];
                }
                break;
            case 2:
                if (strlen($arr[1]) == 2) {
                    $arr[1] = "0" . $arr[1];
                } else if (strlen($arr[1]) == 1) {
                    $arr[1] = "00" . $arr[1];
                }
                $arr[] = "000";
                break;
            case 1:
                if (strlen($arr[0]) == 2) {
                    $arr[0] = "0" . $arr[0];
                } else if (strlen($arr[0]) == 1) {
                    $arr[0] = "00" . $arr[0];
                }
                $arr[] = "000";
                $arr[] = "000";
                break;
            case 0:
                $arr[] = "000";
                $arr[] = "000";
                $arr[] = "000";
                break;
        }
        $arr = array_reverse($arr);
        $calculated_string = implode('/', $arr);
        return sprintf("https://yw-web.yoworld.com/user/images/yo_avatars/%s/%s.png", $calculated_string, $id);
    } else {
        return sprintf("https://yw-web.yoworld.com/user/images/yo_avatars/000/000/000/%s.png", $id);
    }
}

function is_true($val, $return_null = false)
{
    $boolval = (is_string($val) ? filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : (bool)$val);
    return ($boolval === null && !$return_null ? false : $boolval);
}

