<?php

namespace App\Policies;

use App\Models\Permission;
use App\Models\User;
use App\Models\Versioning;
use Illuminate\Auth\Access\HandlesAuthorization;

class VersionPermission
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * @param User $user
     * @param Versioning $versioning
     * @return bool
     */
    public function show(User $user, Versioning $versioning){
        $permission = Permission::check($user->id, $versioning->md5)->first();
        if($permission != null){
            return $permission->permission;
        }else{
            return false;
        }
    }
}
