<?php

namespace App\Console\Commands;

use App\Helpers\SoundCloudHelper;
use App\Models\SoundCloud;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Console\Command;

class SouncloudProvision extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'soundcloud:provision';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Save JPG Link of soundcloud songs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client();
        SoundCloud::query()
            ->whereNull('jpg_link')
            ->get()->map(function (SoundCloud $item) use($client) {
                try {
                    $track_info = SoundCloudHelper::getTrackInfo($item->track_id, $client);
                    $item->jpg_link = str_replace("-large", '-t500x500', $track_info->artwork_url ?? $track_info->user->avatar_url);
                    $item->save();
                    $this->comment(sprintf('Saved: %s', $track_info->artwork_url ?? $track_info->user->avatar));
                    return $item;
                }catch (ClientException $e){
                    $item->delete();
                    return null;
                }
            })->filter(function($item){
                return !is_null($item);
            });
    }
}
