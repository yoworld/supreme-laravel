<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\YovilleAuth;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AuthStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks the last time supreme auth was called on the user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $result = YovilleAuth::query()
            ->where('user_active', true)
            ->whereNotBetween('updated_at', [Carbon::now()->subSeconds(65), Carbon::now()])
            ->update(['user_active'=> false]);
        $result2 = User::query()
            ->where('active', true)
            ->whereNotBetween('updated_at', [Carbon::now()->subSeconds(65), Carbon::now()])
            ->update(['active'=> false]);
        $this->info(sprintf("Number of records have been changed: %i, %i", $result, $result2));
    }
}
