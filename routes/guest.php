<?php
/**
 * Created by PhpStorm.
 * User: Charlton
 * Date: 3/3/18
 * Time: 11:21 AM
 */

use App\Http\Controllers\Controller;
use App\Http\Controllers\SupremeNewsController;
use App\Models\SoundCloud;
use App\Models\YoutubeMP3;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1'], function () {

    //TODO: Move these two API's to access before any publicized release
    Route::get('decode/{code}', function (Request $request, $code) {
        return response(Controller::decode64($code));
    });
    Route::get('encode/{code}', function (Request $request, $code) {
        return response(Controller::encode64($code));
    });
    Route::get('jpg/{id}', function (Request $request, $id) {
        $youtube = SoundCloud::getSoundCloud($id);
        if ($youtube != null) {
            if(!is_null($youtube->jpg_loc)) {
                $path = storage_path($youtube->jpg_loc);
                $timestamp = File::lastModified($path);
                $modified = Carbon::createFromTimestampUTC($timestamp)->toDayDateTimeString();
                $length = File::size($path);
                $hash = File::hash($path);
                $file = File::get($path);
                $type = File::mimeType($path);
                $response = Response::make($file, 200);
                $response->header("Content-Type", $type);
                $response->header("Content-Length", $length);
                $response->header("Content-MD5", $hash);
                $response->header("Last-Modified", $modified);
                $response->header("X-Song-Name", $youtube->title);
                return $response;
            }else{
                return redirect($youtube->jpg_link);
            }
        } else {
            return Controller::json_error_force("Image not found", "error", false, 404);
        }
    });
    Route::get('s-jpg/{id}', function (Request $request, $id) {
        $soundcloud = SoundCloud::getSoundCloud($id);
        if ($soundcloud != null) {
            $path = storage_path($soundcloud->jpg_loc);
            $timestamp = File::lastModified($path);
            $modified = Carbon::createFromTimestampUTC($timestamp)->toDayDateTimeString();
            $length = File::size($path);
            $hash = File::hash($path);
            $file = File::get($path);
            $type = File::mimeType($path);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            $response->header("Content-Length", $length);
            $response->header("Content-MD5", $hash);
            $response->header("Last-Modified", $modified);
            $response->header("X-Song-Name", $soundcloud->title);
            return $response;
        } else {
            return Controller::json_error_force("Image not found", "error", false, 404);
        }
    });
    Route::get('mp4/{id}', function (Request $request, $id) {
        $youtube = YoutubeMP3::find($id);
        if ($youtube != null) {
            $path = storage_path($youtube->mp4_loc);
            $timestamp = File::lastModified($path);
            $modified = Carbon::createFromTimestampUTC($timestamp)->toDayDateTimeString();
            $length = File::size($path);
            $hash = File::hash($path);
            $file = File::get($path);
            $type = File::mimeType($path);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            $response->header("Content-Length", $length);
            $response->header("Content-MD5", $hash);
            $response->header("Last-Modified", $modified);
            $response->header("X-Video-Name", $youtube->title);
            return $response;
        } else {
            return Controller::json_error_force("Video not found", "error", false, 404);
        }
    });
    Route::apiResource('mp3', class_basename(\App\Http\Controllers\SoundcloudController::class))->except('show');
    Route::match(['get', 'post'], 'mp3/{soundcloud_url}', [
        'uses' => 'SoundcloudController@show',
        'parameters' =>
            [
                'soundcloud' => 'soundcloud_url'
            ]

    ]);
    Route::apiResource('youtube', class_basename(\App\Http\Controllers\YoutubeController::class));
    Route::apiResource('soundcloud', class_basename(\App\Http\Controllers\SoundcloudController::class),
        ['parameters' =>
            [
                'soundcloud' => 'soundcloud_url'
            ]
        ]
    );

    Route::apiResource('news', class_basename(SupremeNewsController::class));
    Route::get('joke/dadjokes', 'JokeController@get_random_dad_joke');
    Route::get('joke/catfacts', 'JokeController@get_random_cat_fact');
    Route::get('joke/numberfacts/{number}', 'JokeController@get_number_fact');
    Route::get('joke/numberfacts', 'JokeController@get_number_fact');
    Route::get('joke/yomama', 'JokeController@get_yomama_joke');
    Route::get('joke/bible', 'JokeController@get_bible_joke');

    Route::apiResource('messages', 'Yoville\YovilleMessageController');

});