<?php

use App\Http\Controllers\Controller;
use App\Models\YovilleAuth;
use App\Models\Yoville;
use App\Models\YovilleSecrets;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

/**
 * Created by PhpStorm.
 * User: Charlton
 * Date: 2/23/18
 * Time: 5:58 PM
 */


/**
 * @see \App\Http\Controllers\Yoville\YovilleAuthController
 * @see \App\Http\Controllers\Yoville\YovilleCharacterController
 * @see \App\Http\Controllers\Yoville\SettingController
 * @see \App\Http\Controllers\Yoville\YovilleBuddyController
 * @see \App\Http\Controllers\Yoville\YovilleSupremeBuddyController
 * @see \App\Http\Controllers\Yoville\YovilleXPSController
 * @see \App\Http\Controllers\Yoville\YovilleController
 * @see \App\Http\Controllers\Yoville\YovilleRoomController
 * @see \App\Http\Controllers\Yoville\YovilleMessageController
 */
Route::group(['prefix' => 'v1', 'middleware' => ['auth:access']], function () {
    Route::apiResource('supreme.buddy', 'Yoville\YovilleSupremeBuddyController');
    Route::any('supreme/{id}/domination', function (Request $request, $id) {
        $auth = YovilleAuth::auth();
        if ($auth != null) {
            if (!$auth->user_banned && $auth->user->user_settings->canfreeze) {
                $data = YovilleSecrets::query()->where('id', 1)->first();
                return response(Controller::bitEncode($data->data['walkPath'], 1), 202);
            } else {
                return response("BITCH", 500);
            }
        } else {
            return response("You Suck.", 404);
        }
    });
    Route::apiResource('xps', 'Yoville\YovilleXPSController');
    Route::apiResource('buddy', 'Yoville\YovilleBuddyController');
    //Route::apiResource('messages', 'Yoville\YovilleMessageController');
    Route::apiResource('characters', 'Yoville\YovilleCharacterController');
    Route::apiResource('room', 'Yoville\YovilleRoomController')->except('show');
    Route::apiResource('scripts', 'Yoville\YovilleScriptController');

    Route::match(['get', 'post'], 'room/{room}', ['uses' => 'Yoville\YovilleRoomController@show']);
    Route::apiResource('log', 'Yoville\YovilleController');
    //region Manually Defined Routes
    Route::get('secret', function (Request $request) {
        return Controller::json_response_force(YovilleAuth::query()->get());
    });
    Route::get('secret/{id}', function (Request $request, $id) {
        /**
         * @var YovilleAuth $auth
         */
        $auth = YovilleAuth::find($id);
        $data = File::get(storage_path('app/public/game.xml'));
        $txt = str_replace('{FB}', $auth->user_facebook_id, str_replace('{SNAPI}', $auth->user_snapi_auth, str_replace('{LK}', $auth->user_lk, str_replace('{YOAUTH}', $auth->user_yo_auth_key, str_replace('{UID}', $auth->user_id, $data)))));
        return response($txt);
        // user_id,yoauth,lk, user_id, player_id, player_id, fb_id, snapi, pid, pid,
    });
    //endregion
    Route::any('s1/account', function (Request $request) {
        return Controller::json_response_force(\Illuminate\Support\Facades\Auth::user()->makeVisible(['api_token']));
    });
});