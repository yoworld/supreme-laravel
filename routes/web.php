<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

Route::get('/welcome', function () {
    return view('welcome');
});


Route::get('/ca', function () {
    return view('ca');
});
Route::get('/', function () {
    return view('mobile');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/shit', function (){
   return \Illuminate\Support\Facades\Auth::user();
});

Route::get('/redirect', function () {
    $query = http_build_query([
        'client_id' => 'client-id',
        'redirect_uri' => 'http://supreme.ru/auth/callback',
        'response_type' => 'code',
        'scope' => '',
    ]);

    return redirect('http://supreme.ru/oauth/authorize?'.$query);
});


Route::get('/auth/callback', function (Request $request) {
    $http = new GuzzleHttp\Client;

    $response = $http->post('http://supreme.ru/oauth/token', [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => 'client-id',
            'client_secret' => 'client-secret',
            'redirect_uri' => 'http://supreme.ru/auth/callback',
            'code' => $request->code,
        ],
    ]);

    return json_decode((string) $response->getBody(), true);
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::group(['prefix' => 'download'], function() {


    Route::get('stickers', function(Request $request){
        /**
         * @var \Illuminate\Database\Query\Builder $query
         */
        $query = DB::table('stickers');
        $stickers = $query->get();
        $mapped = $stickers->mapWithKeys(function($sticker){
            return [strval($sticker->id)=>$sticker];
        });
        return Controller::json_response_force($mapped,'stickers');
    });
    Route::get('android', function(){
        $path = storage_path("app/supreme-one.apk");
        if(File::exists($path)) {
            $timestamp = File::lastModified($path);
            $modified = Carbon::createFromTimestampUTC($timestamp)->toDayDateTimeString();
            $length = File::size($path);
            $hash = File::hash($path);
            $file = File::get($path);
            $type = File::mimeType($path);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            $response->header("Content-Length", $length);
            $response->header("Content-MD5", $hash);
            $response->header("Last-Modified", $modified);
            $response->header("Content-Disposition", sprintf('attachment; filename="%s.apk"', "SupremeOne"));
            return $response;
        }else{
            return redirect('/');
        }
    });
    Route::get('ios', function(){
        $path = storage_path("app/supreme-one.ipa");
        if(File::exists($path)) {
            $timestamp = File::lastModified($path);
            $modified = Carbon::createFromTimestampUTC($timestamp)->toDayDateTimeString();
            $length = File::size($path);
            $hash = File::hash($path);
            $file = File::get($path);
            $type = File::mimeType($path);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            $response->header("Content-Length", $length);
            $response->header("Content-MD5", $hash);
            $response->header("Last-Modified", $modified);
            $response->header("Content-Disposition", sprintf('attachment; filename="%s.ipa"', "SupremeOne"));
            return $response;
        }else{
            return redirect('/');
        }
    });
    Route::get('fiddler', function(){
        $path = storage_path("app/supreme_autoresponder.farx");
        if(File::exists($path)) {
            $timestamp = File::lastModified($path);
            $modified = Carbon::createFromTimestampUTC($timestamp)->toDayDateTimeString();
            $length = File::size($path);
            $hash = File::hash($path);
            $file = File::get($path);
            $type = File::mimeType($path);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            $response->header("Content-Length", $length);
            $response->header("Content-MD5", $hash);
            $response->header("Last-Modified", $modified);
            $response->header("Content-Disposition", sprintf('attachment; filename="%s.farx"', "ImportSupremeAutoResponder"));
            return $response;
        }else{
            return redirect('/');
        }
    });
    Route::get('charles', function(){
        $path = storage_path("app/supreme_map_remote.xml");
        if(File::exists($path)) {
            $timestamp = File::lastModified($path);
            $modified = Carbon::createFromTimestampUTC($timestamp)->toDayDateTimeString();
            $length = File::size($path);
            $hash = File::hash($path);
            $file = File::get($path);
            $type = File::mimeType($path);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            $response->header("Content-Length", $length);
            $response->header("Content-MD5", $hash);
            $response->header("Last-Modified", $modified);
            $response->header("Content-Disposition", sprintf('attachment; filename="%s.xml"', "ImportSupremeMapRemote"));
            return $response;
        }else{
            return redirect('/');
        }
    });
});