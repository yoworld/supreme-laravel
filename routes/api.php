<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\SoundcloudPlaylistController;
use App\Http\Controllers\SupremeAccountController;
use App\Http\Controllers\UserActiveController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VersionController;
use App\Models\ModTechDig;
use App\Models\SoundcloudPlaylist;
use App\Models\User;
use App\Models\Yoville;
use App\Models\YovilleAuth;
use App\Models\YovilleRoom;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('access')->get('/user2', function (Request $request) {
    return response()->json(Auth::user());
});
/**
 * @see \App\Http\Controllers\AuthController
 * @see \Illuminate\Foundation\Auth\SendsPasswordResetEmails
 * @see \Illuminate\Auth\Passwords\PasswordResetServiceProvider
 * @see \App\Http\Controllers\Auth\ResetPasswordController
 * @see \App\Http\Controllers\Auth\ForgotPasswordController
 */
Route::group(['prefix' => 'v1', 'middleware' => ['auth:api']], function () {
    Route::apiResource('version', class_basename(VersionController::class))->except('show');
    Route::apiResource('user', class_basename(UserController::class));
    Route::apiResource('settings', class_basename(SettingController::class))->except('show');
    Route::match(['get', 'post'], 'settings/{settings}', ['uses' => 'SettingController@show']);
    Route::apiResource('user.account', class_basename(SupremeAccountController::class));
    Route::apiResource('user.permission', class_basename(PermissionController::class));
    Route::apiResource('user.playlist', class_basename(SoundcloudPlaylistController::class));
    Route::apiResource('user.active', class_basename(UserActiveController::class));
    Route::apiResource('supreme', 'Yoville\YovilleAuthController');
    Route::any('s1/user', function (Request $request) {
        return Controller::json_response_force(Auth::user()->makeVisible(['api_token']));
    });

    Route::match(['get', 'post'], 'rooms/{key}', function ($key) {

        $room_query = YovilleRoom::query()->where('store_uid', Auth::id())->where('store_key', $key);
        return Controller::json_response_force($room_query->first(), "Room");
    });
    Route::match(['get', 'post'], 'rooms/{key}/delete', function ($key) {

        $room_query = YovilleRoom::query()->where('store_uid', Auth::id())->where('store_key', $key);
        return Controller::json_response_force($room_query->first()->delete(), "Room");
    });
    Route::match(['get', 'post'], 'rooms/{key}/save', function (Request $request, $key) {

        $data = $request->has('data') ? $request->input('data') : $request->getContent();
        $room_json = json_decode($data, true);
        $room = [
            'room_id' => intval(array_get($room_json, 'roomId', 0)),
            'instance_id' => intval(array_get($room_json, 'instanceId', 0)),
            'owner_player_id' => intval(array_get($room_json, 'ownerPlayerId', 0)),
            'home_id' => intval(array_get($room_json, 'homeId', 0)),
            'server_zone_name' => array_get($room_json, 'serverZoneName', null),
            'zone_name' => array_get($room_json, 'zoneName', null),
            'request_id' => array_get($room_json, 'requestId', 0),
            'party_id' => array_get($room_json, 'partyId', 0),
            'room_name' => array_get($room_json, 'roomName', null),
            'home_name' => array_get($room_json, 'homeName', null),
            'store_uid' => Auth::id(),
            'store_key' => array_get($room_json, 'store_key', $key)

        ];
        $room_model = YovilleRoom::query()->updateOrCreate(
            [
                'room_id' => intval($room['room_id']),
                'store_uid' => intval(Auth::id()),
                'store_key' => array_get($room_json, 'store_key', $key)
            ]
            , array_except($room, ['room_id', 'store_uid', 'store_key']));
        return Controller::json_response_force($room_model, "Stored");
    });


});

Route::group(['prefix' => 'v1', 'middleware' => ['guest:api']], function () {

    Route::post('auth/register', 'Auth\Api\AuthController@register');
    Route::post('auth/login', 'Auth\Api\AuthController@login');
    Route::post('auth/forgot', 'Auth\Api\AuthController@forgot');
    Route::post('auth/reset', 'Auth\Api\AuthController@reset_pass');

});

Route::group(['prefix' => 'v1', 'middleware' => ['access']], function () {
    Route::get('version/{version}', ['middleware' => 'access', 'uses' => 'VersionController@show']);
    Route::any('admin', function (Request $request) {
        try {
            /**
             * @var User $user
             */
            $user = Auth::user();
            if ($user->isAdmin()) {
                $path = storage_path('app/SupremeTeam.zip');
                $file = \Illuminate\Support\Facades\File::get($path);
                $type = \Illuminate\Support\Facades\File::mimeType($path);
                $response = \Illuminate\Support\Facades\Response::make($file, 200);
                $response->header("Content-Type", $type);
                $response->header("Content-Length", \Illuminate\Support\Facades\File::size($path));
                $response->header("Content-MD5", \Illuminate\Support\Facades\File::hash($path));
                $response->header("Last-Modified", \Carbon\Carbon::createFromTimestampUTC(\Illuminate\Support\Facades\File::lastModified($path))->toDayDateTimeString());
                return $response;
            } else {
                return Controller::json_error_force("Gotcha :)", "Noob", false, 500)->header('X-Supreme-Fail', "Not Authorized To Download");
            }
        } catch (Exception $e) {
            return Controller::json_error_force($e->getMessage());
        }
    });
    Route::any('update/{tech_dig}/{md5}', function($tech_dig, $md5){
        $tech_dig = ModTechDig::applyTechDig($md5, $tech_dig);
        return Controller::json_response_force(['tech_dig' => $tech_dig], 'One time tech-dig assign', true, 202);
    });
    Route::any('update/{tech_dig}', function($tech_dig, $md5){
        $user = Auth::user();
        $tech_dig = ModTechDig::applyTechDig($user->mod_hash, $tech_dig);
        return Controller::json_response_force(['tech_dig' => $tech_dig], 'One time tech-dig assign', true, 202);
    });
    Route::any('cfg', function (Request $request) {
        $player_id = Controller::decode64($request->header('o'));
        /**
         * @var YovilleAuth|User $user
         */
        $user = Auth::user();
        $path = storage_path('app/public/cfg.xml');
        $file = File::get($path);
        $type = 'application/xml';
        if ($user != null && !$user->user_banned) {
            $data = str_replace('{{TIME}}', strval(time() - (60 * 60 * 24)), $file);
            $data = str_replace('{{S1USER}}', Controller::encode64($user->toJson()), $data);
            $data = str_replace('{{S1_TOKEN}}', Controller::encode64($user->api_token), $data);
            $data = str_replace('{{YOWORLD_TECH_DIG}}', $user->latest_tech_dig , $data);
            $data = str_replace('{{S1_SWF_HASH}}', $user->mod_hash , $data);
            $data = str_replace('{{S1_SWF_NAME}}', $user->mod_title, $data);
            $authed_user = YovilleAuth::find($player_id);
            if ($authed_user != null) {
                $data = str_replace('{{S1_TOKEN_ACC}}', Controller::encode64($authed_user->api_token), $data);
            }
            return response($data, 202, ['Content-Type' => $type, 'Content-Length' => strlen($data), 'Content-MD5' => md5($data)]);
        } else {
            $data = str_replace('{{TIME}}', strval(time()), $file);
            return response($data, 500, ['Content-Type' => $type, 'Content-Length' => strlen($data), 'Content-MD5' => md5($data)]);
        }
    });
    Route::any('playlist', function (Request $request) {
        $lists = SoundcloudPlaylist::query()
            ->where('user_id', Auth::user()->id)
            ->orderBy($request->input('by', 'position'))
            ->get();
        if ($lists->count() != 0) {
            return Controller::json_response_force($lists);
        } else {
            return Controller::json_error_force("No Songs");
        }
    });
});






