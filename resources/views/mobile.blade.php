<!doctype html>
<html lang="en">
<!--

Page    : index / MobApp
Version : 1.0
Author  : Colorlib
URI     : https://colorlib.com

 -->

<head>
    <title>Supreme One - Best of Yw Modifications</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Supreme One - Mobile App YoWorld Yoville Mods">
    <meta name="keywords"
          content="Supreme One, SupremeOne, S1, Supreme1, Yoworld, Yoville, YoWorld, YoVille, YoVille Mods, YoVille App, YoWorld Mod, YoWorld App, YoWorld Modders, Supreme One Team, YoWorld Forums, BVG, Big Viking Games">

    <!-- Font -->
    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Themify Icons -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- Main css -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body data-spy="scroll" data-target="#navbar" data-offset="30">

<!-- Nav Menu -->

<div class="nav-menu fixed-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-dark navbar-expand-lg">
                    <a class="navbar-brand" href="#"><img src="images/s1logo.png" class="img-fluid"
                                                          alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
                            aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation"><span
                                class="navbar-toggler-icon"></span></button>
                    <div class="collapse navbar-collapse" id="navbar">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"><a class="nav-link active" href="#home">HOME <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#features">FEATURES</a></li>
                            <li class="nav-item"><a class="nav-link" href="#launch">LAUNCH</a></li>
                            <li class="nav-item" hidden><a class="nav-link" href="#pricing">PRICING</a></li>
                            <li class="nav-item"><a class="nav-link" href="#contact">CONTACT</a></li>
                            <li class="nav-item"><a href="#download"
                                                    class="btn btn-outline-light my-3 my-sm-0 ml-lg-3">Download</a></li>
                            @if (Route::has('login'))
                                <div class="top-right links">
                                    @auth
                                        <li class="nav-item dropdown ">
                                            <a href="#"
                                               class=" btn btn-outline-light my-3 my-sm-0 ml-lg-3 dropdown-toggle"
                                               data-toggle="dropdown" role="button"
                                               aria-expanded="false" aria-haspopup="true">
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <ul class="dropdown-menu">
                                                <li class="nav-link">
                                                    <a href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                        Logout
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                          style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                    @else
                                        <li class="nav-item"><a href="{{ route('login') }}"
                                                                class="btn btn-outline-light my-3 my-sm-0 ml-lg-3">Login</a>
                                        </li>
                                    @endauth
                                </div>
                            @endif
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>


<header class="bg-gradient" id="home">
    <div class="container mt-5">
        <h1>Supreme One App & Mod</h1>
        <p class="tagline">Play better, be better, become Supreme. Join Supreme1 today and walk amongst the Elite of
            YoWorld.</p>
    </div>
    <div class="img-holder mt-3"><img src="images/iphonex.png" alt="phone" class="img-fluid"></div>
</header>

<div class="client-logos my-5">
    <div class="container text-center">
        <img src="images/client-logos.png" alt="client logos" class="img-fluid">
    </div>
</div>

<div class="section light-bg" id="features">


    <div class="container">

        <div class="section-title">
            <small>HIGHLIGHTS</small>
            <h3>Features you love</h3>
        </div>


        <div class="row">
            <div class="col-12 col-lg-4">
                <div class="card features">
                    <div class="card-body">
                        <div class="media">
                            <span class="ti-face-smile gradient-fill ti-3x mr-3"></span>
                            <div class="media-body">
                                <h4 class="card-title">Simple</h4>
                                <p class="card-text">Using our product is relatively easy and getting everything set-up
                                    wont be to hard. You'll have the supreme-team on your side.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card features">
                    <div class="card-body">
                        <div class="media">
                            <span class="ti-settings gradient-fill ti-3x mr-3"></span>
                            <div class="media-body">
                                <h4 class="card-title">Customize</h4>
                                <p class="card-text">With the ability to listen to music and customize your playlist
                                    with either Soundcloud or Youtube so you can bang out to your hearts content!.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card features">
                    <div class="card-body">
                        <div class="media">
                            <span class="ti-lock gradient-fill ti-3x mr-3"></span>
                            <div class="media-body">
                                <h4 class="card-title">Secure</h4>
                                <p class="card-text">We take security very seriously over here at Supreme-One
                                    (Ironically). Rest assured that we aim to have a stable platform for you guys.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


</div>
<!-- // end .section -->
<div class="section">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-6">
                <div class="box-icon"><span class="ti-mobile gradient-fill ti-3x"></span></div>
                <h2>Instant Messaging</h2>
                <p class="mb-4">We've implemented a color coded instant messaging so that you can chat with your friends
                    while away! No more bringing your computer to your bathroom!</p>
                <a href="#" class="btn btn-primary">Read more</a>
            </div>
        </div>
        <div class="perspective-phone">
            <img src="images/perspective.png" alt="perspective phone" class="img-fluid">
        </div>
    </div>

</div>
<!-- // end .section -->


<div class="section light-bg">
    <div class="container">
        <div class="section-title">
            <small>FEATURES</small>
            <h3>Do more with our app</h3>
        </div>

        <ul class="nav nav-tabs nav-justified" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#communication">Communication</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#schedule">Music</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#messages">Settings</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#livechat">Support</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="communication">
                <div class="d-flex flex-column flex-lg-row">
                    <img src="images/graphic.png" alt="graphic"
                         class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                    <div>

                        <h2>Communicate with ease</h2>
                        <p class="lead">Trust me we've all been there </p>
                        <p>
                            Trying to keep up a conversation while having the need to run the bathroom is a pretty
                            hard knock life. That is why we have implemented this feature because we believe that
                            having the ability to message friends within the app is super important and doesn't that
                            bring out a nice cherry ontop for an app?
                        </p>
                        <p>
                            Our chat bubbles are color coded similar to YoWorlds standards, and we made
                            it easy on the eyes so that its readable yet concise. Emojis are in the works but don't
                            worry
                            that should be here soon :)
                        </p>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="schedule">
                <div class="d-flex flex-column flex-lg-row">
                    <div>
                        <h2>Music</h2>
                        <p class="lead">Youtube or Soundcloud?</p>
                        <p>
                            That&apos;s a tough choice, we know. That is why we implemented it both for you guys.
                            Sometimes you have a song that&apos;s available on Soundcloud but not youtube and vice
                            versa.
                            Songs are not stored on our servers but simply streamed to you guys!
                        </p>
                        <p>
                            If you have any other source of music that you would like us to implement let us know.
                            It could be a radio station or podcast, we'll try our best to accommodate your needs.
                        </p>
                    </div>
                    <img src="images/graphic.png" alt="graphic"
                         class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                </div>
            </div>
            <div class="tab-pane fade" id="messages">
                <div class="d-flex flex-column flex-lg-row">
                    <img src="images/graphic.png" alt="graphic"
                         class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                    <div>
                        <h2>Settings</h2>
                        <p class="lead">Are my settings persistent? </p>
                        <p>
                            Yes they are, we make sure that whatever settings (Supreme Settings) you toggle we make note
                            of them
                            so that the next time you load the mod those settings will be loaded back up.
                        </p>
                        <p>
                            Please be aware that as newer versions come out, settings may changed drastically requiring
                            you to update to the latest version.
                        </p>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="livechat">
                <div class="d-flex flex-column flex-lg-row">
                    <div>
                        <h2>Support</h2>
                        <p class="lead">8/7 Support?</p>
                        <p>
                            Well we can't offer 24/7 support because we're a pretty small team.
                            However we will try our best to help you out.
                        </p>
                        <p> The best places you can reach us is in the Supreme One Group
                        </p>
                    </div>
                    <img src="images/graphic.png" alt="graphic"
                         class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                </div>
            </div>
        </div>


    </div>
</div>
<!-- // end .section -->

<div class="section" id="launch">

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="images/dualphone.png" alt="dual phone" class="img-fluid">
            </div>
            <div class="col-md-6 d-flex align-items-center">
                <div>
                    <div class="box-icon"><span class="ti-rocket gradient-fill ti-3x"></span></div>
                    <h2>Launch our App</h2>
                    <p class="mb-4">We know you're aching to use our app. Hit the download button and get started.</p>
                    <a href="#" class="btn btn-primary">Read more</a></div>
            </div>
        </div>

    </div>

</div>
<!-- // end .section -->


<div class="section light-bg">

    <div class="container">
        <div class="row">
            <div class="col-md-8 d-flex align-items-center">
                <ul class="list-unstyled ui-steps">
                    <li class="media">
                        <div class="circle-icon mr-4">1</div>
                        <div class="media-body">
                            <h5>Create an Account</h5>
                            <p>Create an account on our website. It's really a quick and simple process.</p>
                        </div>
                    </li>
                    <li class="media my-4">
                        <div class="circle-icon mr-4">2</div>
                        <div class="media-body">
                            <h5>Fiddler or Charles</h5>
                            <p>
                                That depends! If you're a Windows user, you should go with <a href="https://www.telerik.com/download/fiddler">Fiddler</a>.
                                If you're a Mac or Linux user you should go with <a href="https://www.charlesproxy.com/download/">Charles</a> (Fiddler isn't really stable
                                for Mac &amp; Linux users at the moment).
                            </p>
                        </div>
                    </li>
                    <li class="media">
                        <div class="circle-icon mr-4">3</div>
                        <div class="media-body">
                            <h5>Load &amp; Go</h5>
                            <p>Import our configurations for <a href="{{url('download/charles',[], true)}}">Charles</a> or <a href="{{url('download/fiddler',[], true)}}">Fiddler</a> to get going. Be sure to clear your
                                cache!</p>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <img src="images/iphonex.png" alt="iphone" class="img-fluid">
            </div>

        </div>

    </div>

</div>
<!-- // end .section -->


<div class="section" hidden>
    <div class="container">
        <div class="section-title">
            <small>TESTIMONIALS</small>
            <h3>What our Customers Says</h3>
        </div>

        <div class="testimonials owl-carousel">
            <div class="testimonials-single">
                <img src="images/client.png" alt="client" class="client-img">
                <blockquote class="blockquote">
                    We
                </blockquote>
                <h5 class="mt-4 mb-2">Supreme One</h5>
                <p class="text-primary">United States</p>
            </div>
            <div class="testimonials-single">
                <img src="images/client.png" alt="client" class="client-img">
                <blockquote class="blockquote">
                    Dont Have
                </blockquote>
                <h5 class="mt-4 mb-2">Supreme One</h5>
                <p class="text-primary">United States</p>
            </div>
            <div class="testimonials-single">
                <img src="images/client.png" alt="client" class="client-img">
                <blockquote class="blockquote">
                    Any
                </blockquote>
                <h5 class="mt-4 mb-2">Supreme One</h5>
                <p class="text-primary">United States</p>
            </div>
        </div>

    </div>

</div>
<!-- // end .section -->


<div class="section light-bg" id="gallery" hidden>
    <div class="container">
        <div class="section-title">
            <small>GALLERY</small>
            <h3>App Screenshots</h3>
        </div>

        <div class="img-gallery owl-carousel owl-theme">
            <img src="images/screen1.jpg" alt="image">
            <img src="images/screen2.jpg" alt="image">
            <img src="images/screen3.jpg" alt="image">
            <img src="images/screen1.jpg" alt="image">
        </div>

    </div>

</div>
<!-- // end .section -->


<div class="section" id="pricing" hidden>
    <div class="container">
        <div class="section-title">
            <small>PRICING</small>
            <h3>Upgrade to Pro</h3>
        </div>

        <div class="card-deck">
            <div class="card pricing">
                <div class="card-head">
                    <small class="text-primary">PERSONAL</small>
                    <span class="price">$14<sub>/m</sub></span>
                </div>
                <ul class="list-group list-group-flush">
                    <div class="list-group-item">10 Projects</div>
                    <div class="list-group-item">5 GB Storage</div>
                    <div class="list-group-item">Basic Support</div>
                    <div class="list-group-item">
                        <del>Collaboration</del>
                    </div>
                    <div class="list-group-item">
                        <del>Reports and analytics</del>
                    </div>
                </ul>
                <div class="card-body">
                    <a href="#" class="btn btn-primary btn-lg btn-block">Choose this Plan</a>
                </div>
            </div>
            <div class="card pricing popular">
                <div class="card-head">
                    <small class="text-primary">FOR TEAMS</small>
                    <span class="price">$29<sub>/m</sub></span>
                </div>
                <ul class="list-group list-group-flush">
                    <div class="list-group-item">Unlimited Projects</div>
                    <div class="list-group-item">100 GB Storage</div>
                    <div class="list-group-item">Priority Support</div>
                    <div class="list-group-item">Collaboration</div>
                    <div class="list-group-item">Reports and analytics</div>
                </ul>
                <div class="card-body">
                    <a href="#" class="btn btn-primary btn-lg btn-block">Choose this Plan</a>
                </div>
            </div>
            <div class="card pricing">
                <div class="card-head">
                    <small class="text-primary">ENTERPRISE</small>
                    <span class="price">$249<sub>/m</sub></span>
                </div>
                <ul class="list-group list-group-flush">
                    <div class="list-group-item">Unlimited Projects</div>
                    <div class="list-group-item">Unlimited Storage</div>
                    <div class="list-group-item">Collaboration</div>
                    <div class="list-group-item">Reports and analytics</div>
                    <div class="list-group-item">Web hooks</div>
                </ul>
                <div class="card-body">
                    <a href="#" class="btn btn-primary btn-lg btn-block">Choose this Plan</a>
                </div>
            </div>
        </div>
        <!-- // end .pricing -->


    </div>

</div>
<!-- // end .section -->


<div class="section pt-0">
    <div class="container">
        <div class="section-title">
            <small>FAQ</small>
            <h3>Frequently Asked Questions</h3>
        </div>

        <div class="row pt-4">
            <div class="col-md-6">
                <h4 class="mb-3">Mod isn't loading</h4>
                <p class="light-font mb-5">
                    Maybe its your SSL settings, reach us on facebook for extra help and we'll respond the same day.
                </p>
                <h4 class="mb-3">There's a feature I would like to suggest</h4>
                <p class="light-font mb-5">
                    We're open to hear what you have to say, give your suggestions in our group.
                </p>

            </div>
            <div class="col-md-6">
                <h4 class="mb-3">How can I update my version</h4>
                <p class="light-font mb-5">
                   Simply put, you don't need to. We have set it up so that you guys can get automatic updates for SupremeOne. We'll notify you if theres an app update though.
                </p>
                <h4 class="mb-3">Do you have a contract?</h4>
                <p class="light-font mb-5">
                    Do not tamper, sell, or find anyway to exploit the S1 system or you will be barred from Supreme One.
                </p>
            </div>
        </div>
    </div>

</div>
<!-- // end .section -->


<div class="section bg-gradient" id="download">
    <div class="container">
        <div class="call-to-action">

            <div class="box-icon"><span class="ti-mobile gradient-fill ti-3x"></span></div>
            <h2>Download Anywhere</h2>
            <p class="tagline">Available for all major mobile and desktop platforms. Rapidiously visualize optimal ROI
                rather than enterprise-wide methods of empowerment. </p>
            <div class="my-4">

                <a href="{{url('download/ios',[], true)}}" class="btn btn-light"><img src="images/appleicon.png"
                                                                                       alt="icon"> App Store</a>
                <a href="{{url('download/android', [], true)}}" class="btn btn-light"><img src="images/playicon.png"
                                                                                            alt="icon"> Google play</a>
            </div>
            <p class="text-primary">
                <small><i>*Works on iOS 10.0.5+, Android Lollipop and above. </i></small>
            </p>
        </div>
    </div>

</div>
<!-- // end .section -->

<div class="light-bg py-5" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <p class="mb-2"><span class="ti-location-pin mr-2"></span> Some place down town, New York, NY 10002 USA
                </p>
                <div class=" d-block d-sm-inline-block">
                    <p class="mb-2">
                        <span class="ti-email mr-2"></span> <a class="mr-4" href="mailto:support@mobileapp.com">support@mobileapp.com</a>
                    </p>
                </div>
                <div class="d-block d-sm-inline-block">
                    <p class="mb-0">
                        <span class="ti-headphone-alt mr-2"></span> <a href="tel:51836362800">800-Sup-reme</a>
                    </p>
                </div>

            </div>
            <div class="col-lg-6">
                <div class="social-icons">
                    <a href="#"><span class="ti-facebook"></span></a>
                    <a href="#"><span class="ti-twitter-alt"></span></a>
                    <a href="#"><span class="ti-instagram"></span></a>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- // end .section -->
<footer class="my-5 text-center">
    <!-- Copyright removal is not prohibited! -->
    <p class="mb-2">
        <small>COPYRIGHT © {{\Carbon\Carbon::now()->year}}. ALL RIGHTS RESERVED. <a href="http://s1.supreme-one.net">Supreme One</a>
        </small>
    </p>

    <small>
        <a href="#" class="m-2">PRESS</a>
        <a href="#" class="m-2">TERMS</a>
        <a href="#" class="m-2">PRIVACY</a>
    </small>
</footer>

<!-- jQuery and Bootstrap -->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<!-- Plugins JS -->
<script src="js/owl.carousel.min.js"></script>
<!-- Custom JS -->
<script src="js/script.js"></script>

</body>

</html>
