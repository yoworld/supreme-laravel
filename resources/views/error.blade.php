
<!DOCTYPE html>
<head>
    <meta charset='utf-8' />
    <meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible' />
    <title>Supreme1</title>

    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="apple-touch-icon" href="{{asset('images/favicon.png')}}" />

    <link href='https://fonts.googleapis.com/css?family=Exo:400,900' rel='stylesheet' type='text/css'>

    <style type="text/css" media="screen">
        .gradient {display:none;}
        .bg {
            position: relative;
            width: 100%;
            height: 100%;
            background-image: url({{asset('images/backgrounds/bg-img-1.png')}});
            background-repeat: no-repeat;
            background-size: cover;
            display: block;
        }

        html, body{
            height: 100%;
        }
        body {
            background-image: url({{asset('images/backgrounds/bg-img-1.png')}}) ;
            background-position: center center;
            background-repeat:  no-repeat;
            background-attachment: fixed;
            background-size:  cover;
            background-color: #999;

        }

        div, body{
            margin: 0;
            padding: 0;
            font-family: exo, sans-serif;

        }
        .wrapper {
            height: 100%;
            width: 100%;
        }

        .message {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            width: 100%;
            height:25%;
            bottom: 0;
            display: block;
            position: absolute;
            background-color: rgba(0,0,0,0.6);
            color: #fff;
            padding: 0.5em;
        }


    </style>
</head>
<body>

<div class="wrapper">
    <div class="message">
        <h2>Supreme1</h2>
        <p style="font-size: {{$size}}px">{{$quote}}</p>
    </div>
</div>

</body>
</html>
