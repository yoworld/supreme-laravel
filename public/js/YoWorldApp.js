! function(e, t) {
    "object" == typeof exports && "object" == typeof module ? module.exports = t(require("CommonDependencies")) : "function" == typeof define && define.amd ? define(["CommonDependencies"], t) : "object" == typeof exports ? exports.YoWorldApp = t(require("CommonDependencies")) : e.YoWorldApp = t(e.CommonDependencies)
}(this, function(e) {
    return function(e) {
        function t(a) {
            if (n[a]) return n[a].exports;
            var i = n[a] = {
                exports: {},
                id: a,
                loaded: !1
            };
            return e[a].call(i.exports, i, i.exports, t), i.loaded = !0, i.exports
        }
        var n = {};
        return t.m = e, t.c = n, t.p = "", t(0)
    }([function(e, t, n) {
        var a;
        (function(i) {
            "use strict";
            var o = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
                return typeof e
            } : function(e) {
                return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
            };
            a = function(e, t, a) {
                var r = n(4),
                    s = n(60),
                    l = n(18),
                    c = n(58),
                    d = n(57),
                    u = n(66),
                    p = n(61),
                    m = n(2),
                    h = n(53),
                    _ = n(63),
                    f = n(54),
                    g = n(7),
                    v = n(55),
                    b = n(27),
                    y = n(69);
                if (void 0 === ("undefined" == typeof FB ? "undefined" : o(FB))) return void console.error("You have to load the Facebook JS SDK before loading the yoworld JS!");
                var E = "yoworld-modal",
                    w = 1e3,
                    C = "header-purchase-tab",
                    k = "btnRepayment";
                a.exports = r.create({
                    __fbAppId: null,
                    __channelURL: null,
                    __purchaseManager: null,
                    __externalUIController: null,
                    __modalLayer: null,
                    __playScripts: null,
                    __ga: null,
                    __dataCannon: null,
                    __name: null,
                    __clientVersion: "Not set",
                    __thorTimeoutId: null,
                    __sessionStart: 0,
                    initialize: function(e, t, n, a, o) {
                        m.AUTH_TOKEN = n, this.__fbAppId = e, this.__channelURL = t, window.talkToJS = function(e, t) {
                            m.handleFlashMessage(e, t)
                        };
                        var r = e && e.length > 0;
                        r && this.__initializeFacebook();
                        var T = this.__initializeServerTime();
                        this.__initializeAdminMessages();
                        var S = this;
                        r && FB.getLoginStatus(function(e) {
                            S.__onFacebookReady(e)
                        }), this.__sessionStart = Date.now() - T, this.__dataCannon = new f(T, a, o), this.playerId = a;
                        var I = document.querySelector("#main_content");
                        this.__modalLayer = new s(E, w, I), this.__supportManager = new b("header-help-tab", this.__modalLayer), this.__supportManager.load(), this.__crewManager = new h(this.__modalLayer), this.__multiFriendSelector = new p(this.__modalLayer), this.__multiFriendSelector.load(), this.__freeGiftsManager = new c(this.__modalLayer, this.__multiFriendSelector.show.bind(this.__multiFriendSelector)), this.__freeGiftsManager.load(), this.__freeGiftsInbox = new d("header-inbox-tab", "header-inbox-count", this.__modalLayer), this.__freeGiftsInbox.load(), this.__slotsRewardItems = new u(this.__modalLayer), this.__slotsRewardItems.load(), this.__externalUIController = new v(this.__modalLayer, this.__showSupportTab.bind(this), this.__refreshGifts.bind(this)), this.__purchaseManager = new l(C, this.__modalLayer, this.__externalUIController.show.bind(this.__externalUIController), this.__externalUIController.hide.bind(this.__externalUIController), i("#" + k)), m.dispatchFlashMessage("subscriptionRequest", {
                            productId: 0
                        }, function(e) {
                            var t = !1,
                                n = !1;
                            e.status && (e.paymentStatus === !1 && (t = !0), e.subType.indexOf("premiumVip") > -1 && (n = !0)), S.__externalUIController.setupVIPWarn(t), S.__purchaseManager.setVipStatus(n)
                        }), this.__playScripts = new _, window.fireDataEvent = function(e, t, n, a, i) {
                            "string" == typeof t && (t = JSON.parse(decodeURI(t))), S.__dataCannon.fireEvent(e, t, n, a, void 0 === i || i)
                        }, window.showBlockingModal = function() {
                            S.__modalLayer.show()
                        }, window.hideBlockingModal = function() {
                            S.__modalLayer.hide()
                        }, window.showAdBlockingModal = function(e, t) {
                            i("#brokenAdLink").removeClass("hidden"), i("#brokenAdLink").click(function() {
                                t(), S.__externalUIController.show("reportAdPopup", {
                                    networkId: e
                                })
                            }), window.showBlockingModal()
                        }, window.hideAdBlockingModal = function() {
                            i("#brokenAdLink").addClass("hidden"), window.hideBlockingModal()
                        }, window.triggerPurchasePopup = function() {
                            S.__purchaseManager.triggerPurchasePopup()
                        }, window.triggerSlotsRewardItemsPopup = function(e) {
                            S.__slotsRewardItems.show(e)
                        }, window.bannerClicked = function(e) {
                            S.__externalUIController.bannerClicked(e)
                        }, window.piggyBankClicked = function(e) {
                            S.__externalUIController.show("PiggyBank", e)
                        }, window.triggerNewsletter = function() {
                            S.__externalUIController.showNewsletter()
                        }, window.triggerProductPurchase = function(e) {
                            S.__purchaseManager.triggerProductPurchase(JSON.parse(e))
                        }, window.triggerRefNamePurchase = function(e) {
                            S.__purchaseManager.triggerRefNamePurchase(e)
                        }, window.triggerFreeGifts = function() {
                            S.__freeGiftsManager.show("freegifts")
                        }, window.triggerSubscribePopup = function() {
                            S.__externalUIController.showSubscription()
                        }, window.triggerMaintenancePopup = function(e) {
                            m.dispatchFlashMessage("maintenanceWarning", {
                                message: e
                            })
                        }, window.triggerPaymentStatus = function(e) {
                            S.__externalUIController.setupVIPWarn("failed" === e)
                        }, window.showFeed = function(e, t, n, a) {
                            S.__playScripts.showFeed(e, t, n, a)
                        }, window.getAdminData = function(e) {
                            S.__playScripts.getAdminData(e)
                        }, window.lockCodeCheck = function(e) {
                            S.__playScripts.lockCodeCheck(S.__externalUIController, e)
                        }, window.saveSelfie = function(e, t, n, a, i, o, r) {
                            S.__playScripts.saveSelfie(e, t, n, a, i, o, r)
                        }, window.saveThorStatus = function(e) {
                            null !== this.__thorTimeoutId && (window.clearTimeout(this.__thorTimeoutId), this.__thorTimeoutId = null, m.POST("/gamecontrol/endpoints.php?ep=thorCompatibilityCheck", {
                                result: e
                            }, function(e, t) {}))
                        }, window.openURL = function(e) {
                            S.__externalUIController.openURL(e)
                        }, window.lockCodeUnlock = function() {
                            S.__playScripts.lockCodeUnlock(S.__externalUIController)
                        }, window.allowBrowserScroll = function(e) {
                            S.__playScripts.allowBrowserScroll(e)
                        }, window.showTopBar = function(e) {
                            S.__playScripts.showTopBar(e)
                        }, window.lockCodeInit = function() {
                            S.__playScripts.lockCodeInit()
                        }, window.getFBPhotoURL = function(e) {
                            FB.api("/" + e + "?fields=images", function(t) {
                                if (!t || t.error || 0 == t.images.length) {
                                    var n = {
                                        type: "pictureFrameUrl",
                                        srcURL: null,
                                        id: e
                                    };
                                    return void gameSwf.talkToFlash(JSON.stringify(n))
                                }
                                var a = t.images.reduce(function(e, t) {
                                        return e.width > t.width ? e : t
                                    }),
                                    i = {
                                        type: "pictureFrameUrl",
                                        srcURL: a.source,
                                        id: e
                                    };
                                gameSwf.talkToFlash(JSON.stringify(i))
                            })
                        }, window.trackVideoAd = function(e, t) {
                            var n = "/gamecontrol/ad_videos/TrackAdView.php?network=" + e;
                            void 0 != t && (n += "&params=" + encodeURIComponent(JSON.stringify(t))), m.POST(n)
                        }, window.registerFlashEventHander(function(e, t) {
                            S.__dispatchFlashEvent(e, t)
                        }), window.addEventListener && (window.addEventListener("DOMMouseScroll", S.__playScripts.onMouseWheel, !1), window.addEventListener("beforeunload", function() {
                            m.sendUnloadNotice("/gamecontrol/endpoints.php?ep=timeLoginStep"), window.fireDataEvent(g.SESSION_END, {
                                sessionLength: Date.now() - S.__sessionStart - T
                            }, "", g.LIFE_CYCLE_TYPE)
                        }, !1), window.addEventListener("blur", S.__onWindowBlur, !1), window.addEventListener("focus", S.__onWindowFocus, !1)), window.onmousewheel = document.onmousewheel = S.__playScripts.onMouseWheel, document.onkeypress = function(e) {
                            if (13 == e.keyCode) return !1
                        }, S.__name = new y(n, !1), m.GET("/gamecontrol/endpoints.php?ep=getTrackingBasics", function(e, t) {
                            window.fireDataEvent(g.SESSION_START, t, "", g.LIFE_CYCLE_TYPE)
                        })
                    },
                    __onWindowBlur: function() {
                        if (gameSwf && gameSwf.talkToFlash) {
                            var e = {
                                type: "onWindowBlur"
                            };
                            gameSwf.talkToFlash(JSON.stringify(e))
                        }
                    },
                    __onWindowFocus: function() {
                        if (gameSwf && gameSwf.talkToFlash) {
                            var e = {
                                type: "onWindowFocus"
                            };
                            gameSwf.talkToFlash(JSON.stringify(e))
                        }
                    },
                    __showSupportTab: function() {
                        this.__supportManager.show()
                    },
                    __refreshGifts: function() {
                        this.__freeGiftsInbox.refreshInbox()
                    },
                    __initializeFacebook: function() {
                        FB.init({
                            appId: this.__fbAppId,
                            channelURL: this.__channelURL,
                            status: !0,
                            cookie: !0,
                            oauth: !0,
                            xfbml: !0,
                            frictionlessRequests: !0,
                            version: "v2.9"
                        })
                    },
                    __onFacebookReady: function(e) {
                        var t = this;
                        window.YoWorldBootstrap.waitForFlash(function() {
                            console.debug("Flash loaded and ready"), t.__purchaseManager.load(), t.__checkHardwareSurvey()
                        }), this.__crewManager.load(), this.__externalUIController.load(function() {
                            t.__handleModalMessages()
                        }), i("#accountOpen").click(function() {
                            window.fireDataEvent(g.GAME_TAB_CLICKED, {
                                tab: "Settings"
                            }), t.__externalUIController.show("settings", {
                                clientVersion: t.__clientVersion
                            })
                        }).removeClass("inactive"), i("#logOpen").click(function() {
                            window.fireDataEvent(g.GAME_TAB_CLICKED, {
                                tab: "Log"
                            }), t.__externalUIController.show("transactions", {})
                        }).removeClass("inactive"), i("#header-help-tab").click(function() {
                            window.fireDataEvent(g.GAME_TAB_CLICKED, {
                                tab: "Help"
                            }), t.__externalUIController.show("HelpContainer", {
                                supportManager: t.__supportManager
                            })
                        }).removeClass("inactive"), i("#freeGiftsOpen").click(function() {
                            window.fireDataEvent(g.GAME_TAB_CLICKED, {
                                tab: "Free Gifts"
                            }), t.__freeGiftsManager.show("freegifts")
                        }).removeClass("inactive"), i("#stocksOpen").click(function() {
                            window.fireDataEvent(g.GAME_TAB_CLICKED, {
                                tab: "Stocks"
                            }), t.__externalUIController.show("PopupStocks")
                        }).removeClass("inactive"), i("#subscriptionOpen").click(function() {
                            window.fireDataEvent(g.GAME_TAB_CLICKED, {
                                tab: "VIP"
                            }), t.__externalUIController.showSubscription()
                        }).removeClass("inactive"), i("#btnRepayment").click(function() {
                            t.__externalUIController.show("chargebackRepayment", {
                                purchases: window.chargebackRepayment.purchases,
                                amountOwed: window.chargebackRepayment.amountOwed
                            })
                        }), i("#appealForm").click(function() {
                            t.__externalUIController.show("Appeal", {
                                supportManager: t.__supportManager
                            })
                        }).removeClass("inactive")
                    },
                    __dispatchFlashEvent: function(e, t) {
                        var t = Array.prototype.slice.call(arguments, 1);
                        switch (e) {
                            case "showCrew":
                                return this.__crewManager.show(t);
                            case "showExternalUI":
                                return this.__externalUIController.show.apply(this.__externalUIController, t);
                            case "setClientVersion":
                                return void(this.__clientVersion = t[0]);
                            case "clientCall":
                                return void this.__handleClientCallTrigger(t[0]);
                            case "clientReady":
                                return void m.setFlashClientReady()
                        }
                        console.error("Unhandled flash event: " + e, t)
                    },
                    __handleClientCallTrigger: function(e) {
                        var t = this;
                        switch (e) {
                            case "getBonusOffer":
                                return void t.__fetchBonusOffer();
                            case "getGlobalMessages":
                                return void t.__fetchGlobalMessages()
                        }
                        console.error("Unhandled client call: ", e)
                    },
                    __initializeServerTime: function() {
                        function e(e) {
                            var t = 1 == e.toString().length ? "0" + e : e;
                            return t
                        }

                        function t() {
                            var t = new Date(Date.now() - s),
                                n = i[t.getUTCMonth()],
                                o = e(t.getUTCDate()),
                                r = t.getUTCFullYear(),
                                l = e(t.getUTCHours()),
                                c = e(t.getUTCMinutes()),
                                d = e(t.getUTCSeconds());
                            a.innerHTML = n + " " + o + ", " + r + " " + l + ":" + c + ":" + d
                        }
                        var n = "server_time",
                            a = document.querySelector("#" + n);
                        if (null === a) return void console.debug("Server timer disabled.  Missing component #" + n);
                        var i = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"],
                            o = a.getAttribute("data-server-time"),
                            r = new Date(o + " UTC"),
                            s = Date.now() - r.valueOf();
                        return setInterval(t, 1e3), t(), s
                    },
                    __formatTimeLeft: function(e) {
                        if (e <= 0) return "00:00:00";
                        var t = Math.floor(e / 3600),
                            n = Math.floor((e - 3600 * t) / 60),
                            a = e - 3600 * t - 60 * n;
                        t < 10 && (t = "0" + t), n < 10 && (n = "0" + n), a < 10 && (a = "0" + a);
                        var i = t + ":" + n + ":" + a;
                        return i
                    },
                    __showDetailsInSwf: function(e, t) {
                        var n = this,
                            a = "";
                        if ("function" == typeof e.updateFromJS)
                            if (e.updateFromJS("", ""), t.display_text && (a = t.display_text), e.updateFromJS("", a), n.__updateTimeLeftInterval && clearInterval(n.__updateTimeLeftInterval), t.timeLeft != -1 && 1 == t.show_countdown) {
                                var i = function() {
                                        --o;
                                        var t = n.__formatTimeLeft(o),
                                            i = "Time Remaining: " + t;
                                        n.__purchaseManager.setTime(t), o <= 0 && (i = "Offer Has Expired!"), e && "function" == typeof e.updateFromJS ? e.updateFromJS(i, a) : clearInterval(n.__updateTimeLeftInterval)
                                    },
                                    o = t.timeLeft;
                                n.__updateTimeLeftInterval = setInterval(i, 1e3), i()
                            } else e.updateFromJS("", a)
                    },
                    __fetchBonusOffer: function() {
                        var e = "/gamecontrol/admin.php?ep=getBonusOffer",
                            t = this;
                        m.GET("/gamecontrol/endpoints.php?ep=getSettings", function(n, a) {
                            if (!a.player_id) return void console.error("Cannot find player id");
                            var o = a.player_id;
                            m.POST(e, {
                                playerId: o
                            }, function(e, n) {
                                if (n && 0 !== n.enabledId) {
                                    var a = function e() {
                                        o = document.getElementById("bonusOffer"), null == o ? setTimeout(e, 250) : t.__showDetailsInSwf(o, n)
                                    };
                                    if (t.__currentlyLoadedOffer == n.enabledId) {
                                        var o = document.getElementById("bonusOffer");
                                        return void t.__showDetailsInSwf(o, n)
                                    }
                                    i("#bonusOffer").css("display", "block"), t.__currentlyLoadedOffer = n.enabledId;
                                    var r = 1 == n.show_sale_badge;
                                    t.__purchaseManager.showSaleBadge(r);
                                    var s = STATIC_BASE_URL + "/bonus_offers/" + n.enabledId + ".swf";
                                    swfobject.embedSWF(s, "bonusOffer", "760", "100", "9.0.0", "expressInstall.swf", {
                                        wmode: "opaque"
                                    }, {
                                        wmode: "opaque"
                                    }, {}), o = document.getElementById("bonusOffer"), setTimeout(a, 250)
                                } else t.__currentlyLoadedOffer && (t.__currentlyLoadedOffer = 0, t.__purchaseManager.showSaleBadge(!1), t.__purchaseManager.hidePurchasePopup()), i("#bonusOffer").css("display", "none")
                            })
                        })
                    },
                    __fetchGlobalMessages: function() {
                        var e = "/gamecontrol/admin.php?ep=getGlobalMessages";
                        m.GET(e, function(e, t) {
                            var n = [],
                                a = [];
                            window.MESSAGES && (console.debug(window.MESSAGES), n = n.concat(window.MESSAGES), delete window.MESSAGES), e && console.error("Failed to fetch global messages", e);
                            var o = i("#admin-messages");
                            if (!o) return void console.error("Cannot find admin messages div");
                            if (o.html(""), t && t.message) {
                                var a = t.message;
                                !Array.isArray(a) && a && (a = [a]), n = n.concat(a)
                            }
                            for (var r = 0, s = n.length; r < s; ++r) {
                                var l = "",
                                    c = n[r];
                                c.color && (l = c.color, c = c.message);
                                var d = i("<div>" + c + "</div>");
                                d.addClass(l), d.click(function() {
                                    i(this).hide()
                                }), o.append(d)
                            }
                            n.length > 0 && "maintenance" === t.type && window.triggerMaintenancePopup(n)
                        })
                    },
                    __initializeAdminMessages: function() {
                        var e = this;
                        e.__currentlyLoadedOffer = 0, e.__updateTimeLeftInterval = 0;
                        var t = 5e3,
                            n = 1e4;
                        setTimeout(e.__fetchBonusOffer.bind(this), Math.random() * n + t), setTimeout(e.__fetchGlobalMessages.bind(this), Math.random() * n + t)
                    },
                    __handleModalMessages: function() {
                        function e() {
                            if (0 === t.length) return void i("#generic_modal").unbind("click", e);
                            var n = t.pop();
                            i("#generic_modal .window_title").html(n.title), i("#generic_modal .subtitle").html(""), i("#generic_modal .message").html(n.message), i("#generic_modal").show()
                        }
                        if (window.MODAL_MESSAGES && 0 !== window.MODAL_MESSAGES.length) {
                            var t = window.MODAL_MESSAGES;
                            delete window.MODAL_MESSAGES, i("#generic_modal").on("click", e), e()
                        }
                    },
                    __checkHardwareSurvey: function() {
                        var e = this;
                        m.GET("/gamecontrol/endpoints.php?ep=thorCheck", function(t, n) {
                            return t ? void console.error("Issue checking endpoint", t) : void(n.thorCheck && m.loadTemplate("thor_footer", function(t, n) {
                                if (t) return void console.error("Failed to load template thor_footer", t);
                                var a = i(i.parseHTML(n)),
                                    o = i("#thor_container");
                                a.prependTo(o), e.__thorTimeoutId = setTimeout(function() {
                                    window.saveThorStatus({
                                        message: "fail",
                                        webgl: !1
                                    })
                                }, 6e4)
                            }))
                        })
                    }
                })
            }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
        }).call(t, n(3))
    }, function(e, t, n) {
        e.exports = n(6)(2)
    }, function(e, t, n) {
        var a;
        (function(i) {
            "use strict";
            a = function(e, t, n) {
                var a = 1,
                    o = {},
                    r = [],
                    s = !1;
                n.exports = {
                    GET: function() {
                        function e(e) {
                            var t;
                            try {
                                t = JSON.parse(d.responseText)
                            } catch (e) {
                                return console.error("Failed to parse response: ", d.responseText), r(e)
                            }
                            return r(void 0, t)
                        }

                        function t(e) {
                            return r(e)
                        }

                        function a(e) {
                            return r(e)
                        }
                        var i = "",
                            o = null,
                            r = function() {},
                            s = function() {};
                        switch (0 === arguments.length, arguments.length) {
                            case 0:
                                throw console.error("Invalid Util.GET call.  A resourceName is required."), new Error("resourceName is required");
                            case 1:
                                i = arguments[0];
                                break;
                            case 2:
                                i = arguments[0], r = arguments[1];
                                break;
                            case 4:
                                s = arguments[3];
                            case 3:
                                i = arguments[0], o = arguments[1], r = arguments[2]
                        }
                        r = r || function() {};
                        var l = "GET",
                            c = i,
                            d = new XMLHttpRequest;
                        d.addEventListener("load", e, !1), d.addEventListener("error", t, !1), d.addEventListener("abort", a, !1), "function" == typeof s && d.addEventListener("progress", s, !1), o && (c += c.indexOf("?") === -1 ? "?" : "&", c += "data=" + encodeURIComponent(JSON.stringify(o)));
                        var u = n.exports.AUTH_TOKEN;
                        c += c.indexOf("?") === -1 ? "?snapi_auth=" + u : "&snapi_auth=" + u;
                        var p = "__ts=" + Date.now();
                        c += "&" + p, d.open(l, c, !0), d.send()
                    },
                    lockedPost: function(e, t, n) {
                        var a = this;
                        window.lockCodeCheck(function(i) {
                            t.authTok = i, a.POST(e, t, n)
                        })
                    },
                    POST: function(e, t, a) {
                        function i(e) {
                            var t;
                            try {
                                t = JSON.parse(c.responseText)
                            } catch (e) {
                                return a(e)
                            }
                            return a(void 0, t)
                        }

                        function o(e) {
                            return a(e)
                        }

                        function r(e) {
                            return a(e)
                        }
                        a = a || function() {};
                        var s = "POST",
                            l = e,
                            c = new XMLHttpRequest;
                        c.addEventListener("load", i, !1), c.addEventListener("error", o, !1), c.addEventListener("abort", r, !1), "function" == typeof progressCallback && c.addEventListener("progress", progressCallback, !1);
                        var d = null;
                        t instanceof FormData ? d = t : t && (d = JSON.stringify(t));
                        var u = n.exports.AUTH_TOKEN;
                        l += l.indexOf("?") === -1 ? "?snapi_auth=" + u : "&snapi_auth=" + u, c.open(s, l, !0), null === d ? c.send() : t instanceof FormData ? c.send(d) : (c.setRequestHeader("Content-Type", "application/json;charset=UTF-8"), c.send(d))
                    },
                    loadTemplate: function() {
                        function e(e) {
                            return a(void 0, l.responseText)
                        }

                        function t(e) {
                            return a(e)
                        }

                        function n(e) {
                            return a(e)
                        }
                        var a, i = arguments[0],
                            o = {};
                        2 === arguments.length ? a = arguments[1] : (o = arguments[1], a = arguments[2]);
                        var r = "POST",
                            s = "/templates/" + i,
                            l = new XMLHttpRequest;
                        l.addEventListener("load", e, !1), l.addEventListener("error", t, !1), l.addEventListener("abort", n, !1), l.open(r, s, !0), l.send(JSON.stringify(o))
                    },
                    setFlashClientReady: function() {
                        s = !0;
                        var e = this;
                        r.forEach(function(t) {
                            e.dispatchFlashMessage(t[0], t[1], t[2])
                        }), r = null
                    },
                    dispatchFlashMessage: function(e, t, n) {
                        if (!s) return void r.push([e, t, n]);
                        var i = document.querySelector("#game_swf_div");
                        if (null === i) return void console.error("Failed to dispatchFlashMessage " + e + ":", t);
                        var l = a++;
                        t.type = e, t.__callbackId = l, o[l] = n;
                        var c = JSON.stringify(t);
                        i.talkToFlash(c)
                    },
                    handleFlashMessage: function(e, t) {
                        if (!o[e]) return void console.error("Got callback from flash for unknown id: " + e, arguments);
                        var n = o[e];
                        delete o[e], n(t)
                    },
                    formatDecimal: function(e, t) {
                        if ("number" != typeof e) return "";
                        t = void 0 === t ? 2 : t;
                        var n = e.toFixed(t) + "";
                        return n.replace(/\B(?=(\d{3})+(?!\d))/g, "$&,")
                    },
                    storeAppRequest: function(e) {},
                    formatNumber: function(e) {
                        return (e + "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    },
                    getItemImagePaths: function(e, t) {
                        if (!e || 0 === e.length) return [];
                        var n = this,
                            a = e.split(t),
                            i = a.map(function(e, t) {
                                var a = e.trim();
                                return n.getItemImagePath(a, 60, 60, "gif")
                            });
                        return i
                    },
                    getItemImagePath: function(e, t, n, a) {
                        var i = "";
                        t && n && a && (i = "_" + t + "_" + n + "." + a), 0 == t && 0 == n && (i = "." + a);
                        var o = "0000",
                            r = o.substr(0, o.length - e.length) + e;
                        return "items/" + r.substr(0, 2) + "/" + r.substr(2, 2) + "/" + e + "/" + e + i
                    },
                    scrapeProductLink: function(e) {
                        var t = i.Deferred();
                        return FB.api("https://graph.facebook.com/", "post", {
                            id: e,
                            scrape: !0
                        }, function(e) {
                            !e || e.error ? t.reject(e) : t.resolve(e)
                        }), t
                    },
                    splitPrice: function(e) {
                        return "string" == typeof e ? e.split(".") : e.toFixed(2).split(".")
                    },
                    getPriceParts: function(e, t) {
                        var n = t || !1,
                            a = [];
                        return a = "string" == typeof e ? e.split(".") : e.toFixed(2).split("."), n && "00" === a[1] ? a.slice(0, 1) : a
                    },
                    getPricingString: function(e, t) {
                        var n = "",
                            a = [],
                            i = t || !1;
                        return n = "string" == typeof e ? e : e.toFixed(2), i && (a = n.split("."), a.length > 1 && "00" === a[1]) ? a[0] : n
                    },
                    sendUnloadNotice: function(e, t) {
                        var a = n.exports.AUTH_TOKEN,
                            i = e;
                        if (i += i.indexOf("?") === -1 ? "?snapi_auth=" + a : "&snapi_auth=" + a, navigator.sendBeacon) {
                            navigator.sendBeacon(i, t)
                        }
                    },
                    getHexColor: function(e) {
                        e || (e = 0);
                        for (var t = parseInt(e).toString(16); t.length < 6;) t = "0" + t;
                        return "#" + t
                    },
                    guid: function() {
                        function e(e) {
                            var t = (Math.random().toString(16) + "000000000").substr(2, 8);
                            return e ? "-" + t.substr(0, 4) + "-" + t.substr(4, 4) : t
                        }
                        return e() + e(!0) + e(!0) + e()
                    },
                    formatDateString: function(e, t, n) {
                        if (null === e) return null;
                        if ("date" !== t && "datetime" !== t && "datetime-local" != t) return e;
                        var a = new RegExp("[0-9]{4}-[0-9]{2}-[0-9]{2}"),
                            i = new RegExp("[0-9]{2}:[0-9]{2}"),
                            o = e.match(a),
                            r = e.match(i);
                        return null === o ? e : (e = o[0], "datetime" !== t && "datetime-local" !== t || (n = null == n ? " " : n, e += n + (null !== r ? r[0] : "00:00")), e)
                    },
                    adjustColor: function(e, t) {
                        var n = parseInt(e.substring(1, 3), 16),
                            a = parseInt(e.substring(3, 5), 16),
                            i = parseInt(e.substring(5, 7), 16);
                        n = parseInt(n * (100 + t) / 100), a = parseInt(a * (100 + t) / 100), i = parseInt(i * (100 + t) / 100), n = n < 255 ? n : 255, a = a < 255 ? a : 255, i = i < 255 ? i : 255;
                        var o = 1 == n.toString(16).length ? "0" + n.toString(16) : n.toString(16),
                            r = 1 == a.toString(16).length ? "0" + a.toString(16) : a.toString(16),
                            s = 1 == i.toString(16).length ? "0" + i.toString(16) : i.toString(16);
                        return "#" + o + r + s
                    }
                }
            }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
        }).call(t, n(3))
    }, function(e, t, n) {
        e.exports = n(6)(1)
    }, function(e, t, n) {
        var a, i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
            return typeof e
        } : function(e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        };
        a = function(e, t, n) {
            n.exports = {
                create: function() {
                    for (var e = arguments[arguments.length - 1].initialize || function() {}, t = 0; t < arguments.length; ++t) {
                        var n = arguments[t];
                        if ("object" !== ("undefined" == typeof n ? "undefined" : i(n)) && "function" != typeof n) throw new TypeError("Expected object or function, got " + ("undefined" == typeof n ? "undefined" : i(n)) + ": " + n);
                        if ("function" == typeof n) e.prototype = Object.create(n.prototype);
                        else
                            for (var a = Object.keys(n), o = 0; o < a.length; ++o) e.prototype[a[o]] = n[a[o]]
                    }
                    return e
                }
            }
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, , function(e, t) {
        e.exports = CommonDependencies
    }, function(e, t, n) {
        var a;
        a = function(e, t, n) {
            n.exports = {
                DIALOG_TARGET_TYPE: "Dialog",
                LIFE_CYCLE_TYPE: "Life Cycle Event",
                IAP_TARGET_TYPE: "IAP Event",
                GENERIC_DIALOG_CREATED: "Generic Dialog Created",
                DIALOG_LOADED: "Dialog Loaded",
                DIALOG_SHOWN: "Dialog Shown",
                DIALOG_HIDDEN: "Dialog Hidden",
                DIALOG_BUTTON_CLICKED: "Dialog Button Clicked",
                DIALOG_LIST_ITEM: "Dialog List Item Clicked",
                SEARCHING_STARTED: "Searching Started",
                SEARCHING_CLEARED: "Searching Cleared",
                DIALOG_TAB_CLICKED: "Dialog Tab Clicked",
                GAME_TAB_CLICKED: "Game Tab Clicked",
                DIALOG_LINK_CLICKED: "Dialog Link Clicked",
                DIALOG_TEXT_ENTRY: "Dialog Text Entry",
                DIALOG_LIST_COLUMN: "Dialog List Column Clicked",
                FREE_GIFT_ITEM_CLICKED_EVENT: "Free Gift Item Clicked",
                STOCK_TARGET_ID: "Stocks Dialog",
                STOCK_CONFIRM_PURCHASE_TARGET_ID: "Stocks Confirm Purchase Dialog",
                STOCK_CONFIRM_SALE_TARGET_ID: "Stocks Confirm Sale Dialog",
                STOCK_EARN_MORE_TARGET_ID: "Stocks Earn More Dialog",
                SESSION_START: "SESSION_START",
                SESSION_END: "SESSION_END",
                IAP_CLICKED: "IAP_CLICKED",
                IAP_SUCCESS: "IAP_SUCCESS",
                IAP_DIRECT: "IAP_DIRECT",
                IAP_PROMO: "IAP_PROMO",
                IAP_FAIL: "IAP_FAIL"
            }
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        e.exports = n(6)(4)
    }, , , function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(8);
            a.exports = i.createClass({
                displayName: "exports",
                __renderLayer: function() {
                    var e = i.createElement("div", null);
                    this.props.show && (e = i.createElement("div", null, i.createElement("div", {
                        id: this.props.uniqueId,
                        className: this.props.classes
                    }, this.props.content))), o.render(e, this.renderTarget)
                },
                componentDidUpdate: function() {
                    this.__renderLayer()
                },
                componentDidMount: function() {
                    this.renderTarget = document.createElement("div"), document.body.appendChild(this.renderTarget), this.__renderLayer()
                },
                componentWillUnmount: function() {
                    o.unmountComponentAtNode(this.renderTarget), document.body.removeChild(this.renderTarget)
                },
                getDefaultProps: function() {
                    return {
                        show: !1,
                        content: null,
                        modal: !1,
                        classes: "popup  popup--default",
                        uniqueId: ""
                    }
                },
                render: function() {
                    return i.createElement("div", null)
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, n) {
            n.exports = {
                TYPE_VIP: "vip",
                TYPE_PREMIUM_VIP: "premiumVip",
                TYPE_UPGRADE_PREMIUM_VIP: "vip-upgrade",
                TYPE_FREE: "free",
                PANEL_DETAIL_PREMIUM_VIP: "PREMIUM_PANEL",
                PANEL_UPGRADE_PREMIUM_VIP: "UPGRADE_PREMIUM_PANEL",
                PANEL_DETAIL_VIP: "VIP_PANEL",
                PANEL_SELL_PREMIUM_VIP: "SELL_PREMIUM_PANEL",
                PANEL_SELL_VIP: "SELL_VIP_PANEL",
                PANEL_SELL_FREE: "SELL_FREE_PANEL"
            }
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            function i() {}
            var o = n(20);
            Semaphore = function(e) {
                this.reset(e)
            }, Semaphore.prototype.reset = function(e) {
                o.assertIsFunction(e), this.__count = 0, this.__callback = e, this.__error = void 0
            }, Semaphore.prototype.wait = function(e) {
                o.isUndefined(e) && (e = 1), this.__count += e
            }, Semaphore.prototype.signalError = function(e, t) {
                this.__error || (this.__error = e), this.signal(t)
            }, Semaphore.prototype.signal = function(e) {
                o.isUndefined(e) && (e = 1), 0 !== this.__count && (this.__count -= e, this.__count <= 0 && (this.__callback(this.__error), this.__callback = i, this.__count = 0))
            }, a.exports = Semaphore
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, , function(e, t, n) {
        e.exports = n(6)(10)
    }, function(e, t, n) {
        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function i(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" !== ("undefined" == typeof t ? "undefined" : s(t)) && "function" != typeof t ? e : t
        }

        function o(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + ("undefined" == typeof t ? "undefined" : s(t)));
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        var r, s = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
                return typeof e
            } : function(e) {
                return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
            },
            l = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var a = t[n];
                        a.enumerable = a.enumerable || !1, a.configurable = !0, "value" in a && (a.writable = !0), Object.defineProperty(e, a.key, a)
                    }
                }
                return function(t, n, a) {
                    return n && e(t.prototype, n), a && e(t, a), t
                }
            }();
        r = function(e, t, r) {
            var s = n(1),
                c = n(2),
                d = function(e) {
                    function t() {
                        return a(this, t), i(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments))
                    }
                    return o(t, e), l(t, [{
                        key: "render",
                        value: function() {
                            var e = this,
                                t = "//" + e.props.webHostName,
                                n = t + "cdn/",
                                a = "cash" == this.props.currencyType ? "product-entry" : "product-entry product-entry--coins";
                            if (this.props.extraAmount > 0 && (a += " has-extra"), this.props.bonusAmount > 0 && (a += " has-bonus"), this.props.vipAmount > 0 && (a += " has-vip"), this.props.id) {
                                s.createElement("div", null, s.createElement("b", null, "ID: ", this.props.id))
                            }
                            if (this.props.bonusOfferId) {
                                s.createElement("div", null, s.createElement("b", null, "bonusOfferId: ", this.props.bonusOfferId))
                            }
                            if (this.props.platform) {
                                s.createElement("div", null, s.createElement("b", null, "platform: ", this.props.platform))
                            }
                            var i = "cash" == this.props.currencyType ? " YoCash" : "";
                            if (this.props.baseAmount) var o = s.createElement("div", {
                                className: "iap-base-amount"
                            }, s.createElement("span", {
                                className: "amount"
                            }, c.formatNumber(this.props.baseAmount)), s.createElement("span", {
                                className: "currency"
                            }, i));
                            if (this.props.saleCost && this.props.saleCost > 0 && this.props.saleCost != this.props.cost) var r = s.createElement("div", {
                                className: "iap-cost--sale"
                            }, "$", c.formatNumber(Number(this.props.saleCost).toFixed(2)));
                            if (this.props.extraAmount > 0) var l = c.formatNumber(this.props.extraAmount),
                                d = s.createElement("div", {
                                    className: "extra-amount"
                                }, "+", l, " Extra");
                            if (this.props.bonusAmount > 0) var u = c.formatNumber(this.props.bonusAmount),
                                p = s.createElement("div", {
                                    className: "bonus-amount"
                                }, "+", u, " Bonus");
                            if (this.props.vipAmount > 0) var m = c.formatNumber(this.props.vipAmount),
                                h = s.createElement("div", {
                                    className: "vip-amount"
                                }, "+", m, " VIP");
                            if (this.props.giveItems) var _ = c.getItemImagePaths(this.props.giveItems, ",").map(function(e, t) {
                                    var a = n + e.trim();
                                    return s.createElement("img", {
                                        className: "bonus-image",
                                        src: a,
                                        alt: ""
                                    })
                                }),
                                f = s.createElement("div", {
                                    className: "bonus-items"
                                }, _);
                            var g = Number(this.props.baseAmount) + Number(this.props.extraAmount) + Number(this.props.bonusAmount) + (Number(this.props.vipAmount) || 0),
                                v = c.formatNumber(g),
                                b = s.createElement("div", {
                                    className: "iap-description-col"
                                }, s.createElement("div", null, s.createElement("span", {
                                    className: "total-amount stroke"
                                }, v), s.createElement("span", {
                                    className: "total-currency stroke"
                                }, i)));
                            if (this.props.imagePath) var y = e.props.imagePath,
                                E = s.createElement("div", {
                                    className: "product-image"
                                }, s.createElement("img", {
                                    src: y,
                                    alt: ""
                                }));
                            if (1 == this.props.mostPopular) {
                                a += " most-popular";
                                var y = t + "images/in_app_purchase/most_popular.png",
                                    w = s.createElement("img", {
                                        className: "iap-banner",
                                        src: y,
                                        alt: "Most Popular"
                                    })
                            }
                            if (1 == this.props.bestValue) {
                                a += " best-value";
                                var y = t + "images/in_app_purchase/best_value.png",
                                    C = s.createElement("img", {
                                        className: "iap-banner",
                                        src: y,
                                        alt: "Best Value"
                                    })
                            }
                            var k = s.createElement("div", {
                                className: "base-amount-col"
                            }, o, d, p, h);
                            if (this.props.cost) var T = r ? "iap-cost--strike" : "iap-cost--regular",
                                S = s.createElement("div", {
                                    className: "iap-cost"
                                }, s.createElement("div", {
                                    className: T
                                }, "$", this.props.cost), r);
                            if ("facebook" == this.props.platform) var I = s.createElement("button", {
                                className: "btn--iap"
                            }, "Buy");
                            else var P = t + "images/in_app_purchase/pay-button.png",
                                A = s.createElement("img", {
                                    src: P,
                                    alt: ""
                                }),
                                I = s.createElement("button", {
                                    className: "btn--pay"
                                }, A);
                            var N = function() {
                                return !1
                            };
                            return this.props.onClick && (N = this.props.onClick), s.createElement("div", {
                                className: a,
                                onClick: N
                            }, E, k, b, s.createElement("div", {
                                className: "buy-col"
                            }, f, w, C, S, I))
                        }
                    }]), t
                }(s.Component);
            d.displayName = "ProductComponent", d.propTypes = {
                id: s.PropTypes.number,
                referenceName: s.PropTypes.string,
                kellaaProductId: s.PropTypes.string,
                bonusOfferId: s.PropTypes.string,
                platform: s.PropTypes.string,
                currencyType: s.PropTypes.string,
                cost: s.PropTypes.string,
                saleCost: s.PropTypes.string,
                baseAmount: s.PropTypes.string,
                extraAmount: s.PropTypes.string,
                bonusAmount: s.PropTypes.string,
                vipAmount: s.PropTypes.string,
                giveItems: s.PropTypes.string,
                description: s.PropTypes.string,
                imagePath: s.PropTypes.string,
                mostPopular: s.PropTypes.number,
                bestValue: s.PropTypes.number,
                webHostName: s.PropTypes.string,
                onClick: s.PropTypes.func
            }, r.exports = d
        }.call(t, n, t, e), !(void 0 !== r && (e.exports = r))
    }, , function(e, t, n) {
        var a;
        (function(i) {
            "use strict";
            a = function(e, t, a) {
                var o = n(1),
                    r = n(8),
                    s = n(4),
                    l = n(2),
                    c = n(13),
                    d = n(7),
                    u = n(19),
                    p = window.REST_SERVER_ROOT ? window.REST_SERVER_ROOT : "https://yostageweb.yoworld.com/rest/",
                    m = "facebook/products",
                    h = (window.PRODUCT_HOST ? window.PRODUCT_HOST : "https://iap-stage.bigvikinggames.com/") + "facebook_product?productId=",
                    _ = {
                        1383001: "Unknown",
                        1383004: "InvalidOperation",
                        1383005: "PermissionDenied",
                        1383006: "DatabaseError",
                        1383007: "InvalidApp",
                        1383008: "AppNoResponse",
                        1383009: "AppErrorResponse",
                        1383010: "UserCanceled",
                        1383043: "AppInfoFetchFailure",
                        1383044: "InvalidAppInfo",
                        1383045: "AppInvalidEncodedResponse",
                        1383046: "AppInvalidDecodedResponse",
                        1383047: "AppInvalidMethodResponse",
                        1383048: "AppMissingContentResponse",
                        1383049: "AppUnknownResponseError",
                        1383050: "AppUserValidationFailedResponse",
                        1383051: "AppInvalidItemParam",
                        1383053: "FbobjFailure",
                        1383068: "RateLimitExceeded",
                        1383104: "InvalidPricepointId",
                        1383105: "AppInvalidApi",
                        1353001: "CardAlreadyDisabled",
                        1353008: "CreditCardError",
                        1353009: "TradeRestriction",
                        1353011: "SecurityRestriction",
                        1353013: "UserDisabled",
                        1353014: "PaypalError",
                        1353026: "BAInvalidCredential",
                        1353029: "BAOperationNotAllowed",
                        1353030: "BAAppIssue",
                        1353031: "TrialNotAllowed",
                        1353032: "SubscriptionAlreadyExists",
                        1353033: "DummyCredentialExpectedFailure",
                        1353048: "PaymentCredentialDeclined",
                        1383146: "InvalidOGType"
                    },
                    f = 5,
                    g = 5e4,
                    v = 18,
                    b = "exclusive",
                    y = "promo",
                    E = "bundle",
                    w = 0;
                a.exports = s.create({
                    __accessTab: null,
                    __btnRepayment: null,
                    __modalLayer: null,
                    __showExternalUIModal: null,
                    __hideExternalUI: null,
                    __cashProductContainer: null,
                    __coinProductContainer: null,
                    __purchasingProduct: !1,
                    __isPremium: !1,
                    __vipChecked: !1,
                    __showing: !1,
                    __titleText: null,
                    initialize: function(e, t, n, a, i) {
                        var o = document.querySelector("#" + e);
                        return null === o ? void console.error("Invalid selector #" + e + ".  Purchase functionality will be disabled.") : (null !== i && "undefined" != typeof i && (this.__btnRepayment = i), this.__showExternalUIModal = n, this.__hideExternalUI = a, this.__accessTab = o, void(this.__modalLayer = t))
                    },
                    load: function(e) {
                        function t(e) {
                            e ? a.signalError(e) : a.signal()
                        }
                        if (null == this.__accessTab) return void console.error("Cannot load purchase manager without an access tab.");
                        var n = this,
                            a = new c(function(t) {
                                return t ? void console.error("Error occured trying to load Purchase Manager.  Purchase functionality will be disabled.", t) : (n.__activateHeaderTab(), void(e && e()))
                            });
                        a.wait(1), this.__createContainer(t)
                    },
                    hidePurchasePopup: function() {
                        this.__hideContainer(), this.__modalLayer.hide(), this.__showing = !1
                    },
                    setTime: function(e) {
                        this.__showing && this.__titleText.text("Hurry! Deal Leaves in " + e)
                    },
                    showSaleBadge: function(e) {
                        var t = i("#" + this.__accessTab.id);
                        e ? t.addClass("sale") : t.removeClass("sale")
                    },
                    triggerPurchasePopup: function() {
                        var e = this;
                        l.POST("/gamecontrol/lock_code_endpoints.php?ep=getLockedStatus&" + clientAuth, {}, function(t, n) {
                            return t ? void console.error(t) : n.error ? void console.error(n.error) : void(n.locked || "3" === n.level ? e.__showExternalUIModal("lockCodes", {
                                type: "purchase",
                                trigger: e.showPopup.bind(e)
                            }) : e.showPopup())
                        })
                    },
                    productCheckVIP: function(e) {
                        var t = this;
                        l.dispatchFlashMessage("subscriptionRequest", {
                            productId: 0
                        }, function(n) {
                            t.__vipChecked = !0, e(n)
                        })
                    },
                    setVipStatus: function(e) {
                        this.__isPremium = e, this.__vipChecked = !0
                    },
                    showPopup: function() {
                        function e(e) {
                            e.status && e.paymentStatus && e.subType.indexOf("premiumVip") > -1 ? n.__isPremium = !0 : n.__isPremium = !1, n.__fetchProducts(t);
                        }

                        function t(e, t) {
                            var a = n.__isPremium;
                            return e ? void n.__displayError(e) : (n.__updateProducts(t, a), n.__hiddenTab = n.__cashProductContainer, n.__visibleTab = n.__coinProductContainer, n.__setTab("cash"), n.__showing = !0, void 0)
                        }
                        var n = this;
                        return "undefined" != typeof chargebackRepaymentOwed && chargebackRepaymentOwed > 0 ? void n.__showExternalUIModal("chargebackRepayment") : (n.__modalLayer.show(), n.__clearContainer(), n.__showContainer(), void n.productCheckVIP(e))
                    },
                    __displayError: function(e) {
                        console.error("ERROR: ", e)
                    },
                    __fetchProducts: function(e) {
                        l.GET(p + this._getProductsEndpoint(), null, e)
                    },
                    __fetchItemsPreview: function(e, t) {
                        l.GET("/gamecontrol/get_item_preview.php?give_items=" + e, null, t)
                    },
                    _getProductsEndpoint: function() {
                        return m
                    },
                    __activateHeaderTab: function() {
                        console.debug("Activating purchase tab"), this.__accessTab.className = "cash-tab", null !== this.__btnRepayment && this.__btnRepayment.removeClass("inactive");
                        var e = this;
                        this.__accessTab.addEventListener("click", function() {
                            e.__onTabClick()
                        })
                    },
                    __onTabClick: function() {
                        var e = this;
                        window.fireDataEvent && window.fireDataEvent(d.GAME_TAB_CLICKED, {
                            tab: "Add Cash"
                        }), e.triggerPurchasePopup()
                    },
                    __createContainer: function(e) {
                        var t = this;
                        l.loadTemplate("modal_purchase", function(n, a) {
                            if (n) return console.error("Failed to load modal_purchase template: ", n), e(n);
                            var o = i(i.parseHTML(a));
                            t.__container = o, t.__productContainer = i(o.find("#purchase_page")), t.__titleText = i(o.find("#purchase_page_text")), i(o.find("#tab_yocash")).click(function() {
                                t.__setTab("cash")
                            }), i(o.find("#tab_yocoins")).click(function() {
                                t.__setTab("coins")
                            }), i(o.find(".close_button")).click(function() {
                                console.debug("close"), t.__hideContainer(), t.__modalLayer.hide()
                            });
                            var r = t.__modalLayer.getContainer();
                            t.__modalLayer && t.__container.prependTo(r), t.__hideContainer(), e()
                        })
                    },
                    __clearContainer: function() {
                        this.__productContainer.empty()
                    },
                    __showContainer: function() {
                        var e = this.__modalLayer.getZIndex();
                        this.__container.show(), this.__container.css("zIndex", e + 1)
                    },
                    __hideContainer: function() {
                        this.__container.hide(), this.__showing = !1
                    },
                    __updateProducts: function(e, t) {
                        function n(e) {
                            return e.reference_name.indexOf(b) < 0
                        }

                        function a(e) {
                            var n = e.cost,
                                a = /costs.([0-9,%E]*)/i,
                                i = e.reference_name.match(a);
                            if (null != i)
                                for (var o = i[1].split(","), r = 0; r < o.length; r++) {
                                    var s = parseFloat(unescape(o[r]));
                                    s > n && (n = s)
                                }
                            return t && n > v ? e.reference_name.indexOf("vip") > -1 : !(!t && n > v) || e.reference_name.indexOf("vip") === -1
                        }
                        e.coinProducts = e.coinProducts.filter(n), e.buckProducts = e.buckProducts.filter(n), e.buckProducts = e.buckProducts.filter(a), e.coinProducts.sort(function(e, t) {
                            return t.cost - e.cost
                        }), e.buckProducts.sort(function(e, t) {
                            return t.cost - e.cost
                        }), this.__coinProducts = e.coinProducts, this.__cashProducts = e.buckProducts;
                        for (var i = 0; i < this.__coinProducts.length; i++) this.__populateProduct(this.__coinProducts[i]);
                        for (var i = 0; i < this.__cashProducts.length; i++) this.__populateProduct(this.__cashProducts[i])
                    },
                    __populateProduct: function(e) {
                        var t = this.__getDetailsByReferenceName(e.reference_name);
                        return e.baseAmount = t.baseAmount, e.extraAmount = t.extraAmount, e.bonusAmount = t.bonusAmount, e.vipAmount = t.vipAmount, e.currencyType = t.currencyType, e.cost = t.cost, e.saleCost = t.saleCost, e.bestValue = t.bestValue, e.mostPopular = t.mostPopular, e.giveItems = t.giveItems, e
                    },
                    __setTab: function(e) {
                        i("#tab_yocash").removeClass("selected"), i("#tab_yocoins").removeClass("selected"), i("#tab_yocash").removeClass("active"), i("#tab_yocoins").removeClass("active"), this.__clearContainer();
                        var t = null,
                            n = "";
                        switch (e) {
                            case "cash":
                                t = this.__cashProducts, n = "YoCash", i("#tab_yocash").addClass("selected"), i("#tab_yocash").addClass("active");
                                break;
                            case "coins":
                                t = this.__coinProducts, n = "YoCoins", i("#tab_yocoins").addClass("selected"), i("#tab_yocoins").addClass("active");
                                break;
                            default:
                                return void console.error("unknown tab request")
                        }
                        var a = this.__productContainer,
                            s = document.createElement("ul");
                        s.setAttribute("class", "purchase_list");
                        var l = this;
                        r.render(o.createElement(u, {
                            products: t,
                            platform: l.__getPlatform(),
                            webHostName: window.WEB_HOSTNAME,
                            onClick: l.doPurchaseProduct.bind(l)
                        }), s), a.append(s)
                    },
                    __getPlatform: function() {
                        return "facebook"
                    },
                    __getDetailsByReferenceName: function(e) {
                        var t = "coins",
                            n = "cash",
                            a = "costs",
                            i = "best",
                            o = "most",
                            r = "amounts",
                            s = "items",
                            l = 2;
                        e.indexOf("vip") > -1 && (l = 3);
                        for (var c = e.split("."), d = /bonus/.test(e), u = /items/.test(e), p = 0, m = c[l], h = 0, _ = "", f = 0, g = 0, v = 0, w = 0, C = 0, k = 0, T = 0, S = 0, I = 0, P = 0, A = 0, N = 0, L = l; L < c.length; ++L) {
                            var R = c[L];
                            if (R != t && R != n || (f = h = +c[++L]), R == a) {
                                var D = c[++L].split("%2E").join(".");
                                D = JSON.parse("[" + D + "]"), A = D[0], N = D[1]
                            }
                            if (R == r) {
                                var F = JSON.parse("[" + c[++L] + "]");
                                g = F[0], v = F[1], w = F[2], C = F[3]
                            }
                            R == i && (L++, k = 1), R == o && (L++, T = 1), R == s && (_ = c[++L]), R == b && (p = c[++L]), R == y && (S = +c[++L]), R == E && (I = +c[++L])
                        }
                        return {
                            cost: A,
                            saleCost: N,
                            baseAmount: g,
                            extraAmount: v,
                            bonusAmount: w,
                            vipAmount: C,
                            totalAmount: f,
                            giveAmount: h,
                            bestValue: k,
                            mostPopular: T,
                            giveItems: _,
                            currencyType: m,
                            isBonus: d,
                            bonusOfferId: P,
                            doesGiveItems: u,
                            exclusiveId: p,
                            promoId: S,
                            bundleId: I
                        }
                    },
                    calculateBaseValue: function(e, t) {
                        if ("YoCash" === e) {
                            var n = t;
                            return n < 5 && (n = Math.round(t)), Math.ceil(n * f)
                        }
                        var n = t;
                        return n < 1 && (n = Math.round(t)), Math.round(n * g)
                    },
                    purchaseDataEvent: function(e, t, n, a) {
                        var i = this.__getDetailsByReferenceName(e.reference_name),
                            o = "iap";
                        i.bundleId > 0 ? o = "bundle" : i.promoId > 0 ? o = "promo" : i.bonusOfferId ? iapTye = "bonusOffer" : i.saleCost > 0 && (o = "saleOffer");
                        var r = i.saleCost > 0 ? i.saleCost : i.cost,
                            s = {
                                iapName: e.reference_name,
                                iapType: o,
                                iapCost: r,
                                iapYoCashAmt: "cash" === i.currencyType ? i.baseAmount : 0,
                                iapYoCoinAmt: "cash" !== i.currencyType ? i.baseAmount : 0
                            };
                        n.id !== w && (s.transactionId = n.id), void 0 !== n.yoCash && void 0 !== n.yoCoins && void 0 !== n.level && void 0 !== n.yoPoints && (s.yoCash = n.yoCash, s.yoCoins = n.yoCoins, s.level = n.level, s.yoPoints = n.yoPoints, t === d.IAP_SUCCESS && (s.yoCash = n.yoCash + s.iapYoCashAmt, s.yoCoins = n.yoCoins + s.iapYoCoinAmt)), void 0 !== a && (s.errorCode = void 0 !== _[a] ? _[a] : a);
                        var l = e.reference_name.indexOf("bundle") > 0,
                            c = e.reference_name.indexOf("promo") > 0,
                            u = e.reference_name.indexOf("piggybank") > 0;
                        (l || c || u) && (s.iapType = u ? "piggyBank" : "bundle", s.iapCost = n.productPrice, s.iapYoCashAmt = n.productCash, s.iapYoCoinAmt = n.productCoins), window.fireDataEvent && window.fireDataEvent(t, s, e.reference_name, d.IAP_TARGET_TYPE, !1)
                    },
                    getTransactionInfo: function(e) {
                        var t = {
                            id: w
                        };
                        return void 0 !== e && void 0 !== e.referenceName && e.referenceName.indexOf("chargeback_repayment") === -1 && (t = {
                            id: w,
                            productPrice: e.productPrice,
                            productCash: e.productCash,
                            productCoins: e.productCoins
                        }), t
                    },
                    doPurchaseProduct: function(e, t) {
                        if (!this.__purchasingProduct) {
                            var n = this.getTransactionInfo(t);
                            this.purchaseDataEvent(e, d.IAP_CLICKED, n), this.__purchaseProduct(e, t), this.__purchasingProduct = !0
                        }
                    },
                    triggerRefNamePurchase: function(e) {
                        var t = this,
                            n = {
                                id: w
                            };
                        this.__fetchProducts(function(a, i) {
                            if (a) return void t.__displayError(a);
                            for (var o = 0; o < i.promotionProducts.length; ++o) {
                                var r = i.promotionProducts[o];
                                if (r.reference_name == e) {
                                    t.purchaseDataEvent(r, d.IAP_DIRECT, n), t.doPurchaseProduct(r);
                                    break
                                }
                            }
                        })
                    },
                    triggerProductPurchase: function(e) {
                        var t = this,
                            n = e.referenceName,
                            a = {
                                id: w,
                                productPrice: e.productPrice || 0,
                                productCash: e.productCash || 0,
                                productCoins: e.productCoins || 0,
                                yoCash: e.playerYoCash || 0,
                                yoCoins: e.playerYoCoins || 0,
                                level: e.playerLevel || 0,
                                yoPoints: e.playerYoPoints || 0
                            };
                        this.__fetchProducts(function(i, o) {
                            if (i) return void t.__displayError(i);
                            for (var r = 0; r < o.promotionProducts.length; ++r) {
                                var s = o.promotionProducts[r];
                                if (s.reference_name == n) return t.purchaseDataEvent(s, d.IAP_PROMO, a), void t.doPurchaseProduct(s, e)
                            }
                            console.error("could not find product " + n)
                        })
                    },
                    __addSubscriptionEntry: function(e, t) {
                        var n = document.createElement("div");
                        n.setAttribute("class", "product-cards");
                        var a = document.createElement("div"),
                            i = document.createElement("div"),
                            o = document.createElement("img"),
                            r = document.createElement("button"),
                            s = document.createElement("button");
                        a.setAttribute("class", "left-col"), i.setAttribute("class", "right-col"), o.setAttribute("src", "images/in_app_purchase/giftcards.jpg"), o.setAttribute("alt", "gift cards"), o.setAttribute("class", "cards"), r.setAttribute("type", "button"), r.setAttribute("class", "btn  btn-positive  btn--buy"), r.innerHTML = "Subscription Subscription", s.setAttribute("type", "button"), s.setAttribute("class", "btn  btn-positive  btn--buy"), s.innerHTML = "Buy NOW!!!", a.appendChild(r), i.appendChild(s), n.appendChild(o), n.appendChild(a), n.appendChild(i), e.append(n)
                    },
                    __addGiftcardEntry: function(e, t) {
                        var n = document.createElement("div");
                        n.setAttribute("class", "product-cards");
                        var a = document.createElement("div"),
                            i = document.createElement("div"),
                            o = document.createElement("img"),
                            r = document.createElement("button"),
                            s = document.createElement("button");
                        a.setAttribute("class", "left-col"), i.setAttribute("class", "right-col"), o.setAttribute("src", "images/in_app_purchase/giftcards.jpg"), o.setAttribute("alt", "gift cards"), o.setAttribute("class", "cards"), r.setAttribute("type", "button"), r.innerHTML = "Redeem Cards", s.setAttribute("type", "button"), s.innerHTML = "Find a Store", a.appendChild(r), i.appendChild(s), n.appendChild(o), n.appendChild(a), n.appendChild(i), e.append(n)
                    },
                    __purchaseProduct: function(e, t) {
                        function n(e, t, n) {
                            var i = {
                                method: "pay",
                                action: "purchaseitem",
                                product: c,
                                request_id: t
                            };
                            FB.ui(i, a)
                        }

                        function a(t) {
                            var a = 1383010,
                                p = 1383146;
                            if (o.__purchasingProduct = !1, console.debug("Facebook response: ", t), "undefined" == typeof t || null == t || "undefined" == typeof t.payment_id || 0 == t.payment_id) return !s && t.error_code && t.error_code != a && t.error_code != p ? (s = !0, void l.scrapeProductLink(c).done(function() {
                                n(i, r, !0)
                            })) : (o.purchaseDataEvent(e, d.IAP_FAIL, u, t.error_code), void o.__purchaseFailed(t));
                            var m = t.payment_id,
                                h = o.__getDetailsByReferenceName(e.reference_name);
                            t.exclusiveId = h.exclusiveId, t.promoId = h.promoId, t.bundleId = h.bundleId, l.dispatchFlashMessage("purchaseResponse", {
                                data: t
                            }, function(t) {
                                u.id = m, o.purchaseDataEvent(e, d.IAP_SUCCESS, u), o.__purchaseSuccess(m, e), console.debug("Kellaa transaction id: " + t.paymentId)
                            })
                        }
                        var i = e.kellaaProductId,
                            o = this,
                            r = 0,
                            s = !1,
                            c = h + i,
                            u = this.getTransactionInfo(t);
                        l.dispatchFlashMessage("purchaseRequest", {
                            productId: i
                        }, function(e) {
                            return r = e.id, u.yoCash = e.playerYoCash, u.yoCoins = e.playerYoCoins, u.level = e.playerLevel, u.yoPoints = e.playerYoPoints, e.errorMessage ? (o.__purchasingProduct = !1, void o.__purchaseFailed({
                                error_code: 0,
                                errorMessage: e.errorMessage
                            })) : void n(i, r)
                        })
                    },
                    __purchaseFailed: function(e) {
                        var t = e.error_code;
                        1383010 !== t && (this.__hideContainer(), this.__modalLayer.hide(), i("#generic_modal .window_title").html("Transaction Failed"), i("#generic_modal .subtitle").html(""), i("#generic_modal .message").html("There was an error processing your transaction."), i("#generic_modal .message").append("<br />If this problem persists, please contact support."), i("#generic_modal").show())
                    },
                    __purchaseSuccess: function(e, t) {
                        var n = this;
                        return t.reference_name.indexOf("piggybank") !== -1 ? void l.dispatchFlashMessage("smashPiggyBank", {}, n.__showPurchaseSuccess.bind(n, e, t)) : void this.__showPurchaseSuccess(e, t)
                    },
                    __showPurchaseSuccess: function(e, t) {
                        this.__hideContainer();
                        var n = this.__getDetailsByReferenceName(t.reference_name),
                            a = e,
                            o = +n.giveAmount,
                            r = "cash" === n.currencyType ? "YoCash" : "YoCoins",
                            s = this.calculateBaseValue(r, t.cost),
                            l = o - s,
                            c = t.reference_name.indexOf("repayment") !== -1;
                        if ("YoCoins" === r || c) return i("#generic_modal .window_title").html("Transaction Complete"), i("#generic_modal .subtitle").html(""), i("#generic_modal .message").html("<div>Please keep this ID as a record of your transaction.</div>"), i("#generic_modal .message").append("<div class='alert alert--success section'>" + e + "</div>"), i("#generic_modal .message").append("<div>We will require the <b>Transaction ID</b> above if there are ever any issues with your purchase.</div>"), c && (this.__hideExternalUI("chargebackRepayment"), this.__btnRepayment.hide(), i("#generic_modal .message").append("Once you have noted this transaction, please refresh to continue playing YoWorld."), i("#chargebacksOwed .popup__main").empty(), i("#chargebacksOwed .message").html("Please refresh this page to play YoWorld.")), i("#generic_modal").show(), void this.__modalLayer.hide();
                        var d = "purchase_animation.swf?v=1",
                            u = "swf_stack_div",
                            p = "100%",
                            m = "100%",
                            h = "11.1",
                            _ = window.EXPRESS_INSTALL_URL,
                            f = {
                                purchaseAmount: s,
                                bonusAmount: l,
                                transactionId: a,
                                currencyName: r,
                                isExclusive: n.exclusiveId ? "1" : ""
                            },
                            g = {
                                allowScriptAccess: "always",
                                allowFullScreen: "true",
                                allowFullScreenInteractive: "true",
                                wmode: "transparent",
                                scale: "noscale"
                            },
                            v = {};
                        "ga" in window && (ga("ecommerce:addTransaction", {
                            id: a,
                            affiliation: "FB",
                            revenue: t.cost.toFixed(2),
                            shipping: "0",
                            tax: "0"
                        }), ga("ecommerce:send"), ga("ecommerce:clear"));
                        var b = this;
                        window.closeTransactionCompleteWindow = function() {
                            i("#swf_stack_wrap").html('<div id="swf_stack_div"></div>'), i("#swf_stack_wrap").hide(), b.__modalLayer.hide(), delete window.closeTransactionCompleteWindow
                        }, i("#swf_stack_wrap").show(), i("#swf_stack_wrap").css("zIndex", this.__modalLayer.getZIndex() + 1), swfobject.embedSWF(d, u, p, m, h, _, f, g, v)
                    }
                })
            }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
        }).call(t, n(3))
    }, function(e, t, n) {
        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function i(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function o(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        var r, s = function() {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var a = t[n];
                    a.enumerable = a.enumerable || !1, a.configurable = !0, "value" in a && (a.writable = !0), Object.defineProperty(e, a.key, a)
                }
            }
            return function(t, n, a) {
                return n && e(t.prototype, n), a && e(t, a), t
            }
        }();
        r = function(e, t, r) {
            var l = n(1),
                c = n(16),
                d = function(e) {
                    function t() {
                        return a(this, t), i(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments))
                    }
                    return o(t, e), s(t, [{
                        key: "render",
                        value: function() {
                            var e = this,
                                t = this.props.webHostName,
                                n = this.props.products.map(function(n, a) {
                                    var i = n.kellaaProductId || n.id;
                                    return l.createElement(c, {
                                        key: "ProductComponent_" + i,
                                        id: n.id,
                                        kellaaProductId: n.kellaaProductId,
                                        referenceName: n.reference_name,
                                        currencyType: n.currencyType,
                                        cost: n.cost,
                                        saleCost: n.saleCost,
                                        baseAmount: n.baseAmount,
                                        extraAmount: n.extraAmount,
                                        bonusAmount: n.bonusAmount,
                                        vipAmount: n.vipAmount,
                                        mostPopular: n.mostPopular,
                                        bestValue: n.bestValue,
                                        giveItems: n.giveItems,
                                        imagePath: n.image_path,
                                        webHostName: t,
                                        platform: e.props.platform,
                                        onClick: e.props.onClick.bind(e, n)
                                    })
                                });
                            return l.createElement("div", null, n)
                        }
                    }]), t
                }(l.Component);
            d.displayName = "PurchaseProductsPopup", d.propTypes = {
                products: l.PropTypes.Object,
                platform: l.PropTypes.string,
                webHostName: l.PropTypes.string,
                onClick: l.PropTypes.func
            }, r.exports = d
        }.call(t, n, t, e), !(void 0 !== r && (e.exports = r))
    }, function(e, t, n) {
        var a, i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
            return typeof e
        } : function(e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        };
        a = function(e, t, n) {
            function a(e) {
                var t = "[^\\x0d\\x22\\x5c\\x80-\\xff]",
                    n = "[^\\x0d\\x5b-\\x5d\\x80-\\xff]",
                    i = "[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+",
                    o = "\\x5c[\\x00-\\x7f]",
                    r = "\\x5b(" + n + "|" + o + ")*\\x5d",
                    s = "\\x22(" + t + "|" + o + ")*\\x22",
                    l = i,
                    c = "(" + l + "|" + r + ")",
                    d = "(" + i + "|" + s + ")",
                    u = c + "(\\x2e" + c + ")*",
                    p = d + "(\\x2e" + d + ")*",
                    m = p + "\\x40" + u,
                    h = "^" + m + "$",
                    _ = new RegExp(h);
                return (a = function(e) {
                    return _.test(e)
                })(e)
            }
            var o = 254;
            n.exports = {
                isValidEmail: function(e) {
                    return !("string" != typeof e || !e) && (!(e.length > o) && a(e))
                },
                isObject: function(e) {
                    return e && "object" === ("undefined" == typeof e ? "undefined" : i(e)) && !Array.isArray(e)
                },
                assertIsObject: function(e) {
                    if (!n.exports.isObject(e)) throw new TypeError("expected Object, got " + ("undefined" == typeof e ? "undefined" : i(e)) + ": " + e)
                },
                isNonEmptyObject: function(e) {
                    return n.exports.isObject(e) && Object.keys(e).length > 0
                },
                assertIsNonEmptyObject: function(e) {
                    if (!n.exports.isNonEmptyObject(e)) throw new TypeError("expected non-empty Object, got " + ("undefined" == typeof e ? "undefined" : i(e)) + ": " + e)
                },
                isFunction: function(e) {
                    return "function" == typeof e
                },
                assertIsFunction: function(e) {
                    if (!n.exports.isFunction(e)) throw new TypeError("expected Function, got " + ("undefined" == typeof e ? "undefined" : i(e)) + ": " + e)
                },
                isObjectOrNull: function(e) {
                    return "object" === ("undefined" == typeof e ? "undefined" : i(e)) && !Array.isArray(e)
                },
                isString: function(e) {
                    return "string" == typeof e
                },
                assertIsString: function(e) {
                    if (!n.exports.isString(e)) throw new TypeError("expected String, got " + ("undefined" == typeof e ? "undefined" : i(e)) + ": " + e)
                },
                isNonEmptyString: function(e) {
                    return !(!e || "string" != typeof e)
                },
                assertIsNonEmptyString: function(e) {
                    if (!n.exports.isNonEmptyString(e)) throw new TypeError("expected non empty String, got " + ("undefined" == typeof e ? "undefined" : i(e)) + ": " + e)
                },
                isNumber: function(e) {
                    return "number" == typeof e && !isNaN(e)
                },
                assertIsNumber: function(e) {
                    if (!n.exports.isNumber(e)) throw new TypeError("expected Number, got " + ("undefined" == typeof e ? "undefined" : i(e)) + ": " + e)
                },
                isUndefined: function(e) {
                    return "undefined" == typeof e
                },
                assertIsDefined: function(e) {
                    if (n.exports.isUndefined(e)) throw new TypeError("expected value to be defined: " + e)
                },
                assertIsUndefined: function(e) {
                    if (!n.exports.isUndefined(e)) throw new TypeError("expected value to be undefined, got " ("undefined" == typeof e ? "undefined" : i(e)) + ": " + e)
                },
                isBoolean: function(e) {
                    return "boolean" == typeof e
                },
                assertIsBoolean: function(e) {
                    if (!n.exports.isBoolean(e)) throw new TypeError("expected Boolean, got " + ("undefined" == typeof e ? "undefined" : i(e)) + ": " + e)
                },
                isArray: function(e) {
                    return Array.isArray(e)
                },
                assertIsArray: function(e) {
                    if (!n.exports.isArray(e)) throw new TypeError("expected Array, got " + ("undefined" == typeof e ? "undefined" : i(e)) + ": " + e)
                },
                isNonEmptyArray: function(e) {
                    return Array.isArray(e) && e.length > 0
                },
                assertIsNonEmptyArray: function(e) {
                    if (!n.exports.isNonEmptyArray(e)) throw new TypeError("expected non empty Array, got " + ("undefined" == typeof e ? "undefined" : i(e)) + ": " + e)
                },
                isInstanceOf: function(e, t) {
                    return e && e instanceof t
                },
                assertIsInstanceOf: function(e, t) {
                    if (n.exports.assertIsFunction(t), !n.exports.isInstanceOf(e, t)) throw new TypeError("expected " + t.name + ", got " + ("undefined" == typeof e ? "undefined" : i(e)) + ": " + e)
                },
                conformsTo: function(e, t) {
                    if (!n.exports.isObject(e)) return !1;
                    if (!n.exports.isObject(t)) return !1;
                    for (var a in t)
                        if (n.exports.isFunction(t[a])) {
                            if (!n.exports.isFunction(e[a])) return !1
                        } else console.warn("Mixin has non-function property " + a, t);
                    return !0
                },
                assertConformsTo: function(e, t) {
                    if (!n.exports.conformsTo(e, t)) throw new TypeError("expected conformation to " + Object.keys(t).join(", "))
                },
                isValueInEnum: function(e, t) {
                    if (!n.exports.isObject(t)) return !1;
                    for (var a in t)
                        if (e === t[a]) return !0;
                    return !1
                },
                assertValueInEnum: function(e, t) {
                    if (!n.exports.isValueInEnum(e, t)) throw new TypeError("expected value in enum " + Object.keys(t).join(", "))
                }
            }
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        e.exports = n(6)(14)
    }, function(e, t, n) {
        e.exports = n(6)(6)
    }, , , , , function(e, t, n) {
        var a;
        (function(i) {
            "use strict";
            a = function(e, t, a) {
                var o = n(4),
                    r = n(2),
                    s = {
                        senderFirstName: !0,
                        senderLastName: !0,
                        senderEmail: !0,
                        subject: !0,
                        body: !0
                    },
                    l = {
                        feedback_form: "FEEDBACK",
                        report_form: "BUG",
                        service_form: "SUPPORT"
                    },
                    c = {
                        CONFIRMATION: 1,
                        HELP: 2
                    };
                a.exports = o.create({
                    __modalLayer: null,
                    __container: null,
                    __loaded: !1,
                    __tabId: null,
                    __mode: null,
                    initialize: function(e, t) {
                        this.__tabId = e, this.__modalLayer = t
                    },
                    load: function() {
                        var e = this,
                            t = document.querySelector("#" + e.__tabId),
                            n = t.getAttribute("data-email"),
                            a = t.getAttribute("data-first-name"),
                            i = t.getAttribute("data-last-name"),
                            o = t.getAttribute("data-player-id"),
                            r = t.getAttribute("data-facebook-id"),
                            s = t.getAttribute("data-email-confirmed");
                        if ("1" !== s) this.loadConfirmationModal(n, o);
                        else {
                            var l = {
                                first_name: a,
                                last_name: i,
                                user_email: n,
                                player_id: o,
                                facebook_id: r
                            };
                            this.__loadHelpModal(l)
                        }
                    },
                    loadConfirmationModal: function(e, t, n) {
                        var a = this;
                        r.loadTemplate("modal_email_confirmation", {
                            email: e,
                            playerId: t
                        }, function(e, t) {
                            if (e) return void console.error("Failed to load support_form template: ", e);
                            var o = i(i.parseHTML(t));
                            a.__container = o, i(o.find(".close_button")).click(function() {
                                a.__close()
                            }), i(o.find(".close")).click(function() {
                                a.__close()
                            }), i(o.find(".submit")).click(function() {
                                a.__submitConfirmationForm(i(this))
                            });
                            var r = a.__modalLayer.getContainer();
                            a.__container.prependTo(r), a.__hideContainer(), a.__mode = c.CONFIRMATION, a.__enable(), n && n()
                        })
                    },
                    __loadHelpModal: function(e, t) {
                        var n = this;
                        r.loadTemplate("modal_help", e, function(e, a) {
                            if (e) return void console.error("Failed to load support_form template: ", e);
                            var o = i(i.parseHTML(a));
                            n.__container = o, i(o.find(".close_button")).click(function() {
                                n.__close()
                            }), i(o.find(".close")).click(function() {
                                n.__close()
                            }), n.__currentlyOpen = null, i(o.find(".expander-icon")).click(function() {
                                var e = i(this);
                                null != n.__currentlyOpen && n.__currentlyOpen.next().attr("id") != e.next().attr("id") && (n.__currentlyOpen.next().slideUp(), n.__currentlyOpen.siblings().find(".teaser").removeClass("invisibility"), n.__currentlyOpen.removeClass("expander-icon--open")), e.next().slideToggle(), e.siblings().find(".teaser").toggleClass("invisibility"), e.toggleClass("expander-icon--open"), n.__currentlyOpen = e
                            }), i(o.find(".submit")).click(function() {
                                n.__submitForm(i(this))
                            });
                            var r = n.__modalLayer.getContainer();
                            n.__container.prependTo(r), n.__hideContainer(), n.__mode = c.HELP, n.__enable(), t && t()
                        })
                    },
                    __enable: function() {
                        this.__loaded = !0
                    },
                    show: function() {
                        if (!this.__loaded) return void console.error("SupportManager not loaded yet!");
                        var e = this;
                        this.__modalLayer.show(), r.GET("/gamecontrol/support.php?ep=getEmail", function(t, n) {
                            if (t || !n || "success" !== n.status) {
                                if (e.__mode === c.CONFIRMATION) return void e.__showContainer();
                                var a = document.querySelector("#" + e.__tabId),
                                    i = a.getAttribute("data-email"),
                                    o = a.getAttribute("data-player-id");
                                return void e.loadConfirmationModal(i, o, function() {
                                    e.__showContainer()
                                })
                            }
                            if (e.__mode === c.HELP) return void e.__showContainer();
                            var a = document.querySelector("#" + e.__tabId),
                                i = a.getAttribute("data-email"),
                                r = a.getAttribute("data-first-name"),
                                s = a.getAttribute("data-last-name"),
                                o = a.getAttribute("data-player-id"),
                                l = a.getAttribute("data-facebook-id"),
                                d = (a.getAttribute("data-email-confirmed"), {
                                    first_name: r,
                                    last_name: s,
                                    user_email: i,
                                    player_id: o,
                                    facebook_id: l
                                });
                            e.__loadHelpModal(d, function() {
                                e.__showContainer()
                            })
                        })
                    },
                    __showContainer: function() {
                        var e = this.__modalLayer.getZIndex();
                        this.__container.show(), this.__container.css("zIndex", e + 1)
                    },
                    __hideContainer: function() {
                        this.__container.hide()
                    },
                    __close: function() {
                        this.__hideContainer(), this.__modalLayer.hide(), i(this.__container.find(".submit")).val("Submit"), i(this.__container.find(".submit")).css("opacity", 1), i(this.__container.find(".submit")).removeClass("viking_button_orange"), i(this.__container.find(".submit")).addClass("viking_button_green")
                    },
                    __submitConfirmationForm: function(e) {
                        var t = this,
                            n = e.parents(".support_form"),
                            a = n.find(":input"),
                            o = !1;
                        if (n.checkValidity) o = n.checkValidity();
                        else {
                            var s = a[0],
                                l = s.value; - 1 !== l.indexOf(".") && -1 !== l.indexOf("@") || (o = !0)
                        }
                        if (o) return void i(a[0]).addClass("error");
                        i(a[0]).removeClass("error");
                        var s = a[0].value;
                        e.val("Submitting..."), e.removeClass("viking_button_green"), e.addClass("viking_button_orange"), e.css("opacity", .7), r.POST("/gamecontrol/support.php?ep=confirmEmail", {
                            email: s
                        }, function(n, a) {
                            return e.val("Submit"), e.addClass("viking_button_green"), e.removeClass("viking_button_orange"), e.css("opacity", 1), !n && a && a.status ? "error" === a.status ? (i("#generic_modal .window_title").html("Submission Failed"), i("#generic_modal .subtitle").html(""), i("#generic_modal .message").html("Uh oh.  Something went wrong confirming your email address.<br />" + a.msg), void i("#generic_modal").show()) : (i("#generic_modal .window_title").html("Submission Success"), i("#generic_modal .subtitle").html(""), i("#generic_modal .message").html("Thanks!  We have sent an email to " + s + ".  It should be delivered immediately but may take a few minutes during busy periods."), i("#generic_modal").show(), void i("#generic_modal .js-close").click(function() {
                                t.__close()
                            })) : (i("#generic_modal .window_title").html("Submission Failed"), i("#generic_modal .subtitle").html(""), i("#generic_modal .message").html("Uh oh.  Something went wrong confirming your email address."), i("#generic_modal .message").append("<br />Please try again.  If this problem persists, please email us directy at <strong>yoworld@bigvikinggames.com</strong> with the subject: <strong>'Email Confirmation Error'</strong>"), void i("#generic_modal").show())
                        })
                    },
                    __submitForm: function(e) {
                        var t = e.parents(".support_form"),
                            n = t.find(":input"),
                            a = {},
                            o = !1;
                        if (n.each(function(e, t) {
                                var n = t.getAttribute("name");
                                if (n) {
                                    var r = !!s[n],
                                        l = i(t).val();
                                    r && !l ? (i(t).addClass("error"), o = !0) : i(t).removeClass("error"), a[n] = i(t).val()
                                }
                            }), t.checkValidity && (o = t.checkValidity()), !o) {
                            e.val("Submitting..."), e.removeClass("viking_button_green"), e.addClass("viking_button_orange"), e.css("opacity", .7);
                            var c = this,
                                d = this.__currentlyOpen.next().attr("id");
                            a.contactType = l[d], r.POST("/gamecontrol/endpoints.php?ep=supportForm", a, function(n, a) {
                                if (e.val("Submit"), e.addClass("viking_button_green"), e.removeClass("viking_button_orange"), e.css("opacity", 1), n || !a || !a.status) return i("#generic_modal .window_title").html("Submission Failed"), i("#generic_modal .subtitle").html(""), i("#generic_modal .message").html("Uh oh.  Something went wrong submitting your ticket."), i("#generic_modal .message").append("<br />Please try again.  If this problem persists, please email us directy at <strong>yoworld@bigvikinggames.com</strong> with the subject: <strong>'Support Form Error'</strong>"), void i("#generic_modal").show();
                                if (i("#generic_modal .window_title").html("Submission Success"), i("#generic_modal .subtitle").html(""), i("#generic_modal .message").html("Thanks!  We received your ticket."), i("#generic_modal .message").append("  We will respond as soon as possible.  Responses take longer during busy periods and off-hours."), "success" === a.status) {
                                    var o = document.querySelector("#" + c.__tabId),
                                        r = o.getAttribute("data-player-id");
                                    i("#generic_modal .message").append("<br /><br />You should receive an auto-reply email shortly.  It may be in your spam/junk folder."), i("#generic_modal .message").append("<br /><br />If you do not receive anything, please email us at <strong>yoworld@bigvikinggames.com</strong> with your player ID: <strong>" + r + "</strong>.")
                                }
                                i("#generic_modal").show(), i(t.find(".subject")).val(""), i(t.find(".body")).val(""), c.__close()
                            })
                        }
                    }
                })
            }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
        }).call(t, n(3))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = i.createClass({
                    displayName: "TabSwitcher",
                    render: function() {
                        var e = this.props.active,
                            t = this.props.items.map(function(t, n) {
                                var a = t.key || n;
                                return i.createElement("li", {
                                    key: a,
                                    className: "tab " + (e === n ? "selected" : ""),
                                    onClick: this.onClick.bind(this, n)
                                }, t.title)
                            }.bind(this));
                        return i.createElement("ul", {
                            style: this.props.style,
                            className: "tabs__nav"
                        }, t)
                    },
                    onClick: function(e) {
                        this.props.onClick(e)
                    }
                }),
                r = i.createClass({
                    displayName: "TabContent",
                    render: function() {
                        var e = this.props.active;
                        return i.createElement("div", {
                            style: this.props.style,
                            className: "tabs__content",
                            key: this.props.active
                        }, this.props.items[e].content)
                    }
                });
            a.exports = i.createClass({
                displayName: "exports",
                propTypes: {
                    style: i.PropTypes.object,
                    switcherStyle: i.PropTypes.object,
                    items: i.PropTypes.array.isRequired,
                    active: i.PropTypes.number.isRequired,
                    onClick: i.PropTypes.func
                },
                getInitialState: function() {
                    return {
                        active: this.props.active
                    }
                },
                render: function() {
                    return i.createElement("div", {
                        style: this.props.style,
                        className: "tabs"
                    }, i.createElement(o, {
                        style: this.props.switcherStyle,
                        items: this.props.items,
                        active: this.state.active,
                        onClick: this.handleTabClick
                    }), i.createElement(r, {
                        style: this.props.contentStyle,
                        items: this.props.items,
                        active: this.state.active
                    }, " "))
                },
                handleTabClick: function(e) {
                    void 0 !== this.props.onClick && null !== this.props.onClick && this.props.onClick(e), this.setState({
                        active: e
                    })
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, n) {
            var a = {
                    DESK_IFRAME: "DESK_IFRAME",
                    BUCKET_SELECTOR: "BUCKET_SELECTOR",
                    ACCOUNT_FORM: "ACCOUNT_FORM",
                    FEEDBACK_FORM: "FEEDBACK_FORM",
                    BUG_FORM: "BUG_FORM",
                    SUCCESS_PAGE: "SUCCESS_PAGE",
                    YOMO_FORM: "YOMO_FORM"
                },
                i = {
                    BUCKET_SELECTOR: "What would you like help with?",
                    ACCOUNT_FORM: "Account Help Request",
                    FEEDBACK_FORM: "Send Feedback",
                    BUG_FORM: "Bug Report",
                    SUCCESS_PAGE: "Success",
                    YOMO_FORM: "YoWorld Companion App Help Request"
                },
                o = "Thanks! We will respond as soon as possible. Responses take longer during busy periods and off-hours. <br/><br/> You should receive an auto-reply email shortly. It may be in your spam/junk folder. <br/><br/> If you do not receive anything, please email us at yoworld@bigvikinggames.com with your player ID: ",
                r = "Thanks! We received your report and our talented dev team will be looking into it when they get the chance. <br/><br/> Due to the number of tickets we handle, we'll only be replying to the bug reports that require additional information. Thanks for your help making YoWorld the best virtual world ever!",
                s = "Thanks! We've received your ticket and will be taking a look at what you think. We appreciate your support in making YoWorld the best virtual world out there! <br/><br/> Due to the sheer volume of tickets we won't be able to reply to everyone, but will reach out if we want to get more details. Thank you!",
                l = {
                    "I have feedback about a feature in the game": {
                        Label: "Feature",
                        ExtraInput: ["Subject", "Description", "Attachment"]
                    },
                    "I have feedback about items in the game": {
                        Label: "Content",
                        ExtraInput: ["Subject", "Description", "Attachment"]
                    },
                    Other: {
                        ExtraInput: ["Subject", "Description", "Attachment"]
                    }
                },
                c = {
                    "I’m missing an item or I’m missing currency": {
                        Label: "Missing Item/Currency",
                        Subcategories: {
                            "From a store purchase": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Store"
                            },
                            "From a reward or bonus": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Bonus"
                            },
                            "From a video ad": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Ads"
                            },
                            Other: {
                                ExtraInput: ["Subject", "Description", "Attachment"]
                            }
                        }
                    },
                    "I’m having issues making a payment": {
                        Label: "Payment Issues",
                        Subcategories: {
                            "I can’t purchase YoCash or YoCoins": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Currency Purchases"
                            },
                            "I can’t purchase items with YoCash or YoCoins": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Item Purchase"
                            },
                            "I can't transfer currency between my accounts": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Transfer"
                            },
                            "My payment method isn’t working": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Payment Method"
                            },
                            Other: {
                                ExtraInput: ["Subject", "Description", "Attachment"]
                            }
                        }
                    },
                    "I’m having issues logging in": {
                        Label: "Login",
                        Subcategories: {
                            "I can’t log in": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Account"
                            },
                            Other: {
                                ExtraInput: ["Subject", "Description", "Attachment"]
                            }
                        }
                    },
                    "The game won’t run": {
                        Label: "Device/Server",
                        Subcategories: {
                            "The game won't log in at all": {
                                ExtraInput: ["Subject", "Description", "Attachment"]
                            },
                            "I don't see all my buddies": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Display"
                            },
                            "I don't see my current outfit": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Display"
                            },
                            Other: {
                                ExtraInput: ["Subject", "Description", "Attachment"]
                            }
                        }
                    }
                },
                d = {
                    "I’m having loading issues playing YoWorld": {
                        Label: "Loading Issues",
                        Subcategories: {
                            "My home loads slowly or not at all": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Home Loading",
                                HelpArticle: "/customer/en/portal/articles/2505534-game-loading-issues"
                            },
                            "The game feels slow": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Loads Slow",
                                HelpArticle: "/customer/en/portal/articles/2505542-slow-or-freezing-issues"
                            },
                            "The game keeps freezing up on me": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Freezing",
                                HelpArticle: "/customer/en/portal/articles/2505542-slow-or-freezing-issues"
                            },
                            Other: {
                                ExtraInput: ["Subject", "Description", "Attachment"]
                            }
                        }
                    },
                    "I’m missing an item or I’m missing currency": {
                        Label: "Missing Item/Currency",
                        Subcategories: {
                            "From an Auction House transaction": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Auction House"
                            },
                            "From a store purchase": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Store"
                            },
                            "That I was supposed to get in a trade": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Trade"
                            },
                            "That I had on my account in the past": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Lost Item"
                            },
                            Other: {
                                ExtraInput: ["Subject", "Description", "Attachment"]
                            }
                        }
                    },
                    "I’m having issues making a payment": {
                        Label: "Payment Issues",
                        Subcategories: {
                            "I can’t purchase YoCash or YoCoins": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Currency Purchases"
                            },
                            "My payment method isn’t working": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Payment Method"
                            },
                            Other: {
                                ExtraInput: ["Subject", "Description", "Attachment"]
                            }
                        }
                    },
                    "I’m having trouble with one of my items": {
                        Label: "Item Problem",
                        Subcategories: {
                            "My item isn’t working or is broken": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Broken"
                            },
                            "My item isn’t as described": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Not as Described"
                            },
                            Other: {
                                ExtraInput: ["Subject", "Description", "Attachment"]
                            }
                        }
                    },
                    "I think I may have been scammed": {
                        Label: "Scammed",
                        Subcategories: {
                            "I think someone else accessed my account": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Phished"
                            },
                            "Someone just scammed me": {
                                ExtraInput: ["Subject", "Description", "Attachment"],
                                Label: "Tricked"
                            },
                            Other: {
                                ExtraInput: ["Subject", "Description", "Attachment"]
                            }
                        }
                    },
                    "I need help with my YoPass": {
                        ExtraInput: ["Subject", "Description", "Attachment"],
                        Label: "YoPass",
                        HelpArticle: "/customer/en/portal/articles/2507222-yopass"
                    },
                    "I need help with my Lock Code": {
                        ExtraInput: ["Subject", "Description", "Attachment"],
                        Label: "Lock Code",
                        HelpArticle: "/customer/en/portal/articles/2507244-lock-codes"
                    },
                    Other: {
                        ExtraInput: ["Subject", "Description", "Attachment"],
                        Label: "Other"
                    }
                };
            n.exports = {
                PAGES: a,
                TITLES: i,
                ACCOUNT_FORM_SUCCESS: o,
                BUG_FORM_SUCCESS: r,
                FEEDBACK_FORM_SUCCESS: s,
                FeedbackCategories: l,
                AccountCategories: d,
                YoMoCategories: c
            }
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = i.createClass({
                    displayName: "FileInput",
                    propTypes: {
                        fileUploaded: i.PropTypes.func
                    },
                    onUpload: function(e) {
                        this.props.fileUploaded(e, this.refs[e].files[0])
                    },
                    render: function() {
                        return i.createElement("div", {
                            className: "row--margin-bottom",
                            key: "attachment"
                        }, i.createElement("div", {
                            className: "row--margin-bottom"
                        }, i.createElement("div", {
                            className: "span3of12 ar"
                        }, i.createElement("label", {
                            htmlFor: "file1"
                        }, "Screenshot 1:"), i.createElement("div", {
                            className: "small"
                        }, "(Optional. Max 2MB)")), i.createElement("input", {
                            id: "file1",
                            type: "file",
                            ref: "file1",
                            onChange: this.onUpload.bind(this, "file1")
                        })), i.createElement("div", {
                            className: "row"
                        }, i.createElement("div", {
                            className: "span3of12 ar"
                        }, i.createElement("label", {
                            htmlFor: "file2"
                        }, "Screenshot 2:"), i.createElement("div", {
                            className: "small"
                        }, "(Optional. Max 2MB)")), i.createElement("input", {
                            id: "file2",
                            type: "file",
                            ref: "file2",
                            onChange: this.onUpload.bind(this, "file2")
                        })))
                    }
                });
            a.exports = o
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(2);
            a.exports = i.createClass({
                displayName: "exports",
                render: function() {
                    var e = this.props.data,
                        t = "positive";
                    e < 0 ? t = "negative" : e > 0 && (t = "positive");
                    var n = o.formatDecimal(e) + "%";
                    return e < .005 && e > -.005 && (t = "", n = "---"), i.createElement("div", {
                        className: t
                    }, n)
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(2);
            a.exports = i.createClass({
                displayName: "exports",
                render: function() {
                    var e = this.props.data,
                        t = "",
                        n = o.formatDecimal(e),
                        a = "▲";
                    return e < 0 ? (a = "▼", t = "negative") : e > 0 && (t = "positive", a = "▲"), i.createElement("div", {
                        className: t
                    }, i.createElement("img", {
                        src: "images/yocoin.svg",
                        className: "icon-yocoin",
                        alt: "Yocoin Icon"
                    }), " ", n, " ", a)
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        (function(i) {
            "use strict";
            a = function(e, t, a) {
                var o = n(2),
                    r = n(1),
                    s = n(11),
                    l = n(78),
                    c = n(7),
                    d = 1,
                    u = "REF_SELL_QUANTITY",
                    p = "REF_BUY_QUANTITY",
                    m = 100,
                    h = "timeline1h",
                    _ = "timeline6h",
                    f = "timeline1d",
                    g = "timeline5d",
                    v = "timeline1M",
                    b = {};
                b[h] = 61, b[_] = 37, b[f] = 49, b[g] = 61, b[v] = 31;
                var y = "Generic",
                    E = "Earn More",
                    w = "Confirm Sell",
                    C = "Confirm Buy";
                a.exports = r.createClass({
                    displayName: "exports",
                    getInitialState: function() {
                        return {
                            loadedStockId: 0,
                            buyQuantity: d,
                            sellQuantity: d,
                            timeFrame: _,
                            stock: {
                                abbreviation: "",
                                name: "",
                                description: "",
                                value: "",
                                owned: 0,
                                fee: 1,
                                timeline6h: {
                                    change: 0,
                                    changePercent: 0,
                                    high: 0,
                                    low: 0,
                                    changeSign: "="
                                }
                            },
                            showPopup: !1,
                            popupContent: null,
                            popupId: null
                        }
                    },
                    __processTimeline: function(e, t) {
                        return e.change = Number(e.change), e.changePercent = Number(e.changePercent), e.high = Number(e.high), e.low = Number(e.low), e.changeSign = "▲", e.change < 0 ? (e.change *= -1, e.changeSign = "▼") : 0 == e.change && (e.changeSign = "="), e.shouldShow = e.chartPoints.length >= b[t], e
                    },
                    loadStockData: function() {
                        var e = this,
                            t = e.props.stockId;
                        t <= 0 || o.GET("/gamecontrol/stocks.php?ep=getStockData&stockId=" + t, function(n, a) {
                            return n ? (alert(n), void console.error(n)) : (a && (a.fee = Number(a.fee), a.minFee = Number(a.minFee), a.owned = Number(a.owned), a.value = Number(a.value), a.timeline1h && (a.timeline1h = e.__processTimeline(a.timeline1h, h)), a.timeline6h && (a.timeline6h = e.__processTimeline(a.timeline6h, _)), a.timeline1d && (a.timeline1d = e.__processTimeline(a.timeline1d, f)), a.timeline5d && (a.timeline5d = e.__processTimeline(a.timeline5d, g)), a.timeline1M && (a.timeline1M = e.__processTimeline(a.timeline1M, v))), void e.setState({
                                stock: a,
                                loadedStockId: t,
                                quantity: 1
                            }, function() {
                                e.forceQuantity(p, 1), e.forceQuantity(u, 1)
                            }))
                        })
                    },
                    forceQuantity: function(e, t, n) {
                        if (n = n || function() {}, e === u && this.state.stock) t = Math.min(this.state.stock.owned, t), t = Math.max(d, t);
                        else if (e === p && this.state.stock) {
                            var a = Math.floor(window.coinBalance / this.state.stock.value);
                            t = Math.min(a, t, m - this.state.stock.owned), t = Math.max(d, t)
                        }
                        var i = this.getStateNameByReference(e),
                            o = {};
                        o[i] = t, this.setState(o, n), this.refs[e] && (this.refs[e].value = t)
                    },
                    getStateNameByReference: function(e) {
                        return e === p ? "buyQuantity" : "sellQuantity"
                    },
                    increaseQuantity: function(e, t) {
                        window.fireDataEvent(c.DIALOG_BUTTON_CLICKED, {
                            button: "+",
                            dialog: c.STOCK_TARGET_ID,
                            targetRef: e
                        }, c.STOCK_TARGET_ID, c.DIALOG_TARGET_TYPE), t.preventDefault();
                        var n = this.getStateNameByReference(e);
                        this.forceQuantity(e, this.state[n] + 1)
                    },
                    decreaseQuantity: function(e, t) {
                        window.fireDataEvent(c.DIALOG_BUTTON_CLICKED, {
                            button: "-",
                            dialog: c.STOCK_TARGET_ID,
                            targetRef: e
                        }, c.STOCK_TARGET_ID, c.DIALOG_TARGET_TYPE), t.preventDefault();
                        var n = this.getStateNameByReference(e);
                        this.state[n] > 1 && this.forceQuantity(e, this.state[n] - 1)
                    },
                    onQuantityChanged: function(e, t) {
                        window.fireDataEvent(c.DIALOG_TEXT_ENTRY, {
                            text: t.currentTarget.value,
                            dialog: c.STOCK_TARGET_ID,
                            targetRef: e
                        }, c.STOCK_TARGET_ID, c.DIALOG_TARGET_TYPE);
                        var n = Math.floor(Number(t.currentTarget.value));
                        this.forceQuantity(e, n)
                    },
                    clearPopup: function(e) {
                        if ("function" != typeof e && (e = function() {}), this.state.popupId === C || this.state.popupId === w) {
                            var t = this.state.popupId == w ? c.STOCK_CONFIRM_PURCHASE_TARGET_ID : c.STOCK_CONFIRM_PURCHASE_TARGET_ID;
                            window.fireDataEvent(c.DIALOG_HIDDEN, {
                                dialog: t
                            }, t, c.DIALOG_TARGET_TYPE)
                        } else this.state.popupId === E && window.fireDataEvent(c.DIALOG_HIDDEN, {
                            dialog: c.STOCK_EARN_MORE_TARGET_ID
                        }, c.STOCK_EARN_MORE_TARGET_ID, c.DIALOG_TARGET_TYPE);
                        var n = document.getElementById("main_content");
                        n.className = n.className.replace(/disabled/g, ""), this.setState({
                            showPopup: !1,
                            popupContent: null
                        }, e)
                    },
                    showPopup: function(e, t) {
                        var n = document.getElementById("main_content");
                        n.className += " disabled", this.setState({
                            showPopup: !0,
                            popupClass: "popup  popup--prompt  enabled",
                            popupContent: t,
                            popupId: e
                        })
                    },
                    showUpdatePopup: function(e) {
                        var t = this;
                        window.fireDataEvent(c.GENERIC_DIALOG_CREATED, {
                            button: "OK",
                            dialog: c.STOCK_TARGET_ID,
                            title: "Attention",
                            message: e
                        }, c.STOCK_TARGET_ID, c.DIALOG_TARGET_TYPE), t.showPopup(y, r.createElement("div", null, r.createElement("h1", {
                            className: "title  title--pressed"
                        }, "Attention"), r.createElement("p", null, e), r.createElement("button", {
                            className: "btn  btn-primary",
                            onClick: t.clearPopup
                        }, "OK"))), t.loadStockData(), t.props.onPlayerStatsUpdate && t.props.onPlayerStatsUpdate()
                    },
                    earnMoreOk: function() {
                        window.fireDataEvent(c.DIALOG_BUTTON_CLICKED, {
                            button: "OK",
                            dialog: c.STOCK_EARN_MORE_TARGET_ID
                        }, c.STOCK_EARN_MORE_TARGET_ID, c.DIALOG_TARGET_TYPE), this.clearPopup()
                    },
                    showEarnMoreCoinsPopup: function(e) {
                        var t = this,
                            n = "";
                        window.fireDataEvent(c.DIALOG_SHOWN, {
                            dialog: c.STOCK_EARN_MORE_TARGET_ID
                        }, c.STOCK_EARN_MORE_TARGET_ID, c.DIALOG_TARGET_TYPE), gameSwf && (n = r.createElement("button", {
                            className: "btn  btn-positive  span1of4  float-none",
                            onClick: t.showEarnMoreVideo
                        }, "Earn More")), t.showPopup(E, r.createElement("div", null, r.createElement("h1", {
                            className: "title  title--pressed"
                        }, "Attention"), r.createElement("p", null, e), r.createElement("div", {
                            className: "section"
                        }, r.createElement("button", {
                            className: "btn  btn-positive  span1of4  float-none",
                            onClick: t.showBuyMoreInterface
                        }, "Buy More"), n, r.createElement("button", {
                            className: "btn  btn-primary  span1of4  float-none",
                            onClick: t.earnMoreOk
                        }, "OK")))), t.loadStockData(), t.props.onPlayerStatsUpdate && t.props.onPlayerStatsUpdate()
                    },
                    closeStockPage: function() {
                        this.clearPopup(), i("#stock-market .js-close").click()
                    },
                    showBuyMoreInterface: function() {
                        window.fireDataEvent(c.DIALOG_BUTTON_CLICKED, {
                            button: "Buy More",
                            dialog: c.STOCK_EARN_MORE_TARGET_ID
                        }, c.STOCK_EARN_MORE_TARGET_ID, c.DIALOG_TARGET_TYPE), this.closeStockPage(), window.triggerPurchasePopup()
                    },
                    showEarnMoreVideo: function() {
                        window.fireDataEvent(c.DIALOG_BUTTON_CLICKED, {
                            button: E,
                            dialog: c.STOCK_EARN_MORE_TARGET_ID
                        }, c.STOCK_EARN_MORE_TARGET_ID, c.DIALOG_TARGET_TYPE), this.closeStockPage(), gameSwf && gameSwf.playVideoAd()
                    },
                    onCancel: function(e) {
                        window.fireDataEvent(c.DIALOG_BUTTON_CLICKED, {
                            button: "Cancel",
                            dialog: e
                        }, e, c.DIALOG_TARGET_TYPE), this.clearPopup()
                    },
                    showConfirmPopup: function(e, t, n, a) {
                        var i = this,
                            o = "span1of3  float-none  btn  ";
                        o += "Sell" == n ? "btn-negative" : "btn-positive";
                        var s = C,
                            l = c.STOCK_CONFIRM_PURCHASE_TARGET_ID;
                        "Sell" == n && (s = w, l = c.STOCK_CONFIRM_SALE_TARGET_ID), window.fireDataEvent(c.DIALOG_SHOWN, {
                            dialog: l
                        }, l, c.DIALOG_TARGET_TYPE), i.showPopup(s, r.createElement("div", null, r.createElement("div", {
                            className: "header-bar header-bar--prompt"
                        }, r.createElement("h2", {
                            className: "title  title--popped"
                        }, e)), r.createElement("p", {
                            className: "center"
                        }, t), r.createElement("div", {
                            className: "section"
                        }, r.createElement("button", {
                            className: "btn  btn-default  span1of3  float-none",
                            onClick: i.onCancel.bind(this, l)
                        }, "Cancel"), r.createElement("button", {
                            className: o,
                            onClick: a
                        }, n))))
                    },
                    onAcceptTransaction: function(e, t, n) {
                        function a() {
                            var a = "Sell",
                                r = c.STOCK_CONFIRM_SALE_TARGET_ID;
                            "buyShares" == e && (a = "Buy", r = c.STOCK_CONFIRM_PURCHASE_TARGET_ID), window.fireDataEvent(c.DIALOG_BUTTON_CLICKED, {
                                button: a,
                                dialog: r
                            }, r, c.DIALOG_TARGET_TYPE), i.clearPopup(function() {
                                var a = {
                                    stockId: i.state.loadedStockId,
                                    quantity: i.state[n],
                                    intentPrice: t
                                };
                                o.lockedPost("/gamecontrol/stocks.php?ep=" + e, a, function(e, t) {
                                    if (e) return i.showUpdatePopup(e), void console.error(e);
                                    if (t.error && t.type && "NotEnoughCoins" === t.type) return void i.showEarnMoreCoinsPopup(t.error);
                                    var n = t.error || t.msg || t.message;
                                    i.showUpdatePopup(n), i.props.coinUpdateFunction()
                                })
                            })
                        }
                        var i = this;
                        return a
                    },
                    buySharesClicked: function() {
                        var e = this,
                            t = e.state[e.getStateNameByReference(p)],
                            n = e.state.stock.value,
                            a = Math.ceil(t * n),
                            i = t > 1 ? t + " shares" : "1 share";
                        return window.fireDataEvent(c.DIALOG_BUTTON_CLICKED, {
                            button: "Buy",
                            dialog: c.STOCK_TARGET_ID,
                            quantity: t,
                            price: a
                        }, c.STOCK_TARGET_ID, c.DIALOG_TARGET_TYPE), Number(t) <= 0 ? (e.showUpdatePopup("Please enter a valid number of stocks to purchase"), void e.forceQuantity(p, d)) : void e.showConfirmPopup("Confirm Purchase", "Do you want to buy " + i + " of " + e.state.stock.name + " for " + o.formatDecimal(a, 0) + " coins?", "Buy", e.onAcceptTransaction("buyShares", n, e.getStateNameByReference(p)))
                    },
                    sellMaxClicked: function() {
                        window.fireDataEvent(c.DIALOG_BUTTON_CLICKED, {
                            button: "MAX",
                            dialog: c.STOCK_TARGET_ID,
                            targetRef: u,
                            owned: this.state.stock.owned
                        }, c.STOCK_TARGET_ID, c.DIALOG_TARGET_TYPE), this.state.stock.owned <= 0 || this.forceQuantity(u, this.state.stock.owned)
                    },
                    buyMaxClicked: function() {
                        var e = Math.floor(window.coinBalance / this.state.stock.value);
                        e > m - this.state.stock.owned && (e = m - this.state.stock.owned), e < d && (e = d), window.fireDataEvent(c.DIALOG_BUTTON_CLICKED, {
                            button: "MAX",
                            dialog: c.STOCK_TARGET_ID,
                            targetRef: p,
                            numAffort: e
                        }, c.STOCK_TARGET_ID, c.DIALOG_TARGET_TYPE), this.forceQuantity(p, e)
                    },
                    sellSharesClicked: function() {
                        var e = this,
                            t = e.state[e.getStateNameByReference(u)],
                            n = e.state.stock.value,
                            a = Math.floor(t * n),
                            i = Math.max(e.state.stock.minFee, Math.ceil(t * n * e.state.stock.fee));
                        a -= i, window.fireDataEvent(c.DIALOG_BUTTON_CLICKED, {
                            button: "Sell",
                            dialog: c.STOCK_TARGET_ID,
                            quantity: t,
                            price: a
                        }, c.STOCK_TARGET_ID, c.DIALOG_TARGET_TYPE);
                        var r = t > 1 ? t + " shares" : "1 share";
                        return Number(t) <= 0 ? (e.showUpdatePopup("Please enter a valid number of stocks to sell"), void e.forceQuantity(u, d)) : Number(t) > e.state.stock.owned ? (e.showUpdatePopup("You can not sell more shares than you own."), void e.forceQuantity(u, d)) : void e.showConfirmPopup("Confirm Sale", "Do you want to sell " + r + " of " + e.state.stock.name + " for " + o.formatDecimal(a, 0) + " coins ?", "Sell", e.onAcceptTransaction("sellShares", n, e.getStateNameByReference(u)))
                    },
                    clearStockSelection: function(e) {
                        e.preventDefault();
                        var t = this;
                        t.setState(this.getInitialState(), function() {
                            t.props.clearStockFunction()
                        })
                    },
                    __changeTimeFrame: function(e) {
                        e !== this.state.timeFrame && (window.fireDataEvent(c.DIALOG_TAB_CLICKED, {
                            tab: e,
                            dialog: c.STOCK_TARGET_ID
                        }, c.STOCK_TARGET_ID, c.DIALOG_TARGET_TYPE), this.setState({
                            timeFrame: e
                        }))
                    },
                    render: function() {
                        if (null === this.props.stockId || void 0 === this.props.stockId) return null;
                        this.state.loadedStockId != this.props.stockId && this.loadStockData();
                        var e = r.createElement("div", null);
                        if (this.state.stock.owned) {
                            var t = Math.floor(this.state.stock.value * this.state.sellQuantity) - Math.max(this.state.stock.minFee, Math.ceil(this.state.stock.value * this.state.sellQuantity * this.state.stock.fee));
                            e = r.createElement("div", {
                                className: "row  stock-actions-group  stock-sell"
                            }, r.createElement("span", {
                                className: "action__input"
                            }, r.createElement("span", {
                                className: "action__name"
                            }, "Sell Quantity:"), r.createElement("a", {
                                href: "#",
                                className: "decrease-number",
                                onClick: this.decreaseQuantity.bind(this, u)
                            }, r.createElement("i", {
                                className: "fa fa-minus-circle fa-2x"
                            })), r.createElement("input", {
                                ref: u,
                                className: "form-control  stock-quantity  hide-buttons",
                                type: "number",
                                defaultValue: "1",
                                min: "1",
                                onChange: this.onQuantityChanged.bind(this, u)
                            }), r.createElement("a", {
                                href: "#",
                                className: "increase-number",
                                onClick: this.increaseQuantity.bind(this, u)
                            }, r.createElement("i", {
                                className: "fa fa-plus-circle fa-2x"
                            })), r.createElement("a", {
                                className: "input-link",
                                onClick: this.sellMaxClicked
                            }, "MAX")), r.createElement("span", {
                                className: "action__total"
                            }, r.createElement("span", {
                                className: "bold"
                            }, "Total Earn: "), r.createElement("img", {
                                src: "images/yocoin.svg",
                                className: "icon-yocoin",
                                alt: "Yocoin Icon"
                            }), o.formatDecimal(t, 0)), r.createElement("span", {
                                className: "action__button"
                            }, r.createElement("button", {
                                className: "btn  btn-negative  btn-sell-stocks",
                                onClick: this.sellSharesClicked
                            }, "Sell")))
                        }
                        var n = r.createElement("div", null);
                        this.state.stock.owned < m && (n = r.createElement("div", {
                            className: "row  stock-actions-group  stock-buy"
                        }, r.createElement("span", {
                            className: "action__input"
                        }, r.createElement("span", {
                            className: "action__name"
                        }, "Buy Quantity:"), r.createElement("a", {
                            href: "#",
                            className: "decrease-number",
                            onClick: this.decreaseQuantity.bind(this, p)
                        }, r.createElement("i", {
                            className: "fa fa-minus-circle fa-2x"
                        })), r.createElement("input", {
                            ref: p,
                            className: "form-control  stock-quantity  hide-buttons",
                            type: "number",
                            defaultValue: "1",
                            min: "1",
                            onChange: this.onQuantityChanged.bind(this, p)
                        }), r.createElement("a", {
                            href: "#",
                            className: "increase-number",
                            onClick: this.increaseQuantity.bind(this, p)
                        }, r.createElement("i", {
                            className: "fa fa-plus-circle fa-2x"
                        })), r.createElement("a", {
                            className: "input-link",
                            onClick: this.buyMaxClicked
                        }, "MAX")), r.createElement("span", {
                            className: "action__total"
                        }, r.createElement("span", {
                            className: "label"
                        }, "Total Cost: "), r.createElement("img", {
                            src: "images/yocoin.svg",
                            className: "icon-yocoin",
                            alt: "Yocoin Icon"
                        }), o.formatDecimal(Math.ceil(this.state.stock.value * this.state.buyQuantity), 0)), r.createElement("span", {
                            className: "action__button"
                        }, r.createElement("button", {
                            className: "btn  btn-positive  btn-buy-stocks",
                            onClick: this.buySharesClicked
                        }, "Buy"))));
                        var a = (r.createElement("p", null, "Loading Chart. Please Wait."), []),
                            i = [];
                        if (this.state.stock) var a = this.state.stock[this.state.timeFrame].chartLabels,
                            i = this.state.stock[this.state.timeFrame].chartPoints;
                        var c = "";
                        this.state.stock[this.state.timeFrame].changePercent > 0 ? c = "positive" : this.state.stock[this.state.timeFrame].changePercent < 0 && (c = "negative");
                        var d = [{
                                id: h,
                                title: "1 hour"
                            }, {
                                id: _,
                                title: "6 hours"
                            }, {
                                id: f,
                                title: "1 day"
                            }, {
                                id: g,
                                title: "5 days"
                            }, {
                                id: v,
                                title: "1 month"
                            }],
                            b = this,
                            y = d.map(function(e) {
                                return b.state.stock[e.id] && b.state.stock[e.id].shouldShow ? r.createElement("li", {
                                    className: b.state.timeFrame === e.id ? "selected" : "",
                                    onClick: b.__changeTimeFrame.bind(b, e.id)
                                }, e.title) : null
                            });
                        return r.createElement("div", null, r.createElement(s, {
                            show: this.state.showPopup,
                            content: this.state.popupContent,
                            classes: this.state.popupClass,
                            uniqueId: this.state.popupId
                        }), r.createElement("div", {
                            className: "section  stock-info  content-well"
                        }, r.createElement("div", {
                            className: "row  stock-header"
                        }, r.createElement("a", {
                            href: "javascript:;",
                            className: "close-x",
                            onClick: this.clearStockSelection
                        }, r.createElement("i", {
                            className: "fa fa-times fa-2x"
                        })), r.createElement("h3", null, r.createElement("span", {
                            className: "bold"
                        }, this.state.stock.name), r.createElement("span", {
                            className: "abbrev"
                        }, "(", this.state.stock.abbreviation, ")"), r.createElement("img", {
                            src: "images/yocoin.svg",
                            className: "icon-yocoin",
                            alt: "Yocoin Icon"
                        }), r.createElement("span", null, o.formatDecimal(this.state.stock.value)))), r.createElement("div", {
                            className: "section  stock-description"
                        }, this.state.stock.description), r.createElement("div", {
                            className: "section"
                        }, r.createElement("div", {
                            className: "span3of4  content-box  stock-performance"
                        }, r.createElement("div", {
                            className: "nav  nav--stocks"
                        }, y), r.createElement("div", {
                            className: "span2of3"
                        }, r.createElement(l, {
                            labels: a,
                            points: i,
                            dataLoaded: a && a.length > 0
                        })), r.createElement("div", {
                            className: "span1of3  last"
                        }, r.createElement("table", null, r.createElement("tbody", null, r.createElement("tr", null, r.createElement("td", {
                            className: "bold"
                        }, "High:"), r.createElement("td", {
                            className: "numeral"
                        }, r.createElement("img", {
                            src: "images/yocoin.svg",
                            className: "icon-yocoin",
                            alt: "Yocoin Icon"
                        }), o.formatDecimal(this.state.stock[this.state.timeFrame].high))), r.createElement("tr", null, r.createElement("td", {
                            className: "bold"
                        }, "Low:"), r.createElement("td", {
                            className: "numeral"
                        }, r.createElement("img", {
                            src: "images/yocoin.svg",
                            className: "icon-yocoin",
                            alt: "Yocoin Icon"
                        }), o.formatDecimal(this.state.stock[this.state.timeFrame].low))), r.createElement("tr", null, r.createElement("td", {
                            className: "bold"
                        }, "Change:"), r.createElement("td", {
                            className: "numeral"
                        }, r.createElement("img", {
                            src: "images/yocoin.svg",
                            className: "icon-yocoin",
                            alt: "Yocoin Icon"
                        }), r.createElement("span", {
                            className: c
                        }, o.formatDecimal(this.state.stock[this.state.timeFrame].change), " ", this.state.stock[this.state.timeFrame].changeSign))), r.createElement("tr", null, r.createElement("td", {
                            className: "bold"
                        }, "Change %:"), r.createElement("td", {
                            className: "numeral"
                        }, r.createElement("span", {
                            className: c
                        }, this.state.stock[this.state.timeFrame].changePercent, " %"))))))), r.createElement("div", {
                            className: "span1of4  last  portfolio-info"
                        }, r.createElement("div", {
                            className: "info-label"
                        }, "Portfolio"), r.createElement("div", {
                            className: "info-data row"
                        }, r.createElement("div", {
                            className: "row  bold"
                        }, "You own:"), r.createElement("div", null, o.formatDecimal(this.state.stock.owned, 0), " shares"), r.createElement("div", {
                            className: "row  bold"
                        }, "Total value:"), r.createElement("div", {
                            className: "total-value"
                        }, r.createElement("img", {
                            src: "images/yocoin.svg",
                            className: "icon-yocoin",
                            alt: "Yocoin Icon"
                        }), o.formatDecimal(this.state.stock.owned * this.state.stock.value))))), n, e))
                    }
                })
            }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
        }).call(t, n(3))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(2);
            a.exports = i.createClass({
                displayName: "exports",
                render: function() {
                    var e = o.formatDecimal(this.props.data);
                    return i.createElement("div", null, i.createElement("img", {
                        src: "images/yocoin.svg",
                        className: "icon-yocoin",
                        alt: "Yocoin Icon"
                    }), " ", e)
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(12),
                r = n(2),
                s = "Join Now",
                l = "Select VIP",
                c = "Select Premium VIP",
                d = "Update",
                u = "Cancel Membership*",
                p = "Upgrade Now",
                m = "Resubscribe*";
            a.exports = i.createClass({
                displayName: "SubscriptionButtons",
                getInitialState: function() {
                    return {
                        activeFBRequest: !1
                    }
                },
                propTypes: {
                    productList: i.PropTypes.array,
                    activeProduct: i.PropTypes.object,
                    subscriptionId: i.PropTypes.number,
                    subscriptionType: i.PropTypes.string,
                    handleFbRequest: i.PropTypes.func,
                    handleCancelCb: i.PropTypes.func,
                    purchaseCallback: i.PropTypes.func
                },
                changeSubscription: function(e, t) {
                    this.props.handleFbRequest({
                        subscriptionId: this.props.subscriptionId,
                        fbAction: e,
                        productInfo: t
                    })
                },
                purchaseSubscription: function(e) {
                    this.props.purchaseCallback(e)
                },
                cancelRequested: function(e) {
                    this.props.handleCancelCb(e)
                },
                getProductsByType: function(e, t) {
                    var n = [];
                    if (void 0 === e || 0 === e.length) return {
                        productList: n
                    };
                    switch (t) {
                        case o.TYPE_PREMIUM_VIP:
                            n = this.props.productList.filter(function(e) {
                                return e.reference_name.indexOf("yoworld.subscription.premiumVip") > -1
                            });
                            break;
                        case o.TYPE_UPGRADE_PREMIUM_VIP:
                            n = this.props.productList.filter(function(e) {
                                return "yoworld.subscription.premiumVip" === e.reference_name
                            });
                            break;
                        case o.TYPE_VIP:
                            n = this.props.productList.filter(function(e) {
                                return e.reference_name.indexOf("yoworld.subscription.vip") > -1
                            })
                    }
                    return {
                        productList: n
                    }
                },
                modifySubscriptionButtons: function() {
                    var e, t = " per month",
                        n = i.createElement("em", null);
                    return void 0 === this.props.activeProduct ? (console.warn("Error, no active subscription available"), i.createElement("em", null)) : (this.props.activeProduct.pendingCancel ? (this.props.activeProduct.reference_name.indexOf("yearly") > -1 && (t = " per year"), e = i.createElement("button", {
                        className: "btn btn--positive  btn--block",
                        onClick: this.changeSubscription.bind(this, "reactivate_subscription")
                    }, m), n = i.createElement("div", {
                        className: "flavour-text"
                    }, i.createElement("span", {
                        className: "currency"
                    }, "$", r.getPricingString(this.props.activeProduct.cost, !0)), i.createElement("span", {
                        className: "cycle"
                    }, t))) : e = i.createElement("div", null, i.createElement("a", {
                        className: "block  align-left",
                        href: "#",
                        onClick: this.changeSubscription.bind(this, "modify_subscription")
                    }, d), i.createElement("a", {
                        className: "negative  block  align-left",
                        href: "#",
                        onClick: this.cancelRequested.bind(this, !0)
                    }, u)), i.createElement("div", {
                        className: "vip-tier__subscription-buttons"
                    }, e, n))
                },
                render: function() {
                    var e = [],
                        t = " per month",
                        n = this,
                        a = i.createElement("div", null);
                    if (this.props.activeProduct) a = this.modifySubscriptionButtons();
                    else {
                        if (e = n.getProductsByType(this.props.productList, this.props.subscriptionType), 0 === e.productList.length) return i.createElement("div", null, "Unable to find pricing.");
                        a = e.productList.map(function(e, a) {
                            var d = r.getPricingString(e.cost, !0),
                                u = t;
                            if (e.reference_name.indexOf("yearly") > -1 && (u = " per year", n.props.activeProduct && !e.activeSubscription)) return i.createElement("div", null);
                            var m = i.createElement("div", null),
                                h = s;
                            return n.props.subscriptionType === o.TYPE_PREMIUM_VIP ? h = c : n.props.subscriptionType === o.TYPE_VIP && (h = l), m = n.props.subscriptionType === o.TYPE_UPGRADE_PREMIUM_VIP ? i.createElement("button", {
                                className: "btn btn--positive  btn--block",
                                onClick: n.changeSubscription.bind(n, "change_subscription", e)
                            }, p) : i.createElement("button", {
                                className: "btn btn--positive  btn--block",
                                onClick: n.purchaseSubscription.bind(n, e)
                            }, h), i.createElement("div", {
                                className: "vip-tier__subscription-buttons",
                                key: a
                            }, i.createElement("div", {
                                className: "flavour-text"
                            }, i.createElement("span", {
                                className: "currency"
                            }, "$", d), i.createElement("span", {
                                className: "cycle"
                            }, u)), m)
                        })
                    }
                    return i.createElement("div", null, a)
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(86),
                r = n(88),
                s = n(12),
                l = [{
                    title: "Premium VIP",
                    containerStyle: "vip-tier  vip-tier--tier2",
                    rewardPreamble: ""
                }, {
                    title: "VIP",
                    containerStyle: "vip-tier  vip-tier--tier1",
                    rewardPreamble: ""
                }, {
                    title: "Basic",
                    containerStyle: "vip-tier  vip-tier--tier0",
                    rewardPreamble: "All YoWorld players enjoy these great basic features:"
                }, {
                    title: "Premium VIP Membership",
                    rewardPreamble: "",
                    containerStyle: "vip-tier  vip-tier--tier2 vip-tier--full-panel"
                }, {
                    title: "YoWorld VIP Membership",
                    rewardPreamble: "",
                    containerStyle: "vip-tier  vip-tier--tier1"
                }],
                c = [s.PANEL_SELL_PREMIUM_VIP, s.PANEL_SELL_VIP, s.PANEL_SELL_FREE, s.PANEL_DETAIL_PREMIUM_VIP, s.PANEL_DETAIL_VIP],
                d = "title  title--popped  vip-tier__title";
            a.exports = i.createClass({
                displayName: "SubscriptionPanel",
                propTypes: {
                    panelType: i.PropTypes.string.isRequired,
                    rewardList: i.PropTypes.array.isRequired,
                    productCost: i.PropTypes.oneOfType([i.PropTypes.string, i.PropTypes.number]),
                    subscriptionType: i.PropTypes.string.isRequired,
                    isMember: i.PropTypes.bool,
                    cancelDate: i.PropTypes.string,
                    children: i.PropTypes.element
                },
                render: function() {
                    var e, t = "Your benefits will expire: " + this.props.cancelDate,
                        n = "You'll continue to enjoy these great benefits until your membership expires",
                        a = "Membership",
                        u = i.createElement("em", null),
                        p = i.createElement("em", null),
                        m = l[c.indexOf(this.props.panelType)],
                        h = m.rewardPreamble,
                        _ = m.containerStyle;
                    return e = m.title, this.props.subscriptionType === s.TYPE_VIP ? u = i.createElement(r, {
                        productCost: this.props.productCost
                    }) : this.props.subscriptionType === s.TYPE_PREMIUM_VIP ? u = i.createElement(r, {
                        productCost: this.props.productCost
                    }) : (e = "Basic", a = "Features", u = i.createElement(r, {
                        productCost: this.props.productCost
                    }), p = i.createElement("p", null, "Plus TONS of other features and freebies that make YoWorld your favorite game!")), this.props.isMember && (a = "", u = i.createElement("div", {
                        className: "vip-tier__value"
                    }, i.createElement("div", {
                        className: "full-text"
                    }, "Your Benefits"))), this.props.panelType === s.PANEL_DETAIL_PREMIUM_VIP && (u = i.createElement("em", null)), this.props.cancelDate && this.props.cancelDate.length > 0 && (_ += "  vip-tier--cancelled", u = i.createElement("div", {
                        className: "vip-tier__value"
                    }, i.createElement("div", {
                        className: "full-text"
                    }, "Subscription Cancelled"), i.createElement("div", null, t)), h = n), i.createElement("div", {
                        className: _
                    }, i.createElement("h1", {
                        className: d
                    }, e, i.createElement("br", null), a), u, i.createElement("div", {
                        className: "vip-tier__package-info"
                    }, i.createElement("p", null, h), i.createElement(o, {
                        rewardList: this.props.rewardList,
                        subscriptionType: this.props.subscriptionType
                    }), p), this.props.children)
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, , , , , , , , , , , , , , , , function(e, t, n) {
        var a;
        a = function(e, t, a) {
            function i(e) {
                return o.createElement("div", null, " ", l(e.data).format("MMM DD HH:mm"), " ")
            }
            var o = n(1),
                r = n(2),
                s = n(15),
                l = n(21),
                c = o.createClass({
                    displayName: "costColumnData",
                    render: function() {
                        var e = this.props.rowData.cost;
                        return e > 0 ? o.createElement("div", null, " ", o.createElement("i", {
                            className: "fa fa-arrow-up positive"
                        }, "(", Math.abs(e), ")")) : o.createElement("div", null, " ", o.createElement("i", {
                            className: "fa fa-arrow-down negative"
                        }, "(", Math.abs(e), ")"))
                    }
                }),
                d = o.createClass({
                    displayName: "ChargebackRepaymentResults",
                    propTypes: {},
                    getInitialState: function() {
                        return {}
                    },
                    getColumns: function() {
                        return ["title", "cost", "creationDate"]
                    },
                    btnRepaymentClicked: function(e) {
                        window.triggerRefNamePurchase("yoworld.iap.chargeback_repayment")
                    },
                    render: function() {
                        var e = [{
                                columnName: "title",
                                visible: !0,
                                displayName: "Title"
                            }, {
                                columnName: "cost",
                                visible: !0,
                                displayName: "Cost",
                                customComponent: c
                            }, {
                                columnName: "creationDate",
                                visible: !0,
                                displayName: "Date",
                                customComponent: i
                            }],
                            t = {
                                paddingTop: "10px",
                                paddingBottom: "10px"
                            },
                            n = this.props.purchases.filter(function(e) {
                                return !e.forgiven
                            });
                        return o.createElement("div", {
                            className: "stocks-table"
                        }, o.createElement(s, {
                            results: n,
                            columns: this.getColumns(),
                            columnMetadata: e,
                            useGriddleStyles: !1,
                            showFilter: !0,
                            resultsPerPage: 9,
                            bodyHeight: 240,
                            useFixedHeader: !0,
                            enableInfiniteScroll: !0
                        }), o.createElement("div", {
                            style: t
                        }, o.createElement("button", {
                            className: "btn btn-positive span1of3 push1of3",
                            onClick: this.btnRepaymentClicked
                        }, "Repay $", r.formatDecimal(this.props.amountOwed, 2), " USD")))
                    }
                });
            a.exports = o.createClass({
                displayName: "exports",
                render: function() {
                    return o.createElement("div", null, o.createElement("div", null, o.createElement(d, this.props)))
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        (function(i) {
            "use strict";
            a = function(e, t, a) {
                var o = n(4),
                    r = n(2);
                a.exports = o.create({
                    __modalLayer: null,
                    __container: null,
                    __searchIndex: null,
                    __loaded: !1,
                    initialize: function(e) {
                        this.__modalLayer = e
                    },
                    load: function() {
                        var e = this;
                        r.loadTemplate("invite_frame", function(t, n) {
                            if (t) return void console.error("Failed to load invite_frame template: ", t);
                            var a = i(i.parseHTML(n));
                            e.__container = a, i(a.find(".js-close")).click(function() {
                                console.debug("close"), e.__hideContainer(), e.__modalLayer.hide()
                            }), i(a.find(".js-search")).keyup(function() {
                                e.__applyFilter(i(this).val())
                            }), i(a.find(".js-invite-friends")).click(function() {
                                e.__sendRequests()
                            }), i(a.find(".js-select-all-friends")).click(function() {
                                i(e.__friendContainer).find(":checkbox").each(function(e, t) {
                                    "none" !== t.parentElement.style.display && (t.checked = !0)
                                })
                            }), e.__friendContainer = a.find(".js-friend-content");
                            var o = e.__modalLayer.getContainer();
                            e.__container.prependTo(o), e.__hideContainer(), e.__loaded = !0
                        })
                    },
                    show: function() {
                        if (!this.__loaded) return void console.error("CrewManager not loaded yet!");
                        this.__modalLayer.show();
                        var e = this;
                        FB.api("/me/invitable_friends", "GET", {}, function(t) {
                            e.__renderMultiFriendSelector(t.data), e.__showContainer()
                        })
                    },
                    __showContainer: function() {
                        var e = this.__modalLayer.getZIndex();
                        i(this.__container.find(".search")).val(""), this.__container.show(), this.__container.css("zIndex", e + 1)
                    },
                    __hideContainer: function() {
                        this.__container.hide()
                    },
                    __applyFilter: function(e) {
                        var t = new RegExp("" + e, "i");
                        for (var n in this.__searchIndex) t.test(n) ? this.__searchIndex[n].style.display = "inherit" : this.__searchIndex[n].style.display = "none"
                    },
                    __renderMultiFriendSelector: function(e) {
                        var t = this.__friendContainer;
                        t.empty(), this.__searchIndex = {};
                        for (var n = 0, a = e.length; n < a; ++n) this.__addFriendOption(t, e[n], n)
                    },
                    __addFriendOption: function(e, t, n) {
                        var a = document.createElement("div");
                        a.className = "friend-container";
                        var i = document.createElement("div");
                        i.className = "checkbox";
                        var o = document.createElement("div");
                        o.className = "friend-container__highlight";
                        var r = document.createElement("label");
                        r.setAttribute("for", "cb-" + n);
                        var s = document.createElement("span");
                        s.innerHTML = t.name;
                        var l = document.createElement("img");
                        l.src = t.picture.data.url;
                        var c = document.createElement("input");
                        c.id = "cb-" + n, c.type = "checkbox", c.name = "friends", c.value = t.id, r.appendChild(l), r.appendChild(s), a.appendChild(i), i.appendChild(c), i.appendChild(r), i.appendChild(o), this.__searchIndex[t.name] = a, e.append(a)
                    },
                    __sendRequests: function() {
                        function e(t) {
                            if (0 == t.length) return s.__hideContainer(), void s.__modalLayer.hide();
                            var n = t.splice(0, 50);
                            FB.ui({
                                method: "apprequests",
                                to: n.join(","),
                                title: "Invite friends to YoWorld",
                                message: "Come play YoWorld with me!",
                                data: {
                                    type: "Crew.invite"
                                }
                            }, function(n) {
                                console.debug("App request: ", n), r.POST("/gamecontrol/endpoints.php?ep=sendInvite", n), e(t)
                            })
                        }
                        for (var t = this.__friendContainer, n = t.prop("friends"), a = [], i = 0, o = n.length; i < o; ++i) n[i].checked && a.push(n[i].value);
                        var s = this;
                        e(a)
                    }
                })
            }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
        }).call(t, n(3))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(4),
                o = n(2),
                r = n(7);
            a.exports = i.create({
                CONFIG_REFRESH_TIME: 6e5,
                __offset: 0,
                __eventsQueue: [],
                __eventsURL: "",
                __eventsBlacklist: [],
                __lastConfigFetch: 0,
                __fetchingConfigs: !1,
                __recordEvent: !1,
                __playerId: 0,
                __sessionId: "",
                __dialogIds: {},
                __nextDialogId: 1e3,
                __cachedTracking: {},
                initialize: function(e, t, n) {
                    this.__offset = e, this.__playerId = t, this.__sessionId = n, this.__fetchConfigs()
                },
                __fetchConfigs: function() {
                    if (!this.__fetchingConfigs) {
                        this.__fetchingConfigs = !0;
                        var e = this,
                            t = "/gamecontrol/endpoints.php?ep=getDataCannonConfigs";
                        o.GET(t, function(t, n) {
                            if (e.__lastConfigFetch = Date.now() - e.__offset, e.__fetchingConfigs = !1, t) return void console.error(t);
                            if (!n.error) {
                                if (!n || !n.data_product_id || !n.data_events_url) return void console.error("Error loading data cannon configs.");
                                var a = n.data_product_id,
                                    i = n.data_events_url;
                                for (e.__eventsURL = i + "/api/v1/" + a + "/events", e.__eventsBlacklist = JSON.parse(n.event_blacklist), e.__recordEvent = n.record_events; e.__eventsQueue.length;) e.__sendEvent(e.__eventsQueue.shift())
                            }
                        })
                    }
                },
                getDialogId: function(e) {
                    return this.__dialogIds.hasOwnProperty(e) || (this.__dialogIds[e] = "jsd" + this.__nextDialogId++), this.__dialogIds[e]
                },
                fireEvent: function(e, t, n, a, i) {
                    if ("" !== this.__sessionId) {
                        if (n = n || "", a = a || "", "" === n && (a = ""), i && a === r.DIALOG_TARGET_TYPE) {
                            var s = this.getDialogId(n);
                            e === r.DIALOG_HIDDEN && delete this.__dialogIds[n], n = s
                        }
                        var l = Date.now() - this.__offset;
                        for (var c in t) t[c] = t[c].toString();
                        e === r.SESSION_END || e === r.IAP_CLICKED ? (t.yoCash = this.__cachedTracking.yoCash, t.yoCoins = this.__cachedTracking.yoCoins, t.yoPoints = this.__cachedTracking.yoPoints, t.level = this.__cachedTracking.level) : void 0 !== t.yoCash && void 0 !== t.yoCoins && void 0 !== t.level && void 0 !== t.yoPoints && (this.__cachedTracking.yoCash = t.yoCash, this.__cachedTracking.yoCoins = t.yoCoins, this.__cachedTracking.yoPoints = t.yoPoints, this.__cachedTracking.level = t.level);
                        var d = {
                            platform: "Facebook",
                            timestamp: l,
                            name: e,
                            sessionId: this.__sessionId,
                            eventId: o.guid(),
                            source: this.__playerId,
                            sourceType: "Player",
                            target: n,
                            targetType: a,
                            props: t
                        };
                        return l - this.__lastConfigFetch > this.CONFIG_REFRESH_TIME && this.__fetchConfigs(), "" === this.__eventsURL ? void this.__eventsQueue.push(d) : void this.__sendEvent(d)
                    }
                },
                __sendEvent: function(e) {
                    if (!(this.__eventsBlacklist.indexOf(e.name) > -1)) {
                        if ("" === this.__eventsURL) return void this.__eventsQueue.push(e);
                        if (this.__recordEvent) {
                            var t = "/gamecontrol/data_cannon_endpoint.php";
                            t += "?playerId=" + e.source, t += "&sessionId=" + e.sessionId, t += "&eventType=" + e.name, t += "&eventData=" + encodeURI(JSON.stringify(e.props)), t += "&targetId=" + e.target, t += "&targetType=" + e.targetType, t += "&endpoint=" + this.__eventsURL, t += "&timestamp=" + e.timestamp, o.GET(t, function(e, t) {
                                if (e) return void console.error(e)
                            })
                        } else if (navigator.sendBeacon) navigator.sendBeacon(this.__eventsURL, JSON.stringify(e));
                        else {
                            var n = new XMLHttpRequest;
                            n.open("POST", this.__eventsURL, !0), n.setRequestHeader("Content-type", "text/plain"), n.send(JSON.stringify(e))
                        }
                    }
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        (function(i) {
            "use strict";
            var o = Object.assign || function(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = arguments[t];
                    for (var a in n) Object.prototype.hasOwnProperty.call(n, a) && (e[a] = n[a])
                }
                return e
            };
            a = function(e, t, a) {
                var r = n(4),
                    s = n(2),
                    l = n(13),
                    c = n(56),
                    d = n(59),
                    u = n(1),
                    p = n(8),
                    m = n(80),
                    h = n(67),
                    _ = n(75),
                    f = n(87),
                    g = n(52),
                    v = (n(27), n(62)),
                    b = n(65),
                    y = n(7),
                    E = {
                        generic: "modal_generic",
                        PopFactoryCrew: "recruit_friend",
                        settings: "settings_form",
                        showPicturePicker: "show_picture_picker",
                        transactions: "transactions",
                        lockCodes: "lock_codes",
                        PopupStocks: "stocks",
                        birthday: "modal_birthday",
                        PopupSubscriptions: "subscriptions",
                        newsletter: "modal_newsletter",
                        HelpContainer: "help",
                        chargebackRepayment: "chargebackRepayment",
                        Appeal: "appeal",
                        PiggyBank: "piggy_bank",
                        reportAdPopup: "report_ad_popup"
                    },
                    w = {
                        LOADING: {
                            formClass: ".yopass_form_loading"
                        },
                        DISABLED: {
                            formClass: ".yopass_form_disabled"
                        },
                        ENABLING: {
                            formClass: ".yopass_form_enabling",
                            callback: "__onYoPassEnablingState"
                        },
                        ENABLING_VERIFYING: {
                            formClass: ".yopass_form_enabling_verifying"
                        },
                        ENABLED: {
                            formClass: ".yopass_form_enabled"
                        },
                        DISABLING: {
                            formClass: ".yopass_form_disabling"
                        }
                    },
                    C = 255;
                a.exports = r.create({
                    __modalLayer: null,
                    __showSupportTabFn: null,
                    __refreshGiftInboxFn: null,
                    __interfaces: null,
                    __currentYoPassState: null,
                    __yoPassEnableCheckTimeout: null,
                    __useEmailOnFileForNewsletter: null,
                    __fbPhotos: null,
                    __currentBlockListIndex: 0,
                    __vipWarn: !1,
                    initialize: function(e, t, n) {
                        this.__modalLayer = e, this.__showSupportTabFn = t, this.__refreshGiftInboxFn = n, this.__interfaces = {}, this.__fbPhotos = new c, this.__lockCodes = new d(this), this.__currentYoPassState = w.LOADING, this.__useEmailOnFileForNewsletter = !0
                    },
                    load: function(e) {
                        function t(e, t) {
                            s.loadTemplate(e, function(n, o) {
                                if (n) return console.error("Failed to load template " + e + ": ", n), t(n);
                                var r = i(i.parseHTML(o));
                                i(r.find(".close_button")).click(function(t) {
                                    a.__cancelYoPassCheck(), "stocks" === e && (s.dispatchFlashMessage("currencyUpdate", {}, function() {}), t.hasOwnProperty("originalEvent") && "click" == t.originalEvent.type && window.fireDataEvent(y.DIALOG_BUTTON_CLICKED, {
                                        button: "X",
                                        dialog: y.STOCK_TARGET_ID
                                    }, y.STOCK_TARGET_ID, y.DIALOG_TARGET_TYPE)), console.debug("Closing " + e), "lock_codes" == e && s.dispatchFlashMessage("lockCodeBoxClosed", {}), a.__hideContainer(r, e), a.__modalLayer.hide()
                                });
                                var l = a.__modalLayer.getContainer();
                                r.prependTo(l), r.hide(), t(void 0, r)
                            })
                        }

                        function n() {
                            o.signal()
                        }
                        var a = this,
                            o = new l(function() {
                                a.__loaded = !0, e && e()
                            });
                        o.wait(1);
                        var r = Object.keys(E);
                        r.forEach(function(e) {
                            o.wait(1), t(E[e], function(t, i) {
                                return t ? (console.error("Failed to load " + e), void n()) : (a.__interfaces[e] = i, void a.__onInterfaceLoaded(e, n))
                            })
                        }), n()
                    },
                    hide: function(e) {
                        var t = this.__interfaces[e];
                        t && t.hide()
                    },
                    show: function(e, t) {
                        if (!this.__loaded) return void console.error("ExternalUIController is not yet loaded!  Cannot show " + e);
                        var n = this.__interfaces[e];
                        if (!n) {
                            console.error("Skipping external UI: " + e);
                            var a = document.querySelector("#game_swf_div");
                            return void a.unsupportedOperation()
                        }
                        this.__modalLayer.show();
                        var i = this;
                        i.__showInterface(e, t, function(e) {
                            i.__hideContainer(n, null), i.__modalLayer.hide()
                        }), i.__renderInterface(e, t, function(t) {
                            return t ? void i.__modalLayer.hide() : (i.__showContainer(n), console.debug("showing container: ", n), void("showPicturePicker" == e ? (i.__fbPhotos.registerButtonHandlers(), i.__fbPhotos.getFbPhotos()) : "PopupStocks" == e && window.fireDataEvent(y.DIALOG_SHOWN, {
                                dialog: y.STOCK_TARGET_ID
                            }, y.STOCK_TARGET_ID, y.DIALOG_TARGET_TYPE)))
                        })
                    },
                    __showContainer: function(e) {
                        var t = this.__modalLayer.getZIndex();
                        e.css("zIndex", t + 1), e.show(), e.find(".auto_focus").focus()
                    },
                    __hideContainer: function(e, t) {
                        "stocks" === t && window.fireDataEvent(y.DIALOG_HIDDEN, {
                            dialog: y.STOCK_TARGET_ID
                        }, y.STOCK_TARGET_ID, y.DIALOG_TARGET_TYPE), e.hide()
                    },
                    __onInterfaceLoaded: function(e, t) {
                        switch (e) {
                            case "settings":
                                return void this.__onSettingsLoaded(this.__interfaces[e], t);
                            case "newsletter":
                                return void this.__setupNewsletter(this.__interfaces[e], t);
                            case "birthday":
                                return void this.__setupBirthday(this.__interfaces[e], t)
                        }
                        setTimeout(t, 0)
                    },
                    __onSettingsLoaded: function(e, t) {
                        i(e.find("#refresh_friend_cache_btn")).click(function() {
                            i(e.find("#refresh_friend_cache_btn")).css("display", "none"), i(e.find("#refresh_friend_cache_locked")).html("You can refresh the cache again in 3hrs."), s.POST("/gamecontrol/endpoints.php?ep=refreshFriendCache", {}, function(t, n) {
                                var a = i(e.find(".messages"));
                                if (a.empty(), t) return void a.append("<span class='error'> Uh oh, we couldn't reset your friends cache right now! Please try again later.</span>")
                            })
                        }), i(e.find(".submit_account")).click(function() {
                            function t(t) {
                                return n.css("opacity", 1), n.val("Save"), n.prop("disabled", !1), "" !== t.response ? (i(e.find(".character_name")).val(r), void a.append('<span class="error">' + (t.response ? t.response : "Invalid name.") + "</span>")) : (i(e.find(".character_name")).attr("data-original-name", o), void s.POST("/gamecontrol/endpoints.php?ep=setSettings", {
                                    gender: l,
                                    allow_buddy_requests: c,
                                    allow_private_messages: d,
                                    allow_trade_requests: u,
                                    allow_game_requests: p,
                                    allow_action_requests: m,
                                    allow_apartment_messages: h,
                                    enable_profanity_filter: _
                                }, function(e, t) {
                                    if (e) return a.append("<span class='section error'> Uh oh, we couldn't save your settings! Please try again later.</span>"), void console.error("Failed to save settings", e);
                                    var n = t.status;
                                    return n ? void("error" === n ? a.append('<span class="section error">' + (t.msg ? t.msg : "Invalid name.") + "</span>") : s.POST("/gamecontrol/endpoints.php?ep=setGraphicSettings", {
                                        allow_clothing_animations: f,
                                        allow_room_animations: g,
                                        allow_entrance_actions: v,
                                        hide_snow: b,
                                        show_player_pets: y,
                                        do_prank: E
                                    }, function(e, t) {
                                        if (e) return a.append("<span class='section error'> Uh oh, we couldn't save your settings! Please try again later. </span>"), console.error("Failed to save settings", e), void window.scrollTo(0, 0);
                                        var n = t.status;
                                        return n ? void("error" === n ? (a.append('<span class="section error">' + (t.msg ? t.msg : "Invalid name.") + "</span>"), window.scrollTo(0, 0)) : (a.append('<span class="section success">Your settings were saved</span>'), a.append('<span class="section success">Please reload the game</span>'), window.scrollTo(0, 0))) : (a.append("<span class='section error'> Uh oh, we couldn't save your settings! Please try again later. </span>"), console.error("Response didn't have a status"), void window.scrollTo(0, 0))
                                    })) : (a.append("<span class='section error'> Uh oh, we couldn't save your settings! Please try again later. </span>"), void console.error("Response didn't have a status"))
                                }))
                            }
                            var n = i(this),
                                a = i(e.find(".messages"));
                            a.empty();
                            var o = i(e.find(".character_name")).val(),
                                r = i(e.find(".character_name")).attr("data-original-name"),
                                l = i(e.find("input[name=gender]:checked")).val(),
                                c = i(e.find("input[name=buddy_requests]:checked")).val(),
                                d = i(e.find("input[name=private_messages]:checked")).val(),
                                u = i(e.find("input[name=trade_requests]:checked")).val(),
                                p = i(e.find("input[name=game_requests]:checked")).val(),
                                m = i(e.find("input[name=action_requests]:checked")).val(),
                                h = i(e.find("input[name=apartment_messages]:checked")).val(),
                                _ = i(e.find("input[name=profanity_filter]:checked")).val();
                            i(this).css("opacity", .5), i(this).val("Saving..."), i(this).prop("disabled", !0);
                            var f = i(e.find("input[name=clothing_animations]:checked")).val(),
                                g = i(e.find("input[name=room_animations]:checked")).val(),
                                v = i(e.find("input[name=entrance_actions]:checked")).val(),
                                b = i(e.find("input[name=hide_snow]:checked")).val(),
                                y = i(e.find("input[name=show_player_pets]:checked")).val(),
                                E = i(e.find("input[name=do_prank]:checked")).val();
                            o == i(e.find(".character_name")).attr("data-original-name") ? t({
                                response: ""
                            }) : s.dispatchFlashMessage("changeName", {
                                name: o
                            }, t)
                        });
                        var n = this;
                        i(e.find(".yopassbutton")).click(function() {
                            s.GET("/gamecontrol/yopass_endpoints.php?ep=isEligibleForYoPass", function(t, a) {
                                var o = i(e.find(".messages"));
                                if (o.empty(), t) return n.__setYoPassState(e, w.DISABLED), void o.append('<span class="error">Failed to determine eligibility: ' + t + "</span>");
                                if (a.message) return n.__setYoPassState(e, w.DISABLED), void o.append('<span class="error">' + a.message + "</span>");
                                switch (n.__currentYoPassState) {
                                    case w.DISABLED:
                                        n.__setYoPassState(e, w.LOADING), s.GET("/gamecontrol/yopass_endpoints.php?ep=getYoPassGenerateLinkCode", function(t, a) {
                                            return t ? (n.__setYoPassState(e, w.DISABLED), void o.append('<span class="error">Failed to generate Link Code: ' + t + "</span>")) : a.message ? (n.__setYoPassState(e, w.DISABLED), void o.append('<span class="error">' + a.message + "</span>")) : (n.__setPrettyLinkCode(e, a.linkCode), void n.__setYoPassState(e, w.ENABLING))
                                        });
                                        break;
                                    case w.ENABLING_VERIFYING:
                                        n.__setYoPassState(e, w.LOADING);
                                        var r = i(e.find("#yopass_activate_input")).val();
                                        s.POST("/gamecontrol/yopass_endpoints.php?ep=activateYoPass", {
                                            yoPassCode: r
                                        }, function(t, a) {
                                            return t ? (n.__setYoPassState(e, w.ENABLING_VERIFYING), void o.append('<span class="error">Failed to link YoPass: ' + t + "</span>")) : a.message ? (n.__setYoPassState(e, w.ENABLING_VERIFYING), void o.append('<span class="error">' + a.message + "</span>")) : a.success ? void n.__setYoPassState(e, w.ENABLED) : (n.__setYoPassState(e, w.ENABLING_VERIFYING), void o.append('<span class="error">Unknown error while linking YoPass</span>'))
                                        });
                                        break;
                                    case w.ENABLED:
                                        n.__setYoPassState(e, w.DISABLING);
                                        break;
                                    case w.DISABLING:
                                        n.__setYoPassState(e, w.LOADING);
                                        var r = i(e.find("#yopass_deactivate_input")).val();
                                        s.POST("/gamecontrol/yopass_endpoints.php?ep=disableYoPass", {
                                            yoPassCode: r
                                        }, function(t, a) {
                                            return t ? (n.__setYoPassState(e, w.DISABLING), void o.append('<span class="error">Failed to disable YoPass: ' + t + "</span>")) : a.message ? (n.__setYoPassState(e, w.DISABLING), void o.append('<span class="error">' + a.message + "</span>")) : a.success ? void n.__setYoPassState(e, w.DISABLED) : (n.__setYoPassState(e, w.DISABLING), void o.append('<span class="error">Unknown error while disabling YoPass</span>'))
                                        });
                                        break;
                                    case w.LOADING:
                                    case w.ENABLING:
                                }
                            })
                        });
                        var n = this;
                        i(e.find(".support_submit_support")).click(function() {
                            i(e.find(".close_button")).click(), n.__showSupportTabFn()
                        }), i(e.find(".setup_newsletter")).click(function() {
                            i(e.find(".js-close")).click(), n.showNewsletter()
                        }), i(e.find(".setup_birthday")).click(function() {
                            i(e.find(".close_button")).click(), n.showBirthdaySetup()
                        }), setTimeout(t, 0)
                    },
                    showSubscription: function() {
                        var e = this;
                        s.POST("/gamecontrol/lock_code_endpoints.php?ep=getLockedStatus", {}, function(t, n) {
                            return t ? void console.error(t) : n.error ? void console.error(n.error) : void(n.locked || "3" === n.level ? e.show("lockCodes", {
                                type: "purchase",
                                trigger: function() {
                                    e.show("PopupSubscriptions")
                                }
                            }) : e.show("PopupSubscriptions"))
                        })
                    },
                    showNewsletter: function() {
                        var e = this;
                        e.__modalLayer.show(), s.GET("/gamecontrol/support.php?ep=getEmail", function(t, n) {
                            var a = "";
                            t || (a = n.email), s.POST("/gamecontrol/lock_code_endpoints.php?ep=getLockedStatus", {}, function(t, n) {
                                return t ? void console.error(t) : n.error ? void console.error(n.error) : void(n.locked || "3" === n.level ? e.show("lockCodes", {
                                    type: "newsletter",
                                    trigger: function() {
                                        e.show("newsletter", {
                                            email: a
                                        })
                                    }
                                }) : e.show("newsletter", {
                                    email: a
                                }))
                            })
                        })
                    },
                    showBirthdaySetup: function() {
                        var e = this;
                        s.GET("/gamecontrol/birthday.php?ep=getBirthday", {}, function(t, n) {
                            if (t) return void console.error(t);
                            if (n || console.error("showBirthdaySetup: No response received"), n.error) return void console.error(n.error);
                            if (!n.birthdayInfo) return void console.error("showBirthdaySetup: Invalid response received");
                            var a = n.birthdayInfo.dob,
                                i = n.birthdayInfo.in_birthday_program;
                            e.show("birthday", {
                                birthday: a,
                                inBirthdayProgram: i
                            })
                        })
                    },
                    bannerClicked: function(e) {
                        var t = this;
                        s.GET("/gamecontrol/endpoints.php?ep=getSettings", function(n, a) {
                            if (!a.player_id) return void console.error("Cannot find player id");
                            var i = a.player_id;
                            s.POST("/gamecontrol/admin.php?ep=getBonusOffer", {
                                playerId: i
                            }, function(n, a) {
                                if (!a.name || a.name.indexOf("YoMo") === -1) return void window.triggerPurchasePopup();
                                switch (e) {
                                    case "mcAppStore":
                                        t.openURL("https://itunes.apple.com/us/app/yoworld-mobile-companion-app/id1033581397?ls=1&mt=8");
                                        break;
                                    case "mcPlayStore":
                                        t.openURL("https://play.google.com/store/apps/details?id=com.bigvikinggames.YoMo")
                                }
                            })
                        })
                    },
                    openURL: function(e) {
                        var t = window.open(e, "_blank");
                        t ? t.focus() : console.error("Failed to open url: " + e)
                    },
                    __addShowMoreBlockedPlayersHandler: function(e) {
                        var t = this;
                        i(e.find(".show_more")).click(function() {
                            var n = i(e.find(".blocked_players")),
                                a = i(e.find(".blocked_loading"));
                            i(e.find("#show_blocked_players")).hide(), n.hide(), a.show();
                            var o = i(e.find(".messages"));
                            o.empty(), s.POST("/gamecontrol/endpoints.php?ep=getBlockedList", {
                                startIndex: t.__currentBlockListIndex
                            }, function(r, l) {
                                if (n.show(), a.hide(), r) return void o.append('<span class="error">Couldn\'t read blocked list from server.</span>');
                                if (l.message) return void o.append('<span class="error">' + l.message + "</span>");
                                var c = l.list;
                                return c ? (n.empty(), c.forEach(function(a) {
                                    var o = i('<div class="blocked_player">'),
                                        r = i('<span class="unblock_player">Unblock</span>');
                                    r.click(function() {
                                        r.css("opacity", .5), r.text("Unblocking..."), r.prop("disabled", !0);
                                        var n = i(e.find(".messages"));
                                        n.empty(), s.POST("/gamecontrol/endpoints.php?ep=removeBlockedPlayer", {
                                            blockedPlayerId: a.playerId
                                        }, function(n, a) {
                                            if (r.css("opacity", 1), r.text("Unblock"), r.prop("disabled", !1), n) return void l.append('<span class="error">Couldn\'t unblock player.</span>');
                                            if (a.message) return void l.append('<span class="error">' + a.message + "</span>");
                                            o.remove(), t.__currentBlockListIndex--;
                                            var l = i(e.find(".messages"));
                                            l.empty(), l.append('<span class="section success">Your new blocked buddy list saved</span>'), a.list ? s.dispatchFlashMessage("updateBlockedBuddyList", {
                                                list: a.list
                                            }) : l.append('<span class="section success">Please reload the game</span>')
                                        })
                                    }), o.text(a.name), o.append(r), n.append(o)
                                }), l.hasMore && (n.append('<div class="blocked_player"><div class="show_more  link">Show More...</div></div>'), t.__addShowMoreBlockedPlayersHandler(e)), void(t.__currentBlockListIndex += c.length)) : void o.append('<span class="error">Response didn\'t have a list.</span>')
                            })
                        })
                    },
                    __renderInterface: function(e, t, n) {
                        switch (e) {
                            case "PopFactoryCrew":
                                return void this.__renderFactoryCrew(this.__interfaces[e], t, n);
                            case "settings":
                                return void this.__renderSettings(this.__interfaces[e], t, n);
                            case "transactions":
                                return void this.__renderTransactionLog(this.__interfaces[e], t, n);
                            case "lockCodes":
                                return void this.__lockCodes.renderLockCodes(this.__interfaces[e], t, n);
                            case "PopupStocks":
                                return void this.__renderStocksPage(this.__interfaces[e], t, n);
                            case "PopupSubscriptions":
                                return void this.__renderSubscriptionPage(this.__interfaces[e], t, !1, n);
                            case "newsletter":
                                return void this.__renderNewsletter(this.__interfaces[e], t, n);
                            case "HelpContainer":
                                return void this.__renderHelpContainer(this.__interfaces[e], t, n);
                            case "birthday":
                                return void this.__renderBirthdaySetup(this.__interfaces[e], t, n);
                            case "chargebackRepayment":
                                return void this.__renderChargebackRepayment(this.__interfaces[e], t, n);
                            case "Appeal":
                                return void this.__renderAppealForm(this.__interfaces[e], t, n);
                            case "PiggyBank":
                                return void this.__renderPiggyBankDialog(this.__interfaces[e], t, n);
                            case "reportAdPopup":
                                return void this.__renderReportAdPopup(this.__interfaces[e], t, n)
                        }
                        setTimeout(n, 0)
                    },
                    __renderNewsletter: function(e, t, n) {
                        var a = this;
                        this.__useEmailOnFileForNewsletter = !0;
                        var o = i(e.find(".senderEmail"));
                        o.val(t.email), o.one("focus", function() {
                            o.val(""), a.__useEmailOnFileForNewsletter = !1
                        }), n()
                    },
                    setupVIPWarn: function(e) {
                        var t = i("#header-vip-warn");
                        this.__vipWarn = e, e ? (t.removeClass("hidden"), t.html("!")) : (t.addClass("hidden"), t.html(""))
                    },
                    __setupNewsletter: function(e, t) {
                        var n = this;
                        i(e.find(".js-submit")).click(function() {
                            var t = i(e.find(".js-form")),
                                a = t.find(":input");
                            if (!n.__useEmailOnFileForNewsletter) {
                                var o = !1;
                                if (t.checkValidity) o = t.checkValidity();
                                else {
                                    var r = a[0],
                                        l = r.value; - 1 !== l.indexOf(".") && -1 !== l.indexOf("@") || (o = !0)
                                }
                                if (o) return void i(a[0]).addClass("error");
                                i(a[0]).removeClass("error")
                            }
                            var c = i(e.find(".js-submit"));
                            c.val("Submitting..."), c.addClass("btn-disabled"), c.prop("disabled", !0);
                            var r = a[0].value,
                                d = {
                                    email: r,
                                    useEmailOnFile: n.__useEmailOnFileForNewsletter && "" != r
                                };
                            s.POST("/gamecontrol/newsletter.php?ep=confirmEmail", d, function(t, a) {
                                return c.val("Sign up and get Free Item"), c.removeClass("btn-disabled"), c.prop("disabled", !1), !t && a && a.status ? "error" === a.status ? (i("#generic_modal .header-bar").removeClass().addClass("header-bar header-bar--warning"), i("#generic_modal .window_title").html("Submission Failed"), i("#generic_modal .subtitle").html("Oh, darn!"), i("#generic_modal .message").html("Something went wrong confirming your email address.<br />" + a.msg), i("#generic_modal").css("zIndex", 9001), void i("#generic_modal").show()) : (i("#generic_modal .header-bar").removeClass().addClass("header-bar header-bar--default"), i("#generic_modal .window_title").html("Submission Success"), i("#generic_modal .subtitle").html("Thank you!"), i("#generic_modal .message").html("We have sent an email to " + r + ".<br/>It should be delivered immediately but may take a few minutes during busy periods."), i("#generic_modal").css("zIndex", 9001), i(e.find(".js-close")).click(), n.__modalLayer.show(), void i("#generic_modal").show()) : (i("#generic_modal .header-bar").removeClass().addClass("header-bar header-bar--warning"), i("#generic_modal .window_title").html("Submission Failed"), i("#generic_modal .subtitle").html("Oh, darn!"), i("#generic_modal .message").html("Something went wrong confirming your email address."), i("#generic_modal .message").append("<br />Please try again.  If this problem persists, please email us directy at <strong>yoworld@bigvikinggames.com</strong> with the subject: <strong>'Email Confirmation Error'</strong>"), i("#generic_modal").css("zIndex", 9001), void i("#generic_modal").show())
                            })
                        }), t()
                    },
                    __setupBirthday: function(e, t) {
                        i(e.find(".js-birthday_confirm_button")).click(function() {
                            var t = i(e.find(".submit"));
                            t.prop("disabled", !0);
                            var n = i(e.find(".senderBirthday"))[0],
                                a = n.value;
                            s.POST("/gamecontrol/birthday.php?ep=enterBirthday", {
                                birthday: a
                            }, function(t, n) {
                                return e.hide(), !t && n && n.status && "error" !== n.status ? (i("#generic_modal .header-bar").removeClass().addClass("header-bar header-bar--default"), i("#generic_modal .window_title").html("Submission Success"), i("#generic_modal .subtitle").html(""), i("#generic_modal .message").html("Thanks! You have been signed up for the birthday program!"), i("#generic_modal").css("zIndex", 9001), void i("#generic_modal").show()) : (i("#generic_modal .header-bar").removeClass().addClass("header-bar header-bar--warning"), i("#generic_modal .window_title").html("Submission Failed"), i("#generic_modal .subtitle").html(""), n && n.status && "error" === n.status ? i("#generic_modal .message").html("Uh oh.  Something went wrong with submitting your birthday. " + n.msg) : (i("#generic_modal .message").html("Uh oh.  Something went wrong with submitting your birthday."), i("#generic_modal .message").append("<br />Please try again.  If this problem persists, please email us directy at <strong>yoworld@bigvikinggames.com</strong> with the subject: <strong>'Birthday Submission Error'</strong>")), i("#generic_modal").css("zIndex", 9001), void i("#generic_modal").show())
                            })
                        }), t()
                    },
                    __showInterface: function(e, t, n) {
                        switch (e) {
                            case "PopFactoryCrew":
                                return this.__showFactoryCrew(this.__interfaces[e], t, n);
                            case "lockCodes":
                            case "newsletter":
                            case "birthday":
                            case "settings":
                            case "PopupStocks":
                            case "PopupSubscriptions":
                            case "PopupVIPSellPage":
                            case "transactions":
                            case "HelpContainer":
                            case "chargebackRepayment":
                            case "Appeal":
                            case "PiggyBank":
                            case "reportAdPopup":
                                return
                        }
                        setTimeout(n, 0)
                    },
                    __renderPiggyBankDialog: function(e, t, n) {
                        var a = i(e.find(".main")).get(0);
                        p.unmountComponentAtNode(a), p.render(u.createElement(v, o({}, t, {
                            closeCallback: function() {
                                p.unmountComponentAtNode(a), i(e.find(".close_button")).click()
                            }
                        })), a, function(e) {
                            n()
                        })
                    },
                    __renderTransactionLog: function(e, t, n) {
                        var a = i(e.find(".main")).get(0);
                        p.unmountComponentAtNode(a), p.render(u.createElement(h, null), a, function(e) {
                            n()
                        })
                    },
                    __renderReportAdPopup: function(e, t, n) {
                        var a = i(e.find(".main")).get(0);
                        p.unmountComponentAtNode(a), p.render(u.createElement(b, {
                            networkId: t.networkId,
                            closeCallback: function() {
                                s.dispatchFlashMessage("adNetworkReported", {
                                    networkId: t.networkId
                                }), p.unmountComponentAtNode(a), i(e.find(".close_button")).click()
                            }
                        }), a, function(e) {
                            n()
                        })
                    },
                    __renderChargebackRepayment: function(e, t, n) {
                        var a = i(e.find(".main")).get(0);
                        p.unmountComponentAtNode(a), p.render(u.createElement(g, {
                            purchases: t.purchases,
                            amountOwed: t.amountOwed
                        }), a, function(e) {
                            n()
                        })
                    },
                    __renderHelpContainer: function(e, t, n) {
                        var a = i(e.find(".main")).get(0);
                        p.unmountComponentAtNode(a);
                        var e = u.createElement(_, {
                            firstName: FIRST_NAME,
                            lastName: LAST_NAME,
                            supportManager: t.supportManager
                        });
                        p.render(e, a, function(e) {
                            n()
                        })
                    },
                    __renderStocksPage: function(e, t, n) {
                        function a() {
                            this.updateCoinBalance(), n()
                        }
                        var o = i(e.find(".main")).get(0);
                        p.unmountComponentAtNode(o), p.render(u.createElement(m, null), o, a)
                    },
                    __renderSubscriptionPage: function(e, t, n, a) {
                        function i(t) {
                            p.unmountComponentAtNode(r), o.__hideContainer(e, "subscriptions"), o.__modalLayer.hide(), t && o.__refreshGiftInboxFn()
                        }
                        var o = this,
                            r = e.get(0),
                            s = this.__modalLayer.getZIndex(),
                            s = s + 1;
                        p.unmountComponentAtNode(r), p.render(u.createElement(f, {
                            vipOnly: n,
                            vipWarn: o.__vipWarn,
                            closeCallback: i,
                            updateVipWarn: o.setupVIPWarn.bind(o),
                            zIndex: s
                        }), r, function(e) {
                            a()
                        })
                    },
                    __renderFactoryCrew: function(e, t, n) {
                        FB.api("/me/friends", "GET", {}, function(e) {
                            console.debug(e), n()
                        })
                    },
                    __renderBirthdaySetup: function(e, t, n) {
                        var a = i(e.find(".senderBirthday"));
                        a.val(t.birthday), a.one("focus", function() {
                            a.val("")
                        });
                        var o = i(e.find(".submit"));
                        o.prop("disabled", !1), n()
                    },
                    __renderAppealForm: function(e, t, n) {
                        var a = this;
                        s.GET("/gamecontrol/support.php?ep=getEmail", function(o, r) {
                            if (o || !r || "success" !== r.status) {
                                var l = t.supportManager;
                                return void l.loadConfirmationModal(r.email, 0, function() {
                                    l.show()
                                })
                            }
                            var c = i(e.find(".messages"));
                            c.empty();
                            var d = i(e.find("#senderEmail"))[0];
                            d.value = r.email;
                            var u = i(e.find("#characterCount")),
                                p = i(e.find("#appealText"));
                            u.text(C - p[0].textLength), p.on("input", function(e) {
                                u.text(C - e.target.value.length)
                            }), i(e.find("#closeButton")).click(function() {
                                a.__hideContainer(e, "appeal"), a.__modalLayer.hide()
                            }), i(e.find("#submitButton")).click(function() {
                                s.POST("/gamecontrol/appeal_ban.php?ep=submitAppeal", {
                                    appealText: p[0].value
                                }, function(t, n) {
                                    return c.empty(), t ? (console.error("Failed to submit appeal", t), void c.append("<h3><span style='color:#FF0000'>There was a problem submitting your appeal. Try again later.</span></h3>")) : "success" !== n.msg ? void c.append("<h3><span style='color:#FF0000'>" + n.msg + "</span></h3>") : (a.__hideContainer(e, "appeal"), a.__modalLayer.hide(), void i("#appealStatusText").text("Your appeal has been submitted, and will be reviewed shortly. Thank you for your patience."))
                                })
                            }), n()
                        })
                    },
                    __renderSettings: function(e, t, n) {
                        var a = this,
                            o = i(e.find(".messages"));
                        o.empty();
                        var r = new l(n);
                        r.wait(7), s.GET("/gamecontrol/endpoints.php?ep=getSettings", function(n, a) {
                            if (n) return console.error("Failed to get character settings", n), void r.signalError(n);
                            if (!a.player_id) return console.error("Failed to get player id, no player_id property"), void r.signalError(new Error("Missing player_id property in getSettings response"));
                            if (!a.name) return console.error("Failed to get character name, no name property"), void r.signalError(new Error("Missing name property in getSettings response"));
                            if (!a.gender) return console.error("Failed to get character gender, no gender property"), void r.signalError(new Error("Missing gender property in getSettings response"));
                            if (i(e.find(".player_id")).html("  " + a.player_id + "  "), i(e.find(".client_version")).html("  " + t.clientVersion + "  "), i(e.find(".character_name")).val(a.name), i(e.find(".character_name")).attr("data-original-name", a.name), i(e.find("input[name=gender]")).val([a.gender]), i(e.find("input[name=buddy_requests]")).val([a.allow_buddy_requests]), i(e.find("input[name=private_messages]")).val([a.allow_private_messages]), i(e.find("input[name=trade_requests]")).val([a.allow_trade_requests]), i(e.find("input[name=game_requests]")).val([a.allow_game_requests]), i(e.find("input[name=action_requests]")).val([a.allow_action_requests]), i(e.find("input[name=apartment_messages]")).val([a.allow_apartment_messages]), i(e.find("input[name=profanity_filter]")).val([a.enable_profanity_filter]), 0 == a.pending_friend_cache_clear) i(e.find("#refresh_friend_cache_btn")).css("display", "block"), i(e.find("#refresh_friend_cache_locked")).html("");
                            else {
                                var o = Math.floor(a.pending_friend_cache_clear / 60);
                                if (o < 60) i(e.find("#refresh_friend_cache_locked")).html(o + " mins remaining");
                                else {
                                    var s = Math.floor(o / 60),
                                        l = 1 == s ? "hr " : "hrs ";
                                    i(e.find("#refresh_friend_cache_locked")).html("> " + s + l + "remaining to reset again"), i(e.find("#refresh_friend_cache_btn")).css("display", "none")
                                }
                            }
                            r.signal()
                        }), s.GET("/gamecontrol/endpoints.php?ep=getGraphicSettings", function(t, n) {
                            return t ? (console.error("Failed to get character settings", t), void r.signalError(t)) : (i(e.find("input[name=clothing_animations]")).val([n.allow_clothing_animations]), i(e.find("input[name=room_animations]")).val([n.allow_room_animations]), i(e.find("input[name=entrance_actions]")).val([n.allow_entrance_actions]), i(e.find("input[name=hide_snow]")).val([n.hide_snow]), i(e.find("input[name=show_player_pets]")).val([n.show_player_pets]), n.prank_active ? i(e.find("#prank")).css("display", "block") : i(e.find("#prank")).css("display", "none"), i(e.find("input[name=do_prank]")).val([n.do_prank]), void r.signal())
                        }), s.GET("/gamecontrol/yopass_endpoints.php?ep=getYoPassStatus", function(t, n) {
                            return t ? void console.error("Failed to get YoPass status", t) : (n.enabled ? a.__setYoPassState(e, w.ENABLED) : n.linkCode ? (a.__setPrettyLinkCode(e, n.linkCode), a.__setYoPassState(e, w.ENABLING)) : a.__setYoPassState(e, w.DISABLED), void r.signal())
                        }), s.GET("/gamecontrol/lock_code_endpoints.php?ep=getActiveStatus", function(t, n) {
                            var o = i(e.find(".messages"));
                            return t || !n || null === n.status ? (console.error("Failed to get Lock Code status"), void r.signal()) : n.error ? (o.append("<span class='error'>" + n.error + "</span>"), console.error("Failed to get Lock Code status"), void r.signal()) : (a.__lockCodes.setUpLockCodeSettings(e, n), void r.signal())
                        }), s.GET("/gamecontrol/support.php?ep=getEmail", function(t, n) {
                            if (t || !n || "success" !== n.status) return console.error("Failed to get support email"), void r.signal();
                            var a = n.email;
                            i("#support_email").html(a), i(e.find(".support_submit_support")).hide(), i(e.find(".support_submit")).show().click(function() {
                                s.POST("/gamecontrol/support.php?ep=removeEmail", {}, function(t, n) {
                                    return t ? void console.error("Failed to remove email: ", t) : "error" == n.status ? (i(e.find(".messages")).empty(), i(e.find(".messages")).append("<span class='error'>" + n.msg + "</span>"), console.error("Failed to remove email: ", n.msg), void window.scrollTo(0, 0)) : (i("#support_email").html("None"), i(e.find(".support_submit")).hide(), void i(e.find(".support_submit_support")).show())
                                })
                            }), r.signal()
                        }), s.GET("/gamecontrol/newsletter.php?ep=isSubscribed", function(t, n) {
                            if (t || !n || "success" !== n.status || null === n.data) return console.error("Failed to check if subscribed to news letter "), void r.signal();
                            var a = n.data.isSubscribed;
                            return a ? (a && (i("#newsletter_status").html("Subscribed"), i("#newsletter_status").css("color", "green")), i(e.find(".setup_newsletter")).hide(), i(e.find(".remove_newsletter")).show().click(function() {
                                s.POST("/gamecontrol/newsletter.php?ep=unsubscribe", {}, function(t, n) {
                                    return "error" == n.status ? (i(e.find(".messages")).empty(), i(e.find(".messages")).append("<span class='error'>" + n.msg + "</span>"), console.error("Failed to remove subscription: ", n.msg), void window.scrollTo(0, 0)) : (i("#newsletter_status").html("Not Subscribed!"), i("#newsletter_status").css("color", "grey"), i(e.find(".remove_newsletter")).hide(), void i(e.find(".setup_newsletter")).show())
                                })
                            }), void r.signal()) : void r.signal()
                        }), s.GET("/gamecontrol/casino_lock_out.php?ep=lockoutStatus", function(t, n) {
                            if (t || !n || "success" !== n.status) return console.error("Failed to check casino lockout status "), void r.signal();
                            var a = function(e) {
                                i("#casino_lock_status").html("Locked until " + e + " (UTC)"), i("#casino_lock_status").css("color", "red"), i("#casino_lock_settings").hide(), i("#casino_lock_out_btn").hide()
                            };
                            n.lockoutActive ? a(n.lockOutEnd) : i("#casino_lock_out_btn").click(function() {
                                var t = i("input[name=casino_lock_duration_level]:checked").val();
                                void 0 != t && (i("#casino_lock_out_btn").prop("disabled", !0), s.POST("/gamecontrol/casino_lock_out.php?ep=setLockout&level=" + t, {}, function(t, n) {
                                    return "error" == n.status ? (i(e.find(".messages")).empty(), i(e.find(".messages")).append("<span class='error'>" + n.msg + "</span>"), console.error("Failed to set lockout: ", n.msg), window.scrollTo(0, 0), void i("#casino_lock_out_btn").prop("disabled", !1)) : (s.dispatchFlashMessage("updateCasinoLockOut", {
                                        lockOutEnd: n.lockOutEnd
                                    }), void a(n.lockOutEnd))
                                }))
                            }), r.signal()
                        }), s.GET("/gamecontrol/birthday.php?ep=getBirthday", function(t, n) {
                            if (t || !n || !n.birthdayInfo || "success" !== n.status) return console.error("Failed to get birthday"), void r.signal();
                            var a = n.birthdayInfo.dob,
                                o = n.birthdayInfo.in_birthday_program;
                            "1" === o && (i(e.find(".setup_birthday")).hide(), i(e.find("#birthday_message")).html("You are currently signed up for the birthday program!")), null !== a && (i(e.find("#dob")).html(a), i(e.find("#dob")).css("color", "black")), r.signal()
                        });
                        var c = i(e.find(".blocked_players"));
                        c.hide(), c.empty();
                        var d = i(e.find("#show_blocked_players"));
                        d.empty(), d.append('<div class="show_more  link">Show List...</div>'), d.show();
                        var u = i(e.find(".player-settings")),
                            p = i(e.find(".player-settings-tab")),
                            m = i(e.find(".tabs__content > div")),
                            h = i(e.find(".tabs__nav > li")),
                            _ = i(e.find(".tabs__nav > li.selected"));
                        m.hide(), u.show(), _.removeClass("selected"), _ = p, _.addClass("selected"), h.off("click"), h.on("click", function(t) {
                            if (this.className.indexOf("selected") == -1) {
                                m.hide();
                                var n = this.className.substr(0, this.className.indexOf("-tab")),
                                    a = i(e.find("." + n));
                                a.show(), _.removeClass("selected"), _ = i(this), _.addClass("selected")
                            }
                        }), this.__currentBlockListIndex = 0, this.__addShowMoreBlockedPlayersHandler(e)
                    },
                    __setYoPassState: function(e, t) {
                        this.__currentYoPassState = t;
                        for (var n in w) {
                            var a = w[n];
                            a === t ? (i(e.find(a.formClass)).show(),
                                a.callback && this[a.callback]()) : i(e.find(a.formClass)).hide()
                        }
                        var o = i(e.find(".yopass-disabled-status"));
                        t === w.ENABLED ? o.hide() : o.show()
                    },
                    __setPrettyLinkCode: function(e, t) {
                        var t = t.toString(),
                            n = t.substring(0, 4) + " " + t.substring(4);
                        i(e.find(".linkcode")).html(n)
                    },
                    __showFactoryCrew: function(e, t, n) {
                        var a = {
                            type: "FactoryCrew.recruit"
                        };
                        FB.ui({
                            method: "apprequests",
                            filters: ["app_users"],
                            title: "Recruit friends for your Factory Crew",
                            message: "Please help me in my factory",
                            data: a
                        }, function(e) {
                            console.debug("Recruit response", e), s.POST("/gamecontrol/endpoints.php?ep=sendFactoryCrewRequest", e)
                        }), setTimeout(n, 0)
                    },
                    __onYoPassEnablingState: function() {
                        function e() {
                            console.log("Checking for linked YoPass"), s.GET("/gamecontrol/yopass_endpoints.php?ep=checkForLinkedYoPass", function(a, i) {
                                return t.__currentYoPassState != w.ENABLING && t.__setYoPassState(n, w.ENABLING), a ? void(t.__yoPassEnableCheckTimeout = setTimeout(e, 2e3)) : i.linked ? (t.__cancelYoPassCheck(), void t.__setYoPassState(n, w.ENABLING_VERIFYING)) : void(t.__yoPassEnableCheckTimeout = setTimeout(e, 2e3))
                            })
                        }
                        var t = this;
                        if (!this.__yoPassEnableCheckTimeout) {
                            var n = this.__interfaces.settings;
                            this.__setYoPassState(n, w.LOADING), this.__yoPassEnableCheckTimeout = setTimeout(e, 0)
                        }
                    },
                    __cancelYoPassCheck: function() {
                        null != this.__yoPassEnableCheckTimeout && (clearTimeout(this.__yoPassEnableCheckTimeout), this.__yoPassEnableCheckTimeout = null)
                    }
                })
            }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
        }).call(t, n(3))
    }, function(e, t, n) {
        var a;
        (function(o) {
            "use strict";
            a = function(e, t, a) {
                var r = n(4),
                    s = "FB_Photo_id_";
                a.exports = r.create({
                    __containerId: null,
                    __hasPhotosPermission: !1,
                    __hasAlreadyRegisteredButtonHandlers: !1,
                    __counter: 0,
                    __alreadyCalledForPhotos: !1,
                    __alreadyCalledForAlbumPhotos: !1,
                    __prevCursor: null,
                    __nextCursor: null,
                    __currentAlbumId: null,
                    __REQUEST_ORIGINATES_FROM_PHOTOS: 0,
                    __REQUEST_ORIGINATES_FROM_ALBUMS: 1,
                    __REQUEST_ORIGINATES_FROM_ALBUM_PHOTOS: 2,
                    __requestOriginatesFrom: 0,
                    __albumCovers: null,
                    __numAlbumCovers: 0,
                    initialize: function(e) {
                        null != e && "" != e && "" != e || (this.__containerId = "picture_content_div"), this.__pagingButtonContainerId = "paging_buttons_content", this.__hasPhotosPermission = !1, this.__hasAlreadyRegisteredButtonHandlers = !1, this.__counter = 0, this.__alreadyCalledForPhotos = !1, this.__alreadyCalledForAlbumPhotos = !1, this.__requestOriginatesFrom = this.__REQUEST_ORIGINATES_FROM_PHOTOS, this.__albumCovers = [], this.__numAlbumCovers = 0
                    },
                    registerButtonHandlers: function() {
                        if (1 != this.__hasAlreadyRegisteredButtonHandlers) {
                            var e = this;
                            o(".photos_button").bind("click", function() {
                                e.getFbPhotos()
                            }), o(".album_button").bind("click", function() {
                                e.getFbAlbums()
                            }), o("#next_button_paging_pictures").bind("click", function() {
                                e.getNextPage()
                            }), o("#prev_button_paging_pictures").bind("click", function() {
                                e.getPrevPage()
                            }), this.__hasAlreadyRegisteredButtonHandlers = !0, o(".photos_button").click()
                        }
                    },
                    getFbPhotos: function(e) {
                        if (1 != this.__alreadyCalledForPhotos) {
                            void 0 !== e && null != e || (e = ""), this.__alreadyCalledForPhotos = !0;
                            var t = this;
                            t.__containerId;
                            this.clearPictureContainer(), t.checkPhotosPermission(), o("#prev_button_paging_pictures").prop("disabled", !0), o("#next_button_paging_pictures").prop("disabled", !0), FB.api("me/photos/uploaded?fields=id,picture,source" + e, function(e) {
                                if (e) {
                                    if (e.data)
                                        for (var n = e.data.length, a = 0; a < n; a++) {
                                            var i = e.data[a];
                                            if (i) {
                                                var o = t.createImage(i.picture);
                                                o.setAttribute("data-full_rez_source", i.source), o.setAttribute("id", i.id), document.getElementById(t.__containerId).appendChild(o)
                                            }
                                        } else console.error("ERROR: response.data: ", e.data);
                                    t.updatePaging(e.paging, t.__REQUEST_ORIGINATES_FROM_PHOTOS)
                                } else console.error("response is null, check permissions and try again");
                                t.__alreadyCalledForPhotos = !1
                            })
                        }
                    },
                    getFbAlbums: function(e) {
                        if (1 != this.__alreadyCalledForAlbumPhotos) {
                            void 0 !== e && null != e || (e = ""), this.__alreadyCalledForAlbumPhotos = !0;
                            var t = this;
                            t.__containerId;
                            this.clearPictureContainer(), t.checkPhotosPermission(), o("#prev_button_paging_pictures").prop("disabled", !0), o("#next_button_paging_pictures").prop("disabled", !0), FB.api("me/albums?fields=id,picture,name" + e, function(e) {
                                if (e) {
                                    if (e.data) {
                                        t.__albumCovers = [], t.__numAlbumCovers = e.data.length;
                                        for (var n = (e.data, 0); n < e.data.length; n++) null != e.data[n].picture && void 0 !== e.data[n].picture ? t.addAlbum(e.data[n]) : t.__numAlbumCovers--
                                    } else console.error("ERROR [getFbAlbums]: response.data: ", e.data);
                                    t.updatePaging(e.paging, t.__REQUEST_ORIGINATES_FROM_ALBUMS)
                                } else console.error("ERROR [getFbAlbums]: response is null, check permissions and try again");
                                t.__alreadyCalledForAlbumPhotos = !1
                            })
                        }
                    },
                    addAlbum: function(e) {
                        var t = this;
                        t.__containerId;
                        this.clearPictureContainer(), FB.api("/" + e.id + "?fields=picture", function(n) {
                            if (n) {
                                if (n.picture && n.picture.data && n.picture.data.url) {
                                    var a = document.createElement("img");
                                    a.src = n.picture.data.url, a.id = s + t.__counter++, a.class = "album_img";
                                    var i = document.createElement("div");
                                    i.id = e.name, i.className = "album_cover_cont";
                                    var o = document.createTextNode(e.name);
                                    i.appendChild(a);
                                    var r = document.createElement("span");
                                    r.appendChild(o), i.appendChild(r), t.__albumCovers.push(i), t.__albumCovers.length >= t.__numAlbumCovers && t.sortAndAddAllAlbumCovers(), a.addEventListener("click", function() {
                                        t.getFbAlbumPhotos(e.id)
                                    })
                                }
                            } else console.error("ERROR: [getFbAlbums trying to get cover by id] response is :", n)
                        })
                    },
                    sortAndAddAllAlbumCovers: function() {
                        var e = this;
                        for (e.__albumCovers.sort(function(e, t) {
                                var n = e.id.toLowerCase(),
                                    a = t.id.toLowerCase();
                                return n < a ? -1 : n > a ? 1 : 0
                            }), i = 0; i < e.__numAlbumCovers; i++) document.getElementById(e.__containerId).appendChild(e.__albumCovers[i])
                    },
                    getFbAlbumPhotos: function(e, t) {
                        if (void 0 !== e && null != e) {
                            void 0 !== t && null != t || (t = ""), this.clearPictureContainer(), o("#prev_button_paging_pictures").prop("disabled", !0), o("#next_button_paging_pictures").prop("disabled", !0);
                            var n = this;
                            FB.api("/" + e + "/photos?fields=id,source,picture" + t, function(t) {
                                if (t) {
                                    if (n.__currentAlbumId = e, t.data)
                                        for (var a = t.data, i = a.length, o = 0; o < i; o++) {
                                            var r = n.createImage(a[o].picture);
                                            r.setAttribute("data-full_rez_source", a[o].source), r.setAttribute("id", responseData.id), document.getElementById(n.__containerId).appendChild(r)
                                        }
                                    n.updatePaging(t.paging, n.__REQUEST_ORIGINATES_FROM_ALBUM_PHOTOS)
                                }
                            })
                        }
                    },
                    checkPhotosPermission: function() {
                        if (1 != this.__hasPhotosPermission) {
                            var e = this;
                            FB.api("me/permissions", function(t) {
                                if (t) {
                                    for (var n = t.data.length, a = 0; a < n; a++) {
                                        var i = t.data[a];
                                        if (null != i && "user_photos" == i.permission && "granted" == i.status) return e.__hasPhotosPermission = !0, void e.getFbPhotos()
                                    }
                                    e.__hasPhotosPermission = !1, e.askForPhotosPermission()
                                } else e.__hasPhotosPermission = !1, e.askForPhotosPermission()
                            })
                        }
                    },
                    askForPhotosPermission: function() {
                        var e = this;
                        FB.login(function(t) {
                            t.authResponse ? (e.__hasPhotosPermission = !0, e.__alreadyCalledForPhotos = !1, e.getFbPhotos()) : e.__hasPhotosPermission = !1
                        }, {
                            scope: "user_photos"
                        })
                    },
                    createImage: function(e, t, n) {
                        var a = document.createElement("img");
                        return a.src = e, t && (a.alt = t), n && (a.title = n), a.addEventListener("click", this.onImageClicked), a.id = s + this.__counter++, a
                    },
                    createLink: function(e, t) {
                        var n = document.createElement("a");
                        return n.appendChild(document.createTextNode(e)), n.title = e, n.href = t, n
                    },
                    getPrevPage: function() {
                        o("#prev_button_paging_pictures").prop("disabled", !0);
                        var e = this;
                        e.__requestOriginatesFrom == e.__REQUEST_ORIGINATES_FROM_PHOTOS ? e.getFbPhotos(e.__prevCursor) : e.__requestOriginatesFrom == e.__REQUEST_ORIGINATES_FROM_ALBUMS ? e.getFbAlbums(e.__prevCursor) : e.__requestOriginatesFrom == e.__REQUEST_ORIGINATES_FROM_ALBUM_PHOTOS && e.getFbAlbumPhotos(e.__currentAlbumId, e.__prevCursor)
                    },
                    getNextPage: function() {
                        o("#next_button_paging_pictures").prop("disabled", !0);
                        var e = this;
                        e.__requestOriginatesFrom == e.__REQUEST_ORIGINATES_FROM_PHOTOS ? e.getFbPhotos(e.__nextCursor) : e.__requestOriginatesFrom == e.__REQUEST_ORIGINATES_FROM_ALBUMS ? e.getFbAlbums(e.__nextCursor) : e.__requestOriginatesFrom == e.__REQUEST_ORIGINATES_FROM_ALBUM_PHOTOS && e.getFbAlbumPhotos(e.__currentAlbumId, e.__nextCursor)
                    },
                    getCursorFromLink: function(e, t) {
                        var n = e.split("&"),
                            a = "";
                        return n.forEach(function(e) {
                            e.indexOf(t) > -1 && (a = "&" + e)
                        }), a
                    },
                    getNextCursorFromLink: function(e) {
                        var t = e.split("&"),
                            n = "";
                        return t.forEach(function(e) {
                            e.indexOf("until") > -1 && (n = "&" + e)
                        }), n
                    },
                    onImageClicked: function() {
                        var e = (o("#" + this.id).attr("src"), o("#" + this.id).attr("data-full_rez_source")),
                            t = o("#" + this.id).attr("id"),
                            n = '{"type":"pictureFrameUrl", "srcURL":"' + e + '", "id":"' + t + '"}',
                            a = document.querySelector("#game_swf_div");
                        a.talkToFlash(n), o(".close_button").click()
                    },
                    clearPictureContainer: function() {
                        for (var e = document.getElementById("picture_content_div"); e.firstChild;) e.removeChild(e.firstChild)
                    },
                    updatePaging: function(e, t) {
                        var n = this;
                        e ? (e.previous && e.cursors && e.cursors.before && (n.__prevCursor = "&before=" + e.cursors.before, o("#prev_button_paging_pictures").prop("disabled", !1), n.__requestOriginatesFrom = t), e.next && e.cursors && e.cursors.after && (n.__nextCursor = "&after=" + e.cursors.after, o("#next_button_paging_pictures").prop("disabled", !1), n.__requestOriginatesFrom = t)) : console.error("ERROR [updatePaging]: cannot process responsePaging: ", e)
                    }
                })
            }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
        }).call(t, n(3))
    }, function(e, t, n) {
        var a;
        (function(i) {
            "use strict";
            a = function(e, t, a) {
                var o = n(4),
                    r = n(2),
                    s = n(1),
                    l = n(8),
                    c = n(70),
                    d = n(7),
                    u = 5e4;
                a.exports = o.create({
                    __modalLayer: null,
                    __container: null,
                    __loaded: !1,
                    __totalCounts: {
                        regular: 0,
                        special: 0
                    },
                    __inboxCountElement: "",
                    MAX_FETCH_COUNT_DELAY: 15e3,
                    initialize: function(e, t, n) {
                        var a = this;
                        this.__modalLayer = n, this.__inboxCountElement = i("#" + t), this.refreshInbox(), this.__inboxCountElement.hide().removeClass("hidden");
                        var a = this;
                        i("#" + e).click(function(e) {
                            window.fireDataEvent(d.GAME_TAB_CLICKED, {
                                tab: "Inbox"
                            }), a.show()
                        })
                    },
                    refreshInbox: function() {
                        var e = this;
                        setTimeout(e.__fetchCounts.bind(e), Math.random() * this.MAX_FETCH_COUNT_DELAY)
                    },
                    __fetchCounts: function() {
                        var e = this;
                        r.POST("/gamecontrol/freegift_endpoints.php?ep=getUnclaimedGiftsCount", {}, function(t, n) {
                            t || n.error || e.__updateInboxBadgeCount(n)
                        })
                    },
                    load: function() {
                        var e = this;
                        r.loadTemplate("gifts_inbox", function(t, n) {
                            if (t) return void console.error("Failed to load select_free_gifts template: ", t);
                            var a = i(i.parseHTML(n));
                            e.__container = a, i(a.find(".close_button")).click(function() {
                                e.__queuedBySection = {
                                    regular: 0,
                                    special: 0
                                }, e.__hideContainer(), e.__modalLayer.hide()
                            }), e.__giftsContainer = a.find("#gift_content");
                            var o = e.__modalLayer.getContainer();
                            e.__container.prependTo(o), e.__hideContainer(), e.__loaded = !0
                        })
                    },
                    showErrorCallback: function(e) {
                        i("#generic_modal .window_title").html("Submission Failed"), i("#generic_modal .subtitle").html(""), i("#generic_modal .message").html(e), i("#generic_modal .message").append("<br />Please try again.  If this problem persists, please contact us using the Help tab!"), i("#generic_modal").css("zIndex", u), i("#generic_modal").show()
                    },
                    show: function() {
                        function e() {}
                        if (!this.__loaded) return void console.error("FreeGiftsInbox not loaded yet!");
                        this.__modalLayer.show();
                        var t = this.__modalLayer.getZIndex();
                        this.__container.show(), this.__container.css("zIndex", t + 1);
                        var n = i(this.__container.find("#gift_content")).get(0);
                        l.unmountComponentAtNode(n), l.render(s.createElement(c, {
                            errorCallback: this.showErrorCallback,
                            totalCountCallback: this.__fetchCounts.bind(this)
                        }), n, e)
                    },
                    __populateNewGifts: function(e, t) {
                        var n = this;
                        n.__isFetching = !1;
                        for (var a = 0; a < t.results.length; ++a) {
                            var i = t.results[a];
                            this.__addGiftRow(t.typeId, i)
                        }
                        this.__reassignIds(t.typeId), n.__doFetch()
                    },
                    __addGiftRow: function(e, t) {
                        function n() {
                            o.find(".btn-reject").toggleClass("invisible"), o.find(".accept-only").toggleClass("invisible")
                        }
                        var a = this,
                            i = a.__giftContainersBySectionId[e].find(".gifts");
                        t.id = i[0].children.length;
                        var o = a.__giftRowTemplate.clone();
                        o[0].giftDetails = t, a.__maxIdsBySection[e] ? +t.id > a.__maxIdsBySection[e] && (a.__maxIdsBySection[e] = +t.id) : a.__maxIdsBySection[e] = +t.id;
                        var r = "[sender_name] has sent you a  [gift_type]";
                        r = r.replace("[sender_name]", t.from_name), r = r.replace("[gift_type]", "gift"), 0 == t.canReturn && (r += " back"), r += "!", o.attr("id", "gift_" + t.id), o.find(".title").html(r);
                        var s = "Accept this " + t.name;
                        s += 1 == t.canReturn ? " and send a thank you gift back" : " gift", o.find(".message").html(s), o.find(".gift-image").attr("src", t.imageUrl), o.find(".player-image").attr("src", t.playerImage), 1 != t.canReturn && (o.find(".send-back").hide(), o.find(".checkbox--send-back").prop("checked", !1)), "NPC" != t.from_name && 1 == t.canReturn || (o.find(".btn-accept").prop("value", "Accept"), o.find(".accept-only").hide()), o.find(".btn-accept").click(a.__claimGift.bind(a, e, t, !1, o, !0)), o.find(".accept-only").click(a.__claimGift.bind(a, e, t, !1, o, !1)), o.find(".btn-reject").click(a.__claimGift.bind(a, e, t, !0, o, !0)), o.hover(n, n), i.append(o), a.__updateSectionCount(e)
                    },
                    __updateSectionCount: function(e) {
                        var t = this;
                        if (t.__giftContainersBySectionId[e]) {
                            var n = t.__totalCounts[e],
                                a = t.__giftContainersBySectionId[e].find(".gifts").children(),
                                i = t.__giftContainersBySectionId[e];
                            "special" == e && (0 == n ? i.css("display", "none") : i.css("display", "block")), t.__totalCounts[e] > 0 ? (i.find(".counts").html("Showing " + a.length + " out of " + n), i.find(".gifts").show()) : (i.find(".counts").html("No Free Gifts."), i.find(".gifts").hide()), a.length > 0 ? i.find(".actions").show() : i.find(".actions").hide()
                        }
                    },
                    __claimAll: function(e, t) {
                        for (var n = this, a = n.__giftContainersBySectionId[e].find(".gifts").children(), o = [], s = 0; s < a.length; ++s) {
                            var l = a[s];
                            o.push(l.giftDetails.id), i(l).remove()
                        }
                        n.__totalCounts[e] -= o.length, n.__totalCountsUpdated(), n.__container.find("#loadingSpinner").show(), o.length > 0 && (o = o.join(","), r.POST("/gamecontrol/freegift_endpoints.php?ep=claimGifts", {
                            sentGiftIds: o,
                            ignore: t,
                            return: 1
                        }, function(t, a) {
                            return t || a.error ? (i("#generic_modal .window_title").html("Submission Failed"), i("#generic_modal .subtitle").html(""), i("#generic_modal .message").html("Uh oh.  Something went wrong claiming gifts."), i("#generic_modal .message").append("<br />Please try again.  If this problem persists, please contact us using the Help tab!"), i("#generic_modal").css("zIndex", u), void i("#generic_modal").show()) : (n.__queuedBySection = {
                                regular: 0,
                                special: 0
                            }, void setTimeout(function() {
                                r.POST("/gamecontrol/freegift_endpoints.php?ep=getUnclaimedGifts&special=" + ("special" == e ? 1 : 0), {}, n.__populateNewGifts.bind(n)), n.__container.find("#loadingSpinner").css("display", "none")
                            }, Math.random() * n.DELAY_CLAIM_ALL))
                        }))
                    },
                    __addGiftSection: function(e, t, n, a) {
                        var i = this;
                        i.__totalCounts[t] = a;
                        var o = i.__sectionContainerTemplate.clone();
                        o.find("#btnAcceptAll").click(i.__claimAll.bind(this, t, 0)), o.find("#btnRejectAll").click(i.__claimAll.bind(this, t, 1)), o.attr("id", t + "_gifts"), i.__giftContainersBySectionId[t] = o, o.find(".title").html(e);
                        var r = o.find(".gifts");
                        r.empty(), i.__totalCounts[t] = a;
                        for (var s = 0; s < n.length; ++s) {
                            var l = n[s];
                            i.__addGiftRow(t, l)
                        }
                        i.__updateSectionCount(t), i.__giftsContainer.append(o)
                    },
                    __totalCountsUpdated: function() {
                        var e = this,
                            t = 0;
                        for (var n in e.__totalCounts) t += e.__totalCounts[n], e.__updateSectionCount(n);
                        return e.__updateInboxBadgeCount(t), t
                    },
                    __updateInboxBadgeCount: function(e) {
                        var t = this;
                        t.__inboxCountElement.html(e), e > 0 ? t.__inboxCountElement.show() : t.__inboxCountElement.hide()
                    },
                    __hideContainer: function() {
                        this.__container.hide()
                    }
                })
            }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
        }).call(t, n(3))
    }, function(e, t, n) {
        var a;
        (function(i) {
            "use strict";
            a = function(e, t, a) {
                var o = n(4),
                    r = n(2),
                    s = n(7),
                    l = 5e4,
                    c = o.create({
                        __container: null,
                        __giftDetailsContainer: null,
                        initialize: function(e) {
                            this.__container = e, this.__giftDetailsContainer = e.find("#gift_details")
                        },
                        populate: function(e, t) {
                            if ("function" != typeof t) return void console.error("GiftEntry::populate requires clickCallback to be a function");
                            var n = this;
                            this.__giftDetails = e;
                            var a = this.__giftDetailsContainer.find("#giftName");
                            i(a).html(e.name), e.isLocked ? (this.__giftDetailsContainer.find("#sendButton").css("display", "none"), this.__giftDetailsContainer.find("#lockedText").html("Unlocked at Level " + e.required_level)) : i(this.__giftDetailsContainer).click(function(e) {
                                t(n.__giftDetails)
                            });
                            var o = this.__giftDetailsContainer.find("#giftImage");
                            return i(o).attr("src", e.imageUrl), this.__container
                        }
                    });
                a.exports = o.create({
                    TARGET_ID: "FreeGiftsManager",
                    __modalLayer: null,
                    __container: null,
                    __giftsContainer: null,
                    __giftEntryContainer: null,
                    __loaded: !1,
                    initialize: function(e, t) {
                        this.__modalLayer = e, this.__giftSelectedCallback = t
                    },
                    load: function() {
                        var e = this;
                        r.loadTemplate("gift_entry", function(t, n) {
                            if (t) return void console.error("Failed to load select_free_gifts template: ", t);
                            var a = i(i.parseHTML(n));
                            e.__giftEntryContainer = a
                        }), r.loadTemplate("select_free_gifts", function(t, n) {
                            if (t) return void console.error("Failed to load select_free_gifts template: ", t);
                            var a = i(i.parseHTML(n));
                            e.__container = a, i(a.find(".close_button")).click(function() {
                                window.fireDataEvent(s.DIALOG_BUTTON_CLICKED, {
                                    button: "X",
                                    dialog: e.TARGET_ID
                                }, e.TARGET_ID, s.DIALOG_TARGET_TYPE), e.__hideContainer(), e.__modalLayer.hide()
                            }), e.__giftsContainer = a.find("#gift_content");
                            var o = e.__modalLayer.getContainer();
                            e.__container.prependTo(o), e.__container.hide(), e.__loaded = !0
                        })
                    },
                    show: function() {
                        var e = this;
                        return this.__loaded ? (this.__modalLayer.show(), e.__container.find("#loadingSpinner").css("display", "block"), this.__showContainer(null), e.__giftsContainer.css("display", "none"), e.__giftsContainer.empty(), window.fireDataEvent(s.DIALOG_SHOWN, {
                            dialog: this.TARGET_ID
                        }, this.TARGET_ID, s.DIALOG_TARGET_TYPE), void r.POST("/gamecontrol/freegift_endpoints.php?ep=getActiveGifts", {}, function(t, n) {
                            return t || !n.length ? (i("#generic_modal .window_title").html("Submission Failed"), i("#generic_modal .subtitle").html(""), i("#generic_modal .message").html("Uh oh.  Something went wrong getting active gifts."), i("#generic_modal .message").append("<br />Please try again.  If this problem persists, please contact us using the Help tab!"), i("#generic_modal").css("zIndex", l), void i("#generic_modal").show()) : (e.__container.find("#loadingSpinner").css("display", "none"), e.__giftsContainer.css("display", "block"), void e.__showContainer(n))
                        })) : void console.error("FreeGiftsManager not loaded yet!")
                    },
                    __giftClicked: function(e) {
                        if (window.fireDataEvent(s.FREE_GIFT_ITEM_CLICKED_EVENT, {
                                gift_id: e.gift_id,
                                item_id: e.item_id,
                                name: e.name,
                                required_level: e.required_level,
                                end_date: e.end_date || "",
                                timeLeft: e.timeLeft || "",
                                isLocked: e.isLocked
                            }, this.TARGET_ID, s.DIALOG_TARGET_TYPE), !e.isLocked) {
                            this.__hideContainer();
                            var t = e.description;
                            t && (t = t.replace(/\n/g, "<br />"), e.name === t && (t = ""));
                            var n = {
                                type: "free_gift",
                                title: e.name,
                                description: t,
                                image: e.imageUrl,
                                payload: e
                            };
                            this.__giftSelectedCallback(n)
                        }
                    },
                    __showContainer: function(e) {
                        if (!e) {
                            var t = this.__modalLayer.getZIndex();
                            return this.__container.show(), void this.__container.css("zIndex", t + 1)
                        }
                        for (var n = this, a = 0; a < e.length; ++a) {
                            var i = e[a],
                                o = new c(n.__giftEntryContainer.clone());
                            n.__giftsContainer.append(o.populate(i, n.__giftClicked.bind(n)))
                        }
                    },
                    __hideContainer: function() {
                        window.fireDataEvent(s.DIALOG_HIDDEN, {
                            dialog: this.TARGET_ID
                        }, this.TARGET_ID, s.DIALOG_TARGET_TYPE), this.__container.hide()
                    }
                })
            }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
        }).call(t, n(3))
    }, function(e, t, n) {
        var a;
        (function(i) {
            "use strict";
            a = function(e, t, a) {
                var o = n(4),
                    r = n(2),
                    s = "alert row ",
                    l = s + "alert-danger",
                    c = s + "alert-warning",
                    d = s + "alert-info";
                a.exports = o.create({
                    __externalUIController: null,
                    __currentContext: null,
                    __playerSettingsModal: null,
                    initialize: function(e) {
                        this.__externalUIController = e, this.__playerSettingsModal = null
                    },
                    sendRequestDecorator: function(e, t, n, a) {
                        var i = this;
                        n = n || e, a = a || "positive";
                        var o = function() {
                            r.GET("/gamecontrol/lock_code_endpoints.php?ep=" + e, i.handleErrorsDecorator(t, n, o, a))
                        };
                        return o
                    },
                    handleErrorsDecorator: function(e, t, n, a) {
                        var o = this;
                        return t = t || "Lock Code Action", a = a || "positive",
                            function(r, s) {
                                var c = i(o.__currentContext.find(".messages"));
                                if (r || !s) return c.empty(), c.append("<span class='" + l + "'> An unknown error has occurred while trying to " + t + " Please try again, and ensure you are connected to the internet.</span>"), void console.error("Failed to get Lock Code status");
                                if (s.error) {
                                    if (c.empty(), c.append("<span class='" + l + "'>" + s.error + "</span>"), s.reason) {
                                        if ("no email" == s.reason) return void i(o.__playerSettingsModal.find(".support_submit_support")).click();
                                        "locked" == s.reason && n && (o.__currentContext.css("zIndex", "1000"), o.__externalUIController.show("lockCodes", {
                                            overlay: !0,
                                            error: s.error,
                                            trigger: n,
                                            triggerText: t,
                                            triggerButtonType: a
                                        }), window.scrollTo(0, 0))
                                    }
                                } else e(r, s)
                            }
                    },
                    delayButtonDecorator: function(e, t) {
                        return function n() {
                            var a = i(this),
                                o = clickedText.val();
                            a.val("Sending..."), a.off("click"), a.removeClass("btn-neutral"), a.addClass("btn-positive"), r.GET("/gamecontrol/lock_code_endpoints.php?ep=" + e, function(e, i) {
                                a.val(o), a.on("click", n), a.removeClass("btn-positive"), a.addClass("btn-neutral"), window.scrollTo(0, 0), t(e, i)
                            })
                        }
                    },
                    delayTextDecorator: function(e, t) {
                        return function n() {
                            var a = i(this),
                                o = a.text();
                            a.text("Sending..."), a.off("click"), r.GET("/gamecontrol/lock_code_endpoints.php?ep=" + e, function(e, i) {
                                a.text(o), a.on("click", n), window.scrollTo(0, 0), t(e, i)
                            })
                        }
                    },
                    delayMessageDecorator: function(e, t) {
                        var n = this;
                        return function() {
                            var a = i(n.__currentContext.find(".messages"));
                            a.empty(), a.append("<span class='" + c + "'> Sending... Please wait.</span>"), r.GET("/gamecontrol/lock_code_endpoints.php?ep=" + e, function(e, n) {
                                a.empty(), t(e, n)
                            })
                        }
                    },
                    openLockCodesSetup: function() {
                        var e = this;
                        i(e.__currentContext.find(".close_button")).click(), e.__externalUIController.show("lockCodes", {
                            setup: !0
                        }), window.scrollTo(0, 0)
                    },
                    setUpLockCodeSettings: function(e, t) {
                        var n = this;
                        n.__playerSettingsModal = e, n.__currentContext = e;
                        var a = i(e.find(".messages")),
                            o = i(e.find("#lock_code_btn")),
                            s = i(e.find("#lock_code_update_btn")),
                            l = i(e.find("#reset_lock_code_btn")),
                            c = i(e.find(".level_0"));
                        if (l.hide(), s.hide(), o.off("click"), "active" != t.status) return c.show(), void o.on("click", n.sendRequestDecorator("getEligibility", n.openLockCodesSetup.bind(n)));
                        t.level || (a.append("<span class='error'>No recognized Level found</span>"), console.error("Failed to get Lock Code level"));
                        var d = i(e.find("#lock_code_status")),
                            u = i(e.find("#lock_code_settings")),
                            p = e.find("#lock_code_settings :input");
                        d.html(t.status.charAt(0).toUpperCase() + t.status.slice(1)), d.css("color", "green"), s.show(), u.show(), c.hide(), o.val("Remove"), o.removeClass("btn-positive"), o.addClass("btn-negative"), l.show(), l.off("click"), l.on("click", n.delayTextDecorator("reset", n.handleErrorsDecorator(function(e, t) {
                            "success" == t.status && a.append('<span class="success"> You have successfully reset your lock code. You should receive an email shortly with your new code.</span>')
                        }, "Reset Lock Code")));
                        var m = n.sendRequestDecorator("deactivate", function(e, t) {
                            u.hide(), d.html(t.status.charAt(0).toUpperCase() + t.status.slice(1)), d.css("color", "red"), o.removeClass("btn-negative"), o.addClass("btn-positive"), o.val("Set Lock Code"), c.show(), s.hide(), l.hide(), r.dispatchFlashMessage("updateLockCodeLevel", {
                                level: "0"
                            }), o.on("click", n.openLockCodesSetup.bind(n))
                        }, "Remove Lock Code", "negative");
                        3 == t.level ? o.on("click", function() {
                            n.__currentContext.css("zIndex", "1000"), n.__externalUIController.show("lockCodes", {
                                overlay: !0,
                                trigger: m,
                                triggerText: "Remove Lock Code",
                                triggerButtonType: "negative"
                            }), window.scrollTo(0, 0)
                        }) : o.on("click", m), s.on("click", function e() {
                            var t = i("input[name=lock_code_security_level]:checked", "#lock_code_settings").val(),
                                o = "&level=" + t;
                            r.POST("/gamecontrol/lock_code_endpoints.php?ep=update" + o, {}, n.handleErrorsDecorator(function(e, t) {
                                if (!t.level) return void a.append("<span class='error'> An unknown error has occurred! Please try again, and ensure you are connected to the internet.</span>");
                                var n = i("#lock_code_settings .origional");
                                n.removeClass("origional"), a.append("<span class='section success'>Security Level successfully updated from level " + n.val() + " to level " + t.level + "</span>"), r.dispatchFlashMessage("updateLockCodeLevel", {
                                    level: t.level
                                });
                                var o = i("#lock_code_settings input[value='" + t.level + "']");
                                o.addClass("origional"), o.click(), window.scrollTo(0, 0)
                            }, "Update Security Level", e.bind(n)))
                        }), p.each(function(e, n) {
                            n.value == t.level ? (i(n).prop("checked", !0), i(n).addClass("origional"), i(".lock_code_description.level_" + t.level).show()) : i(".lock_code_description.level_" + n.value).hide(), i(n.parentElement).on("click", function() {
                                i(".lock_code_description").each(function(e, t) {
                                    t.className.indexOf("level_" + n.value) > -1 ? i(t).show() : i(t).hide()
                                })
                            })
                        })
                    },
                    refreshChallengeImages: function() {
                        var e = this,
                            t = i(e.__currentContext.find(".cachedImagesList")),
                            n = "&cached=" + t.val().substr(1);
                        i(".challenge_image_candidate.selected").removeClass("selected"), i(e.__currentContext.find(".challenge_image")).val("none"), i(e.__currentContext.find(".challenge_image_candidate")).removeClass("rejected"), i(e.__currentContext.find(".challenge_image")).val("none"), e.removeStepConfirmation(i(e.__currentContext.find(".js-challenge-images"))), e.sendRequestDecorator("getRandomImages" + n, function(n, a) {
                            var o = document.styleSheets[document.styleSheets.length - 1],
                                r = i(e.__currentContext.find(".challenge_image_candidate")),
                                s = 0;
                            for (var l in a) {
                                var c = l.substr(l.indexOf("_") + 1);
                                "cached" != a[l] && (t.val(t.val() + "," + c), o.insertRule(".selected_" + c + ", .challenge_image_candidate.challenge_image_" + c + " {background-image: url('data:image/png;base64," + a[l] + "'); } ", 0));
                                var d = i(r[s]);
                                d.removeClass(), d.addClass("challenge_image_candidate"), d.addClass("content-box"), d.addClass("challenge_image_" + c), d.attr("data-index", c), s++
                            }
                        }, "getting a new set of images")()
                    },
                    setUpLockCode: function() {
                        var e = this,
                            t = "setUpLockCode",
                            n = ["challenge_phrase", "challenge_image", "challenge_code", "level"];
                        n.forEach(function(n) {
                            var a = i(e.__currentContext.find("." + n)).val();
                            t += "&" + n + "=" + encodeURIComponent(a)
                        }), e.sendRequestDecorator(t, function(t, n) {
                            n.status && "success" == n.status && (r.dispatchFlashMessage("updateLockCodeLevel", {
                                level: "2"
                            }), i(e.__currentContext.find(".challenge_image_candidate")).off("click"), i(e.__currentContext.find(".submit_lock_code")).off("click"), i(e.__currentContext.find(".close_button")).click())
                        }, "setting up your lock code")()
                    },
                    addStepConfirmation: function(e) {
                        i(e.find(".js-step-check")).show(), i(e.find(".js-step-number")).hide(), i(e.find(".js-step-input")).addClass("step-confirmed"), i(e.find(".step-circle")).addClass("step-checked")
                    },
                    removeStepConfirmation: function(e) {
                        i(e.find(".js-step-check")).hide(), i(e.find(".js-step-number")).show(), i(e.find(".js-step-input")).removeClass("step-confirmed"), i(e.find(".step-circle")).removeClass("step-checked")
                    },
                    validateLockCodeInput: function(e) {
                        i.inArray(e.keyCode, [46, 8, 9, 27]) !== -1 || 65 == e.keyCode && (e.ctrlKey === !0 || e.metaKey === !0) || e.keyCode >= 35 && e.keyCode <= 40 || ((e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105) || i(this).val().length >= 4) && e.preventDefault()
                    },
                    renderLockCodes: function(e, t, n) {
                        var a = this;
                        a.__currentContext = e;
                        var o = i(e.find(".messages"));
                        o.empty();
                        var s = i(e.find(".submit_lock_code")),
                            u = i(e.find(".lock_code_set_up")),
                            p = i(e.find(".lock_code_login")),
                            m = i(e.find(".login_challenge_string")),
                            h = i(e.find(".login_challenge_code")),
                            _ = i(e.find(".confirm_lock_code")),
                            f = i(e.find(".loginme.__title"));
                        if (f.text("Please Confirm your Lock Code"), i(".js-step-check").hide(), h.val(""), _.removeClass("btn-negative"), _.addClass("btn-positive"), h.off("keydown"), h.on("keydown", function(e) {
                                return 13 == e.keyCode && _.click(), a.validateLockCodeInput.bind(this)(e)
                            }), t && t.setup) {
                            u.show(), p.hide();
                            var g = i(e.find(".challenge_image_candidate")),
                                v = i(e.find(".challenge_phrase")),
                                b = i(e.find(".chars_left")),
                                y = i(e.find(".challenge_image")),
                                E = i(e.find(".lock_code_check")),
                                w = i(e.find(".go_back")),
                                C = i(e.find(".refresh_challenge_images")),
                                k = i(e.find(".js-challenge-images")),
                                T = i(e.find(".js-challenge-phrase")),
                                S = i(e.find(".js-challenge-code")),
                                I = i(e.find(".challenge_code"));
                            s.val("Create Lock Code..."), i(".challenge_image_candidate.selected").removeClass("selected"), a.removeStepConfirmation(k), a.removeStepConfirmation(T), a.removeStepConfirmation(S), b.removeClass("negative"), b.html("140 characters left"), y.val("none"), v.val(""), E.hide(), g.removeClass("rejected"), g.off("click"), g.on("click", function(e) {
                                g.addClass("rejected"), i(".challenge_image_candidate.selected").removeClass("selected"), a.addStepConfirmation(k), i(this).addClass("selected"), i(this).removeClass("rejected"), y.val(i(this).attr("data-index"))
                            }), I.val(""), I.off("keyup"), I.on("keyup", function() {
                                4 == i(this).val().length ? a.addStepConfirmation(S) : a.removeStepConfirmation(S)
                            }), I.off("keydown"), I.on("keydown", function(e) {
                                return 13 == e.keyCode && s.click(), a.validateLockCodeInput.bind(this)(e)
                            }), v.off("keyup"), v.on("keyup", function() {
                                var e = 140 - i(this).val().length;
                                e >= 0 && e <= 130 ? a.addStepConfirmation(T) : a.removeStepConfirmation(T), e >= 0 ? (b.removeClass("negative"), b.html(e + " characters left")) : (b.addClass("negative"), b.html(Math.abs(e) + " characters over"))
                            });
                            for (var P = !1, A = 0; A < document.styleSheets.length; A++) "lockcodes" != document.styleSheets[A].title ? "lockcodesetup" == document.styleSheets[A].title && (P = !0) : i("style[title='lockcodes']").remove();
                            P || a.sendRequestDecorator("getImageCSS", function(e, t) {
                                for (var n = document.styleSheets[document.styleSheets.length - 1], i = 0; i < 10; i++) {
                                    if (!t["image_" + i]) return void o.append("<span class='" + d + "'>Error getting images. Please reload.</span>");
                                    n.insertRule(".selected_" + i + ", .challenge_image_candidate.challenge_image_" + i + " {background-image: url('data:image/png;base64," + t["image_" + i] + "'); } ", 0)
                                }
                                n.title = "lockcodesetup", C.show(), C.off("click"), C.on("click", a.refreshChallengeImages.bind(a))
                            })(), s.off("click"), s.on("click", function() {
                                o.empty();
                                var t = I.val(),
                                    n = v.val(),
                                    r = y.val();
                                if ("none" == r) return void o.append("<span class='" + c + "'>Please select an image</span>");
                                if (n.length < 10) return void o.append("<span class='" + c + "'>Please enter a longer Security Phrase.</span>");
                                if (n.length > 140) return void o.append("<span class='" + c + "'>Please pick a shorter Security Phrase.</span>");
                                if (4 != t.length) return void o.append("<span class='" + c + "'>Please enter a 4-digit PIN</span>");
                                if (n.indexOf(t) > -1) return void o.append("<span class='" + l + "'> Woah! Don't put your lock code in your Security Phrase! That's really dangerous!</span>");
                                if (isNaN(parseFloat(t)) || !isFinite(t)) return void o.append("<span class='" + c + "'> Please enter a Numeric value for your PIN. </span>");
                                var s = /([0-9])\1{3}|0123|1234|2345|3456|4567|5678|6789|3210|4321|5432|6543|7654|8765|9876/;
                                if (s.test(t)) return void o.append("<span class='" + c + "'> Please enter a non-sequential, non-repeating lock code.</span>");
                                var d = i(e.find(".login_challenge_image"));
                                d.addClass("login_challenge_image"), d.addClass("selected_" + r), m.text(v.val()), p.show(), u.hide(), E.show(), w.off("click"), w.on("click", function() {
                                    a.removeStepConfirmation(S), d.removeClass("selected_" + r), o.empty(), p.hide(), u.hide(), u.show(), I.val(""), _.val(""), E.hide()
                                }), _.val("Confirm and Finalize"), _.off("click"), _.on("click", function() {
                                    return i(h).val() !== t ? (o.empty(), void o.append("<span class='" + l + "'> Codes do not match! </span>")) : void a.setUpLockCode()
                                })
                            })
                        } else {
                            t && t.overlay && t.error ? f.text(t.error) : f.text("Please Confirm your Lock Code");
                            for (var N = !1, A = 0; A < document.styleSheets.length; A++) "lockcodes" == document.styleSheets[A].title && (N = !document.styleSheets[A].disabled);
                            N || a.sendRequestDecorator("getChallenge", function(e, t) {
                                if (t.image && t.text) {
                                    var n = ".login_challenge_image {background-image: url('data:image/png;base64," + t.image + "'); }";
                                    m.empty(), m.text(t.text);
                                    var a = document.head || document.getElementsByTagName("head")[0],
                                        i = document.createElement("style");
                                    i.type = "text/css", i.title = "lockcodes", i.rel = "stylesheet", i.styleSheet ? i.styleSheet.cssText = n : i.appendChild(document.createTextNode(n)), a.appendChild(i)
                                }
                            }, "getting your challenge image.")(), _.val("Unlock"), t.triggerText && (_.val(t.triggerText), "negative" == t.triggerButtonType && (_.addClass("btn-negative"), _.removeClass("btn-positive"))), _.off("click"), p.show(), u.hide(), _.on("click", function() {
                                var n = "&code=" + h.val();
                                return 4 != h.val().length ? (o.empty(), void o.append("<span class='" + l + "'> Code has 4 characters </span>")) : (h.val(""), void a.sendRequestDecorator("unlock" + n, function(n, s) {
                                    if (s.message) {
                                        "no attempts left" == s.reason && (o.empty(), o.append("<span class='" + c + "'>" + s.message + ' <br/> Forgot your code? Click <a id="reset-link"href="#"> here </a> to reset your code by email.</span>'));
                                        var d = i(a.__currentContext.find(".messages #reset-link"));
                                        return d.off("click"), void d.on("click", a.delayMessageDecorator("reset", a.handleErrorsDecorator(function(e, t) {
                                            "success" == t.status && o.append("<span class='row alert alert-success'> You have successfully reset your lock code. You should receive an email shortly with your new code.</span>")
                                        })).bind(a))
                                    }
                                    if (!s.token || !s.expires) return void o.append("<span class='" + l + "'> Lock codes seem to be offline at the moment! Please try again later.</span>");
                                    t && t.overlay && i("#yoworld_settings_manager .messages").empty();
                                    var u = new Date(1e3 * s.expires),
                                        p = u.toGMTString();
                                    document.cookie = "ywlc_key=" + s.token + "; expires=" + p + "; path=/", r.dispatchFlashMessage("updateLockCodeToken", {
                                        token: s.token
                                    }), i(e.find(".close_button")).click(), t.trigger && (a.__currentContext = a.__playerSettingsModal, t.trigger(s.token))
                                }, "unlocking your account")())
                            })
                        }
                        setTimeout(n, 0)
                    }
                })
            }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
        }).call(t, n(3))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(4);
            a.exports = i.create({
                __element: null,
                __elementName: null,
                __container: null,
                __zIndex: null,
                initialize: function(e, t, n) {
                    this.__elementName = e, this.__element = document.createElement("div"), this.__element.setAttribute("id", e), this.__element.className = "modal  modal--hidden", 0 === n.children.length ? n.appendChild(this.__element) : n.insertBefore(this.__element, n.firstChild), this.__container = n, this.__zIndex = t
                },
                getContainer: function() {
                    return this.__container
                },
                getZIndex: function() {
                    return this.__zIndex
                },
                show: function() {
                    this.__element.className = "modal"
                },
                hide: function() {
                    this.__element.className = "modal  modal--hidden", this.removeAllChildren()
                },
                addChildElement: function(e) {
                    this.__element.appendChild(e)
                },
                removeAllChildren: function() {
                    for (; this.__element.firstChild;) this.__element.removeChild(this.__element.firstChild)
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        (function(i) {
            "use strict";
            a = function(e, t, a) {
                var o = n(4),
                    r = n(2),
                    s = n(7),
                    l = ["openweb", "facebook"],
                    c = 50,
                    d = 1e3,
                    u = o.create({
                        __selectorIdx: -1,
                        friendDetails: null,
                        displayDiv: null,
                        isSelected: !1,
                        isBuddy: function() {
                            return l.indexOf(this.friendDetails.usertype) > -1
                        },
                        initialize: function(e, t, n) {
                            this.__selectorIdx = e, this.friendDetails = t, this.displayDiv = document.createElement("div"), this.displayDiv.className = "friend-entry  select-none", this.displayDiv.selectorIdx = e, this.displayDiv.fullName = t.full_name, this.displayDiv.innerHTML = "<img src='images/add.png'> " + t.full_name, this.clickedCallback = n, this.addClickEvent()
                        },
                        __clickedFriend: function() {
                            this.clickedCallback(this)
                        },
                        addClickEvent: function() {
                            this.displayDiv.addEventListener("mousedown", this.__clickedFriend.bind(this))
                        }
                    });
                a.exports = o.create({
                    TARGET_ID: "MultiFriendSelectManager",
                    GIFTS_SENT_TARGET_ID: "GiftsSent",
                    SUBMISSION_FAILED_TARGET_ID: "SubmissionFailed",
                    SUBMISSION_FAILED_MESSAGE: "Uh oh.  Something went wrong sending gifts.",
                    GIFTS_SENT_MESSAGE: "Your friends have received their gifts!",
                    __modalLayer: null,
                    __container: null,
                    __contentContainer: null,
                    __giftDetails: null,
                    __loaded: !1,
                    __selectedTab: null,
                    __lastSelectedTab: null,
                    __allFriends: [],
                    __selectedFriends: [],
                    __beganSend: !1,
                    __totalSendingTo: null,
                    __totalSentTo: 0,
                    __sendingToFriends: [],
                    initialize: function(e) {
                        this.__modalLayer = e
                    },
                    load: function() {
                        var e = this;
                        r.loadTemplate("multi_friend_selector", {
                            hours_threshold: 12
                        }, function(t, n) {
                            if (t) return void console.error("Failed to load multi_friend_selector template: ", t);
                            var a = i(i.parseHTML(n));
                            e.__container = a, i(e.__container.find(".close_button")).click(function() {
                                window.fireDataEvent(s.DIALOG_BUTTON_CLICKED, {
                                    button: "X",
                                    dialog: e.TARGET_ID
                                }, e.TARGET_ID, s.DIALOG_TARGET_TYPE), e.__hideContainer(), e.__modalLayer.hide()
                            }), e.__contentContainer = e.__container.find("#content");
                            var o = e.__modalLayer.getContainer();
                            e.__container.prependTo(o), e.__container.hide(), e.__tabBuddies = i(e.__container.find("#tabBuddies")), e.__tabAll = i(e.__container.find("#tabAllFriends")), e.__selectedTab = e.__tabBuddies, e.__tabBuddies.click(function(t) {
                                window.fireDataEvent(s.DIALOG_TAB_CLICKED, {
                                    dialog: e.TARGET_ID,
                                    tab: "Buddies"
                                }, e.TARGET_ID, s.DIALOG_TARGET_TYPE), e.__selectTab(e.__tabBuddies)
                            }), e.__tabAll.click(function(t) {
                                window.fireDataEvent(s.DIALOG_TAB_CLICKED, {
                                    dialog: e.TARGET_ID,
                                    tab: "All Friends"
                                }, e.TARGET_ID, s.DIALOG_TARGET_TYPE), e.__selectTab(e.__tabAll)
                            }), i(a.find("#sendButton")).click(function() {
                                e.__send()
                            }), i(a.find("#selectAll")).click(function() {
                                e.__selectAll()
                            }), i(a.find(".search-bar")).click(function() {
                                window.fireDataEvent(s.SEARCHING_STARTED, {}, e.TARGET_ID, s.DIALOG_TARGET_TYPE)
                            }), i(a.find(".search-bar")).keyup(function() {
                                e.__applyFilter(i(this).val())
                            }), e.__loaded = !0
                        })
                    },
                    __selectAll: function() {
                        window.fireDataEvent(s.DIALOG_BUTTON_CLICKED, {
                            button: "Select All",
                            dialog: this.TARGET_ID,
                            numFriends: this.__allFriends.length,
                            filterText: i(this.__container.find(".search-bar")).val()
                        }, this.TARGET_ID, s.DIALOG_TARGET_TYPE);
                        for (var e = this, t = 0; t < e.__allFriends.length; ++t) {
                            var n = e.__allFriends[t];
                            e.__selectedTab == e.__tabBuddies && n.isBuddy() ? this.__selectedFriend(n) : e.__selectedTab == e.__tabAll && this.__selectedFriend(n)
                        }
                        e.__resortList(i(e.__container.find("#selectFriends"))), e.__resortList(i(e.__container.find("#chosenFriends")))
                    },
                    __send: function() {
                        var e = this;
                        if (window.fireDataEvent(s.DIALOG_BUTTON_CLICKED, {
                                button: "Send",
                                dialog: this.TARGET_ID,
                                numFriends: this.__allFriends.length,
                                filterText: i(this.__container.find(".search-bar")).val()
                            }, this.TARGET_ID, s.DIALOG_TARGET_TYPE), 0 != this.__selectedFriends.length) {
                            this.__sendingToFriends = e.__container.find("#chosenFriends").children(), e.__beganSend || (e.__totalSendingTo = this.__selectedFriends.length, e.__totalSentTo = 0), e.__beganSend = !0, e.__container.find("#selectAll").hide(), e.__container.find("#send_progress").css("display", "block"), e.__updateProgressBar(e.__totalSentTo, e.__totalSendingTo), e.__container.find("#sendButton").hide(), e.__container.find("#sendingLabel").show(), e.__container.find("#sendButton").html("Send More");
                            for (var t = this.__sendingToFriends.splice(0, Math.min(this.__sendingToFriends.length, c)), n = {
                                    playerIds: [],
                                    anonymousIds: [],
                                    facebookIds: []
                                }, a = 0; a < t.length; ++a) {
                                var o = this.__allFriends[t[a].selectorIdx];
                                i(o.displayDiv).remove(), o.friendDetails.player_id && n.playerIds.push(o.friendDetails.player_id), o.friendDetails.facebook_id && n.anonymousIds.push(o.friendDetails.facebook_id), ++e.__totalSentTo
                            }
                            this.__sendRequests(n, function(t, n) {
                                return t ? void e.__sendGiftsResponse(t, n) : (e.__updateProgressBar(e.__totalSentTo, e.__totalSendingTo), 0 == e.__sendingToFriends.length ? void e.__sendGiftsResponse(t, n) : void setTimeout(function() {
                                    e.__container.find("#sendButton").show(), e.__container.find("#sendingLabel").hide()
                                }, d))
                            })
                        }
                    },
                    __sendGiftsResponse: function(e, t) {
                        var n = this;
                        e ? (i("#generic_modal .window_title").html("Submission Failed"), i("#generic_modal .subtitle").html(""), i("#generic_modal .message").html(n.SUBMISSION_FAILED_MESSAGE), i("#generic_modal .message").append("<br />Please try again.  If this problem persists, please contact us using the Help tab!"), i("#generic_modal").show(), i("#generic_modal .js-close").on("click", function e() {
                            window.fireDataEvent(s.GENERIC_DIALOG_CREATED, {
                                buttons: "X",
                                dialog: "GenericDialog",
                                title: "Submission Failed",
                                message: n.SUBMISSION_FAILED_MESSAGE
                            }, n.SUBMISSION_FAILED_TARGET_ID, s.DIALOG_TARGET_TYPE), i("#generic_modal .js-close").off("click", e)
                        })) : (i("#generic_modal .window_title").html("Gifts Sent"), i("#generic_modal .subtitle").html(""), i("#generic_modal .message").html(n.GIFTS_SENT_MESSAGE + "<br />"), i("#generic_modal .message").append("Check back regularly for gifts sent back to you!"), i("#generic_modal").show(), i("#generic_modal .js-close").on("click", function e() {
                            if (window.fireDataEvent(s.GENERIC_DIALOG_CREATED, {
                                    buttons: "X",
                                    dialog: "GenericDialog",
                                    title: "Gifts Sent",
                                    message: n.GIFTS_SENT_MESSAGE
                                }, n.GIFTS_SENT_TARGET_ID, s.DIALOG_TARGET_TYPE), i("#generic_modal .js-close").off("click", e), t.tokens) {
                                var a = {};
                                a.type = t.type, a.tokenType = t.tokenType, a.tokens = t.tokens, document.querySelector("#game_swf_div").talkToFlash(JSON.stringify(a))
                            }
                        })), n.__hideContainer(), n.__modalLayer.hide()
                    },
                    __updateProgressBar: function(e, t) {
                        var n = this;
                        n.__container.find("#send_select_label").html(e + " of " + t + " Sent");
                        var a = Math.floor(e / t * 100);
                        n.__container.find("#send_select_progress").val(a)
                    },
                    __sendRequests: function(e, t) {
                        function n(e) {
                            if (0 == e.length) return void r.POST("/gamecontrol/freegift_endpoints.php?ep=sendGifts", o, t.bind(a));
                            var i = e.splice(0, c);
                            FB.ui({
                                method: "apprequests",
                                to: i.join(","),
                                title: a.__giftDetails.title,
                                message: "I sent you a gift in YoWorld!",
                                data: {
                                    type: "Gift.send"
                                }
                            }, function(t) {
                                t && t.to && (o.fbIds = o.fbIds.concat(t.to)), n(e)
                            })
                        }
                        var a = this,
                            i = e.anonymousIds,
                            o = {
                                fbIds: [],
                                playerIds: e.playerIds,
                                giftId: a.__giftDetails.payload.gift_id
                            };
                        n(i)
                    },
                    show: function(e) {
                        var t = this;
                        if (!this.__loaded) return void console.error("FreeGiftsManager not loaded yet!");
                        this.__giftDetails = e, t.__sendingToFriends = [], t.__container.find("#sendButton").show(), t.__container.find("#sendingLabel").hide(), t.__container.find("#selectAll").show(), t.__container.find("#sendButton").html("Send"), t.__beganSend = !1, t.__container.find("#send_progress").css("display", "none"), this.__modalLayer.show(), t.__container.find("#loadingSpinner").css("display", "block"), this.__showContainer(null), t.__contentContainer.css("display", "none"), window.fireDataEvent(s.DIALOG_SHOWN, {
                            dialog: t.TARGET_ID
                        }, t.TARGET_ID, s.DIALOG_TARGET_TYPE), r.POST("/gamecontrol/freegift_endpoints.php?ep=getAvailableRecipients", {}, function(e, n) {
                            return e ? (i("#generic_modal .window_title").html("Submission Failed"), i("#generic_modal .subtitle").html(""), i("#generic_modal .message").html("Uh oh.  Something went wrong submitting your ticket."), i("#generic_modal .message").append("<br />Please try again.  If this problem persists, please contact us using the Help tab!"), void i("#generic_modal").show()) : (t.__container.find(".search-bar").val(""), t.__container.find("#loadingSpinner").css("display", "none"), t.__contentContainer.css("display", "block"), void t.__showContainer(n))
                        });
                        var t = this
                    },
                    __showContainer: function(e) {
                        var t = this;
                        if (!e) {
                            var n = this.__modalLayer.getZIndex();
                            return this.__container.show(), void this.__container.css("zIndex", n + 1)
                        }
                        t.__allFriends = [], t.__selectedFriends = [], t.__availableFriendsResponse = e, i(t.__container.find("#selectFriends")).empty(), i(t.__container.find("#chosenFriends")).empty(), t.__container.find("#giftImage").attr("src", t.__giftDetails.image), t.__container.find("#giftName").text(t.__giftDetails.title), t.__container.find("#giftDescription").text(t.__giftDetails.description);
                        for (var a = 0; a < t.__availableFriendsResponse.length; ++a) {
                            var o = t.__availableFriendsResponse[a],
                                r = new u(a, o, this.__friendClicked.bind(t));
                            t.__allFriends.push(r)
                        }
                        t.__selectTab(this.__selectedTab)
                    },
                    __resortList: function(e) {
                        var t = e.children("div").get();
                        t.sort(function(e, t) {
                            return i(e).html().toUpperCase().localeCompare(i(t).html().toUpperCase())
                        }), i.each(t, function(t, n) {
                            e.append(n)
                        })
                    },
                    __friendClicked: function(e) {
                        window.fireDataEvent(s.DIALOG_LIST_ITEM, {
                            dialog: this.TARGET_ID,
                            item: e.friendDetails.player_id,
                            playerId: e.friendDetails.player_id,
                            isBuddy: e.isBuddy(),
                            isSelected: e.isSelected,
                            filterText: i(this.__container.find(".search-bar")).val()
                        }, this.TARGET_ID, s.DIALOG_TARGET_TYPE);
                        var t = this;
                        e.isSelected ? (t.__deselectedFriend(e), t.__resortList(i(t.__container.find("#selectFriends")))) : (t.__selectedFriend(e), t.__resortList(i(t.__container.find("#chosenFriends"))))
                    },
                    __deselectedFriend: function(e) {
                        var t = this;
                        this.__selectedTab == t.__tabBuddies && e.isBuddy() || this.__selectedTab == t.__tabAll ? (e.displayDiv.innerHTML = "<img src='images/add.png'> " + e.displayDiv.fullName, i(t.__container.find("#selectFriends")).append(e.displayDiv)) : i(e.displayDiv).remove(), e.isSelected = !1;
                        var n = t.__selectedFriends.indexOf(e);
                        n > -1 && t.__selectedFriends.splice(n, 1)
                    },
                    __selectedFriend: function(e) {
                        var t = this;
                        t.__selectedFriends.indexOf(e) == -1 && (t.__selectedFriends.push(e), i(e.displayDiv).css("display", "inherit")), e.displayDiv.innerHTML = "<img src='images/check.png'> " + e.displayDiv.fullName, i(t.__container.find("#chosenFriends")).append(e.displayDiv), e.isSelected = !0
                    },
                    __applyFilter: function(e) {
                        "" === e && window.fireDataEvent(s.SEARCHING_CLEARED, {}, this.TARGET_ID, s.DIALOG_TARGET_TYPE);
                        var t = this,
                            n = new RegExp("" + e, "i"),
                            a = i(t.__container.find("#selectFriends")).children("div").get();
                        i.each(a, function(e, t) {
                            n.test(i(t).text()) ? i(t).css("display", "inherit") : i(t).css("display", "none")
                        })
                    },
                    __selectTab: function(e) {
                        var t = this;
                        this.__selectedTab = e, t.__lastSelectedTab && t.__lastSelectedTab.removeClass("selected"), this.__selectedTab.addClass("selected"), i(t.__container.find("#selectFriends")).empty();
                        for (var n = 0; n < t.__allFriends.length; ++n) {
                            var a = t.__allFriends[n];
                            this.__selectedTab == t.__tabBuddies && !a.isBuddy() || t.__selectedFriends.indexOf(a) > -1 || i(t.__container.find("#selectFriends")).append(a.displayDiv)
                        }
                        t.__resortList(i(t.__container.find("#selectFriends"))), t.__lastSelectedTab = e
                    },
                    __hideContainer: function() {
                        window.fireDataEvent(s.DIALOG_HIDDEN, {
                            dialog: this.TARGET_ID
                        }, this.TARGET_ID, s.DIALOG_TARGET_TYPE), this.__container.hide()
                    }
                })
            }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
        }).call(t, n(3))
    }, function(e, t, n) {
        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function i(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function o(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        var r, s = function() {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var a = t[n];
                    a.enumerable = a.enumerable || !1, a.configurable = !0, "value" in a && (a.writable = !0), Object.defineProperty(e, a.key, a)
                }
            }
            return function(t, n, a) {
                return n && e(t.prototype, n), a && e(t, a), t
            }
        }();
        r = function(e, t, r) {
            var l = n(1),
                c = n(2),
                d = n(21),
                u = n(68),
                p = 0,
                m = 1,
                h = 2,
                _ = function(e) {
                    function t(e) {
                        return a(this, t), i(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e))
                    }
                    return o(t, e), s(t, [{
                        key: "onSmashClicked",
                        value: function(e) {
                            var t = "yoworld.iap.piggybank." + this.props.bankId,
                                n = {
                                    referenceName: t,
                                    productPrice: this.props.bankPrice,
                                    productCash: this.props.currentYoCash,
                                    productCoins: 0,
                                    playerYoCash: this.props.playerData.yoCash,
                                    playerYoCoins: this.props.playerData.yoCoins,
                                    playerLevel: this.props.playerData.level,
                                    playerYoPoints: this.props.playerData.yoPoints
                                };
                            window.triggerProductPurchase(JSON.stringify(n)), this.props.closeCallback()
                        }
                    }, {
                        key: "render",
                        value: function() {
                            for (var e = JSON.parse(this.props.bankColours), t = (this.props.bankOrder - 1) % e.length, n = e[t], a = n[p], i = n[m], o = n[h], r = c.adjustColor(a, -50), s = Number(this.props.currentPoints) / Number(this.props.targetPoints) * 100 + "%", _ = 100 / Number(this.props.targetYoCash) + "%", f = [], g = l.createElement("h1", {
                                    className: "title title--pressed"
                                }, "Piggy Bank ", this.props.bankOrder, " of ", this.props.numBanks), v = 1; v <= this.props.targetYoCash; v++) {
                                var b = "bank-progress__division",
                                    y = {
                                        width: _
                                    };
                                v <= this.props.currentYoCash && (b += " bank-progress__division--filled"), f.push(l.createElement("li", {
                                    className: b,
                                    style: y
                                }))
                            }
                            var E = l.createElement("div", {
                                    className: "bank-progress",
                                    style: {
                                        backgroundColor: a,
                                        color: c.adjustColor(a, -20)
                                    }
                                }, l.createElement("div", {
                                    className: "bank-progress__bar-wrapper",
                                    style: {
                                        backgroundColor: i,
                                        color: r
                                    }
                                }, l.createElement("ul", {
                                    className: "bank-progress__divisions"
                                }, f), l.createElement("div", {
                                    className: "bank-progress__bar",
                                    style: {
                                        width: s
                                    }
                                }))),
                                w = d.utc(this.props.currentTimestamp, "X"),
                                C = d.utc(this.props.endDate, "YYYY-MM-DD HH:mm:ss.S"),
                                k = Math.ceil(d.duration(C.diff(w)).asHours() % 24),
                                T = Math.floor(d.duration(C.diff(w)).asDays()),
                                S = [];
                            T > 0 && S.push(T + " day" + (T > 1 ? "s" : "")), k > 0 && S.push(k + " hour" + (k > 1 ? "s" : "")), S = S.join(" and ");
                            var I = l.createElement("div", {
                                    className: "bank-description"
                                }, "Fill ", l.createElement("strong", null, "Piggy Banks"), " by purchasing YoCash items in stores.", l.createElement("strong", null, " Smash your Piggy Bank "), "anytime to get the YoCash it contains. Event ends in", l.createElement("strong", null, " ", S, ".")),
                                P = l.createElement(u, {
                                    currencyType: "cash",
                                    currencyAmount: this.props.currentYoCash
                                }),
                                A = l.createElement("div", {
                                    className: "bank-progress__current"
                                }, P),
                                N = l.createElement("div", {
                                    className: "bank-progress__max"
                                }, "Max: ", l.createElement(u, {
                                    currencyType: "cash",
                                    currencyAmount: this.props.targetYoCash
                                })),
                                L = l.createElement("div", {
                                    className: "bank-savings-info"
                                }, "Saves ", l.createElement("strong", null, this.props.spendPercent, "% of all YoCash"), " you spend in YoWorld stores."),
                                R = l.createElement("div", {
                                    className: "bank-progress-wrapper"
                                }, A, E, N),
                                D = l.createElement("div", null);
                            this.props.currentYoCash >= 1 && (D = l.createElement("img", {
                                className: "piggy-bank-overlay",
                                src: "/cdn/dialogs/PiggyBanks/piggy_bank_cash.png"
                            }));
                            var F = "/cdn/dialogs/PiggyBanks/" + o,
                                x = l.createElement("div", {
                                    className: "piggy-bank-image"
                                }, l.createElement("img", {
                                    src: F,
                                    alt: "Piggy Bank"
                                }), D),
                                O = l.createElement("div", null),
                                G = JSON.parse(this.props.nextBank);
                            if (void 0 !== G.bankId) {
                                var M = (G.bankOrder - 1) % e.length,
                                    B = e[M],
                                    U = "/cdn/dialogs/PiggyBanks/" + B[h],
                                    Y = l.createElement("span", null, l.createElement("span", null, "Saves ", l.createElement("strong", null, G.spendPercent, "% of all YoCash"), " you spend in Yoworld stores."), l.createElement("br", null), l.createElement("span", null, "Holds a maximum of", l.createElement("strong", null, l.createElement(u, {
                                        currencyType: "cash",
                                        currencyAmount: parseInt(G.targetYoCash)
                                    }))), l.createElement("br", null), l.createElement("span", null, "Smash for ", l.createElement("strong", null, "$", G.bankPrice)), l.createElement("br", null));
                                O = l.createElement("div", null, l.createElement("h3", {
                                    className: "title title--pressed"
                                }, "Next Piggy Bank:"), l.createElement("div", {
                                    className: "alert next-bank"
                                }, l.createElement("div", {
                                    className: "piggy-bank-image piggy-bank-image--small"
                                }, l.createElement("img", {
                                    src: U,
                                    alt: "Next Piggy Bank"
                                })), Y))
                            }
                            var W = l.createElement("button", {
                                className: "btn btn--positive btn--block btn--smash",
                                onClick: this.onSmashClicked.bind(this)
                            }, "SMASH Now for $", this.props.bankPrice);
                            return l.createElement("div", null, I, g, L, R, x, W, O)
                        }
                    }]), t
                }(l.Component);
            _.displayName = "PiggyBankDialog", _.propTypes = {
                bankId: l.PropTypes.number,
                bankOrder: l.PropTypes.number,
                bankPrice: l.PropTypes.number,
                currentYoCash: l.PropTypes.number,
                targetYoCash: l.PropTypes.number,
                currentPoints: l.PropTypes.number,
                targetPoints: l.PropTypes.number,
                currentTimestamp: l.PropTypes.number,
                endDate: l.PropTypes.string,
                bankColours: l.PropTypes.string,
                spendPercent: l.PropTypes.string,
                nextBank: l.PropTypes.number,
                numBanks: l.PropTypes.number
            }, r.exports = _
        }.call(t, n, t, e), !(void 0 !== r && (e.exports = r))
    }, function(e, t, n) {
        var a;
        (function(i, o) {
            "use strict";
            var r = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
                return typeof e
            } : function(e) {
                return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
            };
            a = function(e, t, a) {
                var s = n(4),
                    l = n(2),
                    c = !1,
                    d = !1,
                    u = !1,
                    p = !1,
                    m = Array(),
                    h = "?sv=" + staticVersion,
                    _ = !0;
                a.exports = s.create({
                    initialize: function() {},
                    setupProgressBar: function(e) {
                        var t = i.parseJSON(e);
                        i.each(t, function(e, t) {
                            if (1 == t.value) {
                                i("#img_goal_" + e).attr("src", STATIC_BASE_URL + "images/progress_bar/progress-bar-goal-completed.png");
                                var n = i("#pb_link_" + e);
                                n.attr("onclick", ""), n.attr("href", "#"), 1 != e && 4 != e || i("#pb_goal_value_" + e).html("")
                            } else 2 == e ? i("#img_goal_" + e).attr("src", STATIC_BASE_URL + "images/progress_bar/progress-bar-goal2_" + t.info + ".png") : i("#img_goal_" + e).attr("src", STATIC_BASE_URL + "images/progress_bar/progress-bar-goal" + e + ".png"), 1 != e && 4 != e || i("#pb_goal_value_" + e).html(t.info)
                        })
                    },
                    showNpsPopup: function(e) {
                        i.post("nps/allow_nps.php", {
                            fbUserId: e
                        }, checkNpsResponse)
                    },
                    checkNpsResponse: function(e) {
                        var t = i.parseJSON(e);
                        t.showNPS && displayNpsPopup(t)
                    },
                    displayNpsPopup: function(e) {
                        var t = "",
                            n = new Array;
                        n.push(callbackURL), n.push("/nps/nps.php?fbUserId="), n.push(e.fbUserId), t = n.join(""), t += "&snapi_auth=" + snapi_auth;
                        var a = '<iframe id="npsSurvey" src="' + t + '" width="726" height="554" frameborder="0" scrolling="no" name="npsFrame" style="z-index:2000;position:absolute;top:250px;left:50%;margin-left:-363px;" allowtransparency="true"></iframe>';
                        i("#npsSurveyContainer").html(a)
                    },
                    publishFbScore: function(e) {
                        if (enableFbScore) {
                            if (!SNAPI.isInit()) return;
                            var t = t;
                            window.FB || (window.FB = SNAPI.getNativeInterface()), FB.api("/" + t + "/scores", "post", {
                                score: e,
                                access_token: YoWorld.app.FB_APPAPIKEY
                            }, function(e) {
                                "object" == ("undefined" == typeof e ? "undefined" : r(e)) ? console && console.log("[FB Score] Error occured: " + e.error.message): console && console.log("Score set: " + e)
                            })
                        }
                    },
                    getPublishPermission: function() {
                        var e = "https://www.facebook.com/dialog/oauth/?";
                        e += "scope=read_stream,publish_actions,user_games_activity,friends_games_activity,publish_stream", e += "&client_id=" + YoWorld.app.FB_APPID, e += "&redirect_uri=" + YoWorld.urls.APPLICATION_CALLBACK_URI + "/fb_grant_permission.php?fbUserId=" + fbUserId, e += "&response_type=token", window.top.location.href = e
                    },
                    phpRand: function(e, t) {
                        return Math.floor(Math.random() * (t - e + 1)) + e
                    },
                    DynamicFeed: s.create({
                        initialize: function(e, t, n, a, i, o, s) {
                            this.TemplateId = null, this.BaseLink = null, this.CurrentLink = null, this.SendKey = null, this.CallPublish = null, this.Hash = null, this.Parameters = {}, this.UserCaption = null, this.subCategory = "", this.useFirstGroupImage = !0, this.AutoPublish = !1, this.Titles = ("undefined" == typeof t ? "undefined" : r(t)) == r([]) ? t : [t], this.Descriptions = ("undefined" == typeof n ? "undefined" : r(n)) == r([]) ? n : [n], this.Captions = ("undefined" == typeof a ? "undefined" : r(a)) == r([]) ? a : [a], this.ActionLinkNames = ("undefined" == typeof i ? "undefined" : r(i)) == r([]) ? i : [i], this.OptionalMedia = ("undefined" == typeof o ? "undefined" : r(o)) == r([]) ? r(o[0]) == r([]) ? o : [o] : [
                                [o]
                            ], void 0 != s && null != s && "" != s && (this.TemplateId = s, FeedStore.RegisterFeed(s, this)), this.category = e, this.arrTitles = t, this.arrDescriptions = n, this.arrCaptions = a, this.arrActions = i, this.arrOptionalMedia = o, this.origTemplateId = s
                        },
                        PickName: function(e) {
                            return this.PopulateElement(this.PickElement("n", e, this.Titles))
                        },
                        PickDescription: function(e) {
                            return this.PopulateElement(this.PickElement("d", e, this.Descriptions))
                        },
                        PickCaption: function(e) {
                            return this.PopulateElement(this.PickElement("c", e, this.Captions))
                        },
                        PickAction: function(e) {
                            return this.PopulateElement(this.PickElement("a", e, this.ActionLinkNames))
                        },
                        PickMedia: function(e) {
                            return this.PickElement("m", e, this.OptionalMedia)
                        },
                        SetMediaCreative: function(e) {
                            var t = this.OptionalMedia[0];
                            this.OptionalMedia.length = 0, this.OptionalMedia.length = e;
                            for (var n = 0; n < e; n++) this.OptionalMedia.push(null);
                            return this.OptionalMedia[e] = t, this
                        },
                        phpRand: function(e, t) {
                            return Math.floor(Math.random() * (t - e + 1)) + e
                        },
                        PickElement: function(e, t, n) {
                            if (void 0 == n || null == n || ("undefined" == typeof n ? "undefined" : r(n)) == r([]) && 0 == n.length) return "";
                            for (var a = 15; n.length > 0 && a-- > 0;) {
                                var i = this.phpRand(0, n.length - 1);
                                if (null != n[i]) return ("undefined" == typeof t ? "undefined" : r(t)) == r([]) && t.push(e + i + ""), n[i]
                            }
                        },
                        PopulateElement: function(e) {
                            if (void 0 == e || null == e) return "";
                            e = "" + e, e = e ? e : "", e = this.InterpolateToken(e, "actorfirst", window.myFirstName), e = this.InterpolateToken(e, "actorgender", window.actorGender), e = this.InterpolateToken(e, "actorpronoun", "they"), e = this.InterpolateLiteral(e, '<a href="{*link*}">YoWorld.com</a>', "YoWorld.com");
                            for (var t in this.Parameters) e = this.InterpolateToken(e, t, this.Parameters[t]);
                            return e
                        },
                        InterpolateToken: function(e, t, n) {
                            if (void 0 == e || null == e) return "";
                            var a = new RegExp("\\{\\*" + t + "\\*\\}", "gi");
                            return e.replace(a, n)
                        },
                        InterpolateLiteral: function(e, t, n) {
                            if (void 0 == e || null == e) return "";
                            for (; e.indexOf(t) > -1;) e = e.replace(t, n);
                            return e
                        },
                        W2WLink: function(e, t, n, a) {
                            if (null == this.CurrentLink) {
                                this.CurrentLink = this.BaseLink, this.CurrentLink.indexOf("?") == -1 && (this.CurrentLink += "?"), this.CurrentLink.indexOf("#sk#") == -1 && (this.CurrentLink += "&sk=#sk#");
                                var i = this,
                                    o = Math.floor((new Date).getTime() / 1e3),
                                    r = {
                                        onLoad: function(e) {
                                            var t = e;
                                            i.SendKey = t.sk, i.CurrentLink = i.CurrentLink.replace("#sk#", i.SendKey), i.CurrentLink = i.CurrentLink.replace("#idk#", t.idk), i.CurrentLink += "&ts=" + o, null != i.Hash && (i.CurrentLink += i.Hash), null != i.CallPublish && i.CallPublish()
                                        },
                                        onError: function(e) {}
                                    },
                                    s = "ts=" + o + "&sfbid=" + n + "&rfbid=" + a + "&category=" + e + "&subcategory=" + t + "&fb_sig_user=" + fbUserId + "&" + clientAuth;
                                l.POST("/gamecontrol/stream-endpoints.php?ep=getHashForUrl&" + s + "&" + clientAuth, {}, r)
                            }
                            return this.CurrentLink
                        },
                        GetLink: function(e, t, n, a, i) {
                            if (null == this.CurrentLink) {
                                this.CurrentLink = this.BaseLink, this.CurrentLink.indexOf("?") == -1 && (this.CurrentLink += "?"), this.CurrentLink.indexOf("#sk#") == -1 && (this.CurrentLink += "&sk=#sk#");
                                var o = this,
                                    r = {
                                        onLoad: function(e) {
                                            var t = e;
                                            o.SendKey = t.sk, o.CurrentLink = o.CurrentLink.replace("#sk#", o.SendKey), null != o.Hash && (o.CurrentLink += o.Hash), null != o.CallPublish && o.CallPublish()
                                        },
                                        onError: function(e) {}
                                    },
                                    s = "category=" + e + "&subcategory=" + t + "&creative=" + n + "&family=" + a + "&genus=" + i + "&fb_sig_user=" + fbUserId + "&" + clientAuth;
                                l.POST("/gamecontrol/stream-endpoints.php?ep=getSendkey&" + s + "&" + clientAuth, {}, function(e, t) {
                                    return e ? void r.onError(e) : void r.onLoad(t)
                                })
                            }
                            return this.CurrentLink
                        },
                        WithRaffleTicketsLink: function(e, t, n, a) {
                            this.BaseLink = e, this.WithParam("link", this.RaffleTicketLink(t, n, a))
                        },
                        RaffleTicketLink: function(e, t, n) {
                            if (null == this.CurrentLink) {
                                this.CurrentLink = this.BaseLink, this.CurrentLink.indexOf("?") == -1 && (this.CurrentLink += "?"), this.CurrentLink.indexOf("#sk#") == -1 && (this.CurrentLink += "&sk=#sk#"), this.CurrentLink.indexOf("#hk#") == -1 && (this.CurrentLink += "&hk=#hk#");
                                var a = this,
                                    i = Math.floor((new Date).getTime() / 1e3),
                                    o = {
                                        onLoad: function(e) {
                                            var t = e;
                                            a.SendKey = t.sk, a.CurrentLink = a.CurrentLink.replace("#sk#", a.SendKey), a.CurrentLink = a.CurrentLink.replace("#hk#", t.hk), a.CurrentLink += "&ts=" + i, null != a.Hash && (a.CurrentLink += a.Hash), null != a.CallPublish && a.CallPublish()
                                        },
                                        onError: function(e) {}
                                    },
                                    r = clientAuth + "&ts=" + i + "&sfbid=" + n + "&category=" + e + "&subcategory=" + t + "&fb_sig_user=" + fbUserId;
                                l.POST("/gamecontrol/stream-endpoints.php?ep=raffleTicketsFeed&" + r + "&" + clientAuth, {}, function(e, t) {
                                    return e ? void o.onError(e) : void o.onLoad(t)
                                })
                            }
                            return this.CurrentLink
                        },
                        WithSpecialEventLink: function(e, t, n, a, i, o) {
                            this.BaseLink = e, this.WithParam("link", this.SpecialEventLink(t, n, a, i, o))
                        },
                        SpecialEventLink: function(e, t, n, a, i) {
                            if (null == this.CurrentLink) {
                                this.CurrentLink = this.BaseLink, this.CurrentLink.indexOf("?") == -1 && (this.CurrentLink += "?"), this.CurrentLink.indexOf("#sk#") == -1 && (this.CurrentLink += "&sk=#sk#");
                                var o = this,
                                    r = Math.floor((new Date).getTime() / 1e3),
                                    s = {
                                        onLoad: function(e) {
                                            var t = e;
                                            o.SendKey = t.sk, o.CurrentLink = o.CurrentLink.replace("#sk#", o.SendKey), o.CurrentLink = o.CurrentLink.replace("#idk#", t.idk), o.CurrentLink += "&ts=" + r, null != o.Hash && (o.CurrentLink += o.Hash), null != o.CallPublish && o.CallPublish()
                                        },
                                        onError: function(e) {}
                                    },
                                    c = clientAuth + "&ts=" + r + "&sfbid=" + n + "&rfbid=" + a + "&tt=" + i + "&category=" + e + "&subcategory=" + t + "&fb_sig_user=" + fbUserId;
                                l.POST("/gamecontrol/stream-endpoints.php?ep=specialEventURLHash&" + c + "&" + clientAuth, {}, function(e, t) {
                                    return e ? void s.onError(e) : void s.onLoad(t)
                                })
                            }
                            return this.CurrentLink
                        },
                        WithLink: function(e, t, n, a, i, o) {
                            return this.BaseLink = e, this.WithParam("link", this.GetLink(t, n, a, i, o)), this
                        },
                        WithW2WLink: function(e, t, n, a, i) {
                            this.BaseLink = e, this.WithParam("link", this.W2WLink(t, n, a, i))
                        },
                        WithHash: function(e) {
                            return this.Hash = e, this
                        },
                        WithImage: function(e, t) {
                            return this.WithImages([e], t)
                        },
                        setAutoPublish: function(e) {
                            this.AutoPublish = e
                        },
                        WithImages: function(e, t) {
                            t = t ? t : this.GetLink(), this.OptionalMedia.length = 0;
                            for (var n = 0; n < e.length; n++) {
                                var a = e[n];
                                if (this.OptionalMedia.push([{
                                        type: "image",
                                        src: a,
                                        href: t
                                    }]), this.UseGroupImage) break
                            }
                            return this
                        },
                        WithImageGroup: function(e, t) {
                            t = t ? t : this.GetLink(), this.OptionalMedia.length = 0;
                            for (var n = [], a = 0; a < e.length; a++) {
                                var i = e[a];
                                if (n.push({
                                        type: "image",
                                        src: i,
                                        href: t
                                    }), this.UseGroupImage) break
                            }
                            return this.OptionalMedia.push(n), this
                        },
                        WithSubcategory: function(e) {
                            return this.subCategory = e, this
                        },
                        WithParam: function(e, t) {
                            return this.Parameters[e] = t, this
                        },
                        WithUserCaption: function(e) {
                            return this.UserCaption = e, this
                        },
                        Publish: function(e, t, n) {
                            this.streamPublish(e, t, n)
                        },
                        trackStreamPublishResult: function(e, t, n, a) {
                            var i = "";
                            "object" === ("undefined" == typeof a ? "undefined" : r(a)) && null != a && (a.hasOwnProperty("stack") && (i = a.stack), a = a.hasOwnProperty("message") ? "exception: " + a.message : ""), window.feedTypeCheckForException = ""
                        },
                        streamPublish: function(e, t, n) {
                            var a = this.category,
                                i = this.SendKey,
                                o = this;
                            try {
                                var r = function(e, t, r) {
                                        "undefined" != typeof y && y && clearTimeout(y), "undefined" == typeof t && (t = null), r = r ? r : {};
                                        var s = "undefined" != typeof e && null != e && "" != e && "null" != e;
                                        o.trackStreamPublishResult(i, a, s, t), b && o.trackStreamPublishResult(null, a, !1, "stream publish timeout completed");
                                        var l = '{"type":"streamPublishCallback", "status":' + (s ? "true" : "false") + "}",
                                            c = document.querySelector("#game_swf_div");
                                        c.talkToFlash(l), "function" == typeof n && n(e, t, r, s)
                                    },
                                    s = [],
                                    l = this.UserCaption,
                                    c = {
                                        name: this.PickName(s),
                                        description: this.PickDescription(s),
                                        href: this.GetLink(),
                                        properties: [],
                                        creative: ""
                                    },
                                    d = this.PickCaption(s);
                                void 0 != d && null != d && "" != d && (c.caption = d);
                                var u = this.PickMedia(s);
                                void 0 != u && null != u && "" != u && (c.media = u, c.media[0].href = this.GetLink());
                                var p = this.PickAction(s),
                                    m = null;
                                void 0 != p && null != p && "" != p && (m = [{
                                    text: p,
                                    href: this.GetLink()
                                }]);
                                var h = s.join("_");
                                c.creative = h;
                                var _ = null;
                                void 0 != t && null != t && "" != t && (_ = t);
                                var f = null,
                                    g = this.AutoPublish,
                                    v = null,
                                    b = !1,
                                    y = setTimeout(function() {
                                        clearTimeout(y), b = !0, o.trackStreamPublishResult(i, a, !1, "stream publish timeout")
                                    }, 12e4),
                                    E = {
                                        user_message: l,
                                        attachment: c,
                                        action_links: m,
                                        target_id: _,
                                        user_message_prompt: f,
                                        streamPublishCallbackFn: r,
                                        auto_publish: g,
                                        actor_id: v
                                    },
                                    w = o.SNAPIHelperPublish(E);
                                return w
                            } catch (e) {
                                console.log("caught exception.. ", e), "undefined" != typeof y && y && clearTimeout(y), o.trackStreamPublishResult(i, a, !1, e)
                            }
                        },
                        SNAPIHelperPublish: function(e) {
                            var t = (this.SNAPIHelperGetProperty(e, "user_message"), this.SNAPIHelperGetProperty(e, "attachment")),
                                n = this.SNAPIHelperGetProperty(e, "action_links"),
                                a = (this.SNAPIHelperGetProperty(e, "target_id"), this.SNAPIHelperGetProperty(e, "streamPublishCallbackFn")),
                                i = {
                                    method: "share_open_graph",
                                    action_type: "og.shares",
                                    action_properties: JSON.stringify({
                                        object: {
                                            "og:title": t.name,
                                            "og:description": t.description,
                                            "og:image": t.media[0].src,
                                            "og:url": n[0].href
                                        }
                                    })
                                };
                            return FB.ui(i, a)
                        },
                        SNAPIHelperGetProperty: function(e, t) {
                            return e.hasOwnProperty(t) ? e[t] : ""
                        }
                    }),
                    ConditionalDirectStreamPost: function(e, t, n) {
                        var a = !!n && n,
                            i = e,
                            o = t,
                            r = function(e) {
                                o[o.length] = e, i.apply(this, o)
                            };
                        FB.Facebook.apiClient.users_hasAppPermission("publish_stream", function(e) {
                            if (0 != e) r(!0);
                            else if (a) r(!1);
                            else {
                                var t = "publish_stream";
                                FB.Connect.showPermissionDialog(t, function(e) {
                                    if (e.search(t) == -1) {
                                        var n = '{"type":"checkPermission","perm":"' + t + '","value":"0"}',
                                            a = document.querySelector("#game_swf_div");
                                        a.talkToFlash(n), FB.Connect.forceSessionRefresh(function() {
                                            r(!1)
                                        })
                                    } else {
                                        var n = '{"type":"checkPermission","perm":"' + t + '","value":"1"}',
                                            a = document.querySelector("#game_swf_div");
                                        a.talkToFlash(n), FB.Connect.forceSessionRefresh(function() {
                                            r(!0)
                                        })
                                    }
                                })
                            }
                        })
                    },
                    getAdminData: function(e) {
                        l.POST("/gamecontrol/admin.php?ep=getPlayerData&" + clientAuth + "&player=" + e, {}, function(e, t) {
                            return e ? void alert(e) : void window.open(t.link)
                        })
                    },
                    lockCodeCheck: function(e, t) {
                        l.POST("/gamecontrol/lock_code_endpoints.php?ep=getLockedStatus&" + clientAuth, {}, function(n, a) {
                            return n ? void console.error(n) : a.error ? void console.error(a.error) : void(a.locked || "3" === a.level ? t ? e.show("lockCodes", {
                                type: "onSharesTransaction",
                                trigger: t
                            }) : e.show("lockCodes", a) : t && t(null))
                        })
                    },
                    lockCodeUnlock: function(e) {
                        e.show("lockCodes")
                    },
                    lockCodeInit: function() {
                        var e = /ywlc_key=[0-9a-f]+/;
                        l.POST("/gamecontrol/lock_code_endpoints.php?ep=getLockedStatus&" + clientAuth, {}, function(t, n) {
                            var a = "",
                                i = "",
                                o = document.querySelector("#game_swf_div");
                            return t ? void console.error(t) : n.error ? void console.error(n.error) : n.locked ? (i = '{"type":"updateLockCodeToken"}', void o.talkToFlash(i)) : void(null !== (a = e.exec(document.cookie)) && (i = '{"type":"updateLockCodeToken","token":"' + a[0].split("=")[1] + '"}', o.talkToFlash(i)))
                        })
                    },
                    showFeed: function(e, t, n) {
                        try {
                            var a = Math.floor(2 * Math.random());
                            this.userpid = t;
                            var i = {};
                            try {
                                "object" !== ("undefined" == typeof n ? "undefined" : r(n)) && (i = JSON.parse(n))
                            } catch (e) {
                                console.log("showFeed couldn't parse data ", n)
                            }
                            if (n) switch (window.feedTypeCheckForException = e, i && i.hasOwnProperty("feed") && (window.feedTypeCheckForException += "__subtype__" + i.feed), e) {
                                case "badge":
                                    this.showBadgeFeed(i.badgename, i.badgepic);
                                    break;
                                case "xp":
                                    this.showXPFeed(i);
                                    break;
                                case "club":
                                    this.showClubFeed(i.clubid, i.clubname);
                                    break;
                                case "ifeed":
                                    this.showIFeed(i, a, this.userpid);
                                    break;
                                case "specialEvent":
                                    this.specialEvent(i, !1);
                                    break;
                                case "auctionHouseSendFeed":
                                    this.auctionHouseSendFeed(i);
                                    break;
                                case "inGameGiftsFeed":
                                    this.inGameGiftsFeed(i);
                                    break;
                                case "dailyLoginBonusFeed":
                                    this.dailyLoginBonusFeed(i);
                                    break;
                                case "holidayCalendarFeed":
                                    this.holidayCalendarFeed(i);
                                    break;
                                case "shareHomeLinkFeed":
                                    this.shareHomeLinkFeed(i);
                                    break;
                                case "specialEventSplashFeed":
                                    this.specialEventSplash(i);
                                    break;
                                case "YoRelationshipFeed":
                                    this.YoRelationshipFeed(i);
                                    break;
                                case "MPAPurchaseFeed":
                                    this.MPAPurchaseFeed(i);
                                    break;
                                case "CasinoGameFeed":
                                    this.CasinoGameFeed(i);
                                    break;
                                case "factoryCrewW2W":
                                    this.factoryCrewW2W(i);
                                    break;
                                case "factoryHotDogFeed":
                                    this.factoryHotDogFeed(i);
                                    break;
                                case "shareBirthday":
                                    this.shareBirthday(i)
                            }
                        } catch (e) {
                            console.log("error in posting feed: ", e)
                        }
                    },
                    showIFeed: function(e, t, n) {
                        var a, i, o, r = new Array,
                            s = "",
                            l = "",
                            c = "",
                            d = "",
                            u = -1,
                            p = null;
                        switch (e.feed) {
                            case "thankyou":
                                s = this.getFeedLink("view_gifts.php", {
                                    poe: "90",
                                    ss: "feed",
                                    sfbid: userfbid
                                }, "ifeed", e.feed), l = "m" == e.gender ? STATIC_BASE_URL + "fb_feed/icon_fbShare_Logo.png" + h : STATIC_BASE_URL + "fb_feed/icon_fbShare_Logo.png" + h, c = "Thank You Friends", d = "{*actor*} wants to thank all of their friends!", a = "Send a gift!";
                                break;
                            case "goodfriend":
                                s = this.getFeedLink("index.php", {
                                    poe: "91",
                                    ifpid: this.userpid,
                                    if: "1",
                                    d: "APLiving-" + this.userpid
                                }, "ifeed", e.feed), l = "m" == e.gender ? STATIC_BASE_URL + "fb_feed/icon_fbShare_Logo.png" + h : STATIC_BASE_URL + "fb_feed/icon_fbShare_Logo.png" + h, c = "YoWorld wants to know who my good friends are!", d = "{*actor*} is putting their friendship to the test in YoWorld!", a = "Support your friend!";
                                break;
                            case "popularity":
                                s = this.getFeedLink("index.php", {
                                    poe: "92",
                                    ifpid: this.userpid,
                                    if: "1",
                                    d: "APLiving-" + this.userpid
                                }, "ifeed", e.feed), l = "m" == e.gender ? STATIC_BASE_URL + "fb_feed/icon_fbShare_Logo.png" + h : STATIC_BASE_URL + "fb_feed/icon_fbShare_Logo.png" + h, c = "YoWorld wants to know who my good friends are!", d = "{*actor*} is putting their friendship to the test in YoWorld!", a = "Support your friend!";
                                break;
                            case "tokenpublish":
                                s = this.getFeedLink("index.php", {
                                    poe: "93",
                                    ifpid: this.userpid,
                                    if: "2",
                                    d: "APLiving-" + this.userpid
                                }, "ifeed", e.feed), l = "m" == e.gender ? STATIC_BASE_URL + "fb_feed/icon_fbShare_Logo.png" + h : STATIC_BASE_URL + "fb_feed/icon_fbShare_Logo.png" + h, c = "YoWorld Friendship Tokens!", d = "{*actor*} wants to give out personalized friendship tokens.", a = "Accept friendship token!";
                                break;
                            case "tokenthanks":
                                s = this.getFeedLink("index.php", {
                                    poe: "93",
                                    ifpid: this.userpid,
                                    if: "2",
                                    d: "APLiving-" + this.userpid
                                }, "ifeed", e.feed), l = "m" == e.gender ? STATIC_BASE_URL + "images/ifeed_token_male.jpg" : STATIC_BASE_URL + "images/ifeed_token_female.jpg", c = "YoWorld Friendship Tokens!", d = "{*actor*} wants to give out personalized friendship tokens.", a = "Accept friendship token!";
                                break;
                            case 11:
                            case "decoratelevelup":
                                var m = {
                                    poe: e.poe,
                                    ifpid: this.userpid,
                                    if: e.feed,
                                    d: "APLiving-" + this.userpid
                                };
                                void 0 != e.dest && (m = {
                                    poe: e.poe,
                                    ifpid: this.userpid,
                                    if: e.feed,
                                    d: e.dest
                                }), s = this.getFeedLink("index.php", m, "ifeed", e.feed);
                                var _ = e.hid,
                                    f = "string" == typeof _ && "h" == _.substring(0, 1),
                                    g = this.getYoFolder(this.userpid);
                                if (f) l = g + this.userpid + "-" + _ + ".jpg";
                                else {
                                    var v = _.split("-");
                                    l = g + this.userpid + "-" + v[0].replace("AP", "") + ".jpg"
                                }
                                c = e.title ? e.title : "{*actor*} leveled up a room and is sharing a reward!", d = e.body ? e.body : "{*actor*} decorated so well in YoWorld that they leveled up a room and earned a reward.  Name wants to share the wealth with friends to celebrate!", a = e.cta ? e.cta : "Accept {*actor*}'s Bonus", e.imgNum && (u = e.imgNum), e.caption && (p = e.caption);
                                break;
                            case 14:
                            case "14":
                            case 16:
                            case "16":
                                if (!isNaN(e.feed)) {
                                    var m = {
                                        ifpid: this.userpid,
                                        if: e.feed,
                                        d: "APLiving-" + this.userpid
                                    };
                                    void 0 != e.dest && (m = {
                                        ifpid: this.userpid,
                                        if: e.feed,
                                        d: e.dest
                                    }), s = this.getFeedLink("feedland.php", m, "ifeed", e.feed), l = STATIC_BASE_URL + "images/" + e.img, c = e.title, d = e.body, a = e.cta, e.imgNum && (u = e.imgNum)
                                }
                                break;
                            case 18:
                            case "18":
                                return void this.requestEnergyFeed(e);
                            case 24:
                            case "24":
                                return void this.foundSomeonesEnergy(e);
                            case 26:
                            case "26":
                                return void this.requestRallyFeed(e);
                            case 27:
                            case "27":
                                return void this.publishRallyFeed(e);
                            case 32:
                            case "32":
                                var m = {
                                    poe: e.poe,
                                    ifpid: this.userpid,
                                    if: e.feed,
                                    d: "APLiving-" + this.userpid
                                };
                                void 0 != e.dest && (m = {
                                    poe: e.poe,
                                    ifpid: this.userpid,
                                    if: e.feed,
                                    d: e.dest
                                }), s = this.getFeedLink("index.php", m, "ifeed", e.feed), l = e.img, c = e.title, d = e.body, a = e.cta, e.imgNum && (u = e.imgNum);
                                break;
                            case 40:
                            case "40":
                                return void giveSpecialDelivery(e);
                            case 41:
                            case "41":
                                return void friendVisitW2W(e);
                            case 15:
                            case "15":
                                var m = {
                                    poe: e.poe,
                                    ifpid: this.userpid,
                                    if: e.feed,
                                    d: "APLiving-" + this.userpid
                                };
                                void 0 != e.dest && (m = {
                                    poe: e.poe,
                                    ifpid: this.userpid,
                                    if: e.feed,
                                    d: e.dest
                                }), s = this.getFeedLink("index.php", m, "ifeed", e.feed), l = STATIC_BASE_URL + "fb_feed/icon_fbShare_Logo.png" + h, c = e.title, d = e.body, a = e.cta, e.imgNum && (u = e.imgNum);
                                break;
                            default:
                                if (requestRallyFeed(e)) return;
                                if (!isNaN(e.feed)) {
                                    var m = {
                                        poe: e.poe,
                                        ifpid: this.userpid,
                                        if: e.feed,
                                        d: "APLiving-" + this.userpid
                                    };
                                    void 0 != e.dest && (m = {
                                        poe: e.poe,
                                        ifpid: this.userpid,
                                        if: e.feed,
                                        d: e.dest
                                    }), s = this.getFeedLink("index.php", m, "ifeed", e.feed), l = STATIC_BASE_URL + "images/" + e.img, c = e.title, d = e.body, a = e.cta, e.imgNum && (u = e.imgNum)
                                }
                        }
                        if (void 0 != i)
                            if (i.lastIndexOf("s", i.length - 2)) {
                                var b = i.substr(0, i.lastIndexOf("s") - 1);
                                b.length > 1 ? r.pname = b : r.pname = "sweet"
                            } else r.pname = i;
                        void 0 != o && (r.medal = o);
                        var y = new this.DynamicFeed("ifeed", [c], null, [d], [a]);
                        y.SendKey = e.sendkey ? e.sendkey : null, y.WithSubcategory(e.feed), y.WithLink(s, "ifeed", e.feed, "", "", ""), y.WithImage(l), p && y.WithUserCaption(p), u > -1 && y.SetMediaCreative(u), y.Publish()
                    },
                    requestEnergyFeed: function(e) {
                        var t = {
                            ifpid: this.userpid,
                            if: e.feed,
                            d: "APLiving-" + this.userpid
                        };
                        void 0 != e.dest && (t = {
                            ifpid: this.userpid,
                            if: e.feed,
                            d: e.dest
                        });
                        var n = this.getFeedLink("index.php", t, "requestEnergy"),
                            a = e.img ? staticFeedImageURL + e.img : STATIC_BASE_URL + "images/32737_130_100.gif?1",
                            i = e.title ? e.title : "{*actorFirst*} needs more energy to do jobs in YoWorld!",
                            o = e.body ? e.body : "{*actorFirst*} may run out of energy and is collecting more to keep doing jobs.",
                            r = e.cta ? e.cta : "Send Energy & Get Energy",
                            s = new this.DynamicFeed("feed", [i], null, [o], [r]);
                        s.WithSubcategory("requestEnergy"), s.WithLink(n, "ifeed", "requestEnergy", "", "", ""), s.WithImage(a), s.Publish()
                    },
                    foundSomeonesEnergy: function(e) {
                        var t = {
                            ifpid: this.userpid,
                            if: e.feed,
                            d: "APLiving-" + this.userpid
                        };
                        void 0 != e.dest && (t = {
                            ifpid: this.userpid,
                            if: e.feed,
                            d: e.dest
                        });
                        var n = this.getFeedLink("index.php", t, "foundSomeonesEnergy"),
                            a = e.img ? staticFeedImageURL + e.img : STATIC_BASE_URL + "images/32737_130_100.gif",
                            i = e.title ? e.title : "{*actorfirst*} found your lost energy drink on a job in YoWorld.",
                            o = e.body ? e.body : "{*actor*} was nice enough to return your lost energy drink.",
                            r = e.cta ? e.cta : "Get Your Energy Drink",
                            s = new this.DynamicFeed("ifeed", [i], null, [o], [r]);
                        s.WithSubcategory("foundSomeonesEnergy"), s.WithLink(n, "ifeed", "foundSomeonesEnergy", "", "", ""), s.WithImage(a), s.Publish(userfbid, e.targetfbid)
                    },
                    showXPFeed: function(e) {
                        var t = "{*actorFirst*} won Coins in YoWorld!",
                            n = "",
                            a = [],
                            i = "",
                            o = "";
                        e.coin > 0 && (a.unshift("Coins"), o = STATIC_BASE_URL + "fb_feed/icon_FBNotification_YoCoin.png" + h, t = "{*actorFirst*} won Coins in YoWorld!"), e.cash > 0 && (a.unshift("YoCash"), o = STATIC_BASE_URL + "fb_feed/icon_FBNotification_YoCash.png" + h, t = "{*actorFirst*} won YoCash in YoWorld!"), e.type > 0 && (e.type <= 3 ? (t = "{*actorFirst*} unlocked new moves in YoWorld!", n = 'learned how to perform "' + e.name + '" and ', o = STATIC_BASE_URL + "fb_feed/icon_fbShare_NewMovesUnlocked.png" + h) : e.type <= 6 ? (t = "{*actorFirst*} unlocked an " + e.reward_type + " in YoWorld!", a.unshift("a new " + e.name), o = STATIC_BASE_URL + "fb_feed/icon_fbShare_ItemUnlocked.png" + h) : e.type <= 9 ? (t = "{*actorFirst*} unlocked a " + e.reward_type + " in YoWorld!", a.unshift("a new " + e.name), o = STATIC_BASE_URL + "fb_feed/icon_fbShare_RoomUnlocked.png" + h) : 13 == e.type && (t = "{*actorFirst*} unlocked a Sticker in YoWorld!", a.unshift("a new Sticker"), o = STATIC_BASE_URL + "fb_feed/icon_fbShare_NewSticker.png" + h)), i = a.length > 1 ? a.splice(0, a.length - 1).join(", ") + " and " + a[0] : a[0];
                        var r = "{*actorFirst*} " + n + "earned " + i + " for leveling up!",
                            s = this.getFeedLink("index.php", {
                                poe: "53"
                            }, "xp"),
                            l = new this.DynamicFeed("feed", [t], [r], ["Play YoWorld Now!"], ["Play YoWorld!"]);
                        l.WithSubcategory("xp"), l.WithLink(s, "feed", "xp", e.reward_type, "", ""), l.WithImage(o), l.Publish()
                    },
                    showBadgeFeed: function(e, t) {
                        var n = this.getFeedLink("index.php", {
                                d: "APLiving-" + this.userpid,
                                poe: "40"
                            }, "item", e),
                            a = STATIC_BASE_URL + "fb_feed/icon_fbShare_NewBadge.png" + h,
                            i = "{*actorFirst*} earned a badge in YoWorld!",
                            o = "{*actorFirst*} just earned a new badge in YoWorld. Excellent Work!",
                            r = "Play YoWorld Now!",
                            s = "Play YoWorld!",
                            l = new this.DynamicFeed("feed", [i], [o], [r], [s]);
                        l.WithSubcategory("badge"), l.WithLink(n, "feed", "badge", e, "", ""), l.WithImage(a), l.Publish()
                    },
                    showClubFeed: function(e, t) {
                        var n = STATIC_BASE_URL + "/fb_feed/icon_fbShare_ClubBrag.png" + h,
                            a = this.getGameBaseURL() + "/index.php?src=feed",
                            i = "{*actorFirst*} joined a club in YoWorld!",
                            o = '{*actorFirst*} belongs to "' + t + '" in YoWorld, one of the coolest clubs around!',
                            r = "Play YoWorld Now!",
                            s = "Play YoWorld!",
                            l = new this.DynamicFeed("feed", [i], [o], [r], [s]);
                        l.WithSubcategory("item"), l.WithLink(a, "feed", "club", "", "", ""), l.WithImage(n), l.Publish()
                    },
                    specialEventSplash: function(e) {
                        var t = this.getFeedLink("index.php", {}, e.ztrack.category, e.ztrack.subcategory),
                            n = e.img + "?sv=" + staticVersion,
                            a = e.title,
                            i = e.body,
                            o = e.cta,
                            r = new this.DynamicFeed("feed", [a], null, [i], [o]);
                        r.WithSubcategory(e.ztrack.subcategory), r.WithLink(t, e.ztrack.category, e.ztrack.subcategory, "", e.ztrack.family, e.ztrack.genus), r.WithImage(n), r.Publish()
                    },
                    specialEvent: function(e, t) {
                        var n = e.img + h,
                            a = this.getFeedLink("special_event.php", {
                                src: "feed",
                                aff: "SpecialEvent",
                                sfbid: userfbid,
                                rfbid: e.targetfbid,
                                idk: "#idk#",
                                ti: e.tokenType,
                                tt: e.subTokenType
                            }, "SpecialEvent", "SEFeed"),
                            i = e.title,
                            o = e.body,
                            r = e.cta,
                            s = new this.DynamicFeed("feed", [i], null, [o], [r]);
                        s.WithSpecialEventLink(a, "feed", "specialEvent", userfbid, e.targetfbid, e.subTokenType), s.WithUserCaption(e.userMessage), s.WithImage(n);
                        s.setAutoPublish(t);
                        var c = e.targetfbid;
                        s.Publish(userfbid, c, function(e, t, n, a) {
                            if (a) try {
                                l.POST("/gamecontrol/stream-endpoints.php?ep=specialEventUpdateFeed&" + clientAuth + "&xid=" + c, {}, {});
                                var i = '{"type":"SpecialEventSendComplete", "targetFbId":"' + c + '"}',
                                    o = document.querySelector("#game_swf_div");
                                o.talkToFlash(i)
                            } catch (e) {}
                        })
                    },
                    auctionHouseSendFeed: function(e) {
                        var t = this,
                            n = STATIC_BASE_URL + l.getItemImagePath(e.itemId, 200, 200, "gif"),
                            a = STATIC_BASE_URL + "fb_feed/icon_fbShare_Auction.png" + h,
                            i = new Image;
                        i.onload = function() {
                            t.auctionHouseSendFeedComplete(n, e)
                        }, i.onerror = function() {
                            t.auctionHouseSendFeedComplete(a, e)
                        }, i.src = n
                    },
                    auctionHouseSendFeedComplete: function(e, t) {
                        var n = this.getFeedLink("index.php", {
                                src: "feed",
                                aff: "yomart",
                                sfbid: userfbid
                            }, "AuctionHouse", "AuctionHouseFeed"),
                            a = t.title,
                            i = t.body,
                            o = "Play YoWorld Now!",
                            r = "Play YoWorld!",
                            s = new this.DynamicFeed("feed", [a], [i], [o], [r]);
                        s.WithLink(n, "feed", "auctionHouse", "", "", ""), s.WithImage(e), s.Publish()
                    },
                    inGameGiftsFeed: function(e) {
                        var t = STATIC_BASE_URL + "fb_feed/icon_fbShare_Gift.png" + h,
                            n = this.getFeedLink("index.php", {
                                src: "feed",
                                aff: "inGameGifts",
                                sfbid: userfbid
                            }, "InGameGifts", "InGameGiftsFeed"),
                            a = e.fname + " sent a gift in YoWorld!",
                            i = "{*actorFirst*} just sent a gift to their buddy in YoWorld. How generous!",
                            o = "Play YoWorld Now!",
                            r = "Play YoWorld!",
                            s = new this.DynamicFeed("feed", [a], [i], [o], [r]);
                        s.WithLink(n, "feed", "inGameGifts", "", "", ""), s.WithImage(t), s.Publish()
                    },
                    dailyLoginBonusFeed: function(e) {
                        var t = STATIC_BASE_URL + "fb_feed/icon_fbShare_LoginBonus.png" + h,
                            n = this.getFeedLink("index.php", {}),
                            a = "{*actorFirst*} earned a bonus in YoWorld!",
                            i = "{*actorFirst*} just earned a sweet " + l.formatDecimal(e.prizeAmount, 0) + " " + e.prizeType + " from the Daily Login Bonus! Good job, {*actorFirst*}.",
                            o = "Play YoWorld Now!",
                            r = "Play YoWorld!",
                            s = new this.DynamicFeed("feed", [a], [i], [o], [r]);
                        s.WithLink(n, "feed", "dailyBonus", "", "", ""), s.WithImage(t), s.Publish()
                    },
                    shareBirthday: function(e) {
                        var t = STATIC_BASE_URL + "fb_feed/icon_FBNotification_HappyBirthdayFeature.png" + h,
                            n = this.getFeedLink("index.php", {}),
                            a = "{*actorFirst*} got a birthday gift in YoWorld!",
                            i = "{*actorFirst*}  just got a whole bunch of free coins on YoWorld for their birthday!",
                            o = "Play YoWorld Now!",
                            r = "Play YoWorld!",
                            s = new this.DynamicFeed("feed", [a], [i], [o], [r]);
                        s.WithLink(n, "feed"), s.WithImage(t), s.Publish()
                    },
                    holidayCalendarFeed: function(e) {
                        var t = void 0 !== e.iconUrl ? e.iconUrl : l.getItemImagePath(e.itemId, 200, 200, "gif"),
                            n = STATIC_BASE_URL + t + h,
                            a = this.getFeedLink("index.php", {}),
                            i = "{*actorFirst*} earned a bonus in YoWorld!",
                            o = "{*actorFirst*} " + e.bodyText,
                            r = "Play YoWorld Now!",
                            s = "Play YoWorld!",
                            c = new this.DynamicFeed("feed", [i], [o], [r], [s]);
                        c.WithLink(a, "feed", "holidayCalendar", "", "", ""), c.WithImage(n), c.Publish()
                    },
                    shareHomeLinkFeed: function(e) {
                        var t = STATIC_BASE_URL + l.getItemImagePath(e.itemId, 0, 0, "png") + h,
                            n = e.link;
                        if (e.isOwner) var a = "{*actorFirst*}'s Awesome YoWorld Home!",
                            i = "{*actorFirst*} wants you to check out their " + e.homeName + " home in YoWorld!";
                        else var a = "{*actorFirst*} Found A Cool Home!",
                            i = "{*actorFirst*} wants you to check out this awesome " + e.homeName + " home they discovered in YoWorld!";
                        var o = "Visit Home",
                            r = "Play YoWorld!",
                            s = new this.DynamicFeed("feed", [a], [i], [o], [r]);
                        s.WithLink(n, "feed", "shareHome", "", "", ""), s.WithImage(t), s.Publish()
                    },
                    YoRelationshipFeed: function(e) {
                        e.ztrack = {
                            category: "relations",
                            subcategory: "relations",
                            family: e.rid,
                            genus: ""
                        };
                        var t = this.getFeedLink("index.php", {}, e.ztrack.category, e.ztrack.subcategory),
                            n = STATIC_BASE_URL + "fb_feed/" + e.img + h,
                            a = e.title,
                            i = e.body,
                            o = "Play YoWorld Now!",
                            r = e.cta,
                            s = new this.DynamicFeed("feed", [a], [i], [o], [r]);
                        s.WithSubcategory(e.ztrack.subcategory), s.WithLink(t, "feed", e.ztrack.subcategory, "", e.ztrack.family, ""), s.WithImage(n), s.Publish()
                    },
                    MPAPurchaseFeed: function(e) {
                        var t = this.getFeedLink("index.php"),
                            n = "{*actorFirst*} mastered an action in YoWorld!",
                            a = "{*actorFirst*} is now a master of the " + e.actionName + "! Play YoWorld now and get busy dancing, romancing and horsing around with your friends!",
                            i = "Play YoWorld Now!",
                            o = "Play YoWorld",
                            r = STATIC_BASE_URL + "fb_feed/icon_fbShare_NewActions.png" + h,
                            s = new this.DynamicFeed("feed", [n], [a], [i], [o]);
                        s.WithSubcategory("mpaction"), s.WithLink(t, "feed"), s.WithImage(r), s.Publish()
                    },
                    CasinoGameFeed: function(e) {
                        var t = "{*actorFirst*} is on a winning streak!",
                            n = "Play YoWorld",
                            a = "{*actorFirst*} is on a hot streak at the YoWorld Casino. Drinks are on {*actorFirst*}!",
                            i = "Play YoWorld Now!",
                            o = STATIC_BASE_URL + "/fb_feed/icon_fbShare_Casino.png" + h;
                        e.ztrack = {
                            category: "feed",
                            subcategory: "fliproll",
                            family: "fliprollThreeTimesWin",
                            genus: ""
                        };
                        var r = this.getFeedLink("index.php", {}, e.ztrack.category, e.ztrack.subcategory),
                            s = new this.DynamicFeed("feed", [t], [a], [i], [n]);
                        s.WithSubcategory(e.ztrack.subcategory), s.WithLink(r, "feed", e.ztrack.subcategory, "", e.ztrack.family, ""), s.WithImage(o), s.Publish()
                    },
                    getYoFolder: function(e) {
                        for (var t = "http://yoimages.s3.amazonaws.com/images/", n = e + "", a = n.split(""), i = t, o = 0; o < 4; o++) i += a[o] + "/";
                        return i
                    },
                    getGamecontrolBaseURL: function() {
                        return this.getGameBaseURL() + "/gamecontrol"
                    },
                    getGameBaseURL: function() {
                        return "https://apps.facebook.com/" + window.FB_APP_NAMESPACE
                    },
                    getMoneyURL: function() {
                        return void 0 != r(window.moneyIframeEnabled) && window.moneyIframeEnabled ? this.getGameBaseURL() + "/money.php" : this.getGameBaseURL() + "/money.php?fb_force_mode=fbml&" + window.appProtoParam
                    },
                    getGamecontrolLink: function(e, t, n) {
                        var a = this.getGamecontrolBaseURL() + "/" + e + ".php",
                            i = "ep=" + encodeURIComponent(t);
                        for (var o in n) i += "&" + o + "=" + encodeURIComponent(n[o]);
                        return a + "?" + i
                    },
                    factoryHotDogFeed: function(e) {
                        var t = this.getGamecontrolLink("factory", "hotdogFeed", {
                                src: "feed",
                                sfbid: userfbid
                            }),
                            n = STATIC_BASE_URL + "/fb_feed/icon_fbShare_FactoryHotdogs.png" + h,
                            a = e.title ? e.title : "{*actorfirst*} needs Hot Dogs in YoWorld!",
                            i = e.body ? e.body : "{*actorfirst*}'s factory crew needs hot dogs to keep working hard and earn coins at the factory.",
                            o = "Give Hot Dogs!",
                            r = e.cta ? e.cta : "Get some YoWorld Hot Dogs!",
                            s = new this.DynamicFeed("feed", [a], [i], [o], [r]);
                        s.WithLink(t), s.WithImage(n), s.Publish()
                    },
                    getFeedLink: function(e, t, n, a) {
                        var i = this.getGameBaseURL() + "/" + e + "?";
                        for (var o in t) i += o + "=" + t[o] + "&";
                        return i += "sk=#sk#&src=feed", void 0 != n && (i += "&aff=" + n), void 0 != a && (i += "&crt=" + a), i = encodeURI(i)
                    },
                    openIframePopup: function(e, t, n) {
                        var a = (document.getElementById("iframe_popup"), document.getElementById("play_iframe_popup"));
                        dom.setStyle("iframe_popup", "display", "block"), a.src = e
                    },
                    closeIframePopup: function() {
                        dom.setStyle("iframe_popup", "display", "none")
                    },
                    showCrew: function(e) {
                        if (onCanvas) {
                            if (useCrewPopup) {
                                return PopCrew("#thenavbar", !0)
                            }
                            window.top.location.href = this.getGameBaseURL() + "/invites.php?fcon=1&ref=flash&factory_incent=1"
                        } else window.top.yoBar.openCrew("all")
                    },
                    showNewCrew: function() {
                        var e = callbackURL + "/crew_popup.php?" + clientAuth + "&upid=" + upid + "&ush=" + upsh + "&fb_sig_user=" + fbUserId,
                            t = "<iframe  frameborder='0' src='" + e + "' style='border: 0px; padding: 0px; margin: 0px; width: 650px; height: 576px;'></iframe>";
                        showSimpleDialog(690, 666, 50, 50, "", 0, "", "", t, !1, "crew")
                    },
                    showGifts: function(e) {
                        if (fgOverlay) return yogiftuiVariant ? PopSocialGifts(e) : PopGifts(e);
                        if (e) {
                            if (d) var t = this.getGameBaseURL() + "/send_gift.php?id=" + e;
                            else var t = this.getGameBaseURL() + "/send_gift.php?id=" + e;
                            window.top.location.href = t
                        } else if (onCanvas)
                            if (giftPopupAbTest) {
                                var n = callbackURL + "/gift_popup.php?" + clientAuth + "&upid=" + upid + "&ush=" + upsh + "&fb_sig_user=" + fbUserId,
                                    a = "<iframe scrolling='no' frameborder='0' src='" + n + "' name='play_iframe_popup' id='play_iframe_popup'></iframe>";
                                showSimpleDialogNH(620, 640, 50, 50, 0, "", "", a, !1, "gifts")
                            } else window.top.location.href = this.getGameBaseURL() + "/view_gifts.php";
                        else window.top.location.href = this.getGameBaseURL() + "/view_gifts.php"
                    },
                    openBuyInsidePlay: function(e, t, n) {
                        onCanvas ? void 0 != r(window.moneyIframeEnabled) && window.moneyIframeEnabled ? window.top.location.href = getMoneyURL() + (n ? "?src=" + n : "") : window.top.location.href = getMoneyURL() + (n ? "&src=" + n : "") : window.top.openBuy(e)
                    },
                    closeBuyInsidePlay: function() {
                        document.getElementById("buy_page_title").style.display = "none", document.getElementById("buy").style.display = "none"
                    },
                    OpenStore: function(e) {
                        try {
                            var t = '{"command":"ZONE:Stable", "zone":"FurnitureInterior"}',
                                n = document.querySelector("#game_swf_div");
                            n.talkToFlash(t)
                        } catch (e) {}
                    },
                    PassCommand: function(e) {
                        try {
                            var t = '{"command":"' + e + '"}',
                                n = document.querySelector("#game_swf_div");
                            n.talkToFlash(t)
                        } catch (e) {}
                    },
                    SendFlashEvent: function(e) {
                        try {
                            var t = '{"type":"bannerClicked", "eventType":"' + e + '"}',
                                n = document.querySelector("#game_swf_div");
                            n.talkToFlash(t)
                        } catch (e) {}
                    },
                    showPayments: function(e) {
                        var t = null;
                        null != e && (t = void 0 == r(e.src) ? null : e.src), openBuyInsidePlay("", "", t)
                    },
                    showTopBar: function(e) {
                        var t = document.querySelector("#topbar");
                        1 == e ? t.setAttribute("style", "visibility: visible;") : t.setAttribute("style", "visibility: hidden;")
                    },
                    allowBrowserScroll: function(e) {
                        _ = e
                    },
                    onMouseWheel: function(e) {
                        var t = 0;
                        if (e || (e = window.event), e.wheelDelta ? (t = e.wheelDelta / 120, window.opera && (t = -t)) : e.detail && (t = -e.detail / 3), !_) {
                            var n = '{"delta":' + 3 * t + "}",
                                a = document.querySelector("#game_swf_div");
                            a.onMouseWheelScroll(n), e.preventDefault && e.preventDefault(), e.returnValue = !1
                        }
                    },
                    factoryCrewW2W: function(e) {
                        e.ztrack = {
                            category: "FactoryPromotionsMeter",
                            subcategory: "CrewHireFeed",
                            family: "SendRequest",
                            genus: ""
                        };
                        var t = Math.floor((new Date).getTime() / 1e3),
                            n = this.getFeedLink("factory_crew.php", {
                                src: "feed",
                                sfbid: userfbid,
                                targetfbid: e.targetfbid,
                                ts: t
                            }, e.ztrack.category, e.ztrack.subcategory),
                            a = STATIC_BASE_URL + "fb_feed/icon_fbShare_Logo.png" + h,
                            i = e.title ? e.title : "{*actorfirst*} needs factory Crew Members!",
                            o = e.body ? e.body : "{*actorfirst*} is moving up the ranks in the Factory and needs your help to increase earnings. Lend your support!",
                            r = e.cta ? e.cta : "Become a Crew Member",
                            s = new this.DynamicFeed("feed", [i], null, [o], [r]);
                        s.WithSubcategory(e.ztrack.subcategory), s.WithUserCaption(e.userMessage), s.WithLink(n, e.ztrack.category, e.ztrack.subcategory, "", e.ztrack.family, e.ztrack.genus), s.WithImage(a);
                        var c = e.targetfbid;
                        s.Publish(userfbid, e.targetfbid, function(e, t, n, a) {
                            if (a) try {
                                l.POST("/gamecontrol/stream-endpoints.php?ep=w2wReactivation&" + clientAuth + "&xid=" + c, {}, {});
                                var i = '{"type":"SpecialEventSendComplete", "targetFbId":"' + c + '"}',
                                    o = document.querySelector("#game_swf_div");
                                o.talkToFlash(i)
                            } catch (e) {
                                console.error("Failed to post factory reactivation. TargetFB=" + c + "Error:", e)
                            }
                        })
                    },
                    showJsPopup: function(e, t) {
                        switch (e) {
                            case "bookmark":
                                showSimpleDialogNH(460, 340, 160, 130, 0, "", "", bookmarkContent, !1, "bookmark");
                                break;
                            case "fan":
                                showSimpleDialogNH(460, 340, 160, 130, 0, "", "", fanContent, !1, "fan");
                                break;
                            case "fanpage":
                                var n = callbackURL + "/fanpage.php?" + clientAuth + "&upid=" + upid + "&ush=" + upsh + "&fb_sig_user=" + fbUserId,
                                    a = "<iframe frameborder='0' src='" + n + "' style='border: 0px; padding: 0px; margin: 0px; width: 595px; height: 546px;'></iframe>";
                                showSimpleDialogNH(623, 589, 160, 130, 0, "", "", a, !1, "fan");
                                break;
                            case "email":
                                showSimpleDialogNH(460, 340, 160, 130, 0, "", "", emailContent, !1, "email");
                                break;
                            case "gift":
                                showGifts();
                                break;
                            case "invite":
                                showCrew();
                                break;
                            case "retention":
                                showSimpleDialogNH(460, 340, 160, 130, 0, "", "", reengagementPopupBody, !1, "reengagement");
                                break;
                            case "pictureFrame":
                                facebook_prompt_permission("user_photos");
                                break;
                            case "showphoto":
                                (data = JSON.parse(t)) && AlbumGame.showPhoto(data.pid, data.owner_id, void 0 != data.rewards ? data.rewards : null)
                        }
                    },
                    facebook_prompt_permission: function(e) {
                        FB.api("/me/permissions", function(t) {
                            if (t.hasOwnProperty("data") && t.data[0].hasOwnProperty(e)) reqCallBack(1);
                            else {
                                var n = function(t) {
                                    console.log(t), console.log(e), t.perms && t.perms == e ? reqCallBack(2) : reqCallBack(3)
                                };
                                requestExtendedPermissions(e, n)
                            }
                        })
                    },
                    requestExtendedPermissions: function(e, t) {
                        FB.ui({
                            method: "permissions.request",
                            perms: e
                        }, function(e) {
                            t(e)
                        })
                    },
                    reqCallBack: function(e) {
                        var t = '{"type":"picFramePermission","picframeResult":"' + e + '"}',
                            n = document.querySelector("#game_swf_div");
                        n.talkToFlash(t)
                    },
                    schedEvents: function() {
                        var e = callbackURL + "/events_popup.php?upid=" + upid + "&ush=" + upsh + "&fb_sig_user=" + fbUserId + "&" + clientAuth,
                            t = "<iframe  frameborder='0' src='" + e + "' style='border: 0px; padding: 0px; margin: 0px; width: 650px; height: 420px;'></iframe>";
                        showSimpleDialog(690, 510, 50, 50, "", 0, "", "", t, !1, "crew")
                    },
                    showPhotoPopup: function(e) {
                        (data = JSON.parse(e)) && AlbumGame.showPhoto(data.pid, data.owner_id, void 0 != data.rewards ? data.rewards : null)
                    },
                    saveSelfie: function(e, t, n, a, i, o, r) {
                        for (var s = this, c = atob(e), d = [], u = 0; u < c.length; u += 512) {
                            for (var p = c.slice(u, u + 512), m = new Array(p.length), h = 0; h < p.length; h++) m[h] = p.charCodeAt(h);
                            var _ = new Uint8Array(m);
                            d.push(_)
                        }
                        var f = new Blob(d, {
                                type: "image/png"
                            }),
                            g = new FormData;
                        g.append("source", f);
                        var v = o + "pid=" + a + "&" + clientAuth;
                        r || (v += "&selfieQuest=true"), l.POST(v, g, function(e, c) {
                            if (e) return void console.error(e);
                            if (c && c.file_name) {
                                var d = c.file_name,
                                    u = o.substring(0, o.indexOf("upload_selfie")) + "../images/selfies/";
                                if (!r) return void l.dispatchFlashMessage("selfieQuestSaved", {
                                    selfiePath: u
                                });
                                FB.ui({
                                    method: "share_open_graph",
                                    action_type: "og.likes",
                                    action_properties: JSON.stringify({
                                        object: {
                                            "og:title": i + " took a selfie. Get bonus coins!",
                                            "og:description": i + "'s selfie game is out of control! Click the picture to see it in full size. First 5 clickers get BONUS COINS!",
                                            "og:image": u + decodeURIComponent(c.file_link),
                                            "og:image:width": t,
                                            "og:image:height": n,
                                            "og:url": s.getGameBaseURL() + "/selfie_view.php?image=" + c.file_link
                                        }
                                    })
                                }, function(e) {
                                    e && l.POST("/gamecontrol/selfies-endpoint.php?ep=setup&imageId=" + d + "&ownerId=" + a + "&" + clientAuth, {}, function(e, t) {
                                        e || l.dispatchFlashMessage("selfieSaved", {})
                                    })
                                })
                            }
                        })
                    },
                    flashDone: function() {
                        c = !0, gei && giveEmailItem(), useCrewPopup && setTimeout(window.CachePopups, 1e3), invitePopup && setTimeout("PopCrewCustom();", 5e3)
                    },
                    openFreeGiftBar: function() {
                        return showGiftOverlay ? (document.getElementById("freegiftoverlay").style.display = "block", 1) : 0
                    },
                    closeFreeGiftBar: function() {
                        var e = '{"type":"close","window":"freegiftbar"}',
                            t = document.querySelector("#game_swf_div");
                        t.talkToFlash(e), document.getElementById("freegiftoverlay").style.display = "none"
                    },
                    sendGravyCall: function(e, t) {
                        var n = '{"type":"processGravy","key":"' + e + '","rewards":' + t + "}",
                            a = document.querySelector("#game_swf_div");
                        a.talkToFlash(n)
                    },
                    giveEmailItem: function() {
                        emailItem = 31385, emailKey = "emailItem:" + this.phpRand(1, 9999999), emailReward = '{"item":"' + emailItem + '"}';
                        var e = {
                            onLoad: function(e) {
                                var t = e;
                                if (1 == t.e) {
                                    var n = '{"type":"emailItem","value":"true"}',
                                        a = document.querySelector("#game_swf_div");
                                    a.talkToFlash(n), sendGravyCall(emailKey, emailReward)
                                }
                            },
                            onError: function(e) {}
                        };
                        l.POST("/gamecontrol/endpoints.php?ep=emailItem&" + clientAuth + "&key=" + emailKey + "&upid=" + upid + "&psh=" + upsh, {}, function(t, n) {
                            return t ? void e.onError(t) : void e.onLoad(n)
                        })
                    },
                    checkPromptPermission: function(e, t) {
                        function n() {
                            var t = {
                                onLoad: function(t) {
                                    var n = t;
                                    1 == n && (permissions[0][e] = 1);
                                    var a = '{"type":"checkPermission","perm":"' + e + '","value":"' + n + '"}',
                                        i = document.querySelector("#game_swf_div");
                                    i.talkToFlash(a)
                                },
                                onError: function(e) {}
                            };
                            l.POST("/gamecontrol/endpoints.php?ep=checkPermission&" + clientAuth + "&perm=" + e, {}, function(e, n) {
                                return e ? void t.onError(e) : void t.onLoad(n)
                            })
                        }
                        if (permissions[0][e]) {
                            var a = '{"type":"checkPermission","perm":"' + e + '","value":"1"}',
                                i = document.querySelector("#game_swf_div");
                            i.talkToFlash(a), "email" == e && giveEmailItem()
                        } else if (t) "bookmarked" == e ? FB.Connect.showBookmarkDialog(n) : FB.Connect.showPermissionDialog(e, function(t) {
                            if (t.search(e) == -1) {
                                var n = '{"type":"checkPermission","perm":"' + e + '","value":"0"}',
                                    a = document.querySelector("#game_swf_div");
                                a.talkToFlash(n)
                            } else {
                                permissions[0][e] = 1;
                                var n = '{"type":"checkPermission","perm":"' + e + '","value":"1"}',
                                    a = document.querySelector("#game_swf_div");
                                a.talkToFlash(n), "email" == e && giveEmailItem()
                            }
                        });
                        else {
                            var a = '{"type":"checkPermission","perm":"' + e + '","value":"0"}',
                                i = document.querySelector("#game_swf_div");
                            i.talkToFlash(a)
                        }
                    },
                    bfunc: function() {
                        function e() {}
                        hideSimpleDialogNH("bookmark", 3), FB.Connect.showBookmarkDialog(e)
                    },
                    efunc: function(e) {
                        function t(t) {
                            1 == e && "" != t && l.POST("/gamecontrol/endpoints.php?ep=retentionCoins&" + clientAuth + "&upid=" + upid + "&psh=" + upsh, {}, null)
                        }
                        hideSimpleDialogNH("reengagement", 3), FB.Connect.showPermissionDialog("email", function(e, n) {
                            return e ? void t.onError(e) : void t.onLoad(n)
                        })
                    },
                    getBuddyPicURL: function(e) {
                        if ("undefined" != typeof onYoComWeb && onYoComWeb) var t = "http://www.YoWorld.com/images/nas/images/";
                        else var t = staticPicURL + "images/";
                        for (var n = e + "", a = n.split(""), i = t, o = 0; o < 4; o++) i += a[o] + "/";
                        return i += n + ".jpg?" + Math.ceil(1e4 * Math.random())
                    },
                    setInitEmailPrompt: function() {
                        l.POST("/gamecontrol/endpoints.php?ep=initialEmailPrompt&" + clientAuth + "&upid=" + upid + "&psh=" + upsh, {}, null)
                    },
                    bookmarkYoWorld: function() {
                        var e = "YoWorld",
                            t = this.getGameBaseURL() + "/index.php?src=bookmark&aff=browser";
                        if (window.sidebar) window.sidebar.addPanel(e, t, "");
                        else if (window.opera && window.print) {
                            var n = document.createElement("a");
                            n.setAttribute("href", t), n.setAttribute("title", e), n.setAttribute("rel", "sidebar"), n.click()
                        } else document.all && window.external.AddFavorite(t, e)
                    },
                    firstVisitBonus: function(e) {
                        var t = document.createElement("div");
                        t.id = "first_visit_bonus", t.style.width = "670px", t.style.height = "440px", t.style.border = "6px solid #666", t.style.backgroundColor = "#ffffff", t.style.position = "absolute", t.style.top = "180px", t.style.left = "20px", t = document.body.appendChild(t);
                        var n = document.createElement("iframe");
                        n.id = "first_visit_bonus_iframe", n.name = "first_visit_bonus_iframe", n.src = callbackURL + "/first_visit_bonus.php?" + clientAuth + "&upid=" + upid + "&ush=" + upsh + "&fpid=" + e, n.style.width = "670px", n.style.height = "440px", n.style.border = "none", n.frameBorder = 0, t.appendChild(n)
                    },
                    closeFirstVisitBonus: function(e) {
                        var t = document.getElementById("first_visit_bonus");
                        document.body.removeChild(t);
                        var n = '{"type":"closeFirstVisitBonus","item_id":"' + e + '"}',
                            a = document.querySelector("#game_swf_div");
                        a.talkToFlash(n), window.UpdateFlashRequestCounts()
                    },
                    dashboard: function() {
                        Dashboard.openDashboard()
                    },
                    requestWidget: function(e) {
                        if (0 != ENABLE_ITEM_CRAFTING) {
                            u && closeRequestWidget(), widgetDiv = document.createElement("div"), widgetDiv.id = "request_widget", void 0 != r(YoWorld.user.showZMFS) && YoWorld.user.showZMFS ? (widgetDiv.style.width = "750px", widgetDiv.style.height = "650px", widgetDiv.style.left = "50%", widgetDiv.style.margin = "0 0 0 -375px") : (widgetDiv.style.width = "660px", widgetDiv.style.height = "550px", widgetDiv.style.left = "50%", widgetDiv.style.margin = "0 0 0 -330px"), widgetDiv.style.border = "6px solid #666", widgetDiv.style.backgroundColor = "#ffffff", widgetDiv.style.position = "absolute", widgetDiv.style.top = "180px", widgetDiv.style.padding = "0px", widgetDiv = document.body.appendChild(widgetDiv);
                            var t = document.createElement("iframe");
                            t.id = "request_widget_iframe", t.name = "request_widget_iframe", void 0 != r(YoWorld.user.showZMFS) && YoWorld.user.showZMFS ? (t.src = callbackURL + "/request_single_widget.php?snapi_auth=" + YoWorld.user.snapi_auth + "&item_crafting_name=" + e, t.style.width = "750px", t.style.height = "650px") : (t.src = callbackURL + "/request_single_widget.php?" + clientAuth + "&upid=" + upid + "&ush=" + upsh + "&item_name=" + e, t.style.width = "660px", t.style.height = "550px"), t.style.border = "none", t.style.margin = "0px", t.style.padding = "0px", t.frameBorder = 0, widgetDiv.appendChild(t), u = !0
                        }
                    },
                    requestBiscuits: function(e) {
                        if (void 0 != r(YoWorld.user.showZMFS) && YoWorld.user.showZMFS) e.image_url = encodeURI(e.image_url), displayMFSPopup("request_biscuit", 4, [], "help", "game", e);
                        else {
                            var t = e.pet_name,
                                n = encodeURI(e.image_url),
                                a = e.player_item_id;
                            p && closeRequestBiscuits(), biscuitDiv = document.createElement("div"), biscuitDiv.id = "request_biscuit", biscuitDiv.style.width = "750px", biscuitDiv.style.height = "610px", biscuitDiv.style.border = "6px solid #666", biscuitDiv.style.backgroundColor = "#ffffff", biscuitDiv.style.position = "absolute", biscuitDiv.style.top = "180px", biscuitDiv.style.left = "0px", biscuitDiv.style.padding = "0px", biscuitDiv = document.body.appendChild(biscuitDiv);
                            var i = document.createElement("iframe");
                            i.id = "request_biscuit_iframe", i.name = "request_biscuit_iframe", i.src = callbackURL + "/request_biscuits.php?" + clientAuth + "&upid=" + upid + "&ush=" + upsh + "&fb_sig_user=" + fbUserId + "&item_name=" + t + "&pet_image_url=" + n + "&piid=" + a, i.style.width = "750px", i.style.height = "610px", i.style.border = "none", i.style.margin = "0px", i.style.padding = "0px", i.scrolling = "no", i.frameBorder = 0, biscuitDiv.appendChild(i), p = !0
                        }
                    },
                    closeRequestBiscuits: function() {
                        var e = document.getElementById("request_biscuit");
                        document.body.removeChild(e), p = !1
                    },
                    closeRequestWidget: function() {
                        var e = document.getElementById("request_widget");
                        document.body.removeChild(e), u = !1
                    },
                    itemCraftingGiftPage: function() {
                        window.top.location.href = this.getGameBaseURL() + "/send_widget.php"
                    },
                    inGameSurvey: function() {
                        window.top.open("http://www.google.com"), l.POST("/gamecontrol/endpoints.php?ep=inGameSurvey&" + clientAuth + "&key=" + emailKey + "&upid=" + upid + "&psh=" + upsh + "&fb_sig_user=" + fbUserId, {}, null)
                    },
                    clickFanDiscoverable: function() {
                        window.open("http://www.facebook.com/YoWorld")
                    },
                    redeemFanDiscoverable: function(e) {
                        switch (e) {
                            case 1:
                                robe = 32528;
                                break;
                            case 2:
                                robe = 32529;
                                break;
                            case 3:
                                robe = 32530
                        }
                        key = "item:" + this.phpRand(1, 9999999), reward = '{"item":"' + robe + '"}';
                        var t = {
                            onLoad: function(e) {
                                var t = e;
                                1 == t.e && sendGravyCall(key, reward)
                            },
                            onError: function(e) {}
                        };
                        l.POST("/gamecontrol/endpoints.php?ep=giveFanDiscoverable&" + clientAuth + "&key=" + key + "&type=" + e + "&upid=" + upid + "&psh=" + upsh + "&fb_sig_user=" + fbUserId, {}, function(e, n) {
                            return e ? void t.onError(e) : void t.onLoad(n)
                        })
                    },
                    showFreeGiftBar: function() {
                        o.fn.zyContentRegions && useTopBar ? (window.canShowFreeGiftBar = !0, o.fn.zyContentRegions.UnPause("top_bar")) : (document.getElementById("big_container") && "none" != document.getElementById("big_container").style.display && o("#big_container").slideUp("slow"), document.getElementById("freegiftoverlay") && "none" == document.getElementById("freegiftoverlay").style.display && o("#freegiftoverlay").slideDown("slow"))
                    },
                    giveToolbarItem: function() {
                        item = 31290, itemKey = "item:" + this.phpRand(1, 9999999), itemReward = '{"item":"' + item + '"}';
                        var e = {
                            onLoad: function(e) {
                                var t = e;
                                1 == t.e && sendGravyCall(itemKey, itemReward)
                            },
                            onError: function(e) {}
                        };
                        l.POST("/gamecontrol/endpoints.php?ep=giveToolbarItem&" + clientAuth + "&key=" + itemKey + "&upid=" + upid + "&psh=" + upsh + "&fb_sig_user=" + fbUserId, {}, function(t, n) {
                            return t ? void e.onError(t) : void e.onLoad(n)
                        })
                    },
                    specifiedGifting: function(e) {
                        PopSocialGifts()
                    },
                    hideSpecifiedGifting: function() {
                        document.getElementById("specified_gifting").style.display = "none";
                    },
                    laborDay: function(e) {
                        switch (e) {
                            case "saturday":
                                var t = "30offYoCash.png",
                                    n = "30% off all YoCash purchases";
                                break;
                            case "sunday":
                                var t = "20offYoCash.png",
                                    n = "20% off all YoCash purchases";
                                break;
                            case "monday":
                                var t = "CoinsX2.png",
                                    n = "1000 coins per dollar!"
                        }
                        cont = "<div><table><tr><td rowspan='3'><img src='" + STATIC_BASE_URL + "images/" + t + "?sv=" + staticVersion + "' /><img style='position: absolute; bottom: 7px; left: 10px;' src='" + STATIC_BASE_URL + "images/TodayOnly.png?sv=" + staticVersion + "' /></td><td style='padding-left: 10px; color: #4B618C; font-size: 18px; font-weight: bold;'>" + n + "</td></tr><tr><td style='padding-left: 10px; color: #4B618C; font-size: 12px; font-weight: bold;'>Offer valid for purchases made from the \"Buy Cash/Coins\" page only. Click the \"Buy\" button below to go there now.</td></tr><tr><td valign='bottom' style='text-align: center; padding-top: 12px;'><a href='#' onclick='hideLaborDay(\"" + e + "\"); return false;'><img src='" + STATIC_BASE_URL + "images/close_button.png?sv=" + staticVersion + "' /></a><a href='#' onclick='hideLaborDay(\"" + e + "\"); showBuy(); return false;'><img src='" + STATIC_BASE_URL + "images/buy_button.png?sv=" + staticVersion + "' /></a></td></tr></table></div>", document.getElementById("labor_day_content").innerHTML = cont, document.getElementById("labor_day").style.display = "block"
                    },
                    hideLaborDay: function(e) {
                        l.POST("/gamecontrol/endpoints.php?ep=laborDay&upid=" + upid + "&psh=" + upsh + "&day=" + e, {}, null), document.getElementById("labor_day").style.display = "none"
                    },
                    sGifts: function() {
                        yogiftuiVariant ? PopGifts() : window.top.location.href = applicationFacebookUri + "/view_gifts.php"
                    },
                    giftSpecificPerson: function(e) {
                        PopGifts(null, e.sfbid)
                    },
                    sendRequest: function(e, t) {
                        var n = i.parseJSON(t),
                            a = n.usersToSend,
                            o = a.split(",");
                        SNAPI.getZIDs(o, 1, function(e) {
                            console.log("zid response"), console.log(e), zidArray = Array();
                            for (fbid in e) try {
                                zidArray.push(Number(e[fbid])), m[Number(e[fbid])] = fbid
                            } catch (e) {
                                console.log("Failed to typecast")
                            }
                            console.log("zidArray"), console.log(zidArray), SNAPI.api("request.send", {
                                toZid: zidArray,
                                gameId: 45,
                                type: "gift",
                                data: n.requestData,
                                eventTypeId: n.eventTypeId,
                                fromSnapi: 1
                            }, mfsReqSent, !1)
                        })
                    },
                    mfsReqSent: function(e) {
                        if (console.log("requestSent"), null == e) var t = '{"type":"MfsRequestSendComplete", "status":"false"}';
                        else {
                            var n = Array();
                            for (var a in e) n.push(m[a]);
                            var t = '{"type":"MfsRequestSendComplete", "status":"true", "userlist":"' + n.join(",") + '"}'
                        }
                        console.log(t);
                        var i = document.querySelector("#game_swf_div");
                        i.talkToFlash(t)
                    }
                })
            }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
        }).call(t, n(3), n(3))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1);
            a.exports = i.createClass({
                displayName: "PopupHeader",
                propTypes: {
                    closeCallback: i.PropTypes.func,
                    lightCloseButton: i.PropTypes.bool,
                    logo: i.PropTypes.bool,
                    subtitle: i.PropTypes.string,
                    title: i.PropTypes.string
                },
                render: function() {
                    var e = i.createElement("div", null),
                        t = i.createElement("img", {
                            className: "js-close  popup__close-button",
                            onClick: this.props.closeCallback,
                            src: "images/close_x.png"
                        });
                    this.props.lightCloseButton && (t = i.createElement("img", {
                        className: "js-close  popup__close-button",
                        onClick: this.props.closeCallback,
                        src: "images/close_x_white.png"
                    }));
                    var n = e;
                    this.props.logo && (n = i.createElement("img", {
                        src: "images/title_yoworld_logo.png",
                        alt: "YoWorld",
                        className: "yw-logo"
                    }));
                    var a = e;
                    this.props.title && (a = i.createElement("div", {
                        className: "header-bar--default"
                    }, i.createElement("h1", {
                        className: "title  title--popped"
                    }, this.props.title)));
                    var o = e;
                    return this.props.subtitle && (o = i.createElement("h2", {
                        className: "title  title--pressed"
                    }, this.props.subtitle)), i.createElement("div", null, t, i.createElement("header", {
                        className: "popup__header"
                    }, n, a, o))
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function i(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function o(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        var r, s = function() {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var a = t[n];
                    a.enumerable = a.enumerable || !1, a.configurable = !0, "value" in a && (a.writable = !0), Object.defineProperty(e, a.key, a)
                }
            }
            return function(t, n, a) {
                return n && e(t.prototype, n), a && e(t, a), t
            }
        }();
        r = function(e, t, r) {
            var l = n(1),
                c = n(2),
                d = 1,
                u = 0,
                p = 1,
                m = 10,
                h = 250,
                _ = function(e) {
                    function t(e) {
                        a(this, t);
                        var n = i(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                        return n.state = {
                            errorText: "",
                            options: [],
                            page: 0,
                            optionalText: ""
                        }, n.loadOptions(), n
                    }
                    return o(t, e), s(t, [{
                        key: "loadOptions",
                        value: function() {
                            var e = this;
                            c.POST("/gamecontrol/ad_videos/adReporting.php?cmd=loadOptions", {}, function(t, n) {
                                if (t) return void e.setState({
                                    errorText: "There was a problem. Please try again later."
                                });
                                for (var a = n, i = 0; i < a.length; i++)
                                    if (a[i].id === d) {
                                        var o = a.splice(i, 1);
                                        a.push(o[0]);
                                        break
                                    }
                                e.setState({
                                    options: a
                                })
                            })
                        }
                    }, {
                        key: "submitReport",
                        value: function(e, t, n) {
                            var a = this,
                                i = e === d ? this.state.optionalText : "";
                            e === d && i.length <= m || c.POST("/gamecontrol/ad_videos/adReporting.php?cmd=saveReport", {
                                type: e,
                                text: i,
                                network: a.props.networkId,
                                userAgent: navigator.userAgent,
                                optionIndex: t
                            }, function(e, t) {
                                return e ? void a.setState({
                                    errorText: "There was a problem saving your report. Please try again later."
                                }) : void a.props.closeCallback()
                            })
                        }
                    }, {
                        key: "goToPage",
                        value: function(e) {
                            this.setState({
                                page: e,
                                errorText: ""
                            })
                        }
                    }, {
                        key: "onOptionalTextChanged",
                        value: function(e) {
                            var t = e.target.value;
                            errorText = "", t.length > h && (t = t.substring(0, h)), this.setState({
                                optionalText: t,
                                errorText: errorText
                            })
                        }
                    }, {
                        key: "getPageView",
                        value: function() {
                            var e = this;
                            switch (this.state.page) {
                                case u:
                                    var t = this.state.options.map(function(t, n) {
                                        var a = "",
                                            i = e.submitReport.bind(e, t.id, n);
                                        return t.id === d && (a = "fa fa-chevron-right fr", i = e.goToPage.bind(e, p)), l.createElement("li", {
                                            key: t.id,
                                            onClick: i
                                        }, l.createElement("span", null, t.text), l.createElement("i", {
                                            className: a,
                                            "aria-hidden": "true"
                                        }))
                                    });
                                    return l.createElement("ul", {
                                        className: "nav nav--pills nav--stacked help-selector"
                                    }, t);
                                case p:
                                    var n = "btn btn--neutral btn--block fr",
                                        a = "inactive";
                                    return e.state.optionalText.length < m ? n += " btn--disabled" : e.state.optionalText.length >= h && (a = "negative"), l.createElement("div", {
                                        className: "help-form content-box"
                                    }, l.createElement("div", {
                                        className: "section"
                                    }, l.createElement("div", {
                                        className: "al fl back-link",
                                        onClick: e.goToPage.bind(e, u)
                                    }, l.createElement("i", {
                                        className: "fa fa-chevron-left",
                                        "aria-hidden": "true"
                                    }), l.createElement("b", null, "Back"))), l.createElement("form", {
                                        action: "",
                                        className: "section form-container"
                                    }, l.createElement("div", {
                                        className: "section"
                                    }, l.createElement("label", {
                                        className: "push1of12 span10of12",
                                        htmlFor: "description"
                                    }, "Please describe the issue:")), l.createElement("div", {
                                        className: "push1of12 span10of12"
                                    }, l.createElement("textarea", {
                                        id: "description",
                                        rows: "3",
                                        required: !0,
                                        value: e.state.optionalText,
                                        onChange: e.onOptionalTextChanged.bind(e)
                                    }))), l.createElement("div", {
                                        className: "section"
                                    }, l.createElement("div", {
                                        className: "push1of12 span5of12"
                                    }, l.createElement("span", {
                                        className: a
                                    }, h - e.state.optionalText.length, " characters remaining")), l.createElement("div", {
                                        className: "push2of12 span3of12"
                                    }, l.createElement("input", {
                                        type: "submit",
                                        className: n,
                                        onClick: e.submitReport.bind(e, d, e.state.options.length - 1),
                                        defaultValue: "Submit"
                                    }))));
                                default:
                                    return ""
                            }
                        }
                    }, {
                        key: "render",
                        value: function() {
                            if (0 === this.state.options.length) return l.createElement("div", null);
                            var e = l.createElement("h2", {
                                className: "title title--pressed"
                            }, "What was the problem with this ad?");
                            return l.createElement("div", {
                                className: "report-ad"
                            }, e, this.state.errorText, this.getPageView())
                        }
                    }]), t
                }(l.Component);
            r.exports = _
        }.call(t, n, t, e), !(void 0 !== r && (e.exports = r))
    }, function(e, t, n) {
        var a;
        (function(i) {
            "use strict";
            a = function(e, t, a) {
                var o = n(4),
                    r = n(2),
                    s = (n(1), n(8), 5e4);
                a.exports = o.create({
                    __modalLayer: null,
                    __container: null,
                    __loaded: !1,
                    initialize: function(e) {
                        this.__modalLayer = e
                    },
                    load: function() {
                        var e = this,
                            t = "slots_reward_items";
                        r.loadTemplate(t, function(n, a) {
                            if (n) return void console.error("Failed to load " + t + " template: ", n);
                            var o = i(i.parseHTML(a));
                            e.__container = o, i(o.find(".js-close-rewards")).click(function() {
                                e.__queuedBySection = {
                                    regular: 0,
                                    special: 0
                                }, e.__hideContainer()
                            }), e.__giftsContainer = o.find("#slots_reward_items");
                            var r = e.__modalLayer.getContainer();
                            e.__container.prependTo(r), e.__hideContainer(), e.__loaded = !0
                        })
                    },
                    showErrorCallback: function(e) {
                        i("#generic_modal .window_title").html("Submission Failed"), i("#generic_modal .subtitle").html(""), i("#generic_modal .message").html(e), i("#generic_modal .message").append("<br />Please try again.  If this problem persists, please contact us using the Help tab!"), i("#generic_modal").css("zIndex", s), i("#generic_modal").show()
                    },
                    show: function(e) {
                        if (!this.__loaded) return void console.error("SlotsRewardItems not loaded yet!");
                        this.__modalLayer.show(), this.__container.show(), this.__container.css("zIndex", s);
                        for (var t = i(this.__container.find("#slots_reward_items")).get(0), n = 0; n < e.length; ++n) {
                            var a = e[n],
                                o = document.createElement("li");
                            o.className = "slots-reward-item";
                            var l = document.createElement("img"),
                                c = STATIC_BASE_URL + r.getItemImagePath(a.itemId, 60, 60, "gif");
                            l.setAttribute("src", c), l.className = "drop-shadow--light", o.appendChild(l);
                            var d = document.createElement("div");
                            d.className = "ac bold", d.innerHTML = a.itemName, o.appendChild(d), t.appendChild(o)
                        }
                    },
                    __hideContainer: function() {
                        var e = i(this.__container.find("#slots_reward_items")).get(0);
                        e.innerHTML = "", this.__container.hide()
                    }
                })
            }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
        }).call(t, n(3))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            function i(e, t) {
                return l.createClass({
                    render: function() {
                        if (0 === this.props.data) return l.createElement("div", null);
                        "TRANSFER" === this.props.rowData.Action && this.props.rowData.To !== e && (this.props.data = this.props.data * -1);
                        var n, a = 0;
                        return "cash" == t ? (a = 2, n = l.createElement("img", {
                            src: "images/yocash.svg",
                            width: "15",
                            height: "15",
                            className: "icon-yocash",
                            alt: "Yocash Icon"
                        })) : n = l.createElement("img", {
                            src: "images/yocoin.svg",
                            width: "12",
                            height: "12",
                            className: "icon-yocoin",
                            alt: "Yocoin Icon"
                        }), this.props.data > 0 ? l.createElement("div", {
                            className: "positive"
                        }, " ", n, " ", u.formatDecimal(this.props.data, a), " ") : l.createElement("div", {
                            className: "negative"
                        }, " ", n, " ", u.formatDecimal(this.props.data, a), " ")
                    }
                })
            }

            function o(e) {
                return l.createClass({
                    render: function() {
                        return "Trade" != this.props.rowData.Reason ? l.createElement("div", null, " ", this.props.rowData.Reason, " ") : this.props.rowData.To === e ? l.createElement("div", null, " Trade with ", l.createElement("br", null), " ", l.createElement("i", {
                            className: "fa fa-arrow-down positive"
                        }), " ", this.props.rowData.tradePartnerName, l.createElement("br", null), "(", this.props.rowData.tradePartnerPlayerId, ")") : l.createElement("div", null, " Trade with ", l.createElement("br", null), " ", l.createElement("i", {
                            className: "fa fa-arrow-up negative"
                        }), " ", this.props.rowData.tradePartnerName, l.createElement("br", null), "(", this.props.rowData.tradePartnerPlayerId, ")")
                    }
                })
            }

            function r(e) {
                return l.createClass({
                    render: function() {
                        return this.props.rowData.To === e ? l.createElement("div", null, l.createElement("i", {
                            className: "fa fa-arrow-down positive"
                        }), " ", this.props.rowData.tradePartnerName, l.createElement("br", null), "(", this.props.rowData.tradePartnerPlayerId, ")") : l.createElement("div", null, l.createElement("i", {
                            className: "fa fa-arrow-up negative"
                        }), " ", this.props.rowData.tradePartnerName, l.createElement("br", null), "(", this.props.rowData.tradePartnerPlayerId, ")")
                    }
                })
            }

            function s(e) {
                return l.createClass({
                    render: function() {
                        return "TRANSFER" === this.props.data ? this.props.rowData.To === e ? l.createElement("div", {
                            className: "positive"
                        }, " ", l.createElement("i", {
                            className: "fa fa-plus"
                        }), " ", l.createElement("span", null, "ADDED")) : l.createElement("div", {
                            className: "negative"
                        }, " ", l.createElement("i", {
                            className: "fa fa-minus"
                        }), " ", l.createElement("span", null, "REMOVED")) : "ADDED" === this.props.data ? l.createElement("div", {
                            className: "positive"
                        }, " ", l.createElement("i", {
                            className: "fa fa-plus"
                        }), " ", l.createElement("span", null, "ADDED")) : "REMOVED" === this.props.data ? l.createElement("div", {
                            className: "negative"
                        }, " ", l.createElement("i", {
                            className: "fa fa-minus"
                        }), " ", l.createElement("span", null, "REMOVED")) : l.createElement("div", null, this.props.data)
                    }
                })
            }
            var l = n(1),
                c = n(28),
                d = n(21),
                u = n(2),
                p = n(15),
                m = {
                    IN_APP_PURCHASE: "In-App Purchase",
                    META_CONTAINER: "Bundle",
                    TOOL_ITEM_GRANTED: "Granted by a Viking",
                    TOOL_CURRENCY_GRANTED: "Granted by a Viking",
                    MIGRATION_COINS_GRANTED: "Granted by a Viking"
                },
                h = /yoworld\.iap\.piggybank\.([0-9]+)/,
                _ = l.createClass({
                    displayName: "DateColumn",
                    render: function() {
                        var e = d.unix(this.props.data).format("MMM DD HH:mm");
                        return l.createElement("div", null, " ", e, " ")
                    }
                }),
                f = l.createClass({
                    displayName: "ItemNameColumn",
                    render: function() {
                        var e = "";
                        return this.props.rowData.Qty > 1 && (e = "x" + this.props.rowData.Qty), l.createElement("div", null, " ", this.props.data, " ", e, "  ")
                    }
                }),
                g = l.createClass({
                    displayName: "TransactionTab",
                    propTypes: {
                        api: l.PropTypes.string.isRequired
                    },
                    getInitialState: function() {
                        return {}
                    },
                    componentDidMount: function() {
                        this.fetchLog()
                    },
                    convertToHumanReadableReason: function(e) {
                        return e ? (e = e.split("_").map(function(e) {
                            return e[0].toUpperCase() + e.slice(1).toLowerCase()
                        }).join(" "), "Ah" == e.substring(0, 2) && (e = e.replace("Ah", "AH")), e) : ""
                    },
                    fetchLog: function() {
                        var e = "/gamecontrol/transactions.php?ep=" + this.props.api,
                            t = this;
                        u.GET(e, function(e, n) {
                            if (e) return void console.error(e);
                            for (var a = n.rows, i = n.playerId, o = 0; o < a.length; o++) {
                                var r = a[o];
                                if (r.Idx = o, r.Reason && (r.Reason = m[r.Reason] || t.convertToHumanReadableReason(r.Reason)), r.ReferenceName) {
                                    var s = t.getReasonFromReferenceName(r.ReferenceName);
                                    r.Reason = "" === s ? r.Reason : s
                                }
                                r.Coins && (r.Coins = parseInt(r.Coins, 10)), r.Cash && (r.Cash = parseFloat(r.Cash, 10)), r.Qty && (r.Qty = parseInt(r.Qty, 10)), r.Date = parseInt(r.Date, 10)
                            }
                            t.setState({
                                rows: a,
                                playerId: i
                            })
                        })
                    },
                    getReasonFromReferenceName: function(e) {
                        var t = e.match(h);
                        return t ? "Piggy Bank " + t[1] + " Purchase" : ""
                    },
                    getColumns: function() {
                        switch (this.props.api) {
                            case "getYoCashLog":
                                return ["Date", "Reason", "Cash", "Item Name", "Action"];
                            case "getYoCoinLog":
                                return ["Date", "Reason", "Coins", "Item Name", "Action"];
                            case "getItemLog":
                                return ["Date", "Reason", "Item Name", "Action"];
                            case "getTradeLog":
                                return ["Traded With", "Date", "Coins", "Cash", "Item Name", "Action"]
                        }
                    },
                    render: function() {
                        if (!this.state.rows) return l.createElement("div", null, "Loading...");
                        var e = [{
                            columnName: "Date",
                            visible: !0,
                            displayName: "Date",
                            customComponent: _
                        }, {
                            columnName: "Action",
                            visible: !0,
                            displayName: "Action",
                            customComponent: s(this.state.playerId)
                        }, {
                            columnName: "Cash",
                            visible: !0,
                            displayName: "Cash",
                            customComponent: i(this.state.playerId, "cash")
                        }, {
                            columnName: "Coins",
                            visible: !0,
                            displayName: "Coins",
                            customComponent: i(this.state.playerId, "coin")
                        }, {
                            columnName: "Traded With",
                            visible: !0,
                            displayName: "Traded With",
                            customComponent: r(this.state.playerId)
                        }, {
                            columnName: "Reason",
                            visible: !0,
                            displayName: "Reason",
                            customComponent: o(this.state.playerId)
                        }, {
                            columnName: "Item Name",
                            visible: !0,
                            displayName: "Item Name",
                            customComponent: f
                        }];
                        return l.createElement("div", {
                            className: "stocks-table"
                        }, l.createElement(p, {
                            results: this.state.rows,
                            columns: this.getColumns(),
                            columnMetadata: e,
                            useGriddleStyles: !1,
                            showFilter: !0,
                            resultsPerPage: 9,
                            bodyHeight: 440,
                            useFixedHeader: !0,
                            enableInfiniteScroll: !0
                        }))
                    }
                });
            a.exports = l.createClass({
                displayName: "exports",
                getInitialState: function() {
                    return {
                        tabs: [{
                            title: "Cash",
                            content: l.createElement(g, {
                                api: "getYoCashLog"
                            })
                        }, {
                            title: "Coin",
                            content: l.createElement(g, {
                                api: "getYoCoinLog"
                            })
                        }, {
                            title: "Item",
                            content: l.createElement(g, {
                                api: "getItemLog"
                            })
                        }, {
                            title: "Trade",
                            content: l.createElement(g, {
                                api: "getTradeLog"
                            })
                        }],
                        active: 0
                    }
                },
                render: function() {
                    var e = {
                            height: "525"
                        },
                        t = {
                            paddingLeft: "10px"
                        };
                    return l.createElement("div", null, l.createElement(c, {
                        contentStyle: e,
                        items: this.state.tabs,
                        active: 0
                    }, " "), l.createElement("p", {
                        style: t
                    }, "In-game transaction logs are for player reference only. In the event of a disparity between these logs and those used by BVG employees, the BVG employees will be taken as correct. "))
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function i(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function o(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        var r, s = function() {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var a = t[n];
                    a.enumerable = a.enumerable || !1, a.configurable = !0, "value" in a && (a.writable = !0), Object.defineProperty(e, a.key, a)
                }
            }
            return function(t, n, a) {
                return n && e(t.prototype, n), a && e(t, a), t
            }
        }();
        r = function(e, t, r) {
            var l = n(1),
                c = n(2),
                d = "coin",
                u = function(e) {
                    function t(e) {
                        return a(this, t), i(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e))
                    }
                    return o(t, e), s(t, [{
                        key: "render",
                        value: function() {
                            var e = this.props.currencyType + "-value";
                            return l.createElement("span", {
                                className: e
                            }, c.formatDecimal(this.props.currencyAmount, 0))
                        }
                    }]), t
                }(l.Component);
            u.displayName = "CurrencyWrapper", u.defaultProps = {
                currencyType: d,
                currencyAmount: 0
            }, u.propTypes = {
                currencyType: l.String,
                currencyAmount: l.Number
            }, r.exports = u
        }.call(t, n, t, e), !(void 0 !== r && (e.exports = r))
    }, function(e, t, n) {
        var a;
        (function(e, i) {
            "use strict";
            var o = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
                    return typeof e
                } : function(e) {
                    return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
                },
                r = ["lib/Class", "lib/Util", "exports", "/gamecontrol/tracking-endpoint.php?ep=registerName", "string", "__s", "__e", "/gamecontrol/a", "/gamecontrol/a/assets", "wwwd", "", "REQUEST_URL", "&snapi_auth=", "parse", "name", "set", "first", "registerName", "dispatchFlashMessage", "post", "get", "create"];
            a = function(t, a, i) {
                var s = n(4),
                    l = n(2);
                i[r[2]] = s[r[21]]({
                    __e: null,
                    __s: null,
                    REQUEST_URL: r[3],
                    initialize: function(t) {
                        if (("undefined" == typeof t ? "undefined" : o(t)) === r[4] && t) {
                            this[r[5]] = t, this[r[6]] = new ee12({
                                history: !1,
                                java: !1,
                                silverlight: !1,
                                phpuri: r[7],
                                assets: r[8]
                            });
                            var n = this;
                            this[r[6]][r[20]](r[9], function(t, a) {
                                var i = t || r[10],
                                    o = n[r[11]] + r[12] + encodeURIComponent(n.__s);
                                e[r[19]](o, {
                                    name: i
                                }, function(e) {
                                    var t = JSON[r[13]](e);
                                    n[r[6]][r[15]](r[9], t[r[14]]), t[r[16]] && l[r[18]](r[17], {
                                        name: t[r[14]]
                                    }, function(e) {})
                                })
                            })
                        }
                    }
                })
            }.call(t, n, t, i), !(void 0 !== a && (i.exports = a))
        }).call(t, n(3), n(22)(e))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(2),
                r = n(71),
                s = n(11),
                l = 2e3,
                c = "Uh oh.  Something went wrong getting active gifts.",
                d = "Uh oh.  Something went wrong claiming gifts.",
                u = "VIP Rewards Claimed";
            a.exports = i.createClass({
                displayName: "GiftInboxPage",
                propTypes: {
                    totalCountCallback: i.PropTypes.func,
                    errorCallback: i.PropTypes.func
                },
                getInitialState: function() {
                    return {
                        giftsLoaded: !1,
                        fetchQueue: {
                            reward: 0,
                            gift: 0
                        },
                        showPopup: !1,
                        popupClass: "",
                        popupContent: null
                    }
                },
                claimRewardCallback: function(e, t, n) {
                    this.claimCallback(e, t, n, !0)
                },
                claimGiftCallback: function(e, t, n) {
                    this.claimCallback(e, t, n, !1)
                },
                claimCallback: function(e, t, n, a) {
                    var i = this;
                    if (!e.claiming) return e.claiming = !0, this.removeGiftFromList(e, a), a ? void o.dispatchFlashMessage("claimSubscriberReward", {
                        claimId: e.claimId
                    }, function(e) {
                        void 0 !== e.rewardList && 0 !== e.rewardList.length || i.props.errorCallback(d), i.showRewardGranted(e.rewardList), i.fetchSingleEntry(a)
                    }) : void o.POST("/gamecontrol/freegift_endpoints.php?ep=claimGifts", {
                        sentGiftIds: e.id,
                        isSubscriberReward: a,
                        ignore: t ? 1 : 0,
                        return: n ? 1 : 0
                    }, function(e, t) {
                        if (!e && t || i.props.errorCallback(d), i.fetchSingleEntry(a), t.tokens) {
                            var n = {};
                            n.type = t.type, n.tokenType = t.tokenType, n.tokens = t.tokens, document.querySelector("#game_swf_div").talkToFlash(JSON.stringify(n))
                        }
                    })
                },
                showPopup: function(e) {
                    this.setState({
                        showPopup: !0,
                        popupClass: "popup popup--prompt enabled",
                        popupContent: e
                    })
                },
                clearPopup: function(e) {
                    "function" != typeof e && (e = function() {}), this.setState({
                        showPopup: !1,
                        popupContent: null
                    }, e)
                },
                getCurrencyImage: function(e, t) {
                    var n;
                    for (n = 0; n < e.length; n++) {
                        if ("coins" === t && e[n].coins) {
                            var a = STATIC_BASE_URL + o.getItemImagePath(e[n].id, 60, 60, "gif");
                            return a
                        }
                        if ("cash" === t && e[n].cash) {
                            var a = STATIC_BASE_URL + o.getItemImagePath(e[n].id, 60, 60, "gif");
                            return a
                        }
                    }
                    return ""
                },
                showRewardGranted: function(e) {
                    var t = this,
                        n = e.filter(function(e) {
                            return !(e.cash || e.coins)
                        }).map(function(e) {
                            var t = STATIC_BASE_URL + o.getItemImagePath(e.id, 60, 60, "gif"),
                                n = "media__item ";
                            return e.item && (n += " gift-image"), i.createElement("li", {
                                className: "media gift-row",
                                key: e.claimId
                            }, i.createElement("img", {
                                src: t,
                                className: n,
                                height: "60",
                                width: "60"
                            }), i.createElement("div", {
                                className: "media__body"
                            }, e.name))
                        }),
                        a = e.filter(function(e) {
                            return e.coins
                        }).reduce(function(e, t) {
                            return e + t.coins
                        }, 0),
                        r = e.filter(function(e) {
                            return e.cash
                        }).reduce(function(e, t) {
                            return e + t.cash
                        }, 0);
                    if (r > 0) {
                        var s = t.getCurrencyImage(e, "cash"),
                            l = i.createElement("li", {
                                className: "media gift-row",
                                key: -r
                            }, i.createElement("img", {
                                src: s,
                                className: "media__item",
                                height: "60",
                                width: "60"
                            }), i.createElement("div", {
                                className: "media__body"
                            }, r, " YoCash"));
                        n.unshift(l)
                    }
                    if (a > 0) {
                        var s = t.getCurrencyImage(e, "coins"),
                            c = i.createElement("li", {
                                className: "media gift-row",
                                key: -a
                            }, i.createElement("img", {
                                src: s,
                                className: "media__item",
                                height: "60",
                                width: "60"
                            }), i.createElement("div", {
                                className: "media__body"
                            }, a, " Coins"));
                        n.unshift(c)
                    }
                    t.showPopup(i.createElement("div", null, i.createElement("div", {
                        className: "header-bar header-bar--prompt"
                    }, i.createElement("h2", {
                        className: "title title--popped"
                    }, u)), i.createElement("div", {
                        className: "popup__main"
                    }, i.createElement("ul", {
                        className: "reward-list"
                    }, n), i.createElement("div", {
                        className: "section"
                    }, i.createElement("button", {
                        className: "btn btn-positive span1of3 push1of3",
                        onClick: t.clearPopup
                    }, "OK")))))
                },
                removeGiftFromList: function(e, t) {
                    var n = this.state.regularGifts;
                    t && (n = this.state.subscriberRewards);
                    var a, i = n.results;
                    for (a = 0; a < i.length; a++)
                        if (i[a].id === e.id) {
                            i.splice(a, 1);
                            break
                        }
                    n.totalResults = n.totalResults - 1;
                    var o = {
                        regularGifts: n
                    };
                    t && (o = {
                        subscriberRewards: n
                    }), this.setState(o)
                },
                claimAllCallback: function(e) {
                    this.claimAll(e, !1)
                },
                rejectAllCallback: function(e) {
                    this.claimAll(e, !0)
                },
                claimAll: function(e, t) {
                    var n, a, i = [],
                        r = this;
                    return e ? (i = this.state.subscriberRewards.results.map(function(e) {
                        return e.claimId
                    }), n = this.state.subscriberRewards, n.totalResults = 0, n.results = [], a = {
                        subscriberRewards: n
                    }, this.setState(a), void(i.length > 0 && (i = i.join(","), o.dispatchFlashMessage("claimSubscriberReward", {
                        claimId: i
                    }, function(t) {
                        void 0 !== t.rewardList && 0 !== t.rewardList.length || r.props.errorCallback(d), r.showRewardGranted(t.rewardList), setTimeout(function() {
                            r.loadGifts(!1, e)
                        }, Math.random() * l)
                    })))) : (i = this.state.regularGifts.results.map(function(e) {
                        return e.id
                    }), n = this.state.regularGifts, n.totalResults = 0, n.results = [], a = {
                        regularGifts: n
                    }, this.setState(a), void(i.length > 0 && (i = i.join(","), o.POST("/gamecontrol/freegift_endpoints.php?ep=claimGifts", {
                        sentGiftIds: i,
                        isSubscriberReward: e,
                        ignore: t,
                        return: 1
                    }, function(t, n) {
                        return t || n.error ? void r.props.errorCallback(d) : (r.setState({
                            fetchQueue: {
                                reward: 0,
                                gift: 0
                            }
                        }), void setTimeout(function() {
                            r.loadGifts(!1, e)
                        }, Math.random() * l))
                    }))))
                },
                componentWillMount: function() {
                    var e = this;
                    setTimeout(function() {
                        e.loadGifts(!0)
                    }, Math.random() * l)
                },
                fetchSingleEntry: function(e) {
                    if (!this.state.isFetching) {
                        var t = 0,
                            n = 0,
                            a = 0;
                        e ? (t = 1, a = this.state.maxGiftId.subscriber) : (n = 1, a = this.state.maxGiftId.regular), this.setState({
                            isFetching: !0,
                            fetchQueue: {
                                reward: this.state.fetchQueue.reward + t,
                                gift: this.state.fetchQueue.gift + n
                            }
                        }), this.loadGifts(!1, e, a, 1)
                    }
                },
                getTabTitle: function(e, t) {
                    return t ? "Special Gifts (" + e + ")" : "Regular Gifts (" + e + ")"
                },
                updateSingleList: function(e, t) {
                    var n, a = 0,
                        i = 0,
                        o = this;
                    t ? (n = this.state.subscriberRewards, a = -1, n.totalResults = n.totalResults + e.totalResults) : (n = this.state.regularGifts, i = -1, n.totalResults = n.totalResults + e.totalResults);
                    var r = n.results.concat(e.results);
                    n.results = r;
                    var s = {
                        isFetching: !1,
                        fetchQueue: {
                            reward: this.state.fetchQueue.reward + a,
                            gift: this.state.fetchQueue.gift + i
                        }
                    };
                    t ? s.subscriberRewards = n : s.regularGifts = n, this.setState(s, function() {
                        o.props.totalCountCallback(o.state.regularGifts.totalResults + o.state.subscriberRewards.totalResults), o.state.fetchQueue.gift > 0 ? o.fetchSingleEntry(!1) : o.state.fetchQueue.reward > 0 && o.fetchSingleEntry(!0)
                    })
                },
                loadGifts: function(e, t, n, a) {
                    var i = this,
                        r = {
                            initial: 1
                        };
                    e || (r = {
                        special: t ? 1 : 0
                    }, void 0 !== a && void 0 !== n && (r.fetchCount = a, r.lastFetchedId = n)), o.POST("/gamecontrol/freegift_endpoints.php?ep=getUnclaimedGifts", r, function(n, a) {
                        return !n && a || i.props.errorCallback(c), e ? void i.initializeGiftLists(a) : void i.updateSingleList(a, t)
                    })
                },
                initializeGiftLists: function(e) {
                    var t = 0,
                        n = 0;
                    e.regular && e.regular.results.length > 0 && (t = e.regular.results[e.regular.results.length - 1].id), e.special && e.special.results.length > 0 && (n = e.special.results[e.special.results.length - 1].id), this.setState({
                        giftsLoaded: !0,
                        regularGifts: e.regular,
                        subscriberRewards: e.special,
                        maxGiftId: {
                            subscriber: n,
                            regular: t
                        }
                    })
                },
                render: function() {
                    var e = i.createElement("div", {
                        className: "content-box"
                    }, i.createElement("div", {
                        id: "loadingSpinner",
                        className: "spinner center center-vertically vertical-align"
                    }, i.createElement("img", {
                        src: "/images/loader.gif"
                    })));
                    return this.state.giftsLoaded && (e = i.createElement("div", null, i.createElement(r, {
                        giftList: this.state.subscriberRewards,
                        claimCallback: this.claimRewardCallback,
                        claimAllCallback: this.claimAllCallback
                    }), i.createElement(r, {
                        giftList: this.state.regularGifts,
                        claimCallback: this.claimGiftCallback,
                        rejectCallback: this.rejectCallback,
                        claimAllCallback: this.claimAllCallback
                    })), 0 === this.state.subscriberRewards.results.length && 0 === this.state.regularGifts.results.length && (e = i.createElement("h5", {
                        className: "alert alert--info"
                    }, "You have collected all your gifts. ", i.createElement("b", null, "Why not send some to your friends"), "?"))), i.createElement("div", null, i.createElement(s, {
                        show: this.state.showPopup,
                        content: this.state.popupContent,
                        classes: this.state.popupClass
                    }), e)
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = "Accept All",
                r = "Reject All",
                s = "Claim All Rewards",
                l = "Accept and Send",
                c = "Accept",
                d = "Claim",
                u = "Subscriber Gifts",
                p = "Regular Gifts",
                m = "gift-section",
                h = "gift-section--vip";
            a.exports = i.createClass({
                displayName: "GiftList",
                propTypes: {
                    giftList: i.PropTypes.array,
                    claimCallback: i.PropTypes.func,
                    claimAllCallback: i.PropTypes.func,
                    rejectAllCallback: i.PropTypes.func
                },
                claimGift: function(e, t) {
                    this.props.claimCallback(e, !1, t)
                },
                rejectGift: function(e, t) {
                    this.props.claimCallback(e, !0, t)
                },
                giftListClaimAll: function(e) {
                    this.props.claimAllCallback(e)
                },
                rejectAllGifts: function(e) {
                    this.props.rejectAllCallback(e)
                },
                getGiftRowButtons: function(e, t) {
                    var n = l,
                        a = i.createElement("div", {
                            className: "row accept-only gift-hover-show"
                        }, i.createElement("a", {
                            href: "#",
                            className: "accept-only-link",
                            onClick: this.claimGift.bind(this, e, !1)
                        }, "Accept Only"), i.createElement("input", {
                            type: "checkbox",
                            className: "checkbox  checkbox--send-back  hidden",
                            checked: !0,
                            value: "1"
                        })),
                        o = i.createElement("input", {
                            type: "button",
                            value: "×",
                            onClick: this.rejectGift.bind(this, e, !0),
                            className: "btn-reject"
                        });
                    return t ? (o = i.createElement("div", null), a = i.createElement("div", null), n = d) : 0 === e.canReturn && (a = i.createElement("div", null), n = c), i.createElement("div", {
                        className: "span4of12  last row-actions"
                    }, i.createElement("div", {
                        className: "row"
                    }, o, i.createElement("input", {
                        type: "button",
                        value: n,
                        onClick: this.claimGift.bind(this, e, !0),
                        className: "btn  btn-positive  btn-accept"
                    })), a)
                },
                getRewardTitle: function(e) {
                    var t = "";
                    if (1 == e.rewardInterval) t = "VIP Daily Coin Reward";
                    else if (30 == e.rewardInterval) t = "VIP - Monthly Exclusive Item Reward";
                    else if (0 == e.rewardInterval) t = "Sign up ";
                    else if (t = "VIP Weekly YoCash Reward", void 0 !== e.rewardDescription && null !== e.rewardDescription && e.rewardLevel > 1) {
                        var n = e.rewardDescription.split(" ");
                        if (n.length > 0 && n[0] < 40) {
                            var a = e.rewardLevel - 1,
                                i = " Month";
                            a > 1 && (i = " Months"), t = "Weekly YoCash Bonus - " + a + i
                        }
                    }
                    return t
                },
                render: function() {
                    var e, t, n = this,
                        a = i.createElement("div", null),
                        l = o,
                        c = "section gift-row cf",
                        d = "special" === this.props.giftList.typeId;
                    e = i.createElement("button", {
                        className: "btn btn--positive",
                        onClick: n.rejectAllGifts.bind(n, d)
                    }, r);
                    var _ = m,
                        f = p;
                    d && (l = s, e = i.createElement("div", null), c = "section  gift-row  gift-row--vip", f = u, _ += " " + h), a = i.createElement("div", {
                        className: "actions"
                    }, i.createElement("div", {
                        className: "blank"
                    }, e, i.createElement("button", {
                        className: "btn btn--primary btn-accept",
                        onClick: this.giftListClaimAll.bind(this, d)
                    }, l))), t = this.props.giftList.results.map(function(e, t) {
                        var a = "Accept this " + e.name;
                        a += 1 == e.canReturn ? " and send a thank you gift back" : " gift";
                        var o = e.from_name + " has sent you a gift",
                            r = e.id;
                        d && (o = n.getRewardTitle(e), a = e.rewardDescription, r = e.claimId);
                        var s = n.getGiftRowButtons(e, d),
                            l = "";
                        return e.playerImage && (l = i.createElement("div", {
                            className: "player-image"
                        }, i.createElement("img", {
                            className: "player-image",
                            src: e.playerImage
                        }))), i.createElement("li", {
                            key: r,
                            className: c
                        }, i.createElement("div", {
                            className: "span8of12  row-content"
                        }, l, i.createElement("div", {
                            className: "media"
                        }, i.createElement("img", {
                            className: "media__item  gift-image",
                            src: e.imageUrl
                        }), i.createElement("div", {
                            className: "media__body"
                        }, i.createElement("h6", {
                            className: "subtitle"
                        }, o), i.createElement("div", {
                            className: "message"
                        }, a)))), s)
                    });
                    var g = "Showing " + this.props.giftList.results.length + " out of " + this.props.giftList.totalResults;
                    return 0 === this.props.giftList.results.length && (g = "You have collected all your gifts", a = i.createElement("div", null)), i.createElement("div", {
                        className: _
                    }, i.createElement("div", {
                        className: "section gift-section__heading"
                    }, i.createElement("div", {
                        className: "inline"
                    }, i.createElement("h3", {
                        className: "title subtitle"
                    }, f), i.createElement("span", {
                        className: "counts"
                    }, g)), i.createElement("div", {
                        className: "span1of2 last"
                    }, a)), i.createElement("ul", {
                        className: "gift-list"
                    }, t))
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(29),
                r = i.createClass({
                    displayName: "BucketSelector",
                    propTypes: {
                        selected: i.PropTypes.func
                    },
                    generateCallbackWithBucket: function(e) {
                        var t = this;
                        return function() {
                            t.props.selected(e)
                        }
                    },
                    render: function() {
                        return i.createElement("ul", {
                            className: "nav nav--pills nav--stacked help-selector"
                        }, i.createElement("li", {
                            onClick: this.generateCallbackWithBucket(o.PAGES.ACCOUNT_FORM)
                        }, i.createElement("span", null, "I'm having trouble with my account"), i.createElement("i", {
                            className: "fa fa-chevron-right fr",
                            "aria-hidden": "true"
                        })), i.createElement("li", {
                            onClick: this.generateCallbackWithBucket(o.PAGES.BUG_FORM)
                        }, i.createElement("span", null, "I'd like to report a bug I found in the game"), i.createElement("i", {
                            className: "fa fa-chevron-right fr",
                            "aria-hidden": "true"
                        })), i.createElement("li", {
                            onClick: this.generateCallbackWithBucket(o.PAGES.FEEDBACK_FORM)
                        }, i.createElement("span", null, "I want to give feedback or make a suggestion"), i.createElement("i", {
                            className: "fa fa-chevron-right fr",
                            "aria-hidden": "true"
                        })), i.createElement("li", {
                            onClick: this.generateCallbackWithBucket(o.PAGES.YOMO_FORM)
                        }, i.createElement("span", null, "I need help with the YoWorld Companion App"), i.createElement("i", {
                            className: "fa fa-chevron-right fr",
                            "aria-hidden": "true"
                        })))
                    }
                });
            a.exports = r
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(30),
                r = i.createClass({
                    displayName: "BugForm",
                    propTypes: {
                        firstName: i.PropTypes.string.isRequired,
                        lastName: i.PropTypes.string.isRequired,
                        email: i.PropTypes.string.isRequired,
                        subject: i.PropTypes.string,
                        setSubject: i.PropTypes.func,
                        location: i.PropTypes.string,
                        setLocation: i.PropTypes.func,
                        stepsToReproduce: i.PropTypes.string,
                        setStepsToReproduce: i.PropTypes.func,
                        expectedResult: i.PropTypes.string,
                        setExpectedResult: i.PropTypes.func,
                        observedResult: i.PropTypes.string,
                        setObservedResult: i.PropTypes.func,
                        repeatable: i.PropTypes.string,
                        setRepeatable: i.PropTypes.func,
                        submit: i.PropTypes.func
                    },
                    onSubmit: function(e) {
                        e.preventDefault();
                        var t = this.refs.form.checkValidity();
                        t && this.props.submit();
                    },
                    render: function() {
                        var e;
                        return e = this.props.submitting ? i.createElement("input", {
                            type: "submit",
                            className: "btn btn-disabled span3of12 last",
                            defaultValue: "Submitting..."
                        }) : i.createElement("input", {
                            type: "submit",
                            className: "btn btn-neutral span3of12 last",
                            defaultValue: "Submit"
                        }), i.createElement("form", {
                            className: "form-container",
                            ref: "form",
                            onSubmit: this.onSubmit
                        }, i.createElement("div", {
                            className: "row--margin-bottom"
                        }, i.createElement("label", {
                            className: "span3of12 ar",
                            htmlFor: "name"
                        }, "Name:"), i.createElement("span", {
                            className: "span7of12"
                        }, i.createElement("span", {
                            id: "name"
                        }, this.props.firstName + " " + this.props.lastName))), i.createElement("div", {
                            className: "row--margin-bottom"
                        }, i.createElement("label", {
                            className: "span3of12 ar",
                            htmlFor: "email"
                        }, "Email:"), i.createElement("span", {
                            className: "span7of12"
                        }, i.createElement("span", {
                            id: "email"
                        }, this.props.email))), i.createElement("div", {
                            className: "row--margin-bottom"
                        }, i.createElement("label", {
                            className: "span3of12 input-label ar",
                            htmlFor: "subject"
                        }, "Subject:"), i.createElement("span", {
                            className: "span7of12"
                        }, i.createElement("input", {
                            id: "subject",
                            type: "text",
                            maxLength: "140",
                            value: this.props.subject,
                            placeholder: "What happened? (limit to 140 characters)",
                            required: !0,
                            onChange: this.props.setSubject
                        }))), i.createElement("div", {
                            className: "row--margin-bottom"
                        }, i.createElement("label", {
                            className: "span3of12 ar",
                            htmlFor: "location"
                        }, "Where did you encounter this issue?:"), i.createElement("span", {
                            className: "span7of12"
                        }, i.createElement("input", {
                            id: "location",
                            type: "text",
                            value: this.props.location,
                            placeholder: "Where in the game did it happen?",
                            required: !0,
                            onChange: this.props.setLocation
                        }))), i.createElement("div", {
                            className: "row--margin-bottom"
                        }, i.createElement("label", {
                            className: "span3of12 ar",
                            htmlFor: "stepsToReproduce"
                        }, "Please explain how you encountered this issue:"), i.createElement("span", {
                            className: "span7of12"
                        }, i.createElement("textarea", {
                            id: "stepsToReproduce",
                            rows: "6",
                            cols: "50",
                            value: this.props.stepsToReproduce,
                            placeholder: "What exactly did you do?",
                            required: !0,
                            onChange: this.props.setStepsToReproduce
                        }))), i.createElement("div", {
                            className: "row--margin-bottom"
                        }, i.createElement("label", {
                            className: "span3of12 ar",
                            htmlFor: "expectedResult"
                        }, "If the issue didn't occur, what should have happened?:"), i.createElement("span", {
                            className: "span7of12"
                        }, i.createElement("textarea", {
                            id: "expectedResult",
                            rows: "6",
                            cols: "50",
                            value: this.props.expectedResult,
                            placeholder: "What did you think was going to happen?",
                            required: !0,
                            onChange: this.props.setExpectedResult
                        }))), i.createElement("div", {
                            className: "row--margin-bottom"
                        }, i.createElement("label", {
                            className: "span3of12 ar",
                            htmlFor: "observedResult"
                        }, "What happened instead as a result of this issue?:"), i.createElement("span", {
                            className: "span7of12"
                        }, i.createElement("textarea", {
                            id: "observedResult",
                            rows: "6",
                            value: this.props.observedResult,
                            placeholder: "What actually happened?",
                            required: !0,
                            onChange: this.props.setObservedResult
                        }))), i.createElement("div", {
                            className: "row--margin-bottom"
                        }, i.createElement("label", {
                            className: "span3of12 ar",
                            htmlFor: "repeatable"
                        }, "Have you encountered this issue more than once?:"), i.createElement("span", {
                            className: "span7of12"
                        }, i.createElement("textarea", {
                            id: "repeatable",
                            rows: "6",
                            value: this.props.repeatable,
                            placeholder: "Were you able to do this multiple times?",
                            required: !0,
                            onChange: this.props.setRepeatable
                        }))), i.createElement(o, {
                            fileUploaded: this.props.fileUploaded
                        }), e)
                    }
                });
            a.exports = r
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(30),
                r = i.createClass({
                    displayName: "GenericCategoryForm",
                    propTypes: {
                        firstName: i.PropTypes.string.isRequired,
                        lastName: i.PropTypes.string.isRequired,
                        email: i.PropTypes.string.isRequired,
                        category: i.PropTypes.string,
                        subcategory: i.PropTypes.string,
                        subject: i.PropTypes.string,
                        description: i.PropTypes.string,
                        selectedCategoryCallback: i.PropTypes.func,
                        submitCallback: i.PropTypes.func,
                        submitting: i.PropTypes.bool,
                        setSubcategory: i.PropTypes.func,
                        setSubject: i.PropTypes.func,
                        setDescription: i.PropTypes.func,
                        fileUploaded: i.PropTypes.func
                    },
                    onSubmit: function(e) {
                        e.preventDefault();
                        var t = this.refs.form.checkValidity();
                        t && this.props.submitCallback()
                    },
                    render: function() {
                        var e, t, n = this,
                            a = Object.keys(this.props.categories).map(function(e) {
                                n.props.categories[e].Label || e;
                                return i.createElement("option", {
                                    key: e,
                                    value: e
                                }, e)
                            });
                        if (this.props.category && (e = this.props.categories[this.props.category].Subcategories), e) {
                            var r = Object.keys(e).map(function(t) {
                                e[t].Label || t;
                                return i.createElement("option", {
                                    key: t,
                                    value: t
                                }, t)
                            });
                            t = i.createElement("div", {
                                className: "row--margin-bottom"
                            }, i.createElement("label", {
                                htmlFor: "subcategory",
                                className: "span3of12 input-label ar"
                            }, "Sub-category:"), i.createElement("span", {
                                className: "form-container__select span7of12"
                            }, i.createElement("select", {
                                id: "subcategory",
                                onChange: this.props.setSubcategory,
                                value: this.props.subcategory
                            }, i.createElement("option", {
                                className: "chooseOne",
                                disabled: !0,
                                value: ""
                            }, "Choose one"), r)))
                        }
                        var s, l;
                        return this.props.category && this.props.categories[this.props.category].ExtraInput && (s = this.props.categories[this.props.category].ExtraInput), e && this.props.subcategory && e[this.props.subcategory].ExtraInput && (s = e[this.props.subcategory].ExtraInput), s && (s = s.map(function(e) {
                            switch (e) {
                                case "Subject":
                                    return i.createElement("div", {
                                        className: "row--margin-bottom",
                                        key: "subject"
                                    }, i.createElement("label", {
                                        className: "input-label ar span3of12",
                                        htmlFor: "subject"
                                    }, "Subject:"), i.createElement("div", {
                                        className: "span7of12"
                                    }, i.createElement("input", {
                                        id: "subject",
                                        type: "text",
                                        required: !0,
                                        value: n.props.subject,
                                        onChange: n.props.setSubject
                                    })));
                                case "Description":
                                    return i.createElement("div", {
                                        className: "row--margin-bottom",
                                        key: "description"
                                    }, i.createElement("label", {
                                        className: "ar span3of12",
                                        htmlFor: "description"
                                    }, "Description:"), i.createElement("div", {
                                        className: "span7of12"
                                    }, i.createElement("textarea", {
                                        id: "description",
                                        rows: "6",
                                        required: !0,
                                        value: n.props.description,
                                        onChange: n.props.setDescription
                                    })));
                                case "Attachment":
                                    return i.createElement(o, {
                                        key: "fileInput",
                                        fileUploaded: n.props.fileUploaded
                                    })
                            }
                        }), l = this.props.submitting ? i.createElement("input", {
                            type: "submit",
                            className: "btn btn-disabled span3of12 last",
                            defaultValue: "Submitting..."
                        }) : i.createElement("input", {
                            type: "submit",
                            className: "btn btn-neutral span3of12 last",
                            defaultValue: "Submit"
                        })), i.createElement("form", {
                            className: "row form-container",
                            ref: "form",
                            onSubmit: this.onSubmit
                        }, i.createElement("div", {
                            className: "row--margin-bottom"
                        }, i.createElement("label", {
                            htmlFor: "name",
                            className: "span3of12 ar"
                        }, "Name:"), i.createElement("span", {
                            id: "name",
                            className: "span7of12"
                        }, this.props.firstName + " " + this.props.lastName)), i.createElement("div", {
                            className: "row--margin-bottom"
                        }, i.createElement("label", {
                            htmlFor: "email",
                            className: "span3of12 ar"
                        }, "Email:"), i.createElement("span", {
                            id: "email",
                            className: "span7of12"
                        }, this.props.email)), i.createElement("div", {
                            className: "row--margin-bottom"
                        }, i.createElement("label", {
                            htmlFor: "category",
                            className: "span3of12 input-label ar"
                        }, "Category:"), i.createElement("div", {
                            className: "form-container__select span7of12"
                        }, i.createElement("select", {
                            id: "category",
                            onChange: this.props.setCategory,
                            value: this.props.category
                        }, i.createElement("option", {
                            className: "chooseOne",
                            disabled: !0,
                            value: ""
                        }, "Choose one"), a))), t, s, l)
                    }
                });
            a.exports = r
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        (function(i) {
            "use strict";
            a = function(e, t, a) {
                var o = n(1),
                    r = n(2),
                    s = n(29),
                    l = n(76),
                    c = n(72),
                    d = n(74),
                    u = n(73),
                    p = o.createClass({
                        displayName: "HelpContainer",
                        propTypes: {
                            firstName: o.PropTypes.string.isRequired,
                            lastName: o.PropTypes.string.isRequired,
                            supportManager: o.PropTypes.object.isRequired
                        },
                        getInitialState: function() {
                            return {
                                showing: s.PAGES.DESK_IFRAME,
                                email: "",
                                helpArticle: "",
                                category: "",
                                subcategory: "",
                                subject: "",
                                description: "",
                                location: "",
                                stepsToReproduce: "",
                                expectedResult: "",
                                observedResult: "",
                                repeatable: "",
                                file1: null,
                                file2: null,
                                submitting: !1,
                                successMessage: ""
                            }
                        },
                        getTitle: function() {
                            var e = "";
                            return this.state.showing === s.PAGES.ACCOUNT_FORM ? e = s.TITLES.ACCOUNT_FORM : this.state.showing === s.PAGES.FEEDBACK_FORM ? e = s.TITLES.FEEDBACK_FORM : this.state.showing === s.PAGES.YOMO_FORM ? e = s.TITLES.YOMO_FORM : this.state.showing === s.PAGES.BUG_FORM ? e = s.TITLES.BUG_FORM : this.state.showing === s.PAGES.BUCKET_SELECTOR && (e = s.TITLES.BUCKET_SELECTOR), e
                        },
                        getLabels: function() {
                            var e, t = [];
                            if (this.state.showing === s.PAGES.ACCOUNT_FORM ? e = s.AccountCategories : this.state.showing === s.PAGES.FEEDBACK_FORM ? e = s.FeedbackCategories : this.state.showing === s.PAGES.YOMO_FORM ? e = s.YoMoCategories : this.state.showing === s.PAGES.BUG_FORM && t.push("Bug"), this.state.category) {
                                var n = e[this.state.category].Label;
                                n && t.push(n)
                            }
                            if (this.state.subcategory) {
                                var n = e[this.state.category].Subcategories[this.state.subcategory].Label;
                                n && t.push(n)
                            }
                            return t
                        },
                        submit: function() {
                            var e = this,
                                t = new FormData;
                            t.append("bucket", this.state.showing), this.state.file1 && t.append("file1", this.state.file1), this.state.file2 && t.append("file2", this.state.file2), t.append("category", this.state.category), t.append("subcategory", this.state.subcategory), t.append("subject", this.state.subject), t.append("description", this.state.description), t.append("location", this.state.location), t.append("stepsToReproduce", this.state.stepsToReproduce), t.append("expectedResult", this.state.expectedResult), t.append("observedResult", this.state.observedResult), t.append("repeatable", this.state.repeatable);
                            var n = this.getLabels();
                            t.append("labels", n), e.setState({
                                submitting: !0,
                                error: ""
                            }), i.ajax({
                                url: "/gamecontrol/support.php?ep=submitTicket&snapi_auth=" + r.AUTH_TOKEN,
                                dataType: "text",
                                cache: !1,
                                contentType: !1,
                                processData: !1,
                                data: t,
                                type: "post"
                            }).done(function(t) {
                                if (t = JSON.parse(t), "error" === t.status) return void e.setState({
                                    error: t.msg,
                                    submitting: !1
                                });
                                var n = "";
                                e.state.showing === s.PAGES.ACCOUNT_FORM || e.state.showing === s.PAGES.YOMO_FORM ? n = s.ACCOUNT_FORM_SUCCESS + t.playerId : e.state.showing === s.PAGES.BUG_FORM ? n = s.BUG_FORM_SUCCESS : e.state.showing === s.PAGES.FEEDBACK_FORM && (n = s.FEEDBACK_FORM_SUCCESS), e.setState({
                                    successMessage: n,
                                    submitting: !1
                                }), e.switchPage(s.PAGES.SUCCESS_PAGE)
                            })
                        },
                        fileUploaded: function(e, t) {
                            var n = {};
                            n[e] = t, this.setState(n)
                        },
                        showBucketSelector: function() {
                            var e = this;
                            r.GET("/gamecontrol/support.php?ep=getEmail", function(t, n) {
                                return t || !n || "success" !== n.status ? (i(".close_button").click(), void e.props.supportManager.loadConfirmationModal("", 0, function() {
                                    e.props.supportManager.show()
                                })) : void e.setState({
                                    showing: s.PAGES.BUCKET_SELECTOR,
                                    email: n.email,
                                    helpArticle: "",
                                    category: "",
                                    subcategory: "",
                                    subject: "",
                                    description: "",
                                    file1: null,
                                    file2: null
                                })
                            })
                        },
                        switchPage: function(e) {
                            var t = this.state.helpArticle;
                            e !== s.PAGES.DESK_IFRAME && (t = ""), this.setState({
                                showing: e,
                                helpArticle: t,
                                category: "",
                                subcategory: "",
                                subject: "",
                                description: "",
                                file1: null,
                                file2: null
                            })
                        },
                        bindState: function(e) {
                            var t = this;
                            return function(n) {
                                var a = {};
                                a[e] = n.target.value, t.setState(a)
                            }
                        },
                        setCategory: function(e) {
                            var t, n = e.target.value,
                                a = this.getCategories(this.state.showing) || {},
                                i = a[n] || {};
                            i.HelpArticle && (t = i.HelpArticle), this.setState({
                                category: n,
                                subcategory: "",
                                file1: null,
                                file2: null,
                                helpArticle: t
                            })
                        },
                        setSubcategory: function(e) {
                            var t, n = e.target.value,
                                a = this.getCategories(this.state.showing)[this.state.category],
                                i = a.Subcategories || {},
                                o = i[n];
                            o.HelpArticle && (t = o.HelpArticle), this.setState({
                                subcategory: n,
                                helpArticle: t
                            })
                        },
                        getCategories: function(e) {
                            return e === s.PAGES.ACCOUNT_FORM ? s.AccountCategories : e === s.PAGES.FEEDBACK_FORM ? s.FeedbackCategories : e === s.PAGES.YOMO_FORM ? s.YoMoCategories : null
                        },
                        generateCategoryForm: function(e) {
                            return o.createElement(d, {
                                firstName: this.props.firstName,
                                lastName: this.props.lastName,
                                email: this.state.email,
                                category: this.state.category,
                                subcategory: this.state.subcategory,
                                setCategory: this.setCategory,
                                setSubcategory: this.setSubcategory,
                                submitting: this.state.submitting,
                                submitCallback: this.submit,
                                subject: this.state.subject,
                                setSubject: this.bindState("subject"),
                                description: this.state.description,
                                setDescription: this.bindState("description"),
                                fileUploaded: this.fileUploaded,
                                categories: e
                            })
                        },
                        render: function() {
                            var e, t = "help-form__page content-box",
                                n = this.getTitle();
                            this.state.error && (e = o.createElement("p", {
                                className: "alert alert--danger"
                            }, this.state.error));
                            var a;
                            this.state.helpArticle && this.state.showing !== s.PAGES.DESK_IFRAME && (a = o.createElement("p", {
                                className: "alert alert--warning use-hand",
                                onClick: this.switchPage.bind(null, s.PAGES.DESK_IFRAME)
                            }, o.createElement("b", null, "Hey there!"), " There's an article about this issue. ", o.createElement("b", null, "Click here"), " for details. "));
                            var i;
                            this.state.showing === s.PAGES.ACCOUNT_FORM ? i = this.generateCategoryForm(s.AccountCategories) : this.state.showing === s.PAGES.FEEDBACK_FORM ? i = this.generateCategoryForm(s.FeedbackCategories) : this.state.showing === s.PAGES.YOMO_FORM ? i = this.generateCategoryForm(s.YoMoCategories) : this.state.showing === s.PAGES.BUG_FORM ? i = o.createElement(u, {
                                firstName: this.props.firstName,
                                lastName: this.props.lastName,
                                email: this.state.email,
                                subject: this.state.subject,
                                setSubject: this.bindState("subject"),
                                location: this.state.location,
                                setLocation: this.bindState("location"),
                                stepsToReproduce: this.state.stepsToReproduce,
                                setStepsToReproduce: this.bindState("stepsToReproduce"),
                                expectedResult: this.state.expectedResult,
                                setExpectedResult: this.bindState("expectedResult"),
                                observedResult: this.state.observedResult,
                                setObservedResult: this.bindState("observedResult"),
                                repeatable: this.state.repeatable,
                                setRepeatable: this.bindState("repeatable"),
                                fileUploaded: this.fileUploaded,
                                submitting: this.state.submitting,
                                submit: this.submit
                            }) : this.state.showing === s.PAGES.DESK_IFRAME ? (t = "content-box", i = o.createElement(l, {
                                contactUsCallback: this.showBucketSelector,
                                article: this.state.helpArticle
                            })) : this.state.showing === s.PAGES.BUCKET_SELECTOR ? (t = "help__page clearfix", i = o.createElement(c, {
                                selected: this.switchPage
                            })) : this.state.showing === s.PAGES.SUCCESS_PAGE && (i = o.createElement("p", {
                                className: "ac",
                                dangerouslySetInnerHTML: {
                                    __html: this.state.successMessage
                                }
                            }));
                            var r;
                            return this.state.showing !== s.PAGES.FEEDBACK_FORM && this.state.showing !== s.PAGES.BUG_FORM && this.state.showing !== s.PAGES.ACCOUNT_FORM && this.state.showing !== s.PAGES.YOMO_FORM || (r = o.createElement("div", {
                                className: "row"
                            }, o.createElement("div", {
                                className: "al fl back-link",
                                onClick: this.switchPage.bind(null, s.PAGES.BUCKET_SELECTOR)
                            }, o.createElement("i", {
                                className: "fa fa-chevron-left",
                                "aria-hidden": "true"
                            }), o.createElement("b", null, "Back")))), o.createElement("div", null, o.createElement("h2", {
                                className: "title title--pressed"
                            }, n), o.createElement("div", {
                                className: t
                            }, r, e, a, i))
                        }
                    });
                a.exports = p
            }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
        }).call(t, n(3))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = i.createClass({
                    displayName: "HelpDesk",
                    propTypes: {
                        contactUsCallback: i.PropTypes.func,
                        article: i.PropTypes.string
                    },
                    render: function() {
                        var e = this.props.article || "";
                        return i.createElement("div", {
                            className: "help-desk"
                        }, i.createElement("iframe", {
                            src: "https://yoworld.desk.com" + e
                        }), i.createElement("input", {
                            type: "button",
                            className: "btn btn-neutral btn-block center contact-us",
                            onClick: this.props.contactUsCallback,
                            defaultValue: "Contact Us"
                        }))
                    }
                });
            a.exports = o
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(2);
            a.exports = i.createClass({
                displayName: "exports",
                getInitialState: function() {
                    return {
                        coinBalance: "Fetching..."
                    }
                },
                updateBalance: function() {
                    var e = this;
                    o.GET("/gamecontrol/stocks.php?ep=getCoinBalance", function(t, n) {
                        return t ? (e.setState({
                            coinBalance: "Unknown"
                        }), void console.error(t)) : (window.coinBalance = n.balance, void e.setState({
                            coinBalance: n.balance
                        }))
                    })
                },
                componentDidMount: function() {
                    this.updateBalance()
                },
                render: function() {
                    var e = this.state.coinBalance;
                    "number" != typeof e && (e = Number(this.state.coinBalance)), isNaN(e) && (e = 0);
                    var e = o.formatDecimal(e, 0);
                    return i.createElement("div", {
                        className: "coin-balance"
                    }, i.createElement("img", {
                        src: "images/yocoin.svg",
                        className: "icon-yocoin",
                        alt: "Yocoin Icon"
                    }), i.createElement("span", {
                        className: "balance"
                    }, e))
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = (n(2), n(97)),
                r = n(96).Line;
            o.defaults.global.responsive = !0, o.defaults.global.showTooltips = !1, o.defaults.global.animation = !0;
            var s = {
                pointDot: !1,
                datasetStroke: 2
            };
            a.exports = i.createClass({
                displayName: "exports",
                getDefaultProps: function() {
                    return {
                        dataLoaded: !1,
                        points: [],
                        labels: []
                    }
                },
                shouldComponentUpdate: function(e, t) {
                    if (e.points.length != this.props.points.length) return !0;
                    for (var n = 0; n < e.points.length; n++)
                        if (e.points[n] !== this.props.points[n]) return !0;
                    return !1
                },
                render: function() {
                    if (!this.props.dataLoaded) return i.createElement("p", null, "Loading Chart. Please Wait.");
                    var e = {
                        labels: this.props.labels,
                        datasets: [{
                            fillColor: "rgba(0,220,0,0.1)",
                            strokeColor: "rgba(0,220,0,1)",
                            data: this.props.points
                        }]
                    };
                    return i.createElement(r, {
                        data: e,
                        options: s,
                        redraw: !0
                    })
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(2),
                r = n(15),
                s = n(33),
                l = n(31),
                c = n(34),
                d = n(32),
                u = n(7),
                p = [{
                    columnName: "stock_id",
                    visible: !1
                }, {
                    columnName: "abbreviation",
                    visible: !0,
                    displayName: "ID",
                    cssClassName: "bold"
                }, {
                    columnName: "name",
                    visible: !0,
                    displayName: "Business"
                }, {
                    columnName: "current",
                    visible: !0,
                    displayName: "Current Value",
                    customComponent: c,
                    cssClassName: "numeral"
                }, {
                    columnName: "change",
                    visible: !0,
                    displayName: "Change",
                    customComponent: d,
                    cssClassName: "numeral"
                }, {
                    columnName: "percent",
                    visible: !0,
                    displayName: "Change %",
                    customComponent: l,
                    cssClassName: "numeral"
                }],
                m = ["abbreviation", "name", "current", "change", "percent"];
            a.exports = i.createClass({
                displayName: "exports",
                getInitialState: function() {
                    return {
                        stocks: [],
                        activeStockId: null
                    }
                },
                rowClickHandler: function(e, t) {
                    var n = e.props.data.stock_id;
                    window.fireDataEvent(u.DIALOG_LIST_ITEM, {
                        dialog: u.STOCK_TARGET_ID,
                        item: n
                    }, u.STOCK_TARGET_ID, u.DIALOG_TARGET_TYPE), this.setState({
                        activeStockId: n
                    })
                },
                columnClickHandler: function(e, t) {
                    window.fireDataEvent(u.DIALOG_LIST_COLUMN, {
                        dialog: u.STOCK_TARGET_ID,
                        column: t
                    }, u.STOCK_TARGET_ID, u.DIALOG_TARGET_TYPE)
                },
                loadStocks: function() {
                    var e = this;
                    o.GET("/gamecontrol/stocks.php?ep=getMarketOverview", function(t, n) {
                        if (t) return alert(t), void console.error(t);
                        for (var a = 0; a < n.length; a++) n[a].current = Number(n[a].current), n[a].change = Number(n[a].change), n[a].percent = Number(n[a].percent);
                        e.setState({
                            stocks: n
                        }), e.refs.companyPage.loadStockData()
                    })
                },
                clearSelectedStock: function() {
                    this.setState({
                        activeStockId: null
                    })
                },
                reloadClickHandler: function() {
                    window.fireDataEvent(u.DIALOG_BUTTON_CLICKED, {
                        button: "Reload",
                        dialog: u.STOCK_TARGET_ID
                    }, u.STOCK_TARGET_ID, u.DIALOG_TARGET_TYPE), this.loadStocks()
                },
                componentDidMount: function() {
                    this.loadStocks()
                },
                render: function() {
                    function e() {
                        t.props.tabChangeFunction(2)
                    }
                    var t = this,
                        n = {
                            bodyCssClassName: function(e) {
                                return e.stock_id === t.state.activeStockId ? "active-row" : ""
                            }
                        },
                        a = null;
                    a = 0 == this.state.stocks.length ? i.createElement("p", null, "Loading Stocks. Please wait.") : i.createElement("div", {
                        className: "section  stocks-table"
                    }, i.createElement(r, {
                        results: this.state.stocks,
                        showFilter: !1,
                        resultsPerPage: 20,
                        showPager: !1,
                        useGriddleStyles: !1,
                        columnMetadata: p,
                        columns: m,
                        onRowClick: this.rowClickHandler,
                        onColumnClick: this.columnClickHandler,
                        rowMetadata: n
                    }));
                    var o = {
                        display: null == this.state.activeStockId ? "block" : "none"
                    };
                    return i.createElement("div", {
                        className: "row  market-overview"
                    }, i.createElement("h2", {
                        className: "title"
                    }, "Market Overview"), i.createElement("div", {
                        className: "stocks-reload"
                    }, i.createElement("button", {
                        className: "btn btn-default btn-small",
                        onClick: this.reloadClickHandler
                    }, i.createElement("i", {
                        className: "fa fa-refresh fa-lg"
                    }), " Reload")), i.createElement("div", {
                        className: "welcome",
                        style: o
                    }, i.createElement("p", null, "Welcome to the YoWorld Stock Market. Here you can use coins to buy and sell shares in companies and watch your fortunes rise and fall! Stocks update every minute, so check back to see how they're doing often! For more details, click the ", i.createElement("a", {
                        className: "alert-link",
                        href: "#",
                        onClick: e
                    }, " Help tab"))), i.createElement(s, {
                        stockId: this.state.activeStockId,
                        ref: "companyPage",
                        coinUpdateFunction: this.props.coinUpdateFunction,
                        clearStockFunction: this.clearSelectedStock
                    }), a)
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(28),
                r = n(83),
                s = n(79),
                l = n(81),
                c = n(82),
                d = n(77),
                u = n(7);
            a.exports = i.createClass({
                displayName: "exports",
                getInitialState: function() {
                    return {
                        tabs: [{
                            title: "Market",
                            content: i.createElement(s, {
                                coinUpdateFunction: this.updateCoinBalance,
                                tabChangeFunction: this.changeTab
                            })
                        }, {
                            title: "Portfolio",
                            content: i.createElement(l, {
                                coinUpdateFunction: this.updateCoinBalance,
                                tabChangeFunction: this.changeTab
                            })
                        }, {
                            title: "Help",
                            content: i.createElement(r, {
                                tabChangeFunction: this.changeTab
                            })
                        }],
                        active: 0
                    }
                },
                updateCoinBalance: function() {
                    this.refs.coinBalance.updateBalance()
                },
                changeTab: function(e) {
                    window.fireDataEvent(u.DIALOG_LINK_CLICKED, {
                        dialog: u.STOCK_TARGET_ID,
                        linkText: this.state.tabs[e].title + " tab"
                    }, u.STOCK_TARGET_ID, u.DIALOG_TARGET_TYPE), this.refs.stockTabs.handleTabClick(e)
                },
                onClickTab: function(e) {
                    window.fireDataEvent(u.DIALOG_TAB_CLICKED, {
                        dialog: u.STOCK_TARGET_ID,
                        tab: this.state.tabs[e].title
                    }, u.STOCK_TARGET_ID, u.DIALOG_TARGET_TYPE)
                },
                render: function() {
                    return i.createElement("div", null, i.createElement(c, null), i.createElement(d, {
                        ref: "coinBalance"
                    }), i.createElement(o, {
                        ref: "stockTabs",
                        items: this.state.tabs,
                        active: this.state.active,
                        onClick: this.onClickTab
                    }))
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(2),
                r = n(15),
                s = n(33),
                l = n(31),
                c = n(34),
                d = n(32),
                u = n(7),
                p = [{
                    columnName: "stock_id",
                    visible: !1
                }, {
                    columnName: "abbreviation",
                    visible: !0,
                    displayName: "ID"
                }, {
                    columnName: "name",
                    visible: !0,
                    displayName: "Business"
                }, {
                    columnName: "current",
                    visible: !0,
                    displayName: "Total Value",
                    cssClassName: "numeral",
                    customComponent: c
                }, {
                    columnName: "change",
                    visible: !0,
                    displayName: "Profit",
                    cssClassName: "numeral",
                    customComponent: d
                }, {
                    columnName: "percent",
                    visible: !0,
                    displayName: "Profit %",
                    cssClassName: "numeral",
                    customComponent: l
                }, {
                    columnName: "owned",
                    visible: !0,
                    displayName: "Owned"
                }, {
                    columnName: "paid",
                    visible: !0,
                    displayName: "Total Paid",
                    cssClassName: "numeral",
                    customComponent: c
                }],
                m = ["abbreviation", "name", "owned", "paid", "current", "change", "percent"];
            a.exports = i.createClass({
                displayName: "exports",
                getInitialState: function() {
                    return {
                        selectedStockId: null,
                        totalShares: 0,
                        totalPaid: 0,
                        totalValue: 0,
                        ownedStocks: []
                    }
                },
                rowClickHandler: function(e, t) {
                    var n = e.props.data.stock_id;
                    window.fireDataEvent(u.DIALOG_LIST_ITEM, {
                        dialog: u.STOCK_TARGET_ID,
                        item: n
                    }, u.STOCK_TARGET_ID, u.DIALOG_TARGET_TYPE), this.setState({
                        selectedStockId: n
                    })
                },
                columnClickHandler: function(e, t) {
                    window.fireDataEvent(u.DIALOG_LIST_ITEM, {
                        dialog: u.STOCK_TARGET_ID,
                        column: t
                    }, u.STOCK_TARGET_ID, u.DIALOG_TARGET_TYPE)
                },
                reloadClickHandler: function() {
                    window.fireDataEvent(u.DIALOG_BUTTON_CLICKED, {
                        button: "Reload",
                        dialog: u.STOCK_TARGET_ID
                    }, u.STOCK_TARGET_ID, u.DIALOG_TARGET_TYPE), this.loadStocks()
                },
                loadStocks: function() {
                    var e = this;
                    o.GET("/gamecontrol/stocks.php?ep=getPortfolioStocks", function(t, n) {
                        if (t) return alert(t), void console.error(t);
                        if (n.stocks) {
                            for (var a = 0; a < n.stocks.length; a++) n.stocks[a].paid = Number(n.stocks[a].paid), n.stocks[a].owned = Number(n.stocks[a].owned), n.stocks[a].current = Number(n.stocks[a].current), n.stocks[a].change = Number(n.stocks[a].change), n.stocks[a].percent = Number(n.stocks[a].percent);
                            e.setState({
                                totalShares: n.totalShares,
                                totalPaid: n.totalPaid,
                                totalValue: n.totalValue,
                                ownedStocks: n.stocks
                            }), e.refs.companyPage.loadStockData()
                        } else e.setState({
                            totalShares: 0,
                            totalPaid: 0,
                            totalValue: 0,
                            ownedStocks: []
                        })
                    })
                },
                clearSelectedStock: function() {
                    this.setState({
                        selectedStockId: null
                    })
                },
                componentDidMount: function() {
                    this.loadStocks()
                },
                render: function() {
                    function e() {
                        t.props.tabChangeFunction(0)
                    }
                    var t = this,
                        n = {
                            bodyCssClassName: function(e) {
                                return e.stock_id === t.state.selectedStockId ? "active-row" : ""
                            }
                        },
                        a = i.createElement("div", null, i.createElement("p", {
                            className: "alert alert--warning"
                        }, "You currently do not own any shares. Head to the ", i.createElement("a", {
                            href: "#",
                            onClick: e
                        }, "Market tab"), " to buy some!"));
                    return this.state.ownedStocks.length > 0 && (a = i.createElement(r, {
                        results: this.state.ownedStocks,
                        showFilter: !1,
                        resultsPerPage: 20,
                        useGriddleStyles: !1,
                        showPager: !1,
                        columnMetadata: p,
                        columns: m,
                        onRowClick: this.rowClickHandler,
                        onColumnClick: this.columnClickHandler,
                        rowMetadata: n
                    })), i.createElement("div", {
                        className: "portfolio"
                    }, i.createElement("h2", {
                        className: "title"
                    }, "Yo Portfolio"), i.createElement("div", {
                        className: "portfolio-summary"
                    }, i.createElement("div", {
                        className: "span1of4  portfolio-info"
                    }, i.createElement("div", {
                        className: "info-label"
                    }, "Shares"), i.createElement("div", {
                        className: "info-data"
                    }, this.state.totalShares)), i.createElement("div", {
                        className: "span1of4  portfolio-info"
                    }, i.createElement("div", {
                        className: "info-label"
                    }, "Value"), i.createElement("div", {
                        className: "info-total"
                    }, i.createElement("img", {
                        src: "images/yocoin.svg",
                        className: "icon-yocoin",
                        alt: "Yocoin Icon"
                    }), o.formatDecimal(this.state.totalValue))), i.createElement("div", {
                        className: "span1of4  portfolio-info"
                    }, i.createElement("div", {
                        className: "info-label"
                    }, "Cost"), i.createElement("div", {
                        className: "info-total"
                    }, i.createElement("img", {
                        src: "images/yocoin.svg",
                        className: "icon-yocoin",
                        alt: "Yocoin Icon"
                    }), o.formatDecimal(this.state.totalPaid))), i.createElement("div", {
                        className: "span1of4  last  portfolio-info"
                    }, i.createElement("div", {
                        className: "info-label"
                    }, "Profit"), i.createElement("div", {
                        className: "info-total"
                    }, i.createElement("img", {
                        src: "images/yocoin.svg",
                        className: "icon-yocoin",
                        alt: "Yocoin Icon"
                    }), o.formatDecimal(this.state.totalValue - this.state.totalPaid)))), i.createElement("div", {
                        className: "stocks-reload"
                    }, i.createElement("button", {
                        className: "btn btn-default btn-small",
                        onClick: this.reloadClickHandler
                    }, i.createElement("i", {
                        className: "fa fa-refresh fa-lg"
                    }), " Reload")), i.createElement(s, {
                        stockId: this.state.selectedStockId,
                        onPlayerStatsUpdate: this.loadStocks,
                        ref: "companyPage",
                        coinUpdateFunction: this.props.coinUpdateFunction,
                        clearStockFunction: this.clearSelectedStock
                    }), i.createElement("div", {
                        className: "section  stocks-table"
                    }, a))
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(2);
            a.exports = i.createClass({
                displayName: "exports",
                getInitialState: function() {
                    return {
                        tickerOffset: 0,
                        tickerItems: [],
                        stockDefinitions: {}
                    }
                },
                updateStockDefinitions: function() {
                    var e = this;
                    o.GET("/gamecontrol/stocks.php?ep=getTickerStats", function(t, n) {
                        if (t = t || n.error) return console.error(t), void e.setState({
                            tickerItems: [],
                            stockDefinitions: []
                        });
                        var a = Object.keys(n);
                        a.length == e.state.tickerItems.length && (a = e.state.tickerItems);
                        for (var i = 0; i < n.length; i++) n[i].change = Number(n[i].change), n[i].val = Number(n[i].val);
                        e.setState({
                            tickerItems: a,
                            stockDefinitions: n
                        })
                    })
                },
                componentWillMount: function() {
                    this.updateStockDefinitions(), setInterval(this.updateStockDefinitions, 3e4)
                },
                componentDidMount: function() {
                    function e() {
                        if (t.refs.tickerContent) {
                            var e = t.refs.tickerContent.children[0].offsetWidth;
                            if (t.state.tickerOffset < -1 * e) {
                                var n = t.state.tickerItems,
                                    a = n.shift();
                                n.push(a), t.setState({
                                    tickerOffset: t.state.tickerOffset + e,
                                    tickerItems: n
                                })
                            } else t.setState({
                                tickerOffset: t.state.tickerOffset - 1
                            })
                        }
                    }
                    var t = this;
                    setInterval(e, 16)
                },
                render: function() {
                    if (0 == this.state.tickerItems.length) return null;
                    var e = {
                            position: "relative",
                            width: "100%",
                            lineHeight: "28px",
                            overflow: "hidden"
                        },
                        t = {
                            position: "absolute",
                            width: "200px",
                            left: this.state.tickerOffset + "px",
                            whiteSpace: "nowrap"
                        },
                        n = this,
                        a = this.state.tickerItems.map(function(e) {
                            var t = n.state.stockDefinitions[e],
                                a = t.change / t.val * 100,
                                o = "change",
                                r = " --- ";
                            return t.change < 0 ? (o = "change  negative", r = " " + t.change + " ▼" + a.toFixed(2) + "%  ") : t.change > 0 && (o = "change  positive", r = " " + t.change + " ▲" + a.toFixed(2) + "%  "), i.createElement("span", {
                                className: "ticker-item"
                            }, i.createElement("span", {
                                className: "name"
                            }, t.name, " "), i.createElement("span", {
                                className: "value"
                            }, t.val, ": "), i.createElement("span", {
                                className: o
                            }, r))
                        });
                    return i.createElement("div", {
                        className: "stock-ticker-wrapper",
                        style: e
                    }, i.createElement("div", {
                        ref: "tickerContent",
                        className: "stock-ticker-inner",
                        style: t
                    }, a))
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1);
            n(2);
            a.exports = i.createClass({
                displayName: "exports",
                render: function() {
                    function e() {
                        n.props.tabChangeFunction(0)
                    }

                    function t() {
                        n.props.tabChangeFunction(1)
                    }
                    var n = this;
                    return i.createElement("div", {
                        id: "stocks-help"
                    }, i.createElement("div", {
                        className: "overview"
                    }, i.createElement("h2", {
                        className: "title"
                    }, "Welcome to the YoWorld Stock Market!"), i.createElement("p", null, "Buy low, sell high, and make many coins... but only if you're a wise investor! "), i.createElement("p", null, "See all the available stocks on the ", i.createElement("a", {
                        className: "alert-link",
                        href: "#",
                        onClick: e
                    }, "Market tab"), ", and invest in the ones you think will make you money. Check back regularly, as the stocks update every minute, and you never know which way the market will turn (Stocks do not change if YoWorld is offline)."), i.createElement("p", null, "Review your investments on the ", i.createElement("a", {
                        className: "alert-link",
                        href: "#",
                        onClick: t
                    }, "Portfolio tab"), ", where you can see the performance of the stocks you've bought. Click on a stock to see a historical graph of its sale price."), i.createElement("p", null, "To share tips and tricks with other players, head to the ", i.createElement("a", {
                        href: "https://forums.yoworld.com/",
                        target: "_blank"
                    }, "forums"), ".")), i.createElement("div", {
                        className: "details"
                    }, i.createElement("h4", {
                        className: "heading"
                    }, "Market Details"), i.createElement("ul", {
                        className: "default-ul"
                    }, i.createElement("li", null, "There is a 0.25% transaction fee (to a minimum of 10 coins) on all sales of shares, so buying and selling the same stock on very minor price increases is not recommended."), i.createElement("li", null, "You may own a maximum of 100 shares of any one company, so make sure to invest in many!"), i.createElement("li", null, "Due to the decimals in stock prices, prices are rounded up to the next whole coin when you are buying stocks, and down to the next whole coin when you are selling stocks."), i.createElement("li", null, "All purchases of shares are final, and there will be no refunds! The risk and reward of the stock market is only for those who are willing to put their coins on the line."))))
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(2),
                r = n(11);
            a.exports = i.createClass({
                displayName: "PremiumVipDialog",
                propTypes: {
                    productList: i.PropTypes.array.isRequired,
                    closeCallback: i.PropTypes.func.isRequired,
                    purchaseCallback: i.PropTypes.func.isRequired
                },
                getInitialState: function() {
                    return {
                        showPremiumPurchase: !0
                    }
                },
                purchaseSubscription: function(e) {
                    this.props.purchaseCallback(e)
                },
                findProducts: function() {
                    var e = this.props.productList.filter(function(e) {
                            return "yoworld.subscription.premiumVip" === e.reference_name
                        }),
                        t = this.props.productList.filter(function(e) {
                            return "yoworld.subscription.premiumVip.yearly" === e.reference_name
                        });
                    return {
                        monthly: e[0],
                        yearly: t[0]
                    }
                },
                render: function() {
                    var e = this.findProducts(),
                        t = o.getPricingString(e.monthly.cost, !0),
                        n = o.getPricingString(e.yearly.cost, !0),
                        a = o.getPricingString(e.yearly.cost / 12, !0),
                        s = i.createElement("div", null, i.createElement("img", {
                            className: "js-close  popup__close-button",
                            onClick: this.props.closeCallback,
                            src: "images/close_x.png"
                        }), i.createElement("img", {
                            className: "vip-value-badge",
                            src: "images/best_value.png",
                            alt: "Best Value"
                        }), i.createElement("div", {
                            className: "header-bar"
                        }, i.createElement("h2", {
                            className: "title  title--popped"
                        }, "Choose payment plan")), i.createElement("div", {
                            className: "row"
                        }, i.createElement("div", {
                            className: "vip-payment__option"
                        }, i.createElement("div", {
                            className: "header-bar  header-bar--monthly-tier"
                        }, i.createElement("h3", {
                            className: "title  title--popped"
                        }, "Monthly")), i.createElement("p", {
                            className: "description"
                        }, "Choose this option to be billed monthly."), i.createElement("div", {
                            className: "option-price"
                        }, i.createElement("span", {
                            className: "currency"
                        }, "$", t), i.createElement("sup", {
                            className: "cycle"
                        }, "/Month")), i.createElement("div", {
                            className: "vip-tier__subscription-buttons"
                        }, i.createElement("button", {
                            className: "btn btn-positive  btn-block  float-none",
                            onClick: this.purchaseSubscription.bind(this, e.monthly)
                        }, "Join Now"), i.createElement("p", {
                            className: "fine-print invisible"
                        }, "$", t))), i.createElement("div", {
                            className: "vip-payment__option"
                        }, i.createElement("div", {
                            className: "header-bar  header-bar--yearly-tier"
                        }, i.createElement("h3", {
                            className: "title  title--popped"
                        }, "Yearly")), i.createElement("p", {
                            className: "description"
                        }, "Choose this option to be billed yearly and save over $35"), i.createElement("div", {
                            className: "option-price"
                        }, i.createElement("span", {
                            className: "currency"
                        }, "$", a), i.createElement("sup", {
                            className: "cycle"
                        }, "/Month")), i.createElement("div", {
                            className: "vip-tier__subscription-buttons"
                        }, i.createElement("button", {
                            className: "btn  btn-positive  btn-block  float-none",
                            onClick: this.purchaseSubscription.bind(this, e.yearly)
                        }, "Join Now"), i.createElement("p", {
                            className: "fine-print"
                        }, "$", n, " billed yearly")))));
                    return i.createElement("div", null, i.createElement(r, {
                        show: this.state.showPremiumPurchase,
                        content: s,
                        classes: "popup  popup--prompt vip-payment"
                    }))
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(35),
                r = n(36),
                s = n(12),
                l = n(11);
            a.exports = i.createClass({
                displayName: "SubscriberDetails",
                propTypes: {
                    products: i.PropTypes.array.isRequired,
                    activeProduct: i.PropTypes.object.isRequired,
                    rewardList: i.PropTypes.object.isRequired,
                    handleFbRequest: i.PropTypes.func.isRequired,
                    purchaseCallback: i.PropTypes.func.isRequired
                },
                getInitialState: function() {
                    return {
                        showPopup: !1
                    }
                },
                hideCancelDialog: function() {
                    this.setState({
                        showPopup: !1
                    })
                },
                showCancelDialog: function() {
                    this.setState({
                        showPopup: !0
                    })
                },
                cancelSubscription: function() {
                    var e = this.props.activeProduct.subscriptionId;
                    this.props.handleFbRequest({
                        subscriptionId: e,
                        fbAction: "cancel_subscription"
                    }), this.setState({
                        showPopup: !1
                    })
                },
                render: function() {
                    var e, t = this,
                        n = i.createElement("div", null),
                        a = s.TYPE_VIP,
                        c = "vip-member-page",
                        d = i.createElement("em", null),
                        u = i.createElement("em", null),
                        p = [],
                        m = this.props.activeProduct.cost;
                    this.props.activeProduct.subscriptionLevel > 0 ? (a = s.TYPE_PREMIUM_VIP, p = this.props.rewardList.premiumVip, e = s.PANEL_DETAIL_PREMIUM_VIP) : 0 == this.props.activeProduct.subscriptionLevel ? (p = this.props.rewardList.vip, e = s.PANEL_DETAIL_VIP, this.props.activeProduct.pendingCancel || (u = i.createElement(r, {
                        panelType: s.PANEL_SELL_PREMIUM_VIP,
                        rewardList: this.props.rewardList.premiumVip,
                        productCost: this.props.products[1].cost,
                        subscriptionType: s.TYPE_PREMIUM_VIP,
                        isMember: !1
                    }, i.createElement(o, {
                        subscriptionType: s.TYPE_UPGRADE_PREMIUM_VIP,
                        subscriptionId: this.props.activeProduct.subscriptionId,
                        productList: this.props.products,
                        handleFbRequest: this.props.handleFbRequest,
                        purchaseCallback: this.props.purchaseCallback
                    }), i.createElement("img", {
                        className: "vip-tier__value-badge",
                        src: "images/best_value.png",
                        alt: "Best Value"
                    })))) : (a = s.TYPE_FREE, e = s.PANEL_SELL_FREE), d = i.createElement(o, {
                        subscriptionType: a,
                        subscriptionId: this.props.activeProduct.subscriptionId,
                        productList: this.props.products,
                        activeProduct: this.props.activeProduct,
                        handleFbRequest: this.props.handleFbRequest,
                        handleCancelCb: this.showCancelDialog,
                        purchaseCallback: this.props.purchaseCallback
                    });
                    var h = i.createElement("div", {
                            className: "vip-tier__renewal-info"
                        }, i.createElement("div", {
                            className: "bold"
                        }, "Next Renewal:"), this.props.activeProduct.billDate),
                        _ = {
                            height: "25px"
                        },
                        f = i.createElement("em", null);
                    a === s.TYPE_PREMIUM_VIP && (f = i.createElement("div", {
                        className: "vip-tier__renewal-section"
                    }, i.createElement("div", {
                        className: "vip-tier__renewal-info"
                    }, i.createElement("div", {
                        className: "bold"
                    }, "Next Rewards:"), i.createElement("div", {
                        className: "media"
                    }, i.createElement("img", {
                        className: "media__item",
                        style: _,
                        src: "images/in_app_purchase/yocoins.png"
                    }), i.createElement("span", {
                        className: "media__body"
                    }, this.props.activeProduct.dailyReward)), i.createElement("div", {
                        className: "media"
                    }, i.createElement("img", {
                        className: "media__item",
                        style: _,
                        src: "images/in_app_purchase/Yocash.png"
                    }), i.createElement("span", {
                        className: "media__body"
                    }, this.props.activeProduct.weeklyReward)), i.createElement("div", {
                        className: "media"
                    }, i.createElement("img", {
                        className: "media__item",
                        style: _,
                        src: "images/in_app_purchase/mystery_gift.png"
                    }), i.createElement("span", {
                        className: "media__body"
                    }, this.props.activeProduct.monthlyReward)))));
                    var g = "";
                    this.props.activeProduct && this.props.activeProduct.pendingCancel && (g = this.props.activeProduct.billDate, h = i.createElement("div", null)), n = i.createElement(r, {
                        panelType: e,
                        subscriptionType: a,
                        cancelDate: g,
                        rewardList: p,
                        productCost: m,
                        subscriberLevel: this.props.activeProduct.subscriptionLevel,
                        isMember: !0
                    }, i.createElement("div", {
                        className: "vip-tier__renewal-section"
                    }, i.createElement("hr", null), h, d), f);
                    var v = i.createElement("div", null, i.createElement("div", {
                            className: "header-bar header-bar--prompt"
                        }, i.createElement("h2", {
                            className: "title  title--popped"
                        }, "Cancel Subscription")), i.createElement("p", {
                            className: "center"
                        }, "Are you sure you'd like to cancel your subscription?"), i.createElement("div", {
                            className: "section"
                        }, i.createElement("button", {
                            className: "btn btn-default  span1of3  float-none",
                            onClick: t.cancelSubscription
                        }, "Yes"), i.createElement("button", {
                            className: "btn  btn-primary  span1of3  float-none",
                            onClick: t.hideCancelDialog
                        }, "No"))),
                        b = t.state.showPopup;
                    return i.createElement("div", {
                        className: c
                    }, i.createElement(l, {
                        show: b,
                        content: v,
                        classes: "popup  popup--prompt  enabled"
                    }), n, u)
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(12),
                r = "Your 40 YoCash every 7 days will increase by 2 YoCash every month that you are a Premium VIP subscriber, up to a maximum of 50 YoCash total per 7 days.",
                s = {
                    vipRewards: ["Premium Auction cost cap", "Discounted VIP Events", "One time sign up bonus item"],
                    premiumVipRewards: [{
                        value: "Your Weekly YoCash increases over time",
                        toolTip: !0
                    }, {
                        value: "Exclusive mystery gift monthly",
                        toolTip: !1
                    }, {
                        value: "15% Off homes and avenues",
                        toolTip: !1
                    }, {
                        value: "VIP nameplate and chat colors",
                        toolTip: !1
                    }, {
                        value: "5% Bonus YoCash on select packages",
                        toolTip: !1
                    }],
                    freeRewards: ["Free coins every day on login", "Free gifts to send and receive with buddies", "Free exclusive holiday items", "Free player support"]
                };
            a.exports = i.createClass({
                displayName: "SubscriberRewards",
                propTypes: {
                    rewardList: i.PropTypes.array.isRequired,
                    subscriptionType: i.PropTypes.string.isRequired
                },
                render: function() {
                    var e, t;
                    e = s.vipRewards.map(function(e, t) {
                        return i.createElement("li", {
                            className: "li-emphasis",
                            key: t
                        }, e)
                    });
                    var n = i.createElement("div", null),
                        a = i.createElement("div", null);
                    t = this.props.rewardList, this.props.subscriptionType === o.TYPE_PREMIUM_VIP && (n = s.premiumVipRewards.map(function(e, t) {
                        var n = i.createElement("em", null);
                        return e.toolTip && (n = i.createElement("div", {
                            className: "icon-button  icon-button--inline  tooltip",
                            "data-tooltip": r
                        }, i.createElement("i", {
                            className: "fa fa-info-circle fa-3"
                        }))), i.createElement("li", {
                            className: "li-emphasis",
                            key: t
                        }, e.value, n)
                    }));
                    var l = t.filter(function(e) {
                        return e.reward_day_interval < 30
                    }).map(function(e, t) {
                        var n = "every ",
                            a = "";
                        return e.reward_day_interval > 1 ? (e.subscriber_level > 1 && (a = "Bonus "), n += e.reward_day_interval + " Days") : n += "24 Hours", i.createElement("li", {
                            className: "li-emphasis",
                            key: e.reward_item_id
                        }, a, e.name, " ", n)
                    });
                    return i.createElement("ul", {
                        className: "vip-ul"
                    }, l, a, n, e)
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        (function(i) {
            "use strict";
            a = function(e, t, a) {
                var o = n(1),
                    r = n(2),
                    s = n(89),
                    l = n(85),
                    c = n(64),
                    d = n(11),
                    u = window.REST_SERVER_ROOT,
                    p = 1383010,
                    m = window.PRODUCT_HOST + "facebook_product?productId=",
                    h = "YoWorld VIP Membership is a recurring subscription product. You are free to cancel your automatic renewal at any time, and will continue to enjoy the benefits of the subscription until your renewal date. All purchases are subject to our Terms of Service.",
                    _ = "IMPORTANT: Please note that any payment chargebacks or reversals occurring for your purchases may result in temporary suspension of your account until the issue is resolved. If you ever have any issues with your purchase, please use the 'Help' tab above the game, and our team will be happy to help!";
                a.exports = o.createClass({
                    getInitialState: function() {
                        return {
                            active: 0,
                            loaded: !1,
                            nextBillDate: "UNKNOWN",
                            subscriptionLevel: 0,
                            status: !1,
                            activeFBRequest: !1,
                            isCancelling: !1,
                            showVipWarn: this.props.vipWarn
                        }
                    },
                    displayName: "SubscriptionPage",
                    propTypes: {
                        vipOnly: o.PropTypes.bool,
                        vipWarn: o.PropTypes.bool,
                        closeCallback: o.PropTypes.func,
                        updateVipWarn: o.PropTypes.func
                    },
                    getRewards: function(e) {
                        var t = JSON.parse(e.subscriptionRewards.vipReward),
                            n = JSON.parse(e.subscriptionRewards.supporterReward),
                            a = {
                                premiumVip: t,
                                vip: n,
                                free: []
                            };
                        return a
                    },
                    fetchSubscriberDetails: function() {
                        this.fetchProducts(this.parseProductList)
                    },
                    getActiveProduct: function(e, t) {
                        var n, a, i = e.subType;
                        if (e.status && void 0 !== i && 0 != i.length) {
                            var o;
                            for (n = 0; n < t.length; ++n) a = t[n].reference_name, a === i && (t[n].activeSubscription = !0, o = t[n]), t[n].playerSubId = e.subId;
                            if (o) {
                                var r = parseInt(e.subLevel, 10),
                                    s = parseInt(e.subId, 10),
                                    l = "";
                                e.nextBilling && (l = new Date(e.nextBilling), l = l.toDateString()), o.billDate = l, o.subscriptionLevel = r, o.subscriptionId = s, o.pendingCancel = e.pendingCancel, o.dailyReward = e.nextDailyReward, o.weeklyReward = e.nextWeeklyReward, o.monthlyReward = e.nextMonthlyReward
                            }
                            return o
                        }
                    },
                    setPageData: function(e, t) {
                        var n, a = [],
                            i = this.getRewards(e);
                        n = this.getActiveProduct(e, t), this.setState({
                            productList: t,
                            alternateProduct: a,
                            activeProduct: n,
                            rewardList: i,
                            loaded: !0
                        })
                    },
                    parseProductList: function(e, t) {
                        var n = this;
                        return e ? void console.error("ERROR: ", e) : void(t.subscriptionProducts ? r.dispatchFlashMessage("subscriptionRequest", {
                            productId: 0
                        }, function(e) {
                            n.setPageData(e, t.subscriptionProducts)
                        }) : console.error("NO SUBSCRIPTIONS IN PRODUCT LIST"))
                    },
                    fetchProducts: function(e) {
                        r.GET(u + "facebook/products", null, e)
                    },
                    handleFbRequest: function(e) {
                        this.state.activeFBRequest || (this.__alterSubscription(e.subscriptionId, e.fbAction, e.productInfo), this.setState({
                            activeFBRequest: !0
                        }))
                    },
                    __alterSubscription: function(e, t, n) {
                        function a(e) {
                            o.setState({
                                activeFBRequest: !1,
                                isCancelling: !1
                            }, function() {
                                setTimeout(function() {
                                    o.fetchSubscriberDetails()
                                }, 3e4)
                            })
                        }

                        function i(e) {
                            o.setState({
                                activeFBRequest: !1
                            });
                            var t = e.subscription_id;
                            e.subscriptionId = t;
                            var a = e.status;
                            "active" === a ? (o.__showPurchaseAnimation(t, n.title), o.props.closeCallback(!0)) : o.__subscriptionFailed(e)
                        }
                        if (void 0 !== e && null != e) {
                            var o = this,
                                r = {};
                            r = {
                                method: "pay",
                                action: t,
                                subscription_id: e
                            };
                            var s = a;
                            "change_subscription" === t && (r.product = m + n.kellaaProductId, s = i), FB.ui(r, s)
                        }
                    },
                    purchaseSubscription: function(e) {
                        this.state.activeFBRequest || (this.__startSubscription(e), this.setState({
                            activeFBRequest: !0
                        }))
                    },
                    __startSubscription: function(e) {
                        function t(e, t) {
                            var a = {};
                            a = void 0 !== t && 0 != t ? {
                                method: "pay",
                                action: "change_subscription",
                                subscription_id: t,
                                product: l
                            } : {
                                method: "pay",
                                action: "create_subscription",
                                product: l
                            }, FB.ui(a, n)
                        }

                        function n(n) {
                            var o = 1383010,
                                c = 1353058,
                                d = 1353033;
                            if (i.setState({
                                    activeFBRequest: !1
                                }), "undefined" == typeof n || null == n || "undefined" == typeof n.status || "active" != n.status) return !s && n.error_code && n.error_code != o && n.error_code != c && n.error_code != d ? (s = !0, void r.scrapeProductLink(l).done(function() {
                                t(a, u)
                            })) : void i.__subscriptionFailed(n);
                            var u = n.subscription_id;
                            n.subscriptionId = u;
                            var p = n.status;
                            "active" === p && (i.__showPurchaseAnimation(u, e.title), i.props.closeCallback(!0))
                        }
                        var a = e.kellaaProductId,
                            i = this,
                            o = e.playerSubId,
                            s = !1,
                            l = m + a;
                        t(a, o)
                    },
                    __showPurchaseAnimation: function(e, t) {
                        var n = this,
                            a = "subscription_purchase_animation.swf?v=1",
                            o = "swf_stack_div",
                            r = "100%",
                            s = "100%",
                            l = "11.1",
                            c = window.EXPRESS_INSTALL_URL,
                            d = {
                                transactionId: e,
                                membership: t
                            },
                            u = {
                                allowScriptAccess: "always",
                                allowFullScreen: "true",
                                allowFullScreenInteractive: "true",
                                wmode: "transparent",
                                scale: "noscale"
                            },
                            p = {};
                        window.closeTransactionCompleteWindow = function() {
                            i("#swf_stack_wrap").html('<div id="swf_stack_div"></div>'), i("#swf_stack_wrap").hide(), n.__modalLayer.hide(), delete window.closeTransactionCompleteWindow
                        }, i("#swf_stack_wrap").show(), i("#swf_stack_wrap").css("zIndex", this.props.zIndex + 1), swfobject.embedSWF(a, o, r, s, l, c, d, u, p)
                    },
                    __subscriptionFailed: function(e, t) {
                        var n = e.error_code;
                        n !== p && (t && console.log(t), alert("There was an error processing your subscription. If this problem persists, please contact support."))
                    },
                    componentWillMount: function() {
                        this.fetchSubscriberDetails()
                    },
                    updatePayment: function() {
                        var e = this,
                            t = this.state.activeProduct.subscriptionId,
                            n = {
                                method: "pay",
                                action: "settle_subscription",
                                subscription_id: t
                            };
                        FB.ui(n, function(n) {
                            n.subscription_id == t && "active" === n.status && (e.props.updateVipWarn(!1), e.setState({
                                showVipWarn: !1
                            }))
                        })
                    },
                    closeUpdateDialog: function() {
                        this.setState({
                            showVipWarn: !1
                        })
                    },
                    getPageContent: function() {
                        var e = o.createElement("div", null, "LOADING");
                        return this.state.loaded ? this.state.activeProduct && !this.props.vipOnly ? o.createElement(l, {
                            products: this.state.productList,
                            activeProduct: this.state.activeProduct,
                            handleFbRequest: this.handleFbRequest,
                            purchaseCallback: this.purchaseSubscription,
                            rewardList: this.state.rewardList
                        }) : o.createElement(s, {
                            products: this.state.productList,
                            handleFbRequest: this.handleFbRequest,
                            purchaseCallback: this.purchaseSubscription,
                            rewardList: this.state.rewardList
                        }) : e
                    },
                    render: function() {
                        var e = this.getPageContent(),
                            t = this;
                        if (!this.state.loaded) return o.createElement("div", null);
                        if (this.state.loaded && this.props.vipWarn && this.state.showVipWarn) {
                            var n = o.createElement("div", null, o.createElement("div", {
                                className: "header-bar header-bar--prompt"
                            }, o.createElement("h2", {
                                className: "title  title--popped"
                            }, "Subscription Payment Error", o.createElement("img", {
                                className: "js-close  popup__close-button",
                                src: "images/close_x_white.png",
                                onClick: this.closeUpdateDialog
                            }))), o.createElement("p", {
                                className: "center"
                            }, "There was an error processing your subscription payment. Please update your payment method."), o.createElement("div", {
                                className: "section"
                            }, o.createElement("button", {
                                className: "btn btn-primary",
                                onClick: t.updatePayment
                            }, "Update Payment Method")));
                            return o.createElement("div", null, o.createElement(d, {
                                show: this.state.showVipWarn,
                                content: n,
                                classes: "popup  popup--prompt  enabled"
                            }))
                        }
                        return o.createElement("div", {
                            id: "subscriptions-panel",
                            className: "popup  popup--default",
                            style: {
                                zIndex: this.props.zIndex
                            }
                        }, o.createElement(c, {
                            lightCloseButton: !0,
                            closeCallback: this.props.closeCallback
                        }), o.createElement("div", {
                            className: "main popup__main"
                        }, o.createElement("img", {
                            src: "images/viptitle.png",
                            className: "vip-title-image",
                            alt: ""
                        }), e, o.createElement("p", {
                            className: "alert  vip-disclaimer"
                        }, h)), o.createElement("div", {
                            className: "icon-button  icon-button--white tooltip",
                            "data-tooltip": _
                        }, o.createElement("i", {
                            className: "fa fa-info-circle fa-3"
                        })))
                    }
                })
            }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
        }).call(t, n(3))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1);
            a.exports = i.createClass({
                displayName: "SubscriptionPrice",
                propTypes: {
                    priceHeader: i.PropTypes.object,
                    priceFooter: i.PropTypes.string,
                    productCost: i.PropTypes.oneOfType([i.PropTypes.string, i.PropTypes.number])
                },
                render: function() {
                    var e = i.createElement("div", null);
                    return e = 0 === this.props.productCost ? i.createElement("span", {
                        className: "full-text"
                    }, "Free") : "5.00" === this.props.productCost ? i.createElement("h3", {
                        className: "value"
                    }, "Over $10 Value", i.createElement("h4", null, "Every Month")) : i.createElement("h3", {
                        className: "value"
                    }, "Over $60 Value", i.createElement("h4", null, "Every Month")), i.createElement("div", {
                        className: "vip-tier__value"
                    }, e)
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, function(e, t, n) {
        var a;
        a = function(e, t, a) {
            var i = n(1),
                o = n(35),
                r = n(36),
                s = n(12),
                l = n(84),
                c = "Select Premium VIP";
            a.exports = i.createClass({
                displayName: "SubscriptionSellPage",
                propTypes: {
                    products: i.PropTypes.array.isRequired,
                    rewardList: i.PropTypes.object.isRequired,
                    purchaseCallback: i.PropTypes.func.isRequired,
                    handleFbRequest: i.PropTypes.func.isRequired
                },
                getInitialState: function() {
                    return {
                        showPremiumPurchase: !1
                    }
                },
                getProductCost: function(e) {
                    return function(t) {
                        var n = e.filter(function(e) {
                            return t === e.reference_name.split(".").pop()
                        });
                        return 1 === n.length ? n[0].cost.toFixed(2) : 0
                    }
                },
                getButtons: function(e, t) {
                    switch (e) {
                        case s.TYPE_FREE:
                            return i.createElement("em", null);
                        case s.TYPE_VIP:
                            return i.createElement(o, {
                                subscriptionType: e,
                                productList: this.props.products,
                                handleFbRequest: this.props.handleFbRequest,
                                purchaseCallback: this.props.purchaseCallback
                            });
                        case s.TYPE_PREMIUM_VIP:
                            return i.createElement("div", {
                                className: "vip-tier__subscription-buttons"
                            }, i.createElement("div", {
                                className: "flavour-text"
                            }, i.createElement("span", {
                                className: "currency"
                            }, "$", t), i.createElement("span", {
                                className: "cycle"
                            }, " per month")), i.createElement("button", {
                                className: "btn btn--positive  btn--block",
                                onClick: this.showPremiumDialog
                            }, c));
                        default:
                            return i.createElement("em", null)
                    }
                },
                getPanel: function(e, t, n, a) {
                    var o = a(t),
                        s = this.getButtons(t, o);
                    return i.createElement(r, {
                        panelType: e,
                        rewardList: n,
                        productCost: o,
                        subscriptionType: t
                    }, s)
                },
                showPremiumDialog: function() {
                    this.setState({
                        showPremiumPurchase: !0
                    })
                },
                hidePremiumPurchase: function() {
                    this.setState({
                        showPremiumPurchase: !1
                    })
                },
                render: function() {
                    if (void 0 === this.props.products || 0 === this.props.products.length) return i.createElement("div", null, "Loading");
                    var e = this.getProductCost(this.props.products),
                        t = this.getPanel(s.PANEL_SELL_VIP, s.TYPE_VIP, this.props.rewardList.vip, e),
                        n = this.getPanel(s.PANEL_SELL_PREMIUM_VIP, s.TYPE_PREMIUM_VIP, this.props.rewardList.premiumVip, e),
                        a = i.createElement("div", null);
                    return this.state.showPremiumPurchase && (a = i.createElement(l, {
                        closeCallback: this.hidePremiumPurchase,
                        productList: this.props.products,
                        purchaseCallback: this.props.purchaseCallback
                    })), i.createElement("div", {
                        className: "vip-page"
                    }, n, t, a)
                }
            })
        }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
    }, , , , , , , function(e, t, n) {
        e.exports = n(6)(16)
    }, function(e, t, n) {
        e.exports = n(6)(5)
    }])
});