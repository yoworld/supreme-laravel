! function(t, e) {
    "object" == typeof exports && "object" == typeof module ? module.exports = e(require("CommonDependencies")) : "function" == typeof define && define.amd ? define(["CommonDependencies"], e) : "object" == typeof exports ? exports.LoginQueue = e(require("CommonDependencies")) : t.LoginQueue = e(t.CommonDependencies)
}(this, function(t) {
    return function(t) {
        function e(o) {
            if (i[o]) return i[o].exports;
            var n = i[o] = {
                exports: {},
                id: o,
                loaded: !1
            };
            return t[o].call(n.exports, n, n.exports, e), n.loaded = !0, n.exports
        }
        var i = {};
        return e.m = t, e.c = i, e.p = "", e(0)
    }([function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(90),
                r = i(4);
            o.exports = r.create({
                initialize: function(t, e) {
                    var i = new n(t, 443, e);
                    i.run()
                }
            })
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, , , function(t, e, i) {
        t.exports = i(6)(1)
    }, function(t, e, i) {
        var o, n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
            return typeof t
        } : function(t) {
            return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
        };
        o = function(t, e, i) {
            i.exports = {
                create: function() {
                    for (var t = arguments[arguments.length - 1].initialize || function() {}, e = 0; e < arguments.length; ++e) {
                        var i = arguments[e];
                        if ("object" !== ("undefined" == typeof i ? "undefined" : n(i)) && "function" != typeof i) throw new TypeError("Expected object or function, got " + ("undefined" == typeof i ? "undefined" : n(i)) + ": " + i);
                        if ("function" == typeof i) t.prototype = Object.create(i.prototype);
                        else
                            for (var o = Object.keys(i), r = 0; r < o.length; ++r) t.prototype[o[r]] = i[o[r]]
                    }
                    return t
                }
            }
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, i) {
            Function.prototype.method = function(t, e) {
                return this.prototype[t] = e, this
            }, e.create = function() {
                function t(t, e) {
                    for (var i = Object.getOwnPropertyDescriptor(t, e), o = Object.getPrototypeOf(t); void 0 === i && null !== o;) i = Object.getOwnPropertyDescriptor(o, e), o = Object.getPrototypeOf(o);
                    return i
                }
                if (0 === arguments.length) return function() {};
                for (var e = arguments[arguments.length - 1].initialize || function() {}, i = 0; i < arguments.length; ++i) {
                    var o = arguments[i];
                    if (o.class) e.prototype = Object.create(o.prototype);
                    else
                        for (var n in o) {
                            var r = t(o, n).get,
                                s = t(o, n).set;
                            r || s ? Object.defineProperty(e, n, {
                                get: r,
                                set: s
                            }) : e.prototype[n] = o[n]
                        }
                }
                return e.class = !0, e
            }
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e) {
        t.exports = CommonDependencies
    }, , , function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5);
            o.exports = n.create({
                __bytes: null,
                initialize: function(t) {
                    var e = Math.floor(t / 8) + 1;
                    this.__bytes = [];
                    for (var i = 0; i < e; ++i) this.__bytes.push(0)
                },
                clear: function() {
                    for (var t = 0; t < this.__bytes.length; ++t) this.__bytes[t] = 0
                },
                isBitSet: function(t) {
                    var e = Math.floor(t / 8),
                        i = 1 << t % 8;
                    return 0 != (this.__bytes[e] & i)
                },
                setBit: function(t) {
                    var e = Math.floor(t / 8),
                        i = 1 << t % 8;
                    this.__bytes[e] |= i
                },
                clearBit: function(t) {
                    var e = Math.floor(t / 8),
                        i = 1 << t % 8;
                    this.__bytes[e] &= ~i
                },
                serialize: function(t) {
                    for (var e = 0; e < this.__bytes.length; ++e) t.serializeByte("$bitFlags" + e + "$", this.__bytes[e])
                },
                deserialize: function(t) {
                    for (var e = 0; e < this.__bytes.length; ++e) this.__bytes[e] = t.deserializeByte("$bitFlags" + e + "$")
                }
            })
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(17);
            o.exports = n.create(r, {
                initialize: function() {
                    var t = (new Error).stack;
                    console.error(t)
                }
            })
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, , , , function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(17);
            o.exports = n.create(r, {
                initialize: function() {
                    var t = (new Error).stack;
                    console.error(t)
                }
            })
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, , , function(t, e, i) {
        var o;
        o = function(t, e, i) {
            function o(t, e) {
                return {
                    code: t,
                    message: e
                }
            }

            function n(t, e, i, o) {
                this.message = t, this.code = e, this.isUnexpected = i, this.info = o || {};
                var r = n.caller === n.createWithError;
                s(this, r ? n.createWithError : void 0)
            }

            function r(t) {
                if (Array.isArray(t) && 0 === t.length) return "";
                var e = " - Info: ";
                try {
                    if (Array.isArray(t)) {
                        var i = "";
                        t.forEach(function(t) {
                            e += i + JSON.stringify(t), i = ", "
                        })
                    } else e += JSON.stringify(t)
                } catch (t) {
                    e = ""
                }
                return e
            }

            function s(t, e) {
                Error.captureStackTrace(t, e);
                var i = Error.prepareStackTrace;
                Error.prepareStackTrace = a, t.stack, Error.prepareStackTrace = i
            }

            function a(t, e) {
                return t.fullStackTrace = e, [t.toString(), "Info: " + (Array.isArray(t.info) ? t.info.join(" ") : t.info)].concat(e.map(function(t) {
                    var e = " at ";
                    e += t.isConstructor() ? "new " : t.getTypeName() + ".";
                    var i = t.getFunctionName(),
                        o = t.getMethodName();
                    e += o || i ? o ? i ? i == o ? i : i + " [as " + o + "]" : o : i : "<anonymous>", e += " (" + t.getFileName() + ":" + t.getLineNumber() + ":" + t.getColumnNumber() + ")";
                    var n = t.getFunction();
                    return null === n.arguments || void 0 === n.arguments ? e : e += "    Called with arguments: " + Array.prototype.slice.call(t.getFunction().arguments).map(function(t) {
                        var e;
                        try {
                            e = JSON.stringify(t)
                        } catch (e) {
                            return (t && "function" == typeof t.toString || "") && t.toString()
                        }
                        return e
                    }).join(", ")
                })).join("\n")
            }
            ErrorCodes = {
                DEFAULT_ERROR: o(-1, "Internal server error.")
            }, n.createWithError = function(t) {
                t || (t = ErrorCodes.DEFAULT_ERROR);
                var e = arguments,
                    i = t.message.replace(/\{(\d+)\}/g, function(t, i) {
                        return i = +i + 1, e.hasOwnProperty(i) ? e[i] : t
                    });
                return new n(i, t.code, t.isUnexpected, Array.prototype.slice.call(e, 1))
            }, n.create = n.createWithError, n.prototype.__proto__ = Error.prototype, n.prototype.message = ErrorCodes.DEFAULT_ERROR.message, n.prototype.code = ErrorCodes.DEFAULT_ERROR.code, n.prototype.info = null, n.prototype.name = "Error", n.prototype.fullStackTrace = null, n.prototype.inspect = function() {
                this.inspect = function() {
                    return ""
                }, delete this.inspect;
                var t = r(this.info),
                    e = this.fullStackTrace.toString().split(","),
                    i = e[0] || "",
                    o = i.match(/(\w+)\s+\(([^:]*):(\d+)/) || ["", "<unknown>", "<unknown>", "<unknown>"],
                    n = o[1],
                    s = o[2],
                    a = o[3];
                return this.name + " " + this.code + ": " + this.message + " @ " + s + ":" + a + " (" + n + ")" + t
            }, n.prototype.toString = n.prototype.inspect, n.prototype.toJSON = n.prototype.inspect, n.prototype.getWireObject = function() {
                return {
                    code: this.code,
                    message: this.message
                }
            }, i.exports = n
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, , , , , , function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(10),
                s = i(25),
                a = i(26),
                l = n.create({
                    PROTOCOL_NAME: "Bootstrap",
                    PROTOCOL_ID: "Bootstrap:5d9f7a83929bbe9e3fad6f05a23a863307b134f",
                    deserialize: function(t) {
                        var e = t.peekInt("$messageId");
                        switch (e) {
                            case s.prototype.MESSAGE_ID:
                                var i = new s;
                                return i.deserialize(t), i;
                            case a.prototype.MESSAGE_ID:
                                var i = new a;
                                return i.deserialize(t), i
                        }
                        throw new r
                    },
                    getProtocolName: function() {
                        return this.PROTOCOL_NAME
                    },
                    getProtocolId: function() {
                        return this.PROTOCOL_ID
                    }
                });
            o.exports = l
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = (i(9), n.create({
                    initialize: function() {
                        this.reset()
                    },
                    isMessageReady: function() {
                        return !0
                    },
                    _getNumOptionalFields: function() {
                        return 0
                    },
                    _serializeSetOptionalFlags: function() {},
                    _serializeBody: function(t) {},
                    _deserializeBody: function(t) {},
                    reset: function() {},
                    getProtocolId: function() {
                        return "Bootstrap:5d9f7a83929bbe9e3fad6f05a23a863307b134f"
                    },
                    _repackage: function(t) {}
                }));
            o.exports = r
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(10),
                s = i(14),
                a = (i(9), i(24)),
                l = n.create(a, {
                    MESSAGE_ID: 1,
                    MESSAGE_NAME: "RequestProtocol",
                    initialize: function() {
                        a.prototype.initialize.call(this)
                    },
                    __protocolIdHash: null,
                    __protocolIdHashSet: null,
                    getProtocolIdHash: function() {
                        return this.__protocolIdHash
                    },
                    setProtocolIdHash: function(t) {
                        this.__protocolIdHash = t, this.__protocolIdHashSet = !0
                    },
                    _getNumOptionalFields: function() {
                        var t = a.prototype._getNumOptionalFields.call(this);
                        return t + 0
                    },
                    _serializeSetOptionalFlags: function() {
                        a.prototype._serializeSetOptionalFlags.call(this)
                    },
                    _serializeBody: function(t) {
                        a.prototype._serializeBody.call(this, t), t.serializeString("protocolIdHash", this.__protocolIdHash)
                    },
                    serialize: function(t) {
                        if (!this.isMessageReady()) throw new s;
                        t.serializeInt("$messageId", 1), this._serializeBody(t)
                    },
                    _deserializeBody: function(t) {
                        a.prototype._deserializeBody.call(this, t), this.__protocolIdHash = t.deserializeString("protocolIdHash"), this.__protocolIdHashSet = !0
                    },
                    deserialize: function(t) {
                        var e = t.deserializeInt("$messageId");
                        if (e != this.MESSAGE_ID) throw new r;
                        this._deserializeBody(t)
                    },
                    _repackage: function(t) {
                        a.prototype._repackage.call(this, t), t.protocolIdHash = this.getProtocolIdHash()
                    },
                    toFrozenObject: function() {
                        var t = {};
                        return this._repackage(t), Object.freeze(t), t
                    },
                    reset: function() {
                        a.prototype.reset.call(this), this.__protocolIdHash = null, this.__protocolIdHashSet = !1
                    },
                    isMessageReady: function() {
                        return this.__protocolIdHashSet && a.prototype.isMessageReady.call(this)
                    },
                    getMessageId: function() {
                        return 1
                    }
                });
            o.exports = l
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(10),
                s = i(14),
                a = (i(9), i(24)),
                l = n.create(a, {
                    MESSAGE_ID: 2,
                    MESSAGE_NAME: "RequestProtocolResponse",
                    initialize: function() {
                        a.prototype.initialize.call(this)
                    },
                    __protocolIdHash: null,
                    __status: null,
                    __channelId: null,
                    __protocolIdHashSet: null,
                    __statusSet: null,
                    __channelIdSet: null,
                    getProtocolIdHash: function() {
                        return this.__protocolIdHash
                    },
                    getStatus: function() {
                        return this.__status
                    },
                    getChannelId: function() {
                        return this.__channelId
                    },
                    setProtocolIdHash: function(t) {
                        this.__protocolIdHash = t, this.__protocolIdHashSet = !0
                    },
                    setStatus: function(t) {
                        this.__status = t, this.__statusSet = !0
                    },
                    setChannelId: function(t) {
                        this.__channelId = t, this.__channelIdSet = !0
                    },
                    _getNumOptionalFields: function() {
                        var t = a.prototype._getNumOptionalFields.call(this);
                        return t + 0
                    },
                    _serializeSetOptionalFlags: function() {
                        a.prototype._serializeSetOptionalFlags.call(this)
                    },
                    _serializeBody: function(t) {
                        a.prototype._serializeBody.call(this, t), t.serializeString("protocolIdHash", this.__protocolIdHash), t.serializeEnum("status", this.__status), t.serializeInt("channelId", this.__channelId)
                    },
                    serialize: function(t) {
                        if (!this.isMessageReady()) throw new s;
                        t.serializeInt("$messageId", 2), this._serializeBody(t)
                    },
                    _deserializeBody: function(t) {
                        a.prototype._deserializeBody.call(this, t), this.__protocolIdHash = t.deserializeString("protocolIdHash"), this.__status = t.deserializeEnum("status"), this.__channelId = t.deserializeInt("channelId"), this.__protocolIdHashSet = !0, this.__statusSet = !0, this.__channelIdSet = !0
                    },
                    deserialize: function(t) {
                        var e = t.deserializeInt("$messageId");
                        if (e != this.MESSAGE_ID) throw new r;
                        this._deserializeBody(t)
                    },
                    _repackage: function(t) {
                        a.prototype._repackage.call(this, t), t.protocolIdHash = this.getProtocolIdHash(), t.status = this.getStatus(), t.channelId = this.getChannelId()
                    },
                    toFrozenObject: function() {
                        var t = {};
                        return this._repackage(t), Object.freeze(t), t
                    },
                    reset: function() {
                        a.prototype.reset.call(this), this.__protocolIdHash = null, this.__status = null, this.__channelId = 0, this.__protocolIdHashSet = !1, this.__statusSet = !1, this.__channelIdSet = !1
                    },
                    isMessageReady: function() {
                        return this.__protocolIdHashSet && this.__statusSet && this.__channelIdSet && a.prototype.isMessageReady.call(this)
                    },
                    getMessageId: function() {
                        return 2
                    }
                });
            o.exports = l
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, , , , , , , , , , , function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(10),
                s = i(39),
                a = i(40),
                l = n.create({
                    PROTOCOL_NAME: "LoginQueue",
                    PROTOCOL_ID: "LoginQueue:553077f74b973a2efd48c6b1de455dce88058fa",
                    deserialize: function(t) {
                        var e = t.peekInt("$messageId");
                        switch (e) {
                            case s.prototype.MESSAGE_ID:
                                var i = new s;
                                return i.deserialize(t), i;
                            case a.prototype.MESSAGE_ID:
                                var i = new a;
                                return i.deserialize(t), i
                        }
                        throw new r
                    },
                    getProtocolName: function() {
                        return this.PROTOCOL_NAME
                    },
                    getProtocolId: function() {
                        return this.PROTOCOL_ID
                    }
                });
            o.exports = l
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = (i(9), n.create({
                    initialize: function() {
                        this.reset()
                    },
                    isMessageReady: function() {
                        return !0
                    },
                    _getNumOptionalFields: function() {
                        return 0
                    },
                    _serializeSetOptionalFlags: function() {},
                    _serializeBody: function(t) {},
                    _deserializeBody: function(t) {},
                    reset: function() {},
                    getProtocolId: function() {
                        return "LoginQueue:553077f74b973a2efd48c6b1de455dce88058fa"
                    },
                    _repackage: function(t) {}
                }));
            o.exports = r
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(10),
                s = i(14),
                a = (i(9), i(38)),
                l = n.create(a, {
                    MESSAGE_ID: 1,
                    MESSAGE_NAME: "QueuePosition",
                    initialize: function() {
                        a.prototype.initialize.call(this)
                    },
                    __position: null,
                    __outOf: null,
                    __estimatedTimeLeft: null,
                    __positionSet: null,
                    __outOfSet: null,
                    __estimatedTimeLeftSet: null,
                    getPosition: function() {
                        return this.__position
                    },
                    getOutOf: function() {
                        return this.__outOf
                    },
                    getEstimatedTimeLeft: function() {
                        return this.__estimatedTimeLeft
                    },
                    setPosition: function(t) {
                        this.__position = t, this.__positionSet = !0
                    },
                    setOutOf: function(t) {
                        this.__outOf = t, this.__outOfSet = !0
                    },
                    setEstimatedTimeLeft: function(t) {
                        this.__estimatedTimeLeft = t, this.__estimatedTimeLeftSet = !0
                    },
                    _getNumOptionalFields: function() {
                        var t = a.prototype._getNumOptionalFields.call(this);
                        return t + 0
                    },
                    _serializeSetOptionalFlags: function() {
                        a.prototype._serializeSetOptionalFlags.call(this)
                    },
                    _serializeBody: function(t) {
                        a.prototype._serializeBody.call(this, t), t.serializeInt("position", this.__position), t.serializeInt("outOf", this.__outOf), t.serializeString("estimatedTimeLeft", this.__estimatedTimeLeft)
                    },
                    serialize: function(t) {
                        if (!this.isMessageReady()) throw new s;
                        t.serializeInt("$messageId", 1), this._serializeBody(t)
                    },
                    _deserializeBody: function(t) {
                        a.prototype._deserializeBody.call(this, t), this.__position = t.deserializeInt("position"), this.__outOf = t.deserializeInt("outOf"), this.__estimatedTimeLeft = t.deserializeString("estimatedTimeLeft"), this.__positionSet = !0, this.__outOfSet = !0, this.__estimatedTimeLeftSet = !0
                    },
                    deserialize: function(t) {
                        var e = t.deserializeInt("$messageId");
                        if (e != this.MESSAGE_ID) throw new r;
                        this._deserializeBody(t)
                    },
                    _repackage: function(t) {
                        a.prototype._repackage.call(this, t), t.position = this.getPosition(), t.outOf = this.getOutOf(), t.estimatedTimeLeft = this.getEstimatedTimeLeft()
                    },
                    toFrozenObject: function() {
                        var t = {};
                        return this._repackage(t), Object.freeze(t), t
                    },
                    reset: function() {
                        a.prototype.reset.call(this), this.__position = 0, this.__outOf = 0, this.__estimatedTimeLeft = null, this.__positionSet = !1, this.__outOfSet = !1, this.__estimatedTimeLeftSet = !1
                    },
                    isMessageReady: function() {
                        return this.__positionSet && this.__outOfSet && this.__estimatedTimeLeftSet && a.prototype.isMessageReady.call(this)
                    },
                    getMessageId: function() {
                        return 1
                    }
                });
            o.exports = l
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(10),
                s = i(14),
                a = (i(9), i(38)),
                l = n.create(a, {
                    MESSAGE_ID: 2,
                    MESSAGE_NAME: "Token",
                    initialize: function() {
                        a.prototype.initialize.call(this)
                    },
                    __token: null,
                    __tokenSet: null,
                    getToken: function() {
                        return this.__token
                    },
                    setToken: function(t) {
                        this.__token = t, this.__tokenSet = !0
                    },
                    _getNumOptionalFields: function() {
                        var t = a.prototype._getNumOptionalFields.call(this);
                        return t + 0
                    },
                    _serializeSetOptionalFlags: function() {
                        a.prototype._serializeSetOptionalFlags.call(this)
                    },
                    _serializeBody: function(t) {
                        a.prototype._serializeBody.call(this, t), t.serializeString("token", this.__token)
                    },
                    serialize: function(t) {
                        if (!this.isMessageReady()) throw new s;
                        t.serializeInt("$messageId", 2), this._serializeBody(t)
                    },
                    _deserializeBody: function(t) {
                        a.prototype._deserializeBody.call(this, t), this.__token = t.deserializeString("token"), this.__tokenSet = !0
                    },
                    deserialize: function(t) {
                        var e = t.deserializeInt("$messageId");
                        if (e != this.MESSAGE_ID) throw new r;
                        this._deserializeBody(t)
                    },
                    _repackage: function(t) {
                        a.prototype._repackage.call(this, t), t.token = this.getToken()
                    },
                    toFrozenObject: function() {
                        var t = {};
                        return this._repackage(t), Object.freeze(t), t
                    },
                    reset: function() {
                        a.prototype.reset.call(this), this.__token = null, this.__tokenSet = !1
                    },
                    isMessageReady: function() {
                        return this.__tokenSet && a.prototype.isMessageReady.call(this)
                    },
                    getMessageId: function() {
                        return 2
                    }
                });
            o.exports = l
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(17);
            o.exports = n.create(r, {
                initialize: function() {
                    var t = (new Error).stack;
                    console.error(t)
                }
            })
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(41),
                s = i(23),
                a = i(26),
                l = n.create({
                    __handler: null,
                    initialize: function(t) {
                        this.__handler = t
                    },
                    getHandler: function() {
                        return this.__handler
                    },
                    dispatch: function(t, e) {
                        if (e.getProtocolId() != s.prototype.PROTOCOL_ID) throw new r;
                        switch (e.getMessageId()) {
                            case a.prototype.MESSAGE_ID:
                                this.__handler.onRequestProtocolResponseMessage(t, e);
                                break;
                            default:
                                throw new r
                        }
                    }
                });
            o.exports = l
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(23),
                s = n.create(r, {
                    __dispatcher: null,
                    initialize: function(t) {
                        this.__dispatcher = t
                    },
                    getHandler: function() {
                        return this.__dispatcher.getHandler()
                    },
                    dispatch: function(t, e) {
                        this.__dispatcher.dispatch(t, e)
                    }
                });
            o.exports = s
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, i) {
            var o = {
                SUCCESS: 0,
                FAIL_NO_MATCH: 1
            };
            i.exports = o
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(25),
                s = i(44);
            o.exports = n.create({
                __link: null,
                __supportedProtocols: null,
                __allProtocolsEstablishedCallback: null,
                initialize: function(t) {
                    this.__supportedProtocols = t
                },
                linkAdded: function(t) {
                    console.log("Bootstrap link added");
                    for (var e in this.__supportedProtocols) {
                        var i = this.__supportedProtocols[e],
                            o = new r;
                        o.setProtocolIdHash(i.getProtocolId()), t.send(o)
                    }
                    this.__link = t
                },
                linkRemoved: function(t) {
                    delete this.__link
                },
                addProtocol: function(t) {
                    if (this.__supportedProtocols[t.getProtocolId()] = t, this.__link) {
                        var e = new r;
                        e.setProtocolIdHash(t.getProtocolId()), this.__link.send(e)
                    }
                },
                removeProtocol: function(t) {
                    delete this.__supportedProtocols[t.getProtocolId()]
                },
                onRequestProtocolResponseMessage: function(t, e) {
                    var i = e.getProtocolIdHash(),
                        o = this.__supportedProtocols[i];
                    if (e.getStatus() !== s.SUCCESS) {
                        var n = "Failed to establish protocol: " + o.getProtocolName() + ". The protocol most likely changed on the other end. Your protocol ID was: " + i;
                        return this.__allProtocolsEstablishedCallback && this.__allProtocolsEstablishedCallback(n), void(this.__allProtocolsEstablishedCallback = null)
                    }
                    var r = e.getChannelId(),
                        a = t.getNegotiatedProtocols();
                    a.addNegotiatedProtocol(r, o), o.getHandler().linkAdded(t);
                    var l = a.getNumberOfProtocols() - 1,
                        c = Object.keys(this.__supportedProtocols).length;
                    l === c && (this.__allProtocolsEstablishedCallback && this.__allProtocolsEstablishedCallback(), this.__allProtocolsEstablishedCallback = null)
                },
                setAllProtocolsEstablishedCallback: function(t) {
                    this.__allProtocolsEstablishedCallback = t
                }
            })
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(47);
            o.exports = n.create({
                __transport: null,
                __linkListener: null,
                __negotiatedProtocols: null,
                initialize: function(t, e) {
                    this.__transport = t, this.__linkListener = e, this.__negotiatedProtocols = new r
                },
                send: function(t) {
                    console.log(t);
                    this.__transport.write(t)
                },
                closeLink: function() {
                    this.__transport.close()
                },
                getNegotiatedProtocols: function() {
                    return this.__negotiatedProtocols
                },
                notifyOpened: function() {
                    this.__linkListener.linkAdded(this), this.__negotiatedProtocols.notifyLinkAdded(this)
                },
                notifyClosed: function() {
                    this.__linkListener.linkRemoved(this), this.__negotiatedProtocols.notifyLinkRemoved(this)
                }
            })
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5);
            o.exports = n.create({
                __protocols: null,
                __protocolMapping: null,
                __nextChannelId: null,
                initialize: function() {
                    this.__protocols = {}, this.__protocolMapping = {}, this.__nextChannelId = 1
                },
                setBootstrapProtocol: function(t) {
                    this.setProtocolForChannelId(0, t)
                },
                addProtocol: function(t) {
                    var e = this.__nextChannelId;
                    return this.setProtocolForChannelId(e, t), ++this.__nextChannelId, e
                },
                addNegotiatedProtocol: function(t, e) {
                    this.setProtocolForChannelId(t, e)
                },
                setProtocolForChannelId: function(t, e) {
                    this.__protocols[t] = e, this.__protocolMapping[e.getProtocolId()] = t
                },
                dispatchMessage: function(t, e) {
                    var i = this.__protocols[this.__protocolMapping[e.getProtocolId()]];
                    i.dispatch(t, e)
                },
                notifyLinkAdded: function(t) {
                    for (var e in this.__protocols) {
                        var i = this.__protocols[e].getHandler();
                        i.linkAdded(t)
                    }
                },
                notifyLinkRemoved: function(t) {
                    for (var e in this.__protocols) {
                        var i = this.__protocols[e].getHandler();
                        i.linkRemoved(t)
                    }
                },
                getProtocol: function(t) {
                    return this.__protocols[t]
                },
                getChannelId: function(t) {
                    return this.__protocolMapping[t.getProtocolId()]
                },
                getNumberOfProtocols: function() {
                    return Object.keys(this.__protocols).length
                }
            })
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(45),
                s = i(51);
            g_NullBamLinkListener = {
                linkAdded: function() {},
                linkRemoved: function() {}
            }, o.exports = n.create({
                __host: null,
                __port: null,
                __secure: null,
                __channel: null,
                __supportedProtocols: null,
                __bootstrapClientHandler: null,
                initialize: function(t, e, i, o, n) {
                    this.__host = t, this.__port = e, this.__secure = i, this.__supportedProtocols = {}, o = o || [], n = n || g_NullBamLinkListener;
                    for (var a = 0; a < o.length; ++a) {
                        var l = o[a];
                        this.__supportedProtocols[l.getProtocolId()] = l
                    }
                    this.__bootstrapClientHandler = new r(this.__supportedProtocols), this.__channel = new s(n, this.__bootstrapClientHandler)
                },
                addProtocol: function(t) {
                    this.__supportedProtocols[t.getProtocolId()] = t, this.__bootstrapClientHandler.addProtocol(t)
                },
                removeProtocol: function(t) {
                    delete this.__supportedProtocols[t.getProtocolId()], this.__bootstrapClientHandler.removeProtocol(t)
                },
                connect: function(t) {
                    var e = this;
                    console.log("Attempting to connect to " + this.__host + ":" + this.__port), this.__channel.connect(this.__host, this.__port, this.__secure, function(i) {
                        return i ? void t(i) : (console.log("Connected to " + e.__host + ":" + e.__port), void t())
                    })
                },
                disconnect: function() {
                    this.__channel.close()
                },
                send: function(t, e) {
                    this.__channel.write(t, e)
                },
                waitForDisconnected: function(t) {
                    this.__channel.waitForDisconnected(t)
                }
            })
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5);
            o.exports = n.create({
                __buffer: null,
                __view: null,
                __protocol: null,
                __dataIndex: null,
                initialize: function(t, e, i) {
                    this.__dataIndex = i, this.__buffer = t, this.__view = new DataView(this.__buffer), this.__protocol = e
                },
                peekInt: function(t) {
                    return this.__view.getInt32(this.__dataIndex)
                },
                deserializeByte: function(t) {
                    var e = this.__view.getUint8(this.__dataIndex);
                    return this.__dataIndex += 1, e
                },
                deserializeInt: function(t) {
                    var e = this.__view.getInt32(this.__dataIndex);
                    return this.__dataIndex += 4, e
                },
                deserializeLong: function(t) {
                    throw console.error("You cannot use longs on javascript!"), new Error("Long support is not available on javascript!")
                },
                deserializeString: function(t) {
                    for (var e = this.deserializeInt(), i = "", o = 0; o < e;) {
                        var n = this.__readUtf8Char(this.__dataIndex),
                            r = this.__utf8CharLength(n);
                        i += String.fromCharCode(n), o += r, this.__dataIndex += r
                    }
                    return i
                },
                __utf8CharLength: function(t) {
                    return t < 128 ? 1 : t < 2048 ? 2 : t < 65536 ? 3 : t < 2097152 ? 4 : t < 67108864 ? 5 : 6
                },
                __readUtf8Char: function(t, e) {
                    var i, o = this.__view.getInt8(t);
                    return i = o >= 252 && o < 254 ? 1073741824 * (o - 252) + (this.__view.getInt8(t + 1) - 128 << 24) + (this.__view.getInt8(t + 2) - 128 << 18) + (this.__view.getInt8(t + 3) - 128 << 12) + (this.__view.getInt8(t + 4) - 128 << 6) + (this.__view.getInt8(t + 5) - 128) : o >= 248 && o < 252 ? (o - 248 << 24) + (this.__view.getInt8(t + 1) - 128 << 18) + (this.__view.getInt8(t + 2) - 128 << 12) + (this.__view.getInt8(t + 3) - 128 << 6) + (this.__view.getInt8(t + 4) - 128) : o >= 240 && o < 248 ? (o - 240 << 18) + (this.__view.getInt8(t + 1) - 128 << 12) + (this.__view.getInt8(t + 2) - 128 << 6) + (this.__view.getInt8(t + 3) - 128) : o >= 224 && o < 240 ? (o - 240 << 12) + (this.__view.getInt8(t + 1) - 128 << 6) + (this.__view.getInt8(t + 2) - 128) : o >= 192 && o < 224 ? (o - 192 << 6) + (this.__view.getInt8(t + 1) - 128) : o
                },
                deserializeFloat: function(t) {
                    var e = this.__view.getFloat32(this.__dataIndex);
                    return this.__dataIndex += 4, e
                },
                deserializeBoolean: function(t) {
                    var e = this.__view.getInt8(this.__dataIndex);
                    return this.__dataIndex += 1, !!e
                },
                deserializeMessage: function(t) {
                    return this.__protocol.deserialize(this)
                },
                deserializeIntArray: function(t) {
                    for (var e = this.deserializeInt(), i = [], o = 0; o < e; ++o) i.push(this.deserializeInt());
                    return i
                },
                deserializeLongArray: function(t) {
                    for (var e = this.deserializeInt(), i = [], o = 0; o < e; ++o) i.push(this.deserializeLong());
                    return i
                },
                deserializeStringArray: function(t) {
                    for (var e = this.deserializeInt(), i = [], o = 0; o < e; ++o) i.push(this.deserializeString());
                    return i
                },
                deserializeFloatArray: function(t) {
                    for (var e = this.deserializeInt(), i = [], o = 0; o < e; ++o) i.push(this.deserializeFloat());
                    return i
                },
                deserializeBooleanArray: function(t) {
                    for (var e = this.deserializeInt(), i = [], o = 0; o < e; ++o) i.push(this.deserializeBoolean());
                    return i
                },
                deserializeArray: function(t, e) {
                    for (var i = this.deserializeInt(), o = [], n = 0; n < i; ++n) {
                        var r = new e;
                        r.deserialize(this), o.push(r)
                    }
                    return o
                },
                deserializeIntDictionary: function(t, e) {
                    for (var i = this.deserializeInt(), o = {}, n = 0; n < i; ++n) {
                        var r = this.deserializeInt(),
                            s = new e;
                        s.deserialize(this), o[r] = s
                    }
                    return o
                },
                deserializeLongDictionary: function(t, e) {
                    throw console.error("You cannot use longs on javascript!"), new Error("Long support is not available on javascript!")
                },
                deserializeStringDictionary: function(t, e) {
                    for (var i = this.deserializeInt(), o = {}, n = 0; n < i; ++n) {
                        var r = this.deserializeString(),
                            s = new e;
                        s.deserialize(this), o[r] = s
                    }
                    return o
                },
                deserializeFloatDictionary: function(t, e) {
                    for (var i = this.deserializeInt(), o = {}, n = 0; n < i; ++n) {
                        var r = this.deserializeFloat(),
                            s = new e;
                        s.deserialize(this), o[r] = s
                    }
                    return o
                },
                deserializeBooleanDictionary: function(t, e) {
                    for (var i = this.deserializeInt(), o = {}, n = 0; n < i; ++n) {
                        var r = this.deserializeBoolean(),
                            s = new e;
                        s.deserialize(this), o[r] = s
                    }
                    return o
                },
                deserializeEnum: function(t) {
                    var e = this.deserializeInt();
                    return e
                }
            })
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5);
            o.exports = n.create({
                __buffer: null,
                __bufferSize: null,
                __view: null,
                __dataLength: null,
                __dataIndex: null,
                __startingIndex: null,
                initialize: function(t, e) {
                    this.__buffer = t, this.__bufferSize = t.byteLength, this.__view = new DataView(this.__buffer), this.__dataIndex = e, this.__startingIndex = e
                },
                getBuffer: function() {
                    return this.__buffer
                },
                serializeByte: function(t, e) {
                    this.__ensureBufferSpace(1), this.__view.setUint8(this.__dataIndex, e), this.__dataIndex += 1
                },
                serializeInt: function(t, e) {
                    this.__ensureBufferSpace(4), this.__view.setInt32(this.__dataIndex, e), this.__dataIndex += 4
                },
                serializeLong: function(t, e) {
                    throw console.error("You cannot use longs on javascript!"), new Error("Long support is not available on javascript!")
                },
                serializeString: function(t, e) {
                    for (var i = 0, o = 0; o < e.length; ++o) i += this.__utf8CharLength(e.charCodeAt(o));
                    this.serializeInt("", i), this.__ensureBufferSpace(i);
                    for (var o = 0; o < e.length; ++o) this.__dataIndex = this.__writeUtf8Char(this.__dataIndex, e.charCodeAt(o))
                },
                __utf8CharLength: function(t) {
                    return t < 128 ? 1 : t < 2048 ? 2 : t < 65536 ? 3 : t < 2097152 ? 4 : t < 67108864 ? 5 : 6
                },
                __writeUtf8Char: function(t, e) {
                    return e < 128 ? this.__view.setInt8(t++, e) : e < 2048 ? (this.__view.setInt8(t++, 192 + (e >>> 6)), this.__view.setInt8(t++, 128 + (63 & e))) : e < 65536 ? (this.__view.setInt8(t++, 224 + (e >>> 12)), this.__view.setInt8(t++, 128 + (e >>> 6 & 63)), this.__view.setInt8(t++, 128 + (63 & e))) : e < 2097152 ? (this.__view.setInt8(t++, 240 + (e >>> 18)), this.__view.setInt8(t++, 128 + (e >>> 12 & 63)), this.__view.setInt8(t++, 128 + (e >>> 6 & 63)), this.__view.setInt8(t++, 128 + (63 & e))) : e < 67108864 ? (this.__view.setInt8(t++, 248 + (e >>> 24)), this.__view.setInt8(t++, 128 + (e >>> 18 & 63)), this.__view.setInt8(t++, 128 + (e >>> 12 & 63)), this.__view.setInt8(t++, 128 + (e >>> 6 & 63)), this.__view.setInt8(t++, 128 + (63 & e))) : (this.__view.setInt8(t++, 252 + e / 1073741824), this.__view.setInt8(t++, 128 + (e >>> 24 & 63)), this.__view.setInt8(t++, 128 + (e >>> 18 & 63)), this.__view.setInt8(t++, 128 + (e >>> 12 & 63)), this.__view.setInt8(t++, 128 + (e >>> 6 & 63)), this.__view.setInt8(t++, 128 + (63 & e))), t
                },
                serializeFloat: function(t, e) {
                    this.__ensureBufferSpace(4), this.__view.setFloat32(this.__dataIndex, e), this.__dataIndex += 4
                },
                serializeBoolean: function(t, e) {
                    this.__ensureBufferSpace(1), this.__view.setInt8(this.__dataIndex, e ? 1 : 0), this.__dataIndex += 1
                },
                serializeMessage: function(t, e) {
                    e.serialize(this)
                },
                serializeIntArray: function(t, e) {
                    var i = e.length;
                    this.serializeInt("", i);
                    for (var o = 0, n = e.length; o < n; ++o) this.serializeInt(e[o])
                },
                serializeLongArray: function(t, e) {
                    var i = e.length;
                    this.serializeInt("", i);
                    for (var o = 0, n = e.length; o < n; ++o) this.serializeLong(e[o])
                },
                serializeStringArray: function(t, e) {
                    var i = e.length;
                    this.serializeInt("", i);
                    for (var o = 0, n = e.length; o < n; ++o) this.serializeString(e[o])
                },
                serializeFloatArray: function(t, e) {
                    var i = e.length;
                    this.serializeInt("", i);
                    for (var o = 0, n = e.length; o < n; ++o) this.serializeFloat(e[o])
                },
                serializeBooleanArray: function(t, e) {
                    var i = e.length;
                    this.serializeInt("", i);
                    for (var o = 0, n = e.length; o < n; ++o) this.serializeBoolean(e[o])
                },
                serializeArray: function(t, e) {
                    var i = e.length;
                    this.serializeInt("", i);
                    for (var o = 0, n = e.length; o < n; ++o) e[o].serialize(this)
                },
                serializeIntDictionary: function(t, e) {
                    var i = Object.keys(e);
                    this.serializeInt("", i);
                    for (var o in e) this.serializeInt("", +o), e[o].serialize(this)
                },
                serializeLongDictionary: function(t, e) {
                    throw console.error("You cannot use longs on javascript!"), new Error("Long support is not available on javascript!")
                },
                serializeStringDictionary: function(t, e) {
                    var i = Object.keys(e);
                    this.serializeInt("", i);
                    for (var o in e) this.serializeString("", o), e[o].serialize(this)
                },
                serializeFloatDictionary: function(t, e) {
                    var i = Object.keys(e);
                    this.serializeInt("", i);
                    for (var o in e) this.serializeFloat("", +o), e[o].serialize(this)
                },
                serializeBooleanDictionary: function(t, e) {
                    var i = Object.keys(e);
                    this.serializeInt("", i);
                    for (var o in e) this.serializeBoolean("", o), e[o].serialize(this)
                },
                serializeEnum: function(t, e) {
                    this.serializeInt(t, e)
                },
                getSize: function() {
                    return this.__dataIndex - this.__startingIndex
                },
                __ensureBufferSpace: function(t) {
                    var e = this.__dataIndex + t;
                    if (!(e <= this.__bufferSize)) {
                        var i = this.__bufferSize;
                        do i *= 2; while (i < e);
                        var o = new ArrayBuffer(i),
                            n = new Int8Array(o);
                        n.set(new Int8Array(this.__buffer)), this.__buffer = o, this.__bufferSize = i, this.__view = new DataView(this.__buffer)
                    }
                }
            })
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(46),
                s = i(50),
                a = i(49),
                l = i(43),
                c = i(42),
                _ = 1e3;
            o.exports = n.create({
                __linkListener: null,
                __bootstrapClientHandler: null,
                __link: null,
                __webSocket: null,
                __writeBuffer: null,
                __writeBufferView: null,
                __closeListenerList: null,
                __connectCallback: null,
                initialize: function(t, e) {
                    this.__linkListener = t, this.__bootstrapClientHandler = e, this.__writeBuffer = new ArrayBuffer(_), this.__writeBufferView = new DataView(this.__writeBuffer), this.__closeListenerList = []
                },
                connect: function(t, e, i, o) {
                    var n;
                    n = i ? "wss://" : "ws://", this.__webSocket = new WebSocket(n + t + ":" + e + "/bamWs"), this.__webSocket.binaryType = "arraybuffer";
                    console.log("BITCH");
                    console.log(n + t + ":" + e + "/bamWs");
                    var s = this;
                    this.__connectCallback = o, this.__bootstrapClientHandler.setAllProtocolsEstablishedCallback(function() {
                        s.__connectCallback(), s.__connectCallback = null
                    }), this.__webSocket.onopen = this.__onOpenListener.bind(this), this.__webSocket.onclose = this.__onCloseListener.bind(this), this.__webSocket.onerror = this.__onErrorListener.bind(this), this.__webSocket.onmessage = this.__onMessageListener.bind(this), this.__link = new r(this, this.__linkListener)
                },
                close: function(t) {
                    this.__webSocket.close(), this.__webSocket.onopen = null, this.__webSocket.onclose = null, this.__webSocket.onerror = null, this.__webSocket.onmessage = null, this.__webSocket = null, setTimeout(t || function() {})
                },
                waitForDisconnected: function(t) {
                    this.__closeListenerList.push(t)
                },
                write: function(t) {
                    var e = new s(this.__writeBuffer, 1);
                    t.serialize(e);
                    var i = e.getBuffer();
                    i != this.__writeBuffer && (this.__writeBuffer = i, this.__writeBufferView = new DataView(i)), this.__writeBufferView.setInt8(0, this.__link.getNegotiatedProtocols().getChannelId(t));
                    var o = new Int8Array(this.__writeBuffer, 0, 1 + e.getSize());
                    this.__webSocket.send(o)
                },
                __onOpenListener: function(t) {
                    console.log("Connected!");
                    var e = this.__link.getNegotiatedProtocols();
                    e.setBootstrapProtocol(new l(new c(this.__bootstrapClientHandler))), this.__link.notifyOpened()
                },
                __onMessageListener: function(t) {
                    if (!(t.data instanceof ArrayBuffer)) return void console.error("Got non-ArrayBuffer webSocket frame!");
                    var e = t.data,
                        i = new DataView(e),
                        o = i.getInt8(0),
                        n = this.__link.getNegotiatedProtocols().getProtocol(o),
                        r = new a(e, n, 1),
                        s = n.deserialize(r);

                        console.log(s.__token)
                    this.__dispatchReceivedMessage(s)
                },
                __dispatchReceivedMessage: function(t) {
                    this.__link.getNegotiatedProtocols().dispatchMessage(this.__link, t)
                },
                __onErrorListener: function(t) {
                    console.log("webSocket#error", t)
                },
                __onCloseListener: function(t) {
                    console.log("webSocket#close"), this.__connectCallback && this.__connectCallback("Failed to connect, error code: " + t.code), this.__link.notifyClosed();
                    for (var e = 0; e < this.__closeListenerList.length; ++e) {
                        var i = this.__closeListenerList[e];
                        i()
                    }
                    this.__closeListenerList = []
                }
            })
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , function(t, e, i) {
        var o;
        (function(n) {
            "use strict";
            o = function(t, e, o) {
                var r = i(48),
                    s = i(91),
                    a = i(92),
                    l = i(93),
                    c = i(94),
                    _ = i(5);
                o.exports = _.create({
                    __client: null,
                    __connectionTargetString: null,
                    initialize: function(t, e, i) {
                        var o = new a(i),
                            n = new c(new l(o)),
                            _ = [n],
                            u = new s;
                        this.__client = new r(t, e, !0, _, u), this.__connectionTargetString = t + ":" + e
                    },
                    run: function() {
                        this.__client.connect(function(t) {
                            if (t) return void console.error("Failed to connect: ", t)
                        }), this.__client.waitForDisconnected(function(t) {
                            return t ? void console.error("Failed to disconnect: ", t) : (n(".error").text("Uh oh, you were disconnected from the queue. Refresh in a few minutes."), n(".snakeGame").css("visibility", "visible"), n(".queuePosition").text("Position in queue: DISCONNECTED"), void n(".queueEstimatedTimeLeft").text("Estimated time to play: DISCONNECTED"))
                        })
                    }
                })
            }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
        }).call(e, i(3))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5);
            o.exports = n.create({
                linkAdded: function(t) {},
                linkRemoved: function(t) {}
            })
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        (function(n) {
            "use strict";
            o = function(t, e, o) {
                var r = i(5),
                    s = i(95);
                o.exports = r.create({}, s, {
                    __queryString: "",
                    initialize: function(t) {
                        this.__queryString = t || ""
                    },
                    onTokenMessage: function(t, e) {
                        t.closeLink(), this.__redirectWithToken(e.getToken())
                    },
                    onQueuePositionMessage: function(t, e) {
                        n(".queuePosition").text("Position in queue: " + e.getPosition()), n(".queueEstimatedTimeLeft").text("Estimated time to play:  " + e.getEstimatedTimeLeft()), n(".snakeGame").css("visibility", "visible")
                    },
                    __redirectWithToken: function(t) {
                        var e = window.location.protocol + "//" + window.location.host + window.location.pathname + "?lgt=" + encodeURIComponent(t);
                        "" !== this.__queryString && (e += "&" + this.__queryString), window.location.href = e
                    },
                    linkAdded: function(t) {},
                    linkRemoved: function(t) {}
                })
            }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
        }).call(e, i(3))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(41),
                s = i(37),
                a = i(39),
                l = i(40),
                c = n.create({
                    __handler: null,
                    initialize: function(t) {
                        this.__handler = t
                    },
                    getHandler: function() {
                        return this.__handler
                    },
                    dispatch: function(t, e) {
                        if (e.getProtocolId() != s.prototype.PROTOCOL_ID) throw new r;
                        switch (e.getMessageId()) {
                            case a.prototype.MESSAGE_ID:
                                this.__handler.onQueuePositionMessage(t, e);
                                break;
                            case l.prototype.MESSAGE_ID:
                                this.__handler.onTokenMessage(t, e);
                                break;
                            default:
                                throw new r
                        }
                    }
                });
            o.exports = c
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, o) {
            var n = i(5),
                r = i(37),
                s = n.create(r, {
                    __dispatcher: null,
                    initialize: function(t) {
                        this.__dispatcher = t
                    },
                    getHandler: function() {
                        return this.__dispatcher.getHandler()
                    },
                    dispatch: function(t, e) {
                        this.__dispatcher.dispatch(t, e)
                    }
                });
            o.exports = s
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }, function(t, e, i) {
        var o;
        o = function(t, e, i) {
            var o = {
                onQueuePositionMessage: function(t, e) {
                    console.warn("LoginQueueServerProtocolHandler = Default onQueuePositionMessage called with link: ", t, " and message ", e)
                },
                onTokenMessage: function(t, e) {
                    console.warn("LoginQueueServerProtocolHandler = Default onTokenMessage called with link: ", t, " and message ", e)
                },
                linkAdded: function(t) {},
                linkRemoved: function(t) {},
                isImplementedBy: function(t) {
                    return !!t && (!(!t.linkAdded || "function" != typeof t.linkAdded) && (!(!t.linkRemoved || "function" != typeof t.linkRemoved) && (!(!t.onQueuePositionMessage || "function" !== t.onQueuePositionMessage) && !(!t.onTokenMessage || "function" !== t.onTokenMessage))))
                }
            };
            i.exports = o
        }.call(e, i, e, t), !(void 0 !== o && (t.exports = o))
    }])
});